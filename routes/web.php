<?php
// use QrCode;
Auth::routes();
// Auth::routes(['register' => false]);

Route::middleware(['auth'])->group(function () {


    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('home/scanqrcode', 'HomeController@scanQrCode')->name('home.scanqrcode');
    Route::get('home/scanqrcode/{id}', 'HomeController@scanQrCode');
    Route::post('/home/direct_transfer', 'HomeController@directTransfer');
    //Route::get('/getmedia/{type}', 'MediaStockController@media');
    Route::get('/growthstocks/getmedias/{type}', 'MediaStockController@getMedias')->name('getmedias');
    Route::get('/getcontainers/{type}', 'MediaStockController@getContainers')->name('getcontainers');

    Route::get('/rnd/growthroom', 'GrowthRoomStockController@getRndGrowthRoom');
    Route::get('/rnd/transferroom', 'GrowthRoomStockController@getRndTransferRoom');
    Route::get('/prod/growthroom', 'GrowthRoomStockController@getProdGrowthRoom');
    Route::get('/prod/transferroom', 'GrowthRoomStockController@getProdTransferRoom');
    Route::get('/rooting', 'GrowthRoomStockController@getRooting');

    // routes in create growth stocks
    Route::get('/growthstocks/getcontainersbymedia/{mtype}/{media}', 'GrowthRoomStockController@getContainersByMedia')->name('getcontainersbymedia');
    Route::get('/growthstocks/getcontainerbatches/{mtype}/{container}/{media}', 'GrowthRoomStockController@getContainerBatches')->name('getcontainerbatches');
    Route::get('/growthstocks/multiplication/{id}', 'GrowthRoomStockController@createMultiplication')->name('createmultiplication');
    Route::get('/growthstocks/bulkmultiplication/{bulk_id}', 'GrowthRoomStockController@createBulkMultiplication')->name('createbulkmultiplication');
    Route::post('/growthstocks/storemultiplication', 'GrowthRoomStockController@storeMultiplication')->name('storemultiplication');
    Route::get('/growthstocks/getsubcategories/{id}', 'GrowthRoomStockController@getSubCategories')->name('getsubcategories');
    Route::get('/growthstocks/getitems/{id}', 'GrowthRoomStockController@getItems')->name('getitems');
    Route::get('/growthstocks/getplants/{container_id}/{container_batch_id}/{container_quantity}', 'GrowthRoomStockController@getPlants')->name('getplants');
    Route::get('/growthstocks/createrooting/{id}', 'GrowthRoomStockController@createRooting')->name('createrooting');
    Route::post('/growthstocks/storerooting', 'GrowthRoomStockController@storeRooting')->name('storerooting');
    // actions
    Route::get('/growthstocks/transfertogrowthroom/{id}', 'GrowthRoomStockController@transferToGrowthRoom')->name('transfertogrowthroom');
    Route::get('/growthstocks/transfertotransferroom/{id}/{type?}', 'GrowthRoomStockController@transferToTransferRoom')->name('transfertotransferroom');
    Route::get('/growthstocks/transfertoproductionroom/{id}', 'GrowthRoomStockController@transferToProduction')->name('transfertoproductionroom');
    Route::get('/growthstocks/revert/{id}', 'GrowthRoomStockController@revert')->name('revert');
//    Route::get('/growthstocks/grsrecords/{bulk_number}/{condition?}', 'GrowthRoomStockController@grsRecords')->name('grsrecords');

    Route::get('/growthstocks/showbulklabels/{id}', 'GrowthRoomStockController@showBulkLabels')->name('showbulklabels');
    Route::get('/growthstocks/showbulklabels', 'GrowthRoomStockController@showBulkLabels')->name('showbulklabels');
    Route::get('/growthstocks/searchserialcode', 'GrowthRoomStockController@searchSerialCode')->name('searchserialcode');
    Route::get('/growthstocks/complete/{id}', 'GrowthRoomStockController@complete')->name('complete');
    Route::get('/growthstocks/dispatch/{id}', 'GrowthRoomStockController@dispatch')->name('dispatch');

    Route::patch('/growthstocks/reject/{id}', 'GrowthRoomStockController@reject')->name('growthroomstock.reject');
    Route::post('/mediastocks/reject/', 'MediaStockController@reject')->name('mediastocks.reject');
    Route::get('/mediastocks/rejection_report', 'MediaStockController@rejectionReport');

    Route::patch('users/change_password/{id}', 'UserController@changePassword')->name('users.change_password');

    Route::post('/growthstocks/createqrcode', 'GrowthRoomStockController@createQrCode');
    Route::post('/growthstocks/printlabels', 'GrowthRoomStockController@printLabels');
    Route::get('/growthstocks/print_bulk_number/{bulk_number}', 'GrowthRoomStockController@printBulkNumber');

    // Initiation Stock
    Route::get('/initiations/create', 'InitiationController@create')->name('initiations.create');
    Route::get('/initiations/growth_room', 'InitiationController@growthRoom')->name('initiations.growth');
    Route::get('/initiations/transfer_room', 'InitiationController@transferRoom')->name('initiations.transfer');
    Route::get('/initiations/to_transfer_room/{id}', 'InitiationController@toTransferRoom')->name('initiations.transfer_move');
    Route::get('/initiations/to_growth_room/{id}', 'InitiationController@toGrowthRoom')->name('initiations.growth_move');
    Route::get('/initiations/to_prod_transfer_room/{id}', 'InitiationController@toProdTransferRoom')->name('initiations.prod_transfer_move');
    Route::get('/initiations/to_rnd_transfer_room/{id}', 'InitiationController@toRndTransferRoom')->name('initiations.rnd_transfer_move');
    Route::post('/initiations/store', 'InitiationController@store')->name('initiations.store');


    // reports
    Route::get('/reports/performance', 'ReportsController@performance')->name('performance');
    Route::post('/reports/performance', 'ReportsController@performance')->name('reports.filter');
    Route::get('/reports/room_conditions', 'ReportsController@room_conditions')->name('room_conditions');
    Route::get('/reports/customer_reports/{id}', 'ReportsController@customerReports')->name('customer_reports');
    Route::get('/reports/plant_status', 'ReportsController@plantStatus')->name('plant_status');
    Route::post('/reports/plant_status', 'ReportsController@plantStatus')->name('plant_status.filter');
    Route::get('/reports/bulk_status/', 'ReportsController@setBulkStatus')->name('bulk_status');
    Route::get('/reports/bulk_status/{status}', 'ReportsController@setBulkStatus')->name('bulk_status');
    Route::get('/reports/plants_media_status', 'ReportsController@plantsMediaStatus')->name('plants_media_status');
    Route::post('/reports/plants_media_status', 'ReportsController@plantsMediaStatus')->name('plants_media_status.filter');

    Route::get('/reports/plant_stock', 'ReportsController@PlantStock')->name('plant_stock');
    Route::post('/reports/plant_stock', 'ReportsController@PlantStock')->name('plant_stock.filter');
    Route::get('/reports/week_bulk/{plant_id}/{year}/{week}/{media_id?}/{plant_size?}/', 'ReportsController@weekBulk')->name('weekBulk');

    Route::post('/reports/achievement', 'ReportsController@achievement')->name('operator.achievement');

    // Performance Breakdown
    Route::get('/reports/performance_breakdown', 'ReportsController@performanceBreakdown')->name('performance.bd');
    Route::post('/reports/performance_breakdown', 'ReportsController@performanceBreakdown')->name('performance.bd.filter');

    // Contamination Report
    Route::get('/reports/contamination', 'ReportsController@contamination')->name('contamination');
    Route::post('/reports/contamination', 'ReportsController@contamination')->name('contamination.filter');

    // Cutters performance
    Route::get('/reports/cutters_performance', 'ReportsController@cuttersPerformance')->name('cutters.performance');
    Route::post('/reports/cutters_performance', 'ReportsController@cuttersPerformance')->name('cutters.performance');

    // Visualization
    Route::resource('visualization', 'VisualizationController');
    Route::post('visualization/index', 'VisualizationController@index')->name('visualization.filter');
    Route::get('visualization/show', 'VisualizationController@show')->name('visualization.sales');
    Route::post('visualization/show', 'VisualizationController@show')->name('visualization.sales.filter');
    //tasks
    Route::get('/tasks/mark_completed/{id}', 'TaskController@mark_completed')->name('tasks.mark_completed');

    //orders
    Route::delete('/orders/destroy_order_item/{id}', 'OrderController@destroyOrderItem')->name('orders.destroy_order_item');
    Route::post('/orders/store_order_item', 'OrderController@storeOrderItems')->name('orders.store_order_item');


    // backend
    Route::get('/backend/find_bulk_number', 'BackEndController@findBulkNumber');
    Route::post('/backend/find_bulk_number', 'BackEndController@findBulkNumber')->name('findBulkNumber');
    Route::post('/backend/update_bulk_number', 'BackEndController@updateBulkNumber')->name('updateBulkNumber');
    Route::post('/backend/update_bulk_completed', 'BackEndController@updateBulkCompleted')->name('updateBulkCompleted');
    Route::get('/backend/set_customer_to_plant', 'BackEndController@setCustomersToPlant')->name('setCustomersToPlant');
    Route::post('/backend/set_customer_to_plant', 'BackEndController@setCustomersToPlant')->name('storeCustomersToPlant');
    Route::post('/backend/transfer_bulk', 'BackEndController@transferBulk')->name('transferBulk');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');

    Route::get('/backend/find_media', 'BackEndController@findMedia');
    Route::get('/backend/get_media_plant_contamination/{media_id?}/{plant_id?}/{year}/{week}/', 'BackEndController@getMediaPlantContamination')->name('mediaPlantContamination');
    Route::post('/backend/find_media', 'BackEndController@findMedia')->name('findMedia');

    Route::get('/backend/find_container', 'BackEndController@findContainer');
    Route::get('/backend/get_container_plant_contamination/{container_id?}/{plant_id?}/{year}/{week}/', 'BackEndController@getContainerPlantContamination')->name('containerPlantContamination');
    Route::post('/backend/find_container', 'BackEndController@findContainer')->name('findContainer');

    Route::get('/backend/{user_id}/set_relationships', 'BackEndController@setRelationships')->name('setRelationships');


    // download apk file
    Route::get('/backend/download/{file}', 'BackEndController@download')->name('download_apk');

    Route::delete('/backend/deleteBulk/', 'BackEndController@deleteBulk')->name('deleteBulk');
    Route::delete('/backend/deleteSerialNumber/', 'BackEndController@deleteSerialNumber')->name('deleteSerialNumber');

    // greenhouse
    Route::post('/greenhouses', 'GreenhouseController@index')->name('greenhouses.filter');
    Route::post('/greenhouses/store', 'GreenhouseController@store')->name('greenhouses.store');
    Route::post('/greenhouses/plant_stock', 'GreenhouseController@plantStock')->name('greenhouses.plant_stock');
    Route::post('/greenhouses/sold', 'GreenhouseController@sold')->name('greenhouses.sold');
    Route::post('/greenhouses/reserve_stock', 'GreenhouseController@reserveStock')->name('greenhouses.reserve_stock');
    Route::get('/greenhouses/{id}/reserve', 'GreenhouseController@reserve')->name('greenhouses.reserve');
    Route::get('/greenhouses', 'GreenhouseController@index')->name('greenhouses.index');
    Route::get('/greenhouses/plant_stock', 'GreenhouseController@plantStock')->name('greenhouses.plant_stock');
    Route::get('/greenhouses/{id}/sell', 'GreenhouseController@sell')->name('greenhouses.sell');
    Route::get('/greenhouses/{id}/cancel', 'GreenhouseController@cancel')->name('greenhouses.cancel');
    Route::get('/greenhouses/{id}/plant_create', 'GreenhouseController@plantCreate')->name('greenhouses.plant_create');
    Route::get('/greenhouses/show/{id}/{task_id}', 'GreenhouseController@show');
    Route::get('/greenhouses/plants/{id}', 'GreenhouseController@plants');

    //RetailCustomer
    Route::get('/retail_customers/{id}/print_address', 'RetailCustomerController@printAddress')->name('retail_customers.print_address');

    //SalesRecords
    Route::get('sales_records', 'SalesRecordController@index');
    Route::get('/sales_records/{invoice_number}/invoiceNumber', 'SalesRecordController@invoiceNumber')->name('invoiceNumber');

    // Label Printers
    Route::get('printers', 'PrinterController@index');
    Route::get('printers/{id}/edit', 'PrinterController@edit')->name('printer.edit');
    Route::post('printers/store', 'PrinterController@store')->name('printer.store');
    Route::patch('printers/update/{id}', 'PrinterController@update')->name('printer.update');

    // Media requests
    Route::get('media_requests/cancel/{id}', 'MediaRequestController@cancel')->name('media_requests.cancel');
    Route::get('media_requests/revert/{id}', 'MediaRequestController@revert')->name('media_requests.revert');

    Route::resource('shares', 'ShareController');
    Route::resource('medias', 'MediaController');
    Route::resource('mediastocks', 'MediaStockController');
    Route::resource('growthstocks', 'GrowthRoomStockController');
    Route::resource('categories', 'CategoryController');
    Route::resource('reasons', 'ReasonController');
    Route::resource('subcategories', 'SubCategoryController');
    Route::resource('users', 'UserController');
    Route::resource('containers', 'ContainerController');
    Route::resource('items', 'ItemController');
    Route::resource('frequencies', 'FrequencyController');
    Route::resource('tasks', 'TaskController');
    Route::resource('companies', 'CompanyController');
    Route::resource('customers', 'CustomerController');
    Route::resource('retail_customers', 'RetailCustomerController');
    Route::resource('sales_records', 'SalesRecordController');
    Route::resource('orders', 'OrderController');
    Route::resource('greenhouses', 'GreenhouseController', ['except' => ['store']]);
    Route::resource('roles', 'RoleController');
    Route::resource('media_requests', 'MediaRequestController');
});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('media/autocomplete', 'MediaController@autocomplete')->name('media.autocomplete');
Route::get('item/autocomplete', 'ItemController@autocomplete')->name('item.autocomplete');
Route::get('sales/autocomplete', 'SalesRecordController@autocomplete')->name('sales.autocomplete');
Route::post('users/update_printer', 'UserController@updatePrinter')->name('user.printer');
