<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'Administrator')->first();
        $role_operator = Role::where('name', 'Operator')->first();
        $role_user = Role::where('name', 'User')->first();
        $role_supervisor = Role::where('name', 'Supervisor')->first();

        $admin = new User();
        $admin->id = 1;
        $admin->name = 'Admin';
        $admin->email = 'admin@golgitec.com';
        $admin->password = bcrypt('123');
        $admin->user_code = 'AD01';
        $admin->save();
        $admin->roles()->attach($role_admin);

        $mahi = new User();
        $mahi->id = 2;
        $mahi->name = 'Mahissara';
        $mahi->email = 'mahissara@tailorstore.com';
        $mahi->password = bcrypt('123');
        $mahi->user_code = 'M001';
        $mahi->save();
        $mahi->roles()->attach($role_admin);

        $chin = new User();
        $chin->id = 3;
        $chin->name = 'Chinthaka';
        $chin->email = 'chinthaka@tailorstore.com';
        $chin->password = bcrypt('123');
        $chin->user_code = 'C001';
        $chin->save();
        $chin->roles()->attach($role_admin);

        $susa = new User();
        $susa->id = 4;
        $susa->name = 'Susantha';
        $susa->email = 'susantha@tailorstore.com';
        $susa->password = bcrypt('123');
        $susa->user_code = 'S001';
        $susa->save();
        $susa->roles()->attach($role_admin);

        $user1 = new User();
        $user1->id = 12;
        $user1->name = 'Chathurani';
        $user1->email = 'chathurani@golgitec.com';
        $user1->password = bcrypt('123');
        $user1->user_code = 'JC01';
        $user1->save();
        $user1->roles()->attach($role_admin);

        $user2 = new User();
        $user2->id = 13;
        $user2->name = 'Wasanthi';
        $user2->email = 'wasanthi@golgitec.com';
        $user2->password = bcrypt('123');
        $user2->user_code = 'WC01';
        $user2->save();
        $user2->roles()->attach($role_operator);

        $user3 = new User();
        $user3->id = 14;
        $user3->name = 'Sanuja';
        $user3->email = 'sanuja@golgitec.com';
        $user3->password = bcrypt('123');
        $user3->user_code = 'SD01';
        $user3->save();
        $user3->roles()->attach($role_operator);

        $user4 = new User();
        $user4->id = 15;
        $user4->name = 'Kaushalya';
        $user4->email = 'kaushalya@golgitec.com';
        $user4->password = bcrypt('123');
        $user4->user_code = 'KC01';
        $user4->save();
        $user4->roles()->attach($role_operator);

        $user5 = new User();
        $user5->id = 16;
        $user5->name = 'Hashani';
        $user5->email = 'hashani@golgitec.com';
        $user5->password = bcrypt('123');
        $user5->user_code = 'HI01';
        $user5->save();
        $user5->roles()->attach($role_operator);

        $user6 = new User();
        $user6->id = 17;
        $user6->name = 'Saumya';
        $user6->email = 'saumya@golgitec.com';
        $user6->password = bcrypt('123');
        $user6->user_code = 'ST01';
        $user6->save();
        $user6->roles()->attach($role_operator);

        $user7 = new User();
        $user7->id = 18;
        $user7->name = 'Manisha';
        $user7->email = 'manisha@golgitec.com';
        $user7->password = bcrypt('123');
        $user7->user_code = 'MM01';
        $user7->save();
        $user7->roles()->attach($role_operator);

        $user8 = new User();
        $user8->id = 19;
        $user8->name = 'Sandun';
        $user8->email = 'sandun@golgitec.com';
        $user8->password = bcrypt('123');
        $user8->user_code = 'SC01';
        $user8->save();
        $user8->roles()->attach($role_operator);

        $user9 = new User();
        $user9->id = 20;
        $user9->name = 'Shalika Madushani';
        $user9->email = 'shalika@golgitec.com';
        $user9->password = bcrypt('SM01');
        $user9->user_code = 'SM01';
        $user9->save();
        $user9->roles()->attach($role_operator);

        $user8 = new User();
        $user8->id = 22;
        $user8->name = 'Sulani Awanthika';
        $user8->email = 'sulani@golgitec.com';
        $user8->password = bcrypt('SA01');
        $user8->user_code = 'SA01';
        $user8->save();
        $user8->roles()->attach($role_operator);

        $user8 = new User();
        $user8->id = 23;
        $user8->name = 'Chamila Kumari';
        $user8->email = 'chamila@golgitec.com';
        $user8->password = bcrypt('CK01');
        $user8->user_code = 'CK01';
        $user8->save();
        $user8->roles()->attach($role_operator);
    }
}
