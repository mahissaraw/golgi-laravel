<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = "Administrator";
        $role->save();

        $roleS = new Role();
        $roleS->name = 'Supervisor';
        $roleS->save();

        $roleO = new Role();
        $roleO->name = 'Operator';
        $roleO->save();

        $roleU = new Role();
        $roleU->name = 'User';
        $roleU->save();
    }
}
