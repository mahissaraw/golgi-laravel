<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bulk_number');
            $table->integer('user_id');
            $table->integer('item_id');
            $table->integer('media_id');
            $table->integer('container_id');
            $table->integer('max_container_capacity');
            $table->integer('total_containers');
            $table->integer('item_qty');
            $table->integer('rejected_qty');
            $table->float('add_amount');
            $table->float('rejected_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance');
    }
}
