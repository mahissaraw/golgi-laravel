<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefNoToMediaRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media_requests', function (Blueprint $table) {
            $table->text('ref_no')->after('confirmed')->nullable();
//            $table->text('operators')->after('completed_quantity')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media_requests', function (Blueprint $table) {
            $table->dropColumn('ref_no');
//            $table->dropColumn('operators');
        });
    }
}
