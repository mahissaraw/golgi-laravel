<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('media_type');
            $table->integer('media_id');
            $table->integer('container_id');
            $table->integer('media_amount');
            $table->decimal('requested_quantity', 8, 2);
            $table->date('requested_date');
            $table->integer('requested_by');
            $table->text('location');
            $table->date('need_date')->nullable();
            $table->integer('completed_quantity')->nullable();
            $table->text('incomplete_reason')->nullable();
            $table->dateTime('confirmed')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_requests');
    }
}
