<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipsToTransferRoomStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_room_stock', function (Blueprint $table) {
            $table->integer('operator_id')->after('user')->nullable();
            $table->integer('user_id')->after('operator_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_room_stock', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('operator_id');
        });
    }
}
