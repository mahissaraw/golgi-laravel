<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('user_id');
            // $table->integer('quantity');
            $table->dateTime('order_date');
            $table->dateTime('required_date')->nullable();
            $table->dateTime('shipped_date')->nullable();
            $table->tinyInteger('order_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }

    /**
     * Delete related table manually
     * 
     * php artisan tinker
     * 
     * Schema::drop('orders')
     * 
     * q
     */
}
