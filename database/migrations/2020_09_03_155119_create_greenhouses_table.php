<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGreenhousesTable extends Migration
{
    /**
     * Run the migrations.
     * php artisan migrate
     * php artisan migrate:rollback
     * php artisan cache:clear
     * php artisan config:clear
     * php artisan route:clear
     * @return void
     */
    public function up()
    {
        Schema::create('greenhouses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('quantity');
            $table->integer('task');
            $table->string('remark')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->index(['item_id', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('greenhouses');
    }
}
