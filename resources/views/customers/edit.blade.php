@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        Edit Customer
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('customers.update', $customer->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                        <label for="company">Company :</label><br />
                        <select class="form-control" name="company_id" id="company_id" required>
                            <option value="">Select Company</option>
                            @foreach ($companies as $key => $value)
                                <option value="{{ $value->id }}" {{ $value->id ==  $customer->company_id ? 'selected' : ''}}>
                                    {{ $value->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name" value="{{ $customer->name }}" required />
                    </div>
                    <div class="form-group">
                        <label for="name">Email:</label>
                        <input type="email" class="form-control" name="email" value="{{ $customer->email }}" required />
                    </div>

                    <div class="form-group">
                        <label for="name">Phone:</label>
                        <input type="tel" class="form-control" name="phone" value="{{ $customer->phone }}" required />
                    </div>
                    <div class="form-group">
                        <label for="name">Street:</label>
                        <input type="text" class="form-control" name="street" value="{{ $customer->street }}"
                            required />
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                        <label for="name">City:</label>
                        <input type="text" class="form-control" name="city" value="{{ $customer->city }}" required />
                    </div>
                    <div class="form-group">
                        <label for="name">State:</label>
                        <input type="text" class="form-control" name="state" value="{{ $customer->state }}" required />
                    </div>
                    <div class="form-group country_list">
                        @include('partials.country_list')
                    </div>
                    <div class="form-group">
                        <label for="name">Status:</label>
                        <select class="form-control" id="is_active" name="is_active">
                            <option value="1" {{ $customer->is_active == 1 ? 'selected' : '' }}>Active</option>
                            <option value="0" {{ $customer->is_active == 0 ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>
                    <div class="custom-file mt-4">
                        <input type="file" class="custom-file-input" name="logo" id="logo">
                        <label class="custom-file-label" for="logo">Change logo</label>
                    </div>
                    <img src="/customer_logo/{{ $customer->logo ?? 'nologo.png' }}" width="100px" class="mt-2" />
                </div>
            </div>

            <button type="submit" class="btn btn-primary mt-2 float-right">Update</button>
        </form>
    </div>
</div>
<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
@endsection
