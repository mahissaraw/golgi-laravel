@extends('layouts.app')

@section('content')

<h3>
    List of Customers
    <a href="{{ route('customers.create') }}" class="btn btn-secondary ml-5">Add New Cooperate Customer</a>
    <a href="{{ route('companies.create') }}" class="btn btn-outline-primary ml-5">Create Cooperate Company</a>
</h3>
<div class="uper">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Company Name</th>
                <th>Name</th>
                <th>Logo</th>
                <th>email</th>
                <th>Phone</th>
                <th>Street</th>
                <th>City</th>
                <th>State</th>
                <th>Country</th>
                <th>Status</th>
                <th colspan="2">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($customers as $customer)
            <tr>
                <td>
                    @if ($customer->company_id)
                        <a href="{{ route('customer_reports',$customer->user->id)}}">{{ $customer->company->name }}</a>
                    @endif
                </td>
                <td>{{ $customer->name }}</td>
                <td>
                    @if (file_exists( public_path(). '/customer_logo/'.$customer->company->logo))
                        <img src="/customer_logo/{{ $customer->company->logo ?? 'nologo.png' }}" height="50px" width="50px" />
                    @else
                        <img src="http://124.43.9.226/customer_logo/{{ $customer->company->logo ?? 'nologo.png' }}" height="50px" width="50px" />
					@endif
                </td>
                <td>{{ $customer->email }}</td>
                <td>{{ $customer->phone }}</td>
                <td>{{ $customer->street ?? '' }}</td>
                <td>{{ $customer->city ?? '' }}</td>
                <td>{{ $customer->state ?? '' }}</td>
                <td>{{ $customer->country }}</td>
                <td>{{ $customer->is_active ? 'Active' : 'Inactive' }}</td>
                <td>
                    <a href="{{ route('customers.edit',$customer->id)}}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                    <form action="{{ route('customers.destroy', $customer->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
