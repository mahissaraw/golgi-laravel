@extends('layouts.app')

@section('content')
<div class="card uper">
    <div class="card-header">
        Edit User
        <button type="button" class="btn btn-warning btn-sm float-right" data-toggle="modal"
            data-target="#exampleModal">Change
            Password</button>
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('users.update', $user->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="name">User Name:</label>
                <input type="text" class="form-control" name="user_name" value="{{ $user->name }}" />
            </div>
            <div class="form-group">
                <label for="name">Email:</label>

                <input type="text" class="form-control" name="email" value="{{ $user->email }}" />
            </div>
            <div class="form-group">
                <label for="name">User Code</label>

                <input type="text" class="form-control" name="user_code" value="{{  $user->user_code }}" />
            </div>
            <div class="form-group">
                <label for="active">User Availability</label>
                <select class="form-control" id="active" name="active">
                    <option value="1" {{ $user->active == 1 ? 'selected' : '' }}>Active</option>
                    <option value="0" {{ $user->active == 0 ? 'selected' : '' }}>Inactive</option>
                </select>
            </div>
            <div class="form-group">
                <label for="user_roles">Roles</label>
                <select multiple class="form-control" id="user_roles" name="user_roles[]">
                    @foreach ($roles as $role)
                    <option value="{{ $role->id }}" {{ $user->hasRole($role->name) == $role->name ? 'selected' : '' }}>
                        {{ $role->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group cutter-date-form-group {{ $user->cutter_date == null ? 'd-none' : ''}}">
                <label for="name">Cutter Date:</label>
                <input type="text" class="form-control" name="cutter_date" id="cutter_date"
                       autocomplete=“off” value="{{  $user->cutter_date }}" data-maxdate="today"/>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('users.change_password', $user->id) }}">
                    @method('PATCH')
                    @csrf
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <div class="form-group">
                        <label for="password">{{ __('Password') }}</label>
                        <input id="password" type="password"
                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                            required>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            required>
                    </div>
                    <button type="submit" class="btn btn-primary float-right mt-2">Save change</button>
                </form>
                <button type="button" class="btn btn-secondary mt-2" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        // $( ".date_change" ).click(function() {
        //     if($('#cutter_date').val()){
        //         if ($(".date_change").hasClass("btn-info") ) {
        //             $('.date_change').addClass("btn-secondary").removeClass("btn-info").html('Set');
        //             $('#cutter_date').attr("readonly", false);
        //         } else {
        //             $('.date_change').removeClass("btn-secondary").addClass("btn-info").html('Change');
        //             $('#cutter_date').attr("readonly", true);
        //         }
        //     }
        // });
        $(function() {
            $('#user_roles').change(function() {
                // console.log($(this).val().indexOf('6'));
                //6 - cutter
                if ($(this).val().indexOf('6') >= 0 || $("#cutter_date").val()){
                    $(".cutter-date-form-group").removeClass("d-none");
                } else {
                    $(".cutter-date-form-group").addClass("d-none");
                }
            });
        });
        $(function () {
            $('#cutter_date').duDatepicker({format: 'yyyy-mm-dd', theme: 'teal', auto: true});
        });
    });
</script>
@endsection
