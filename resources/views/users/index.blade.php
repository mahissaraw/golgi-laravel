@extends('layouts.app')

@section('content')
    <h3>
        List of {{ $users->count() }} Users
        <a href="/register" class="btn btn-secondary ml-5 add-reason">Create New Account</a>
    </h3>
    <div class="col-xs-12 filters">
        <a class="btn btn-sm btn-outline-success" href="/users">Reset list</a>
        <a class="btn btn-sm btn-outline-info" href="/users?select_inactive=true">Show Inactive</a>
    </div>
    <div class="uper">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>User Code</td>
                <td>User Role</td>
                <td colspan="2">Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr class="{{ $user->active == 0  ? 'alert-inactive': '' }}">
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->user_code}}</td>
                    <td>
                        @if ($user->active)
                            @foreach ($user->roles as $role)
                                <p> {{ $role->name }}</p>
                            @endforeach
                        @else
                            <b>Inactive</b>
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('users.show', $user->id)}}" class="btn btn-success">View</a>
                        <a href="{{ route('users.edit', $user->id)}}" class="btn btn-primary">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <div>
    <style>
        .alert-inactive {
            color: #f21e21;
            font-weight: bold;
        }
    </style>
@endsection
