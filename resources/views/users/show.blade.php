@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card uper">
        <div class="card-body">
            <div class="row">
                <h3>
                    <i class="fas fa-user-circle ml-2"></i> {{ $user->name }}'s Details
                </h3>
            </div>
            <table class="table">
                <tbody>
                    <tr>
                        <th>Email</th>
                        <td>{{$user->email}}</td>
                    </tr>
                    <tr>
                        <th>User Code</th>
                        <td>{{$user->user_code}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td class="{{ $user->active !== 1 ? 'text-danger' : '' }}">
                            {{ $user->active == 1 ? 'Active' : 'Inactive' }}
                        </td>
                    </tr>
                    <tr>
                        <th>{{ Illuminate\Support\Str::plural('Role', $user->roles->count()) }}</th>
                        <td>
                            @foreach ($user->roles as $role)
                            <p>{{ $role->name }}</p>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Label Printer</th>
                        <td>
                            <select class="form-control" id="printer" name="printer">
                                <option value="0">--- Select Printer ---</option>
                                @foreach ($printers as $printer)
                                    <option value="{{ $printer->id }}" {{ $user->printer_id === $printer->id ? 'selected' : '' }}>
                                        {{ $printer->name }}
                                    </option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    @if($user->cutter_date)
                    <tr>
                        <th>Cutter Date</th>
                        <td>
                            {{ $user->cutter_date ?? '' }} <label class="text-muted float-right">{{ \Carbon\Carbon::parse($user->cutter_date)->diffForHumans() }}</label>
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
            <div class="alert-msg"></div>
            @if(Auth::user()->hasAnyRole(['Administrator', 'Supervisor']))
            <hr />
            <a href="{{ route('users.edit', $user->id)}}" class="btn btn-primary"><i class="far fa-edit"></i> Update</a>
                @if(Auth::user()->isDeveloper())
                    <a href="{{ route('setRelationships', $user->id)}}" class="btn btn-warning float-right"><i class="fas fa-network-wired"></i> Update Relationship</a>
                @endif
            @endif
{{--            @if(Auth::user()->hasAnyRole(['Administrator']))--}}
{{--            <a href="#" class="btn btn-danger" onclick="--}}
{{--                    var result = confirm('Are you sure you wish to DELETE {{ $user->name }}?');--}}
{{--                    if (result){--}}
{{--                    event.preventDefault();--}}
{{--                    $('#delete-{{ $user->id }}').submit();--}}
{{--                    }"><i class="far fa-trash-alt"></i> Delete</a>--}}

{{--            <form id="delete-{{ $user->id }}" action="{{ route('users.destroy', $user->id)}}" method="POST"--}}
{{--                style="display:none">--}}
{{--                @csrf--}}
{{--                @method('DELETE')--}}
{{--                <input value="delete" name="_method" type="hidden">--}}
{{--            </form>--}}
{{--            @endif--}}

        </div>
    </div>
    <script>
        $(document).ready(function(){
            $("#printer").change(function () {
                var selectedText = $(this).find("option:selected").text();
                var selectedValue = $(this).val();
                var user = {{ Auth::user()->id }};

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    method: 'POST',
                    url: '/users/update_printer',
                    data: {'printer_id' : selectedValue, 'user_id': user},
                    success: function(response){

                        $('.alert-msg').prepend(
                            '<div class="alert alert-info qr-alert" role="alert">'+response.success+'</div>'
                        );
                        $(".qr-alert").delay(3000).fadeOut("slow");
                        // window.history.back();
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            });
        });
    </script>
    @endsection
