@extends('layouts.app')

@section('content')
<div id="add_reason" style="display:none">
    <h3 class="display-5">
        Add Reason
    </h3>
    <form method="POST" action="{{ route('reasons.store') }}">
        <div class="form-group">
            {{ csrf_field() }}
            <input type="text" class="form-control" name="reason_name" required placeholder="Reason Name"
                minlength="3" />
        </div>
        <button type="submit" class="btn btn-primary btn-lg">Save</button>
    </form>
    <hr />
</div>

<h3>
    List of Reasons
    <button type="button" class="btn btn-secondary ml-5" data-toggle="modal" data-target="#addContainerModel">
        Add New Reason
    </button>
</h3>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($reasons as $reason)
        <tr>
            <td>{{ $reason->name }}</td>
            <td>
                <a href="/reasons/{{ $reason->id}}/edit" class="btn btn-primary">Edit</a>
                {{--<a href="#" class="btn btn-danger" onclick="--}}
                    {{--var result = confirm('Are you sure you wish to DELETE {{ $reason->name }}?');--}}
                    {{--if (result){--}}
                        {{--event.preventDefault();--}}
                                   {{--$('#delete-{{ $reason->id }}').submit();--}}
                    {{--}">Delete</a>--}}
                {{--<form id="delete-{{ $reason->id }}" action="{{ route('reasons.destroy', $reason->id)}}" method="POST"--}}
                    {{--style="display:none">--}}
                    {{--@csrf--}}
                    {{--@method('DELETE')--}}
                    {{--<input value="delete" name="_method" type="hidden">--}}
                {{--</form>--}}
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

<div class="modal fade" id="addContainerModel" tabindex="-1" role="dialog"
     aria-labelledby="addContainerModelTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('reasons.store') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="addContainerModelLongTitle">Add Reason</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="form-control" name="reason_name" id="reason_name" required placeholder="Reason Name" minlength="3" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#addContainerModel').on('shown.bs.modal', function() {
        $('#reason_name').focus();
    })
</script>

@endsection
