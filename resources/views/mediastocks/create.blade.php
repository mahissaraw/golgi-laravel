@extends('layouts.app')

@section('content')
<script type='text/javascript'>
    $(document).ready(function(){

        $('#sel_type').change(function(){

            var id = $(this).val();

            // Empty the dropdown
            $('#sel_media').find('option').not(':first').remove();
            $('#sel_container').find('option').not(':first').remove();

            // AJAX request
            $.ajax({
                url: '/growthstocks/getmedias/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }

                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            if(response['data'][i].active == 1){
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#sel_media").append(option);
                            }
                        }
                    }
                }
            });

            $.ajax({
                url: '../getcontainers/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }

                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var option = "<option value='"+id+"'>"+name+"</option>";
                            $("#sel_container").append(option);
                        }
                    }
                }
            });
        });

    });

</script>
<div class="container">
    <div class="card">
        <div class="card-header">
            Add Media Stock
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('mediastocks.store') }}">
                <div class="form-group">
                    <label for="name">Operator Name:</label>
                    <select class="form-control" name="user_id" id="user_id" required>
                        <option>Select User</option>
                        @foreach ($users as $key => $value)
                        <option value="{{ $key }}">
                            {{ $value }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="price">Media Type :</label>
                    <select class="form-control" name="sel_type" id='sel_type' required>
                        <option value="">-- Select Media Type --</option>
                        @foreach (config('app.media_types') as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Media Name:</label>
                    <select class="form-control" id='sel_media' name='sel_media' required>
                        <option value="">-- Select Media --</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Container Name:</label>
                    <select class="form-control" id='sel_container' name='sel_container' required>
                        <option value="">-- Select Container --</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Container Quantity:</label>
                    <input type="text" class="form-control" name="container_qty" required />
                </div>
                <button type="submit" class="btn btn-primary">Add Media Stock</button>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>
@endsection