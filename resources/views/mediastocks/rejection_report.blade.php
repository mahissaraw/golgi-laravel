@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ URL::asset('js/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/daterangepicker/daterangepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            let start = moment().subtract('days', 6);
            let end = moment();

            //Set the initial state of the picker label
            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            $('#reportrange').daterangepicker(
                {
                    startDate: start,
                    endDate: end,
                    minDate: '01/01/2019',
                    maxDate: moment(),
                    dateLimit: { days: 365 },
                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    opens: 'center',
                    buttonClasses: ['btn btn-default'],
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small',
                    format: 'MM/DD/YYYY',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'Submit',
                        fromLabel: 'From',
                        toLabel: 'To',
                        customRangeLabel: 'Custom Range',
                        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        firstDay: 1
                    }
                },
                function(start, end) {
                    console.log("Callback has been called!");
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    $('input[name="fromDate"]').val(start.format('YYYY-MM-DD'));
                    $('input[name="toDate"]').val(end.format('YYYY-MM-DD'));
                }
            );
            cb(start, end);
            $('input[name="fromDate"]').val(start.format('YYYY-MM-DD'));
            $('input[name="toDate"]').val(end.format('YYYY-MM-DD'));
        });
    </script>

    <div class="upper">
        <div class="shadow-sm p-3 mb-2 bg-white rounded">
            <form method="post" action="{{ route('contamination.filter') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <b class="h3">
                            Media Rejections Report
                        </b>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="upper">
        <table class="table table-hover sticky-header">
            <thead class="thead-dark">
                <tr>
                    <th>Batch No.</th>
                    <th>Media Type</th>
                    <th>Media</th>
                    <th>Container</th>
                    <th>Quantity</th>
                    <th>Reasons</th>
                    <th>Create</th>
                    <th class="text-center">Created Date</th>
                    <th class="text-center">Rejected Date</th>
                </tr>
            </thead>
            <tbody>
            @foreach($rejected_stocks as $rjs)
                <tr>
                    <td>{{  $rjs->trs_ref_no }}</td>
                    <td>{{ config('app.media_types')[$rjs->t_m_type] }}</td>
                    <td>{{  $rjs->media }}</td>
                    <td>{{  $rjs->container }}</td>
                    <td class="text-center">{{  $rjs->trs_quantity }}</td>
                    <td>
                        @foreach (json_decode( $rjs->trs_reasons) as $key => $value)
                            @php
                                $reason = DB::table('reasons')->find(explode('_',$value)[1]);
                            @endphp
                            {{  $reason->name }}<br />
                        @endforeach
                    </td>
                    <td>{{  $rjs->t_operator_code }} - {{  $rjs->t_operator }}</td>
                    <td class="text-center">{{  substr($rjs->t_date, 0, 10) ?? '' }}</td>
                    <td class="text-center">{{  substr($rjs->trs_created_at, 0 , 10) ?? '' }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $rejected_stocks->links() }}
    </div>
    <style>
        table.floatThead-table {
            border-top: none;
            border-bottom: none;
            background-color: #fff;
        }

        th {
            position: sticky;
            top: 50px;
        }
    </style>
@endsection
