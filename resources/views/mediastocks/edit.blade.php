@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Media
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('medias.update', $media->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="name">Media Name:</label>
          <input type="text" class="form-control" name="media_name" value={{ $media->name }} />
        </div>
        <div class="form-group">
          <label for="price">Media Type :</label>
          <input type="text" class="form-control" name="media_type" value={{ $media->type }} />
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@endsection
