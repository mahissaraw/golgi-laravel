@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.css">
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.js"></script>
    <script src="{{ asset('js/submit.js') }}"></script>
{{--<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>--}}
{{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">--}}
<div>
    <h2>Media Stocks</h2>
    <table class="table table-striped" id="mediaStocks" data-toggle="table"
           data-search="true"
           data-pagination="true">
        <thead>
            <tr>
                <th data-sortable="true">Batch No.</th>
                <th data-sortable="true">Media Type</th>
                <th data-sortable="true">Media</th>
                <th data-sortable="true">Container</th>
                <th data-sortable="true">Available Containers</th>
                <th data-sortable="true">Operator</th>
                <th data-sortable="true">Created By</th>
                <th data-sortable="true">Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($mediastocks as $mediastock)
            <tr>
                <td>{{$mediastock->ref_no}}</td>
                <td>{{ config('app.media_types')[$mediastock->m_type] }}</td>
                <td>{{$mediastock->media}}</td>
                <td>{{$mediastock->container}}</td>
                <td class="text-center">{{$mediastock->qty}}</td>
                <td>{{$mediastock->user_code}} - {{ $mediastock->user_name }}</td>
                <td>{{$mediastock->operator_code}} - {{$mediastock->operator}}</td>
                <td>{{substr($mediastock->date,0,10)}}</td>
                <td>
                    <button type="button" class="btn btn-danger reject-button btn-sm" data-toggle="modal"
                        data-target="#rejectModal" data-row-id="{{ $mediastock->id }}"
                        data-row-batch-id="{{ $mediastock->ref_no }}">
                        <i class="far fa-times-circle"></i> Reject
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{-- {{ $mediastocks->onEachSide(1)->links() }} --}}
</div>
{{-- reject model --}}
<div class="modal" id="rejectModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-secondary">
                <h4 class="modal-title text-white">Remove Media Stock Containers</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <h5 class="text-primary">Batch No: <span id="ref_no"></span></h5>
                </div>
                <span class="text-danger mb-2 mt-2 reasons-validate" style="display:none">
                    <strong><i class="fas fa-exclamation-circle"></i> Please select reason(s)</strong>
                </span>
                <form id="reject_form" method="post" action="{{ route('mediastocks.reject') }}" class="form-prevent-multiple-submits">
                    @method('POST')
                    @csrf
                    @foreach ($reasons as $reason)
                    <div class=" form-group">
                        <input type="checkbox" name="reasons[]" value="reason_{{ $reason->id }}">
                        <span class="ml-1">{{ $reason->name }}</span>
                    </div>
                    @endforeach
                    <div class="form-group">
                        <label><strong>Container Quantity for Remove</strong>
                        </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">
                                    <i class="far fa-edit"></i>
                                </span>
                            </div>
                            <input type="number" id="qty_remove" autocomplete="off" class="form-control"
                                aria-describedby="basic-addon1" name="qty_remove" placeholder="Enter amount" min="1"
                                step="1" required>
                        </div>
                    </div>
                    <input type="hidden" id="batch_id" class="form-control" name="batch_id" value="">

                    <hr />
                    <button type="submit" class="btn btn-danger float-left mt-2 btn-prevent-multiple-submits">
                        <i class="fas fa-times-circle"></i> Reject
                    </button>
                </form>
                <button type="button" data-dismiss="modal" class="btn btn-secondary float-right mt-2">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        // $('#mediaStocks').DataTable();

        $('#reject_form button[type=submit]').click(function(e) {
            var isValid = $('input[name="reasons[]"]:checked').length > 0;
            if(!isValid) {
                e.preventDefault();
                $(".reasons-validate").show();
            } else {
                return true;
            }
        });

        $('input[name="reasons[]"]').click(function(e) {
            $(".reasons-validate").hide();
        });

        $(document).on('click', '.reject-button' , function() {
            $("#batch_id").val($(this).attr('data-row-batch-id'));
            $("#ref_no").html($(this).attr('data-row-batch-id'));
        });

        var isMobile = window.orientation > -1;
        if (isMobile){
            $('table').addClass('table-responsive');
        } else {
            $('table').removeClass('table-responsive');
        }
    });
</script>
<style>
    .dataTables_filter input[type="search"] {
        border: 1px solid #ced4da;
        border-radius: .25rem;
        padding: 2px;
    }
</style>
@endsection
