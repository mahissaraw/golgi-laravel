@if (Auth::check())
@if(!Auth::user()->isGreenhouseUser())
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-archive"></i> Media Stock
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="/mediastocks">All Media Stocks</a>
        @if(Auth::user()->hasAnyRole(['Administrator', 'Media Stock']))
            <a class="dropdown-item" href="/mediastocks/create">Add Media Stock</a>
            <a class="dropdown-item" href="/mediastocks/rejection_report">Media Rejections Report</a>
        @endif
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="/media_requests/">Media Requests</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-microscope"></i> R&D
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="/rnd/transferroom">Transfer Room</a>
        <a class="dropdown-item" href="/rnd/growthroom">Growth Room</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{ route('growthstocks.create')}}">Add Growth Stock</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-industry"></i> Production
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="/prod/transferroom">Transfer Room</a>
        <a class="dropdown-item" href="/prod/growthroom">Growth Room</a>
        <a class="dropdown-item" href="/rooting">Rooting</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{ route('showbulklabels')}}">Search Bulk Number</a>
        <a class="dropdown-item" href="{{ route('searchserialcode')}}">Search Serial Code</a>
    </div>
</li>
@endif
@if(Auth::user()->hasAnyRole(['Administrator', 'Supervisor']))
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-tasks"></i> Tasks
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="/tasks">Tasks List</a>
        <a class="dropdown-item" href="/tasks/create">Create Task</a>
        @if(Auth::user()->hasAnyRole(['Administrator']))
            <a class="dropdown-item" href="/backend/find_bulk_number"><i class="fas fa-search"></i> Find bulk number</a>
        @endif
    </div>
</li>
@endif
@if(Auth::user()->hasAnyRole(['Administrator', 'Supervisor', 'Operator', 'Report Analyse']))
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-file-contract"></i> Reports
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="{{ route('performance') }}">Performance</a>
        @if(Auth::user()->hasAnyRole(['Administrator', 'Supervisor']))
        <a class="dropdown-item" href="{{ route('plant_status') }}">Plant Status</a>
        <a class="dropdown-item" href="{{ route('plants_media_status') }}">Media Status</a>
        @endif
        @if(Auth::user()->isSupperAdmin() || Auth::user()->hasAnyRole(['Administrator', 'Report Analyse']))
        <a class="dropdown-item" href="{{ route('plant_stock') }}">Plants Stock</a>
        <a class="dropdown-item" href="{{ route('performance.bd') }}">Performance Breakdown</a>
        @endif
        @if(Auth::user()->hasAnyRole(['Administrator']))
        <a class="dropdown-item" href="{{ route('contamination') }}">Contamination Report</a>
        @endif
        <a class="dropdown-item" href="{{ route('cutters.performance') }}">Cutters Performance</a>
        @if(Auth::user()->hasAnyRole(['Administrator']))
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/visualization">Data Visualization</a>
        @endif
    </div>
</li>
@endif
@if(Auth::user()->isSupperAdmin() || Auth::user()->hasRole('Greenhouse'))
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-spa"></i> Greenhouse
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="/greenhouses"> Plant Available Stock</a>
        <a class="dropdown-item" href="{{ route('greenhouses.plant_stock') }}"> Plant Stock Filter</a>
        <a class="dropdown-item" href="/greenhouses/create"> Create Plant Stock</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{ route('retail_customers.index') }}"> Retail Customers</a>
        @if(Auth::user()->hasAnyRole(['Administrator', 'Sales']))
            <a class="dropdown-item" href="{{ route('sales_records.index') }}"> Sales Records</a>
        @endif
    </div>
</li>
@endif {{-- end isSupperAdmin --}}
@if(Auth::user()->isSupperAdmin())
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-cogs"></i> Settings
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="/medias">Media</a>
        <a class="dropdown-item" href="/items">Plants</a>
        <a class="dropdown-item" href="/subcategories">Plant Sub Categories</a>
        <a class="dropdown-item" href="/categories">Plant Categories</a>
        <a class="dropdown-item" href="/containers">Containers</a>
        <a class="dropdown-item" href="/reasons">Reasons</a>
        <a class="dropdown-item" href="/users">Users</a>
        <a class="dropdown-item" href="/frequencies">Frequencies</a>
        <a class="dropdown-item" href="/printers">Label Printers</a>

        @if(Auth::user()->hasAnyRole(['Administrator']))
        <a class="dropdown-item" href="/customers">Cooperate Customers</a>
        @endif
    </div>
</li>
@endif {{-- end Settings role validate condition --}}
@if(Auth::user()->hasAnyRole(['Administrator']))
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-laptop-code"></i> Backend
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        @if(Auth::user()->isSupperAdmin())
            <a class="dropdown-item" href="{{ route('logs') }}" target="_blank"><i class="fas fa-clipboard-list"></i> Logging</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/backend/find_bulk_number"><i class="fas fa-search"></i> Find bulk number</a>
            <a class="dropdown-item" href="{{ route('setCustomersToPlant') }}"><i class="fas fa-user-friends"></i> Set Company to plant</a>
        @endif
        <a class="dropdown-item" href="/backend/find_media"><i class="fas fa-vials"></i> Find Silicon Rubber Media</a>
        <a class="dropdown-item" href="/backend/find_container"><i class="fas fa-prescription-bottle"></i> Find PP Bag Containers</a>
        <a class="dropdown-item" href="/backend/download/app-debug.apk"><i class="fas fa-download"></i>
            Download GT Production App</a>
        <a class="dropdown-item" href="/backend/download/BarcodeScanner-4.7.6.apk"><i class="fas fa-barcode"></i>
            Download GT Barcode App</a>
{{--        <a class="dropdown-item" href="{{ route('room_conditions') }}"><i class="fas fa-temperature-low"></i>--}}
{{--            Room Conditions</a>--}}
        <a class="dropdown-item" href="/visualization"><i class="fas fa-chart-pie"></i> Visualization</a>
    </div>
</li>
@endif {{-- end Administrator --}}
@endif {{-- end auth check --}}
