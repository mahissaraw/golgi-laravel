<div class="modal" id="rejectModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Media Stock Remove</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <span class="text-danger mb-1 reasons-validate" style="display:none">
                    <b><i class="fas fa-exclamation-circle"></i> Please select reason(s)</b>
                </span>
                <form id="reject_form" method="post" action="{{ route('growthroomstock.reject', $status_data->id) }}">
                    @method('PATCH')
                    @csrf
                    @foreach ($reasons as $reason)
                    <div class=" form-group">
                        <input type="checkbox" name="reasons[]" value="reason_{{ $reason->id }}">
                        <span class="ml-1">{{ $reason->name }}</span>
                    </div>
                    @endforeach

                    <hr />
                    <button type="submit" class="btn btn-danger float-left mt-2">
                        <i class="fas fa-times-circle"></i> Reject
                    </button>
                </form>
                <button type="button" data-dismiss="modal" class="btn btn-secondary float-right mt-2">Close</button>
            </div>
        </div>
    </div>

    <script>
        $('#reject_form button[type=submit]').click(function(e) {
            var isValid = $('input[name="reasons[]"]:checked').length > 0;
            if(!isValid) {
                e.preventDefault(); 
                $(".reasons-validate").show();
            } else {
                return true;
            }
        });

        $('input[name="reasons[]"]').click(function(e) {
            $(".reasons-validate").hide();
        });
        
    </script>