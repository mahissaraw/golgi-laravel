<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Select Label Printer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="printer_form" action="#">
                <div class="modal-body">
                    <select class="form-control" id="printer" name="printer">
                        <option value="0">--- Select Printer ---</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $.ajax({
            url: '/printers',
            type: 'get',
            dataType: 'json',
            success: function(response){

                jQuery.each( response.data , function( i, val ) {
                    var option = "<option value='"+val.id+"'>"+val.name+"</option>";
                    $("#printer").append(option);
                });
            }
        });
    });
</script>
<script>
    $(document).ready(function(){
        $("#printer_form").submit(function () {
            var selectedText = $("#printer").find("option:selected").text();
            var selectedValue = $("#printer").val();
            var user = {{ Auth::user()->id }};

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url: '/users/update_printer',
                data: {'printer_id' : selectedValue, 'user_id': user},
                success: function(response){

                    $('.alert-msg').prepend(
                        '<div class="alert alert-info qr-alert" role="alert">'+response.success+'</div>'
                    );
                    $(".qr-alert").delay(3000).fadeOut("slow");
                    // window.history.back();
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        });
    });
</script>
