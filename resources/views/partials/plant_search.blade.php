<script src="{{ asset('js/bootstrap3-typeahead.min.js') }}"></script>
<form action="#" method="get" class="col-md-3 col-sm-5 offset-md-4">
    @method('GET')
    @csrf
    <div class="input-group mb-3 float-right">
        <input type="text" name="search_item_name" id="search_item_name"
               class="form-control typeahead border-primary" placeholder="Search Plant Barcode"
               aria-label="Search Item Name" aria-describedby="basic-addon2" autocomplete="off">
        <div class="input-group-append">
            <button class="btn btn-outline-primary" type="submit" id="search_item_name_btn"><i
                    class="fas fa-search"></i></button>
        </div>
    </div>
    <input type="hidden" value="" name="search_item_id" id="search_item_id">
</form>
<script type="text/javascript">
    var path = "{{ route('item.autocomplete') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        },
        afterSelect: function (data) {
            $('#search_item_id').val(data.id);
            console.log(data.id);
        }
    });
</script>
