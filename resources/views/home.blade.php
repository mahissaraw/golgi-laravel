@extends('layouts.app')

@section('content')
<link href="{{ asset('css/bootstrap4-toggle.min.css') }}" rel="stylesheet">
<script src="{{ asset('js/bootstrap4-toggle.min.js') }}"></script>
<script src="{{ asset('js/production.app.client.js') }}" defer></script>
<div class="box">
    <div class="row justify-content-center mr-1 ml-1">
        <div class="col-lg-4 col-md-4 col-sm-6 col-sm-offset-2 col-xs-12">
            <div class="qr-input mt-3 mb-2">
                <form action="{{route('home.scanqrcode')}}" method="POST" id="form_qr_scan">
                    @csrf
                    @method('POST')
                    <div class="input-group input-group-lg">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-qrcode"></i></div>
                        </div>
                        <input id="s_no" name="s_no" type="number" class="form-control input-lg" autocomplete="off"
                            placeholder="Scan QR Code" autofocus required min="1">
                    </div>
                    <input type="hidden" name="label_scan" id="label_scan" value="true">
                </form>
            </div>
            <div class="qr-scan text-center">
                <div class="box-part text-center">
                    <a href="#" id="scanBarcodeButton" class="btn" data-trigger="progress">
                        <i class="fas fa-qrcode"></i>
                        <div class="title">
                            <h4>Scan QR Code</h4>
                        </div>
                    </a>
                </div>
            </div>
            <div>

            </div>
            @if(Auth::user()->hasAnyRole(['Administrator', 'Label Scan']))
                <div class="col-xs-12">
                    <div class="shadow-sm p-1 bg-white rounded text-center">
                        <div class="form-group">
                            <label for="one-click">Select one-click Action:</label>
                            <select class="form-control" id="one-click" name="one-click">
                                <option class="text-danger" value="0">DISABLE</option>
                                <option value="1" {{ session()->get('user.direct_transfer') == 1 ? 'selected' : '' }}>Production Transfer</option>
                                <option value="2" {{ session()->get('user.direct_transfer') == 2 ? 'selected' : '' }}>Complete</option>
                                <option value="3" {{ session()->get('user.direct_transfer') == 3 ? 'selected' : '' }}>Dispatch</option>
                            </select>
                        </div>
                    </div>
                    <div class="skip-info alert-success text-center font-weight-bold"></div>
                </div>
            @endif
        </div>
    </div>

    <div class="row justify-content-center mr-1 ml-1">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

            <div class="box-part text-center">

                <a href="medias" class="btn">
                    <i class="fa fa-flask fa-4x" aria-hidden="true"></i>

                    <div class="title">
                        <h4>Media stocks List</h4>
                    </div>
                    <div class="text">
                        <span>Multiplication: | Rooting: </span>
                    </div>
                </a>
                <div>
                    <a href="{{route('mediastocks.index')}}" class="btn btn-outline-success">View</a>
                </div>

            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

            <div class="box-part text-center">

                <a href="rnd/growthroom" class="btn">
                    <i class="fa fa-leaf fa-4x" aria-hidden="true"></i>

                    <div class="title">
                        <h4>R&D Growth Room</h4>
                    </div>

                    <div class="text">
                        <span>No. of Containers :</span>
                    </div>
                </a>
                <div>
                    <a href="rnd/growthroom" class="btn btn-outline-success">View</a>
                </div>

            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

            <div class="box-part text-center">

                <a href="rnd/transferroom" class="btn">
                    <i class="fa fa-share-square fa-4x" aria-hidden="true"></i>

                    <div class="title">
                        <h4>R&D Transfer Room</h4>
                    </div>

                    <div class="text">
                        <span>No. of Transfers :</span>
                    </div>
                </a>
                <div>
                    <a href="rnd/transferroom" class="btn btn-outline-success">View</a>
                </div>

            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

            <div class="box-part text-center">
                <a href="prod/growthroom" class="btn ">
                    <i class="fab fa-pagelines"></i>

                    <div class="title">
                        <h4>Production Growth Room</h4>
                    </div>

                    <div class="text">
                        <span>No. of Containers :</span>
                    </div>
                </a>
                <div>
                    <a href="prod/growthroom" class="btn btn-outline-success">View</a>
                </div>

            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

            <div class="box-part text-center">

                <a href="/prod/transferroom" class="btn">
                    <i class="fa fa-share fa-4x" aria-hidden="true"></i>

                    <div class="title">
                        <h4>Production Transfer Room</h4>
                    </div>


                    <div class="text">
                        <span>No. of Transfers : </span>
                    </div>
                </a>
                <div>
                    <a href="/prod/transferroom" class="btn btn-outline-success">View</a>
                </div>

            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

            <div class="box-part text-center">

                <a href="/rooting" class="btn">
                    <i class="fa fa-tree fa-4x" aria-hidden="true"></i>

                    <div class="title">
                        <h4>Rooting</h4>
                    </div>


                    <div class="text">
                        <span>No. of Rooting : </span>
                    </div>
                </a>
                <div>
                    <a href="/rooting" class="btn btn-outline-success">View</a>
                </div>

            </div>
        </div>
    </div>
</div>
@if ($tasks->count() > 0)
<div class="col-lg-6 col-sm-12 offset-lg-3 mb-2">
    <div class="card bg-info">
        <div class="card-header text-center">
            <h5><i class="fas fa-list-ul"></i> Tasks To Do</h5>
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                @foreach ($tasks as $task)
                <li class="list-group-item {{ $task->is_complete == 1 ? 'bg-success' : ''}}">
                    <b class="h4">{{ $task->frequency->frequency ?? 'No frequency set' }}</b>
                    <label class="float-right">
                        Start Date: {{ date('Y-m-d', strtotime($task->date_started)) }} <br>
                        End Date: {{ date('Y-m-d', strtotime($task->date_completed)) }}
                    </label>
                    <p>{{ $task->description }}</p>

                    @php
                    $start = date_create(date("Y-m-d"));
                    $end = date_create(date('Y-m-d', strtotime($task->date_completed)));
                    $diff=date_diff($start,$end);
                    @endphp
                    <label class="float-right">
                        {{ $task->is_complete ==1 ? "Done":"Pending ".$diff->format("%R%a days") }}
                    </label>
                    @if ($task->is_complete != 1)
                    <button class="btn btn-warning btn-sm mark-completed shadow-sm" data-task-id="{{ $task->id }}">
                        <i class="far fa-calendar-check"></i> Mark Completed</button>
                    @endif
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif

<style>
    .box-part {
        background: #FFF;
        border-radius: 15px;
        padding: 30px 10px;
        margin: 20px 0px;
    }

    .box-part:hover {
        background: #4183D7;
    }

    .box-part:hover .fa,
    .box-part:hover .title,
    .box-part:hover .text,
    .box-part:hover a {
        color: #FFF;
        -webkit-transition: all 1s ease-out;
        -moz-transition: all 1s ease-out;
        -o-transition: all 1s ease-out;
        transition: all 1s ease-out;
    }

    .text {
        margin: 10px 0px;
    }

    .input-group-addon {
        border-color: #4d81ea !important;
        background-color: #4d81ea !important;
    }

    .form-control:focus {
        border-color: #66afe9;
        outline: 0;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(102, 175, 233, 0.6);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(102, 175, 233, 0.6);
    }

    .btn .fa,
    .fab,
    a .fa-qrcode {
        font-size: 70px !important;
        margin-right: 0px !important;
    }

    .title {
        margin-top: 20px;
        color: #000;
        word-wrap: break-word;
    }

    .input-group-text {
        background-color: #3490dc !important;
        color: #FFF;
    }

    .card-body {
        padding: 0 !important;
    }
</style>
<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#one-click").change(function(e){
        e.preventDefault();
        let value = $('#one-click option:selected').val();
        $.ajax({
            method:'POST',
            url: '/home/direct_transfer',
            data:{
                action: value
            },
            success:function(data){
                console.log(data);
                if(data.success === 1){
                    $('.skip-info').text(data.message).show("slow").delay(5000).hide("slow");
                } else {
                    alert(data.message);
                }
            }
        });
    });
</script>

@endsection
