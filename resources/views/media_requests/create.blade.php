@extends('layouts.app')

@section('content')
<script type='text/javascript'>
    $(document).ready(function(){

        $('#media_type').change(function(){

            var id = $(this).val();

            // Empty the dropdown
            $('#media_id').find('option').not(':first').remove();
            $('#container_id').find('option').not(':first').remove();

            // AJAX request
            $.ajax({
                url: '/growthstocks/getmedias/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }

                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            if(response['data'][i].active == 1){
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#media_id").append(option);
                            }
                        }
                    }
                }
            });

            $.ajax({
                url: '../getcontainers/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }

                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var option = "<option value='"+id+"'>"+name+"</option>";
                            $("#container_id").append(option);
                        }
                    }
                }
            });
        });

    });

</script>
<div class="container">
    <div class="card">
        <div class="card-header">
            Add Media Request
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('media_requests.store') }}" class="form-prevent-multiple-submits">
                <div class="form-group">
                    <label for="name">Requested by:</label>
                    <strong>{{ auth()->user()->user_code.' - '.auth()->user()->name }}</strong>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="media_type">Media Type :</label>
                        <select class="form-control" name="media_type" id='media_type' required>
                            <option value="">-- Select Media Type --</option>
                            @foreach (config('app.media_types') as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="media_id">Media Name:</label>
                        <select class="form-control" id='media_id' name='media_id' required>
                            <option value="">-- Select Media --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="container_id">Container Name:</label>
                    <select class="form-control" id='container_id' name='container_id' required>
                        <option value="">-- Select Container --</option>
                    </select>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="media_amount">Media Amount: (ml)</label>
                        <select class="form-control" name="media_amount" id='media_amount' required>
                            <option value="">-- Select Amount --</option>
                            @foreach (config('app.media_amounts') as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="requested_quantity">Requested Quantity: (l)</label>
                        <input type="number" step=".01" class="form-control" name="requested_quantity" id="requested_quantity"
                               required value="{{ old('requested_quantity') }}" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="requested_date">Requested Date:</label>
                        <input type="date" class="form-control" id='requested_date' name="requested_date" required
                               value="{{ old('requested_date') }}" />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="need_date">Need Date:</label>
                        <input type="date" class="form-control" id='need_date' name="need_date"
                               value="{{ old('need_date') }}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="location">Location:</label>
                    <select multiple class="form-control" name="location[]" id='location' required>
                        @foreach (config('app.locations') as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary float-right btn-prevent-multiple-submits">Add Media Request</button>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>
<script src="{{ asset('js/submit.js') }}"></script>
@endsection
