@extends('layouts.app')

@section('content')
    <div class="upper">
        <div class="shadow-sm p-3 mb-2 bg-white rounded">
            <b class="h3">
                Media Requests
            </b>
            <a class="float-right btn btn-primary btn-sm" href="{{ route('media_requests.create') }}">Add Media Request</a>
        </div>
    </div>

    <div class="upper">
        <table class="table table-hover sticky-header">
            <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Media Type</th>
                    <th>Media</th>
                    <th>Container</th>
                    <th>Media Amount (ml)</th>
                    <th>Requested Quantity (l)</th>
                    <th class="text-center">Requested Date</th>
                    <th class="text-center">Need Date</th>
                    <th>Requested By</th>
                    <th>Location</th>
                    <th>Completed Quantity</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($media_requests as $mr)
                @php
                $bg = '';
                if($mr->confirmed){$bg = 'alert-success';}
				if($mr->cancelled_by){$bg = 'alert-warning';}
                @endphp
                <tr class="{{$bg}}">
                    <td>{{  $mr->id }}</td>
                    <td>{{ config('app.media_types')[$mr->media_type] }}</td>
                    <td>{{  $mr->media->name }}</td>
                    <td>{{  $mr->container->name }}</td>
                    <td>{{  $mr->media_amount }}</td>
                    <td>{{  $mr->requested_quantity }}</td>
                    <td>{{  $mr->requested_date }}</td>
                    <td>{{  $mr->need_date }}</td>
                    <td>{{  $mr->requestedBy->user_code }} - {{  $mr->requestedBy->name }}</td>
                    <td>{{  $mr->getLocationName($mr->location) }}</td>
                    <td>{{  $mr->completed_quantity }}</td>
                    <td>
                        <a href="{{ route('media_requests.show',$mr->id) }}" class="btn btn-success btn-sm" role="button">View</a>
                        @if($mr->confirmed)
                            <span class="text-primary">CONFIRMED</span>
                        @endif
                        @if($mr->cancelled_by)
                            <span class="text-danger">CANCELLED</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $media_requests->links() }}
    </div>
    <style>
        table.floatThead-table {
            border-top: none;
            border-bottom: none;
            background-color: #fff;
        }

        th {
            position: sticky;
            top: 50px;
        }
    </style>
@endsection
