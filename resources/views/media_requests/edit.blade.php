@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            #{{ $media_request->id }} Edit Media Request
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('media_requests.update', $media_request->id) }}" class="form-prevent-multiple-submits">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Requested by:</label>
                    <b>{{ $media_request->requestedBy->user_code }} - {{ $media_request->requestedBy->name }}</b>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="media_type">Media Type :</label>
                        <select class="form-control" name="media_type" id='media_type' required>
                            <option value="">-- Select Media Type --</option>
                            @foreach (config('app.media_types') as $key => $value)
                                <option value="{{ $key }}" {{ $key == $media_request->media_type ? 'selected' : 'disabled' }} >{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="media_id">Media Name:</label>
                        <select class="form-control" id='media_id' name='media_id' required>
                            <option value="">-- Select Media --</option>
                            @foreach ($medias as $m)
                                <option value="{{ $m->id }}" {{ $m->id == $media_request->media_id ? 'selected' : '' }}>
                                    {{ $m->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="container_id">Container Name:</label>
                    <select class="form-control" id='container_id' name='container_id' required>
                        <option value="">-- Select Container --</option>
                        @foreach ($containers as $c)
                            <option value="{{ $c->id }}" {{ $c->id == $media_request->container_id ? 'selected' : '' }}>
                                {{ $c->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="media_amount">Media Amount: (ml)</label>
                        <select class="form-control" name="media_amount" id='media_amount' required>
                            <option value="">-- Select Amount --</option>
                            @foreach (config('app.media_amounts') as $key => $value)
                                <option value="{{ $key }}" {{ $key == $media_request->media_amount ? 'selected' : '' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="requested_quantity">Requested Quantity: (l)</label>
                        <input type="number" step=".01" class="form-control" name="requested_quantity" id="requested_quantity"
                               required value="{{ $media_request->requested_quantity }}" {{ $readonly ?? ''  }}/>
                    </div>
                    @if ($media_request->requestedQuantityLog->isNotEmpty())
                        <div class="form-group col-md-12">
                            <p>Adjustment History:</p>
                            <ul class="list-group">
                                @foreach($media_request->requestedQuantityLog as $rql)
                                    <li class="list-group-item list-group-item-info">
                                        <span class="float-left">Quantity: {{ $rql->requested_quantity }} L</span>
                                        <span class="float-right">{{ $rql->created_at }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <input type="hidden" name="adjustment_history" id="adjustment_history" value="{{ $media_request->requested_quantity }}">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="requested_date">Requested Date:</label>
                        <input type="date" class="form-control" id='requested_date' name="requested_date" required
                               value="{{ $media_request->requested_date }}" {{ $readonly ?? '' }} />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="need_date">Need Date:</label>
                        <input type="date" class="form-control" id='need_date' name="need_date"
                               value="{{ $media_request->need_date }}" {{ $readonly ?? '' }}/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="location">Location:</label>
                    <select multiple class="form-control" name="location[]" id='location' required>
                        @foreach (config('app.locations') as $key => $value)
                            <option value="{{ $key }}"
                                {{ $media_request->hasLocation($key, $media_request->location) ? 'selected' : '' }}>
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="completed_quantity">Completed Quantity:</label>
                    <input type="number" class="form-control" name="completed_quantity" id="completed_quantity" value="{{ $media_request->completed_quantity }}"/>
                </div>
                <div class="form-group">
                    <label for="incomplete_reason">Incomplete Reason:</label>
                    <input type="text" class="form-control" id='incomplete_reason' name="incomplete_reason" value="{{ $media_request->incomplete_reason }}" />
                </div>
                <button type="submit" class="btn btn-primary float-right btn-prevent-multiple-submits">Update</button>
            </form>
        </div>
    </div>
</div>
@if ($readonly)
    <script>
        $( "select option:not(:selected)" ).each(function() {
             $( this ).attr('disabled',true);
        });
    </script>
@endif
<script src="{{ asset('js/submit.js') }}"></script>
@endsection
