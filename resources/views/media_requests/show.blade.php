@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="p-3 mb-2 bg-white rounded">
                    <b class="h3">#{{ $media_request->id }}</b>
                    @if (!$media_request->cancelled_by && !$media_request->confirmed)
                        <a class="float-right btn btn-secondary btn-sm" href="{{ route('media_requests.edit', $media_request->id) }}"><i class="far fa-edit"></i> Edit</a>
                    @endif
                </div>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Requested By</th>
                            <td>{{ $media_request->requestedBy->user_code }} - {{  $media_request->requestedBy->name }}</td>
                        </tr>
                        <tr>
                            <th>Media</th>
                            <td>{{ config('app.media_types')[$media_request->media_type] }} - {{ $media_request->media->name }}</td>
                        </tr>
                        <tr>
                            <th>Container</th>
                            <td>{{ $media_request->container->name }}</td>
                        </tr>
                        <tr>
                            <th>Media Amount</th>
                            <td>{{ $media_request->media_amount }} ml</td>
                        </tr>
                        <tr>
                            <th>Requested Quantity</th>
                            <td>{{ $media_request->requested_quantity }} L</td>
                        </tr>
                        <tr>
                            <th>Requested Date</th>
                            <td>{{ $media_request->requested_date }}</td>
                        </tr>
                        <tr>
                            <th>Need Date</th>
                            <td>{{ $media_request->need_date }}</td>
                        </tr>
                        <tr>
                            <th>Location</th>
                            <td>
                                {{ $media_request->getLocationName($media_request->location) }}
                            </td>
                        </tr>
                        @if ($media_request->completed_quantity)
                            <tr>
                                <th>Completed Quantity</th>
                                <td>{{ $media_request->completed_quantity }}</td>
                            </tr>
                            <tr>
                                <th>Available Containers</th>
                                <td>{{ intval(($media_request->completed_quantity * 1000) / $media_request->media_amount) }}</td>
                            </tr>
                            <tr>
                        @endif
                            <th>Incomplete Reason</th>
                            <td>{{ $media_request->incomplete_reason ?? '-' }}</td>
                        </tr>
                        @if ($media_request->user_id)
                            <tr>
                                <th>Edited By</th>
                                <td>{{ $media_request->user->name }}</td>
                            </tr>
                        @endif
                        @if ($media_request->confirmed)
                            <tr>
                                <th>Confirmed on</th>
                                <td>
                                    {{ $media_request->confirmed }}
                                    @if ($media_request->ref_no !== null)
                                        <span class="float-right">Batch No: {{ $media_request->getBatchNo($media_request->ref_no) }}</span>
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if ($media_request->cancelled_by)
                            <tr>
                                <th>Cancelled By</th>
                                <td>{{ $media_request->cancelledBy->name }} on {{ $media_request->updated_at }}</td>
                            </tr>
                        @endif
                        <tr>
                            <th>Created at</th>
                            <td>{{ $media_request->created_at }}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-6">
                        @if ($media_request->completed_quantity && !$media_request->confirmed && !$media_request->cancelled_by)
                            <form action="{{ route('mediastocks.store')}}" method="post" class="form-prevent-multiple-submits">
                                @csrf
                                @method('POST')
                                <input type="hidden" id="media_request" name="media_request" value="{{ $media_request->id }}">
                                <input type="hidden" id="user_id" name="user_id" value="{{ auth()->user()->id }}">
                                <input type="hidden" id="operator" name="operator" value="{{ $media_request->requestedBy->name }}">
                                <input type="hidden" id="operator_id" name="operator_id" value="{{ $media_request->requested_by }}">
                                <input type="hidden" id="sel_type" name="sel_type" value="{{ $media_request->media_type }}">
                                <input type="hidden" id="sel_media" name="sel_media" value="{{ $media_request->media_id }}">
                                <input type="hidden" id="sel_container" name="sel_container" value="{{ $media_request->container_id }}">
                                {{--
                                BY Karin
                                The available container will get from =(Completed qty * 1000 / Entered Media amount (ml))
                                Ex: (10L * 1000 / 30ml) = 333 bottles (No decimal number)
                                --}}
                                <input type="hidden" id="container_qty" name="container_qty" value="{{ intval(($media_request->completed_quantity * 1000) / $media_request->media_amount) }}">
                                <button class="float-left btn btn-success btn-lg btn-prevent-multiple-submits" onclick="return confirm('Are you sure you wont to confirm?')"
                                        type="submit"><i class="fas fa-check-circle"></i> Confirm</button>
                            </form>
                        @endif
                    </div>
                    <div class="col-6">
                        @if (!$media_request->confirmed && !$media_request->cancelled_by)
                            <a class="btn btn-info float-right btn-sm" href="{{ route('media_requests.cancel', $media_request) }}"><i class="far fa-window-close"></i> Cancel</a>
                        @endif
                        @if ($media_request->cancelled_by && Auth::user()->hasAnyRole(['Administrator']))
                            <a class="btn btn-outline-info float-right btn-sm" href="{{ route('media_requests.revert', $media_request) }}"><i class="fas fa-external-link-alt"></i> Revert Cancel</a>
                        @endif
                        @if(Auth::user()->isDeveloper())
                            <div class="row">
                                <div class="col-12">
                                    <form action="{{ route('media_requests.destroy', $media_request->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger float-right btn-sm" onclick="return confirm('Are you sure?')"
                                                type="submit"><i class="far fa-trash-alt"></i> Delete</button>
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if ($media_request->requestedQuantityLog->isNotEmpty())
            <div class="card mt-2">
                <div class="card-body">
                    <p>Adjustment History</p>
                    <ul class="list-group">
                        @foreach($media_request->requestedQuantityLog as $rql)
                            <li class="list-group-item">
                                <span class="float-left">Quantity: {{ $rql->requested_quantity }} L</span>
                                <span class="float-right">{{ $rql->created_at }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
    <script src="{{ asset('js/submit.js') }}"></script>
@endsection
