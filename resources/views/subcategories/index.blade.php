@extends('layouts.app')

@section('title', 'Sub Category')

@section('content')
<h3>
    List of Sub categories
    <button type="button" class="btn btn-secondary ml-5" data-toggle="modal" data-target="#addContainerModel">
        Add New Sub Category
    </button>
</h3>
<div class="col-xs-12 filters">
    <a class="btn btn-sm btn-outline-success" href="/subcategories">Reset list</a>
    <a class="btn btn-sm btn-outline-info" href="/subcategories?select_inactive=true">Show Inactive</a>
</div>
<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Main Category</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($subcategories as $subcategory)
        <tr class="{{ $subcategory->active == 0 ? 'text-danger' : '' }}">
            <td>{{ $subcategory->id }}</td>
            <td>{{ $subcategory->subcat }}</td>
            <td>
                <a href="/categories/{{ $subcategory->category->id }}">{{ $subcategory->category->name }}</a>
                <label class="text-danger">{{ !$subcategory->category->active ? ' (Inactive)' : ''}}</label>
            </td>
            <td>
                <a href="/subcategories/{{ $subcategory->id}}" class="btn btn-secondary">View</a>
                <a href="/subcategories/{{ $subcategory->id}}/edit" class="btn btn-primary">Edit</a>
                {{--<a href="#" class="btn btn-danger" onclick="--}}
                    {{--var result = confirm('Are you sure you wish to DELETE {{ $subcategory->name }}?');--}}
                    {{--if (result){--}}
                        {{--event.preventDefault();--}}
                        {{--$('#delete-{{ $subcategory->id }}').submit();--}}
                    {{--}">Delete</a>--}}
                {{--<form id="delete-{{ $subcategory->id }}" action="{{ route('subcategories.destroy', $subcategory->id)}}"--}}
                    {{--method="POST" style="display:none">--}}
                    {{--@csrf--}}
                    {{--@method('DELETE')--}}
                    {{--<input value="delete" name="_method" type="hidden">--}}
                {{--</form>--}}
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

<div class="modal fade" id="addContainerModel" tabindex="-1" role="dialog"
     aria-labelledby="addContainerModelTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('subcategories.store') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="addContainerModelLongTitle">Add New Sub Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Main Category:</label>
                        <select class="form-control" name="cat_id" id="cat_id" required>
                            <option value="">-- Select Category --</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="sub_category_name" required placeholder="Sub Category Name" />
                    </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
