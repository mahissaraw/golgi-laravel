@extends('layouts.app')

@section('content')
<div class="list-group">
    <a href="#" class="list-group-item list-group-item-dark">Sub Category: {{ $sub_category->subcat }}</a>
    @foreach ($sub_category->items as $item)
    <a href="#" class="list-group-item list-group-item-action">
        {{ $item->item_code }} - {{ $item->item_name }}
        <span class="float-right">{{ $item->active == 1 ? 'Active' : 'Inactive' }}</span>
    </a>
    @endforeach
</div>

@endsection
