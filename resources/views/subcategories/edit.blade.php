@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Edit Media
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <form method="post" action="{{ route('subcategories.update', $subCategory->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Category Name:</label>
                    <select class="form-control" name="category_name" id="cat_id" required>
                        <option value="">-- Select Category --</option>
                        @foreach ($categories as $category)
                            <option
                                {{ $category->id === $subCategory->cat_id ? 'selected': ''}} value="{{ $category->id }}" {{ !$category->active ? 'disabled' : ''}}>
                                {{ $category->name }}
                            </option>
                        @endforeach
                    </select>

                </div>

                <div class="form-group">
                    <label for="name">Sub Category Name:</label>

                    <input type="text" class="form-control" name="subCategory_name" value="{{ $subCategory->subcat }}"/>
                </div>

                <div class="form-group">
                    <label for="active">Status</label>
                    <select class="form-control" id="active" name="active">
                        <option value="1" {{ $subCategory->active == 1 ? 'selected' : '' }}>Active</option>
                        <option value="0" {{ $subCategory->active == 0 ? 'selected' : '' }}>Inactive</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
