@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Share
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('frequencies.store') }}">
          <div class="form-group">
              {{ csrf_field() }}
              <label for="name">Frequency:</label>
              <input type="text" class="form-control" name="frequency" required/>
          </div>
          <div class="form-group">
              <label for="name">Data:</label>
              <input type="text" class="form-control" name="data" />
          </div>
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@endsection
