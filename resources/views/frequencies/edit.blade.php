@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Edit Frequency
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('frequencies.update', $frequency->id) }}">
                <div class="form-group">
                    @csrf
                    @method('PATCH')
                    <label for="name">Frequency:</label>
                    <input type="text" class="form-control" name="frequency" value="{{ $frequency->frequency }}"
                        required />
                </div>
                <div class="form-group">
                    <label for="name">Data:</label>
                    <input type="text" class="form-control" name="data" value="{{ $frequency->data }}" />
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
</div>

@endsection