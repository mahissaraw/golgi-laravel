@extends('layouts.app')

@section('content')
<div id="add_new" style="display:none">
    <h3 class="display-5">
        Create Frequency
    </h3>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('frequencies.store') }}">
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Frequency:</label>
                <input type="text" class="form-control" name="frequency" required />
            </div>
            <div class="form-group">
                <label for="name">Data:</label>
                <input type="text" class="form-control" name="data" />
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
    <hr />
</div>
<h3>
    List of Frequencies
    <a href="#" class="btn btn-secondary ml-5 add_frequency">Create Frequency</a>
</h3>
<div class="uper">
    <table class="table table-striped">
        <thead>
            <tr>
                <td>ID</td>
                <td>Frequency</td>
                <td>Data</td>
                <td>Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach($frequencies as $frequency)
            <tr>
                <td>{{$frequency->id}}</td>
                <td>{{$frequency->frequency}}</td>
                <td>{{$frequency->data}}</td>
                <td>
                    <div class="btn-group" role="group">
                        <a href="{{ route('frequencies.edit',$frequency->id)}}" class="btn btn-primary mr-1">Edit</a>
                        <form action="{{ route('frequencies.destroy', $frequency->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div>
        <script>
            $(document).ready(function(){
            $(".add_frequency").click(function(){
                $("#add_new").toggle();
            });
        });
        </script>

        @endsection