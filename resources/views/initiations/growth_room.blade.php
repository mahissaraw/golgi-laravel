@extends('layouts.app')

@section('content')
<script src="{{ asset('js/bootstrap3-typeahead.min.js') }}"></script>
<div>
    <div class="row">
        <h2 class="col-md-5 col-sm-8">Initiation Growth Room</h2>
        <form action="#" method="get" class="col-md-3 offset-md-4 col-sm-4">
            @method('GET')
            @csrf
            <div class="input-group mb-3 float-right">
                <input type="text" name="search_item_name" id="search_item_name"
                    class="form-control typeahead border-primary" placeholder="Search Item Name"
                    aria-label="Search Item Name" aria-describedby="basic-addon2" autocomplete="off">
                <div class="input-group-append">
                    <button class="btn btn-outline-primary" type="submit" id="search_item_name_btn"><i
                            class="fas fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <td>Serial No</td>
                <td>Bulk No</td>
                <td>Media Name</td>
                <td>Container Name</td>
                <td>Item Name</td>
                <td>Created Date</td>
                <td colspan="2">Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach($growthstocks as $growthstock)
            <tr>
                <td>{{$growthstock->serial_no}}</td>
                <td>{{$growthstock->bulk_number}}</td>
                <td>{{$growthstock->mediaVariety->name}}</td>
                <td>{{$growthstock->containerVariety->name}}</td>
                <td>{{$growthstock->itemVariety->item_name}}</td>
                <td>[{{$growthstock->getWeekAttribute()}}] {{substr($growthstock->date,0,10)}}</td>
                <td>
                    <form action="{{ route('home.scanqrcode')}}" method="post">
                        @csrf
                        @method('POST')
                        <input id="s_no" name="s_no" type="hidden" value="{{$growthstock->serial_no}}">
                        <button class="btn btn-success" type="submit">View</button>
                    </form>
                </td>
            </tr>
            {{--@endif--}}
            @endforeach
        </tbody>
    </table>
    {{ $growthstocks->onEachSide(1)->links() }}

    <script type="text/javascript">
        var path = "{{ route('item.autocomplete') }}";
            $('input.typeahead').typeahead({
                source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                        return process(data);
                    });
                }
            });
    </script>
    <div>
@endsection
