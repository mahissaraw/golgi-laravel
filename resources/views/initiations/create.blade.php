@extends('layouts.app')

@section('content')
<script type='text/javascript'>
    $(document).ready(function(){

        $('#sel_type').change(function(){

            // get the media type 1 or 2 from the media type select box
            var id = $(this).val();

            // Empty the dropdown
            $('#sel_media').find('option').not(':first').remove();
            $('#sel_container').find('option').not(':first').remove();

            // get medias according to media type
            $.ajax({
                url: '/growthstocks/getmedias/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }

                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            if(response['data'][i].active == 1){
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#sel_media").append(option);
                            }
                        }
                    }
                }
            });


        });


        $('#sel_media').change(function () {

            // get media type and media stock id from selection boxes
            var id = $(this).val();
            var mtype = $('#sel_type').val();

            $('#sel_container').find('option').not(':first').remove();
            // get containers according to media type
            $.ajax({
                url: '/growthstocks/getcontainersbymedia/'+mtype+'/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }

                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var option = "<option value='"+id+"'>"+name+"</option>";
                            $("#sel_container").append(option);
                        }
                    }
                }
            });
        })

        $('#sel_container').change(function () {

            // get media type and media stock id from selection boxes
            var id = $(this).val();
            var mtype = $('#sel_type').val();
            var media = $('#sel_media').val();

            $('#sel_container_batch').find('option').not(':first').remove();
            $('#con_qty').find('option').not(':first').remove();
            // get containers according to media type
            $.ajax({
                url: '/growthstocks/getcontainerbatches/'+mtype+'/'+id+'/'+media,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }

                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var qty = response['data'][i].qty;
                            var option = "<option value='"+id+"'>Batch No. "+id+" -> Available Quantity: "+qty+"</option>";
                            $("#sel_container_batch").append(option);
                        }
                    }
                }
            });
        })
        // insert available container quantity to hidden field
        $("#sel_container_batch").change(function() {
            var selectedText = $("#sel_container_batch option:selected").html();
            var result = selectedText.split(':');
            document.getElementById("con_qty").value = $.trim(result[1]);
        });

        // sub categories
        $("#cat_id").change(function () {

            var id = $(this).val();

            $('#sel_sub_cat').find('option').not(':first').remove();

            $.ajax({
                url: '/growthstocks/getsubcategories/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }

                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var subcat = response['data'][i].subcat;
                            var option = "<option value='"+id+"'>"+subcat+"</option>";
                            $("#sel_sub_cat").append(option);
                        }
                    }
                }
            });
        })

        // items
        $("#sel_sub_cat").change(function () {

            var id = $(this).val();

            $('#sel_item').find('option').not(':first').remove();

            $.ajax({
                url: '/growthstocks/getitems/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }

                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var item_name = response['data'][i].item_name;
                            //if(response['data'][i].active == 1){
                                var option = "<option value='"+id+"'>"+item_name+"</option>";
                                $("#sel_item").append(option);
                            //}

                        }
                    }
                }
            });
        })

        // Plants per Container validation
        $("#plant_qty").keyup(function () {
            $("#con_out_stock").empty();
            var plant = $(this);
            var container_qty = $("#container_qty").val();

            if (container_qty <= 0){
                $("#con_out_stock").append("<p class='text-danger'>Please Enter Container Quantity</p>");
                plant.val("");
                return false;
            }

            if (plant.val() > 0){
                $('#no_of_plants').attr('value', plant.val() * container_qty);
            } else {
                $("#con_out_stock").append("<p class='text-danger'>Plants per Container Need to be more than ZERO</p>");
                plant.val("");
                $("#no_of_plants").val("");
                return false;
            }
        })

        // no of plants validation
        $("#container_qty").keyup(function () {

            $("#con_out_stock").empty();

            var qty = $(this).val();
            var container_id = $('#sel_container').val();
            var container_batch_id = $('#sel_container_batch').val();
            var container_quantity = $('#con_qty').val();
            var plant = $("#plant_qty");

            if (plant.val() > 0){
                $('#no_of_plants').attr('value', plant.val() * qty);
            }

            // if (qty < 1){
            //     $("#con_out_stock").append("<p class='text-danger'>Container Negative values not allowed!!</p>");
            //     $(this).val('');
            //     return false;
            // }
            //
            // $.ajax({
            //     url: '/growthstocks/getplants/'+container_id+'/'+container_batch_id+'/'+container_quantity,
            //     type: 'get',
            //     dataType: 'json',
            //     success: function(response){
            //
            //         if(response['data'] != null){
            //             var val = JSON.stringify(response['data']);
            //         }
            //
            //         if(val > 0){
            //             if (parseInt(qty) <= parseInt(container_quantity)) {
            //                 $("#con_out_stock").empty();
            //                 $('#no_of_plants').attr('value', val*qty);
            //
            //             } else if (qty == "") {
            //                 $("#con_out_stock").empty();
            //                 $('#no_of_plants').attr('value', 0);
            //             } else {
            //                 $("#con_out_stock").append("<p class='text-danger'>Out of stock. Your quantity must be "+container_quantity+" or less</p>");
            //                 $('#no_of_plants').attr('value', 0);
            //                 $('#container_qty').val('');
            //             }
            //         }
            //     }
            // });
        })
    });

</script>
<script src="{{ asset('js/submit.js') }}"></script>
<script>
    $(document).ready(function () {
        $(function () {
            $('#date').duDatepicker({format: 'yyyy-mm-dd', theme: 'teal', auto: true});
        });
    });
</script>
<div class="card">
    <div class="card-header">
        Add Initiation Stock
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('initiations.store') }}" class="form-prevent-multiple-submits">

            <div class="row">
                <div class="form-group ml-3">
                    <label for="media_type">Section :</label><br />
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="section" id="section" checked value="Initiation"
                               required>
                        <h4 class="form-check-label">Initiation</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-12">

                    <div class="form-group">
                        <label for="media_type">Customer :</label><br />
                        <select class="form-control" name="customer_id" id="customer_id" required>
                            <option value="">Select Customer</option>
                            @foreach ($customers as $key => $value)
                                <option value="{{ $value->id }}">
                                    {{ $value->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Operator Name:</label>
                        <select class="form-control" name="user_id" id="user_id" required>
                            <option value="">Select User</option>
                            @foreach ($users as $key => $value)
                            <option value="{{ $key }}">
                                {{ $value }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="price">Media Type :</label>
                        <select class="form-control" name="sel_type" id='sel_type' required>
                            <option value="">-- Select Media Type --</option>;
                            <option value="1">Multiplication Media</option>
                            <option value="2">Rooting Media</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Media Name:</label>
                        <select class="form-control" id='sel_media' name='sel_media' required>
                            <option value=''>-- Select Media --</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Container Name:</label>
                        <select class="form-control" id='sel_container' name='sel_container' required>
                            <option value=''>-- Select Container --</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Available Container Batches:</label>
                        <select class="form-control" id='sel_container_batch' name='sel_container_batch' required>
                            <option value=''>-- Select Batch --</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                        <label for="name">Date:</label>
                        <input type="text" class="form-control" name="date" id="date" required
                               autocomplete=“off” value="{{date("Y-m-d")}}" data-maxdate="today" data-mindate="2020-12-31"/>
                    </div>
                    <div class="row">
                        <div class="col-12" id="con_out_stock"></div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Container Quantity:</label>
                                <input type="number" class="form-control" name="container_qty" id="container_qty" required
                                       min="1" />
                                <div id="con_out_stock"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Plants per Container:</label>
                                <input type="number" class="form-control" name="plant_qty" id="plant_qty" required
                                       min="1" />
                                <div id="con_out_stock"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">No. of Plants:</label>
                        <input type="number" class="form-control" id='no_of_plants' name="no_of_plants" readonly
                               required />
                    </div>
                    <div class="form-group">
                        <label for="name">Main Item Category:</label>
                        <select class="form-control" name="cat_id" id="cat_id" required>
                            <option value="">-- Select Category --</option>
                            @foreach ($categories as $key => $value)
                            <option value="{{ $value->id }}">
                                {{ $value->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Sub Item Category:</label>
                        <select class="form-control" id='sel_sub_cat' name='sel_sub_cat' required>
                            <option value=''>-- Select Sub Category --</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Item:</label>
                        <select class="form-control" id='sel_item' name='sel_item' required>
                            <option value=''>-- Select Item --</option>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-prevent-multiple-submits float-right">Add Initiation Stock</button>
            <input type="hidden" id="con_qty" name="con_qty" value="" />
            {{ csrf_field() }}
        </form>
    </div>
</div>
@endsection
