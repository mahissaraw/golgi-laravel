@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="{{ URL::asset('js/charts/loader.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                // $('#date_from').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, rangeTo: '#date_to'});
                // $('#date_to').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, rangeFrom: '#date_from'});
                $('#datepicker').duDatepicker({format: 'yyyy-mm-dd', theme: 'teal', auto: true, cancelBtn: true});
            });
        });
    </script>
    <div class="upper">
        <div class="shadow-sm p-3 mb-2 bg-white rounded">
            <form method="post" action="{{ route('visualization.filter') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <b class="h3">
                            Data Visualization {{ $current_date }}
                        </b>
                    </div>
                    <div class="col-lg-5 col-md-12">
                        <div class="input-group">
                            <input type="text" class="form-control" name="datepicker" id="datepicker" required
                                   placeholder="Pick a Date" autocomplete=“off” data-maxdate="today"/>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-filter"></i> Filter
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawPieChart);
        google.charts.setOnLoadCallback(plantComboChart);
        google.charts.setOnLoadCallback(employeeComboChart);

        function drawPieChart() {

            var data = google.visualization.arrayToDataTable([
                ['Task', 'Amount'],
                ['Cut Plants',     {{$performance_piechart[0]->total_quantity}}],
                ['Rejected Plants',      {{$performance_piechart[0]->total_rejected_quantity}}]
            ]);

            let options = {
                title: 'Daily Activities',
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);



        }

        function plantComboChart() {

            var data = google.visualization.arrayToDataTable(
                @php
                    echo json_encode($plant_combo);
                @endphp
            );

            var options = {
                title: 'Plant Production',
                vAxis: {title: 'Amount'},
                hAxis: {title: 'Plant'},
                seriesType: 'bars',
            };

            var chart = new google.visualization.ComboChart(document.getElementById('plant_combo_chart'));
            chart.draw(data, options);
        }

        function employeeComboChart() {

            var data = google.visualization.arrayToDataTable(
                @php
                    echo json_encode($employee_combo);
                @endphp
            );

            var options = {
                title: 'Employee Performance',
                vAxis: {title: 'Amount'},
                hAxis: {title: 'Employee'},
                seriesType: 'bars',
            };

            var chart = new google.visualization.ComboChart(document.getElementById('employee_combo_chart'));
            chart.draw(data, options);
        }

        $(window).resize(function(){
            drawPieChart();
            plantComboChart();
            employeeComboChart();
        });
    </script>
    <div class="container">
        <div class="row mb-1">
            <div id="piechart" class="chart"></div>
        </div>
        <div class="row mb-1">
            <div id="plant_combo_chart" class="chart"></div>
        </div>
        <div class="row mb-1">
            <div id="employee_combo_chart" class="chart"></div>
        </div>

    </div>
    <style>
        .chart {
            width: 100%;
            min-height: 450px;
            border: 1px solid #ccc;
        }
    </style>
@endsection
