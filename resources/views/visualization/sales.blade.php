@extends('layouts.app')

@section('content')
{{--    https://www.daterangepicker.com/--}}
    <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ URL::asset('js/charts/loader.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/daterangepicker/daterangepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            let start = moment().subtract(1, 'month').startOf('month');
            let end = moment().subtract(1, 'month').endOf('month');

            //Set the initial state of the picker label
            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            $('#reportrange').daterangepicker(
                {
                    startDate: start,
                    endDate: end,
                    minDate: '01/01/2021',
                    maxDate: moment(),
                    dateLimit: { days: 365 },
                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    opens: 'center',
                    buttonClasses: ['btn btn-default'],
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small',
                    format: 'MM/DD/YYYY',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'Submit',
                        fromLabel: 'From',
                        toLabel: 'To',
                        customRangeLabel: 'Custom Range',
                        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        firstDay: 1
                    }
                },
                function(start, end) {
                    console.log("Callback has been called!");
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    $('input[name="fromDate"]').val(start.format('YYYY-MM-DD'));
                    $('input[name="toDate"]').val(end.format('YYYY-MM-DD'));
                }
            );
            cb(start, end);
            $('input[name="fromDate"]').val(start.format('YYYY-MM-DD'));
            $('input[name="toDate"]').val(end.format('YYYY-MM-DD'));
        });
    </script>

    <div class="upper">
        <div class="shadow-sm p-3 mb-2 bg-white rounded">
            <form method="post" action="{{ route('visualization.sales.filter') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <b class="h3">
                            Sales Data Visualization {{ substr($from, 0,10) }} - {{ substr($to,0,10) }}
                        </b>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <label for="reportrange">Select Date Range with Option</label>
                        <div class="input-group">
                            <div id="reportrange" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar icon-calendar icon-large"></i>
                                <span></span> <b class="caret"></b>
                            </div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-calendar"></i> Filter</button>
                            </div>
                            <input type="hidden" id="fromDate" name="fromDate" value="">
                            <input type="hidden" id="toDate" name="toDate" value="">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawSalesPieChart);
        google.charts.setOnLoadCallback(drawPlantPieChart);

        function drawSalesPieChart() {

            var data = google.visualization.arrayToDataTable([
                ['Name', 'Amount'],
                @foreach($sales_pie_chart as $sales)
                ['({{$sales->id}}) {{$sales->name}}', {{$sales->amount_total}}],
                @endforeach
            ]);

            // var formatter = new google.visualization.NumberFormat(
            //     {prefix: 'Rs '});
            // formatter.format(data, 1);

            @php
                /** @var object $sales_pie_chart */
                $sum = 0;
                foreach($sales_pie_chart as $sales) {
                    $sum += $sales->amount_total;
                }
            @endphp

            let options = {
                title: 'Market Cap Rs:{{number_format($sum,2)}}',
            };

            var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));

            chart.draw(data, options);
        }

        function drawPlantPieChart() {

            var data = google.visualization.arrayToDataTable([
                ['Plant Name', 'Quantity'],
                    @foreach($sales_plant_pie_chart as $plant)
                ['{{$plant->subcat}} - {{$plant->item_name}} ({{$plant->item_code}})', {{$plant->quantity_total}}],
                @endforeach
            ]);

            @php
                /** @var object $sales_plant_pie_chart */
                $plant_sum = 0;
                foreach($sales_plant_pie_chart as $plant) {
                    $plant_sum += $plant->quantity_total;
                }
            @endphp
            let options = {
                title: 'Total Plants: {{$plant_sum}}',
            };

            var chart = new google.visualization.PieChart(document.getElementById('plant_pie_chart'));

            chart.draw(data, options);
        }

        $(window).resize(function(){
            drawSalesPieChart();
            drawPlantPieChart();
        });
    </script>


        <div class="row mb-1">
            <div class="col-md-12 col-lg-6">
                <legend>Best 10 Customers :</legend>
                <div id="pie_chart" class="chart"></div>
            </div>
            <div class="col-md-12 col-lg-6">
                <legend>Best 10 Plants :</legend>
                <div id="plant_pie_chart" class="chart"></div>
            </div>
        </div>
        <div class="row mb-1">

        </div>

    <style>
        .chart {
            width: 99%;
            min-height: 500px;
            border: 1px solid #ccc;
        }
    </style>
@endsection
