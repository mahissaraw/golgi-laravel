@extends('layouts.app')

@section('content')
    <div>
        <div class="row">
            <h2 class="col-md-5 col-sm-8">Production Growth Room</h2>
            @include('partials.plant_search')
        </div>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Serial No</th>
                <th>Bulk No</th>
                <th>Media Name</th>
                <th>Container Name</th>
                <th>Item Name</th>
                <th>Item Barcode</th>
                <th>Status</th>
                <th>Created Date</th>
                <td colspan="2">Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($growthstocks as $growthstock)
                {{--@if ($mediastock->qty > 0)--}}
                <tr>
                    <td>{{$growthstock->serial_no}}</td>
                    <td>{{$growthstock->bulk_number}}</td>
                    <td>{{$growthstock->mediaVariety->name}}</td>
                    <td>{{$growthstock->containerVariety->name}}</td>
                    <td>{{$growthstock->itemVariety->item_name}}</td>
                    <td>{{$growthstock->itemVariety->bar_code}}</td>
                    <td>{{$growthstock->status}}</td>
                    <td>[{{$growthstock->getWeekAttribute()}}] {{$growthstock->date}}</td>
                    <td>
                        <form action="{{ route('home.scanqrcode')}}" method="post">
                            @csrf
                            @method('POST')
                            <input id="s_no" name="s_no" type="hidden" value="{{$growthstock->serial_no}}">
                            <button class="btn btn-success" type="submit">View</button>
                        </form>
                    </td>
                </tr>
                {{--@endif--}}
            @endforeach
            </tbody>
        </table>
        {{ $growthstocks->onEachSide(1)->links() }}
        <div>
@endsection
