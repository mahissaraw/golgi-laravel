@extends('layouts.app')

@section('content')
<script type='text/javascript'>
    $(document).ready(function(){

        $('#sel_type').change(function(){

            // get the media type 1 or 2 from the media type select box
            var id = $(this).val();

            // Empty the dropdown
            $('#sel_media').find('option').not(':first').remove();
            $('#sel_container').find('option').not(':first').remove();

            // get medias according to media type
            $.ajax({
                url: 'getmedias/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    $.each( response, function( key, value ) {
                        $.each( value, function( data_key , data ) {
                            let option = "<option value='"+data.id+"'>"+data.name+"</option>";
                            $("#sel_media").append(option);
                        });
                    });
                }
            });

        });


        $('#sel_media').change(function () {

            // get media type and media stock id from selection boxes
            var id = $(this).val();
            var mtype = $('#sel_type').val();

            $('#sel_container').find('option').not(':first').remove();
            // get containers according to media type
            $.ajax({
                url: 'getcontainersbymedia/'+mtype+'/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    $.each( response, function( key, value ) {
                        $.each( value, function( data_key , data ) {
                            let option = "<option value='"+data.id+"'>"+data.name+"</option>";
                            $("#sel_container").append(option);
                        });
                    });
                }
            });
        })

        $('#sel_container').change(function () {

            // get media type and media stock id from selection boxes
            var id = $(this).val();
            var mtype = $('#sel_type').val();
            var media = $('#sel_media').val();

            if($('#plant_qty').val()){
                $('#total_plants').val('');
                $('#container_qty').val('');
                $('#plant_qty').val('');
            }

            $('#sel_container_batch').find('option').not(':first').remove();
            $('#con_qty').find('option').not(':first').remove();
            // get containers according to media type
            $.ajax({
                url: 'getcontainerbatches/'+mtype+'/'+id+'/'+media,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    $.each( response, function( key, value ) {
                        $.each( value, function( data_key , data ) {
                            // console.log(data);
                            let option = "<option value='"+data.id+"' data-quantity='"+data.qty+"'>Batch: "+data.id+"-> Quantity: "+data.qty+" ["+data.operator+"]</option>";
                            $("#sel_container_batch").append(option);
                        });
                    });

                }
            });
        })
        // insert available container quantity to hidden field
        $("#sel_container_batch").change(function() {
            $("#con_qty").val($("#sel_container_batch option:selected").attr('data-quantity'));
        });

        // sub categories
        $("#cat_id").change(function () {

            var id = $(this).val();

            $('#sel_sub_cat').find('option').not(':first').remove();

            $.ajax({
                url: 'getsubcategories/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    $.each( response, function( key, value ) {
                        $.each( value, function( data_key , data ) {
                            let option = "<option value='"+data.id+"'>"+data.subcat+"</option>";
                            $("#sel_sub_cat").append(option);
                        });
                    });
                }
            });
        })

        // items
        $("#sel_sub_cat").change(function () {

            var id = $(this).val();

            $('#sel_item').find('option').not(':first').remove();

            $.ajax({
                url: 'getitems/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    $.each( response, function( key, value ) {
                        $.each( value, function( data_key , data ) {
                            let option = "<option value='"+data.id+"'>"+data.item_name+" ("+data.bar_code+")</option>";
                            $("#sel_item").append(option);
                        });
                    });
                }
            });
        })

        // no of plants validation
        $("#container_qty").bind('keyup mouseup', function () {

            $("#con_out_stock").empty().addClass('d-none');
            $('#total_plants').val('');

            var qty = $(this).val();
            var container_id = $('#sel_container').val();
            var container_batch_id = $('#sel_container_batch').val();
            var container_quantity = $('#con_qty').val();

            console.log(container_quantity);

            if (qty < 1){
                $("#con_out_stock").html("Container Quantity MUST be greater than 0").removeClass('d-none');
                $(this).val('');
                return false;
            }

            $.ajax({
                url: 'getplants/'+container_id+'/'+container_batch_id+'/'+container_quantity,
                type: 'get',
                dataType: 'json',
                success: function(response){

                    if(response['data'] != null){
                        var plant_qty = JSON.stringify(response['data']);
                    }

                    if($('#plant_qty').val()){
                        plant_qty = $('#plant_qty').val();
                    }

                    if(parseInt(plant_qty) > 0){
                        if (parseInt(qty) <= parseInt(container_quantity)) {
                            $("#con_out_stock").empty().addClass('d-none');
                            $("#plant_out_stock").empty().addClass('d-none');
                            $('#plant_qty').val(parseInt(plant_qty));
                            $('#total_plants').val(parseInt(plant_qty) * parseInt(qty));

                        } else if (qty === "") {
                            $("#con_out_stock").empty().addClass('d-none');
                            $('#total_plants').val('');
                            $('#plant_qty').val('');
                        } else {
                            $("#con_out_stock").html("Out of stock. Your quantity must be "+container_quantity+" or less").removeClass('d-none');
                            $('#total_plants').val('');
                            $('#container_qty').val('');
                        }
                    }
                }
            });
        });
        //
        $("#plant_qty").bind('keyup mouseup', function () {
            let plant_qty = parseInt($(this).val());
            let container_qty = parseInt($('#container_qty').val());

            if (plant_qty > 0){
                $("#plant_out_stock").empty().addClass('d-none');
                $('#total_plants').val( plant_qty * container_qty);
            } else {
                $("#plant_out_stock").html("Plants per Container MUST be greater than 0").removeClass('d-none');
                $('#total_plants').val('');
            }
        });

        $( ".reset-plant-size" ).click(function(e) {
            e.preventDefault();
            $('input[name="plant_size"]').prop('checked', false);
        });
    });

</script>
<script src="{{ asset('js/submit.js') }}"></script>
<div class="card">
    <div class="card-header">
        Add Growth Stock
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('growthstocks.store') }}" class="form-prevent-multiple-submits">

            <div class="row">
                <div class="col-6">
                    <div class="form-group ml-3">
                        <label for="media_type">Section :</label><br />
                        @foreach (config('app.sections') as $key => $value)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="section" id="section" value="{{ $value }}"
                                       required>
                                <h5 class="form-check-label">{{ $value }}</h5>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="media_type">Plant Size :</label><br />
                        @foreach (config('app.plant_sizes') as $key => $value)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="plant_size" id="plant_size" value="{{ $key }}">
                                <h5 class="form-check-label">{{ $value }}</h5>
                            </div>
                        @endforeach
                        <button class="btn btn-sm reset-plant-size btn-outline-success"><i class="fas fa-undo"></i></button>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                        <label for="media_type">Cooperate Customer :</label><br />
                        <select class="form-control" name="customer_id" id="customer_id" required>
                            <option value="">Select Customer</option>
                            @foreach ($customers as $key => $value)
                            <option value="{{ $value->id }}">
                                {{ $value->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Operator Name:</label>
                        <select class="form-control" name="user_id" id="user_id" required>
                            <option value="">Select User</option>
                            @foreach ($users as $key => $value)
                            <option value="{{ $key }}">
                                {{ $value }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="price">Media Type :</label>
                                <select class="form-control" name="sel_type" id='sel_type' required>
                                    <option value="">-- Select Media Type --</option>;
                                    <option value="1">Multiplication Media</option>
                                    <option value="2">Rooting Media</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Media Name:</label>
                                <select class="form-control" id='sel_media' name='sel_media' required>
                                    <option value=''>-- Select Media --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Container Name:</label>
                        <select class="form-control" id='sel_container' name='sel_container' required>
                            <option value=''>-- Select Container --</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Available Container Batches:</label>
                        <select class="form-control" id='sel_container_batch' name='sel_container_batch' required>
                            <option value=''>-- Select Batch --</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="row">
                        <div class="col-12 d-none alert alert-danger" role="alert" id="con_out_stock"></div>
                        <div class="col-12 d-none alert alert-danger" role="alert" id="plant_out_stock"></div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Container Quantity:</label>
                                <input type="number" class="form-control" name="container_qty" id="container_qty" required
                                       min="1" />
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Plants per Container:</label>
                                <input type="number" class="form-control" name="plant_qty" id="plant_qty" required
                                       min="1" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Total Plants:</label>
                        <input type="number" class="form-control" id='total_plants' name="total_plants" readonly
                            required min="1"/>
                    </div>
                    <div class="form-group">
                        <label for="name">Main Plant Category:</label>
                        <select class="form-control" name="cat_id" id="cat_id" required>
                            <option value="">-- Select Category --</option>
                            @foreach ($categories as $key => $value)
                            <option value="{{ $value->id }}">
                                {{ $value->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Sub Plant Category:</label>
                        <select class="form-control" id='sel_sub_cat' name='sel_sub_cat' required>
                            <option value=''>-- Select Sub Category --</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Plant:</label>
                        <select class="form-control" id='sel_item' name='sel_item' required>
                            <option value=''>-- Select Plant --</option>
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary ml-3 btn-prevent-multiple-submits">Add Media Stock</button>
                <input type="hidden" id="con_qty" name="con_qty" value="" />
                {{ csrf_field() }}
            </div>
        </form>
    </div>
</div>
@endsection
