@extends('layouts.app')

@section('content')
<script type='text/javascript'>
    $(document).ready(function(){

           // $('#sel_type').change(function(){

                // get the media type 1 or 2 from the media type select box
               // var id = $(this).val();

                // Empty the dropdown
                $('#sel_media').find('option').not(':first').remove();
                $('#sel_container').find('option').not(':first').remove();

                // get rooting medias
                $.ajax({
                    url: '../getmedias/'+2,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){

                        var len = 0;
                        if(response['data'] != null){
                            len = response['data'].length;
                        }

                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++){
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#sel_media").append(option);
                            }
                        }
                    }
                });


           // });


            $('#sel_media').change(function () {

                // get media type and media stock id from selection boxes
                var id = $(this).val();
                var mtype = 2;

                $('#sel_container').find('option').not(':first').remove();
                // get containers according to media type
                $.ajax({
                    url: '../getcontainersbymedia/'+mtype+'/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){

                        var len = 0;
                        if(response['data'] != null){
                            len = response['data'].length;
                        }

                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++){
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#sel_container").append(option);
                            }
                        }
                    }
                });
            })

            $('#sel_container').change(function () {

                // get media type and media stock id from selection boxes
                var id = $(this).val();
                var mtype = 2;
                var media = $('#sel_media').val();

                if($('#plant_qty').val()){
                    $('#total_plants').val('');
                    $('#container_qty').val('');
                    $('#plant_qty').val('');
                }

                $('#sel_container_batch').find('option').not(':first').remove();
                $('#con_qty').find('option').not(':first').remove();
                // get containers according to media type
                $.ajax({
                    url: '../getcontainerbatches/'+mtype+'/'+id+'/'+media,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){

                        $.each( response, function( key, value ) {
                            $.each( value, function( data_key , data ) {
                                // console.log(data);
                                let option = "<option value='"+data.id+"' data-quantity='"+data.qty+"'>Batch: "+data.id+"-> Quantity: "+data.qty+" ["+data.operator+"]</option>";
                                $("#sel_container_batch").append(option);
                            });
                        });
                    }
                });
            })
            // insert available container quantity to hidden field
            $("#sel_container_batch").change(function() {
                $("#con_qty").val($("#sel_container_batch option:selected").attr('data-quantity'));
            });

            // sub categories
            $("#cat_id").change(function () {

                var id = $(this).val();

                $('#sel_sub_cat').find('option').not(':first').remove();

                $.ajax({
                    url: 'getsubcategories/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response['data'] != null){
                            len = response['data'].length;
                        }

                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++){
                                var id = response['data'][i].id;
                                var subcat = response['data'][i].subcat;
                                var option = "<option value='"+id+"'>"+subcat+"</option>";
                                $("#sel_sub_cat").append(option);
                            }
                        }
                    }
                });
            })

            // items
            $("#sel_sub_cat").change(function () {

                var id = $(this).val();

                $('#sel_item').find('option').not(':first').remove();

                $.ajax({
                    url: '../getitems/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response['data'] != null){
                            len = response['data'].length;
                        }

                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++){
                                var id = response['data'][i].id;
                                var item_name = response['data'][i].item_name;
                                var option = "<option value='"+id+"'>"+item_name+"</option>";
                                $("#sel_item").append(option);
                            }
                        }
                    }
                });
            })

            // no of plants validation
            $("#container_qty").bind('keyup mouseup', function () {

                $("#con_out_stock").empty().addClass('d-none');
                $('#total_plants').val('');

                var qty = $(this).val();
                var container_id = $('#sel_container').val();
                var container_batch_id = $('#sel_container_batch').val();
                var container_quantity = $('#con_qty').val();

                if (qty < 1){
                    $("#con_out_stock").html("Container Quantity MUST be greater than 0").removeClass('d-none');
                    $(this).val('');
                    return false;
                }

                $.ajax({
                    url: '../getplants/'+container_id+'/'+container_batch_id+'/'+container_quantity,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){

                        if(response['data'] != null){
                            var plant_qty = JSON.stringify(response['data']);
                        }

                        if($('#plant_qty').val()){
                            plant_qty = $('#plant_qty').val();
                        }

                        if(parseInt(plant_qty) > 0){
                            if (parseInt(qty) <= parseInt(container_quantity)) {
                                $("#con_out_stock").empty().addClass('d-none');
                                $("#plant_out_stock").empty().addClass('d-none');
                                $('#plant_qty').val(parseInt(plant_qty));
                                $('#total_plants').val(parseInt(plant_qty) * parseInt(qty));

                            } else if (qty === "") {
                                $("#con_out_stock").empty().addClass('d-none');
                                $('#total_plants').val('');
                                $('#plant_qty').val('');
                            } else {
                                $("#con_out_stock").html("Out of stock. Your quantity must be "+container_quantity+" or less").removeClass('d-none');
                                $('#total_plants').val('');
                                $('#container_qty').val('');
                            }
                        }
                    }
                });

                // if(plant_qty > 0){
                //     if (parseInt(qty) <= container_quantity) {
                //         $("#con_out_stock").empty().addClass('d-none');
                //         $('#total_plants').val(plant_qty * parseInt(qty));
                //
                //     } else if (qty === "") {
                //         $("#con_out_stock").empty().addClass('d-none');
                //         $('#total_plants').val('');
                //     } else {
                //         $("#con_out_stock").html("Out of stock. Your quantity must be "+container_quantity+" or less").removeClass('d-none');
                //         $('#total_plants').val('');
                //         $('#container_qty').val('');
                //     }
                // }
            });

            $("#edit_transfers").click(function(event){
                event.preventDefault();
                var isDisabled = $('#transfers').prop('readonly');
                if (isDisabled){
                    $('#transfers').prop("readonly", false);
                    $(this).html('Lock');
                } else {
                    $('#transfers').prop("readonly", true);
                    $(this).html('Edit');
                }

            });

            $("#plant_qty").bind('keyup mouseup', function () {
                let plant_qty = parseInt($(this).val());
                let container_qty = parseInt($('#container_qty').val());

                if (plant_qty > 0){
                    $("#plant_out_stock").empty().addClass('d-none');
                    $('#total_plants').val( plant_qty * container_qty);
                } else {
                    $("#plant_out_stock").html("Plants per Container MUST be greater than 0").removeClass('d-none');
                    $('#total_plants').val('');
                }
            });
        });

</script>
<script src="{{ asset('js/submit.js') }}"></script>
<div class="container">
    <div class="card">
        <div class="card-header">
            Add Rooting Stock
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <div>
                <ul>
                    <li>Bulk Number : {{$growthstocks->bulk_number}}</li>
                    <li>Media Name : {{$growthstocks->media}}</li>
                    <li>Container Name : {{$growthstocks->container}}</li>
                </ul>
            </div>
            <form method="post" action="{{ route('storerooting') }}" class="form-prevent-multiple-submits">

                <div class="row">
                    <div class="form-group ml-3">
                        <label for="media_type">Section :</label><br />
                        {{--@foreach (config('app.sections') as $key => $value)--}}
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="section" id="section" value="Production"
                                required checked="checked">
                            <label class="form-check-label">Production</label>
                        </div>
                        {{--@endforeach--}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Operator Name:</label>
                    <select class="form-control" name="user_id" id="user_id" required>
                        <option value="">Select User</option>
                        @foreach ($users as $key => $value)
                        <option value="{{ $key }}">
                            {{ $value }}
                        </option>
                        @endforeach
                    </select>
                </div>
                {{--<div class="form-group">--}}
                {{--<label for="price">Media Type :</label>--}}
                {{--<select class="form-control" name="sel_type" id='sel_type' required>--}}
                {{--<option value="">-- Select Media Type --</option>--}}
                {{--@foreach (config('app.media_types') as $key => $value)--}}
                {{--<option value="{{ $key }}">{{ $value }}</option>--}}
                {{--@endforeach--}}
                {{--</select>--}}
                {{--</div>--}}
                <div class="form-group">
                    <label for="name">Media Name:</label>
                    <select class="form-control" id='sel_media' name='sel_media' required>
                        <option value=''>-- Select Media --</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Container Name:</label>
                    <select class="form-control" id='sel_container' name='sel_container' required>
                        <option value=''>-- Select Container --</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Available Container Batches:</label>
                    <select class="form-control" id='sel_container_batch' name='sel_container_batch' required>
                        <option value=''>-- Select Batch --</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Container Quantity:</label>
                    <input type="number" class="form-control" name="container_qty" id="container_qty" required
                        min="1" />
                    <div id="con_out_stock" class="alert alert-danger mt-1 d-none" role="alert"></div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Plants per Container:</label>
                            @php
                                list($week, $transfers, $plants) = explode(' ',$growthstocks->item_serial_no);
                            @endphp
                            <input type="number" class="form-control" name="plant_qty" id="plant_qty"
                                   value="{{$plants}}" min="1" required/>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Total Plants:</label>
                            <input type="number" class="form-control" id='total_plants' name="total_plants" readonly
                                   required min="1"/>
                        </div>
                    </div>
                    <div class="col-12">
                        <div id="plant_out_stock" class="alert alert-danger d-none" role="alert"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">No. of Transfers:</label>
                    <div class="d-flex">
                        <input type="number" class="form-control mr-1" name="transfers" id="transfers"
                            value="{{$growthstocks->batch_code}}" required readonly>
                        <button type="submit" class="btn btn-secondary ml-3" id="edit_transfers">Edit</button>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Item Name:</label>
                    <div class="d-flex">
                        <input type="text" class="form-control mr-1" name="item_name" id="item_name"
                            value="{{$growthstocks->item}}" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Bar Code:</label>
                    <div class="d-flex">
                        <input type="text" class="form-control mr-1" name="bar_code" id="bar_code"
                            value="{{$growthstocks->bar_code}}" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Item Code</label>
                    <div class="d-flex">
                        <input type="text" class="form-control mr-1" name="item_code" id="item_code"
                            value="{{$growthstocks->item_code}}" required readonly>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mt-2 btn-prevent-multiple-submits">
                    Add Rooting Stock
                </button>
                <input type="hidden" id="con_qty" name="con_qty" value="" />
                <input type="hidden" id="serial_no" name="serial_no" value="{{$growthstocks->serial_no}}" />
                <input type="hidden" id="batch_code" name="batch_code" value="{{$growthstocks->batch_code}}" />
                <input type="hidden" id="id" name="id" value="{{$growthstocks->id}}" />
                <input type="hidden" id="item_id" name="item_id" value="{{$growthstocks->item_id}}" />
                <input type="hidden" id="bulk_number" name="bulk_number" value="{{$growthstocks->bulk_number}}" />
                <input type="hidden" id="pre_status" name="pre_status" value="{{$growthstocks->status}}" />
                <input type="hidden" id="pre_type" name="pre_type" value="{{$growthstocks->type}}" />
                {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>
@endsection
