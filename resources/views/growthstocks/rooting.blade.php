@extends('layouts.app')

@section('content')
    <div class="uper">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br/>
        @endif
        <div class="row">
            <h2 class="col-md-5 col-sm-7">Rooting</h2>
            @include('partials.plant_search')
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Bulk No</th>
                <th>Serial No</th>
                <th>Media Name</th>
                <th>Container Name</th>
                <th>Item Name</th>
                <th>Item Barcode</th>
                <th>Created Date</th>
                <th>Created Week</th>
                <td colspan="4">Actions</td>
            </tr>
            </thead>
            <tbody>
            @foreach($growthstocks as $growthstock)
                <tr>
                    <td>{{$growthstock->bulk_number}}</td>
                    <td>{{$growthstock->serial_no}}</td>
                    <td>{{$growthstock->mediaVariety->name}}</td>
                    <td>{{$growthstock->containerVariety->name}}</td>
                    <td>{{ isset($growthstock->item) ? $growthstock->itemVariety->item_name : 0 }}</td>
                    <td>{{$growthstock->itemVariety->bar_code}}</td>
                    <td>{{$growthstock->date}}</td>
                    <td>{{$growthstock->getWeekAttribute()}}</td>
                    <td>
                        <form action="{{ route('home.scanqrcode')}}" method="post">
                            @csrf
                            @method('POST')
                            <input id="s_no" name="s_no" type="hidden" value="{{$growthstock->serial_no}}">
                            <button class="btn btn-success" type="submit">View</button>
                        </form>
                    </td>
                </tr>
                {{--@endif--}}
            @endforeach
            </tbody>
        </table>
        {{ $growthstocks->onEachSide(1)->links() }}
        <div>
@endsection
