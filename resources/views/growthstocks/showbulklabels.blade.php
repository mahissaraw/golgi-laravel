@extends('layouts.app')

@section('content')
<link href="{{ asset('css/bootstrap4-toggle.min.css') }}" rel="stylesheet">
<script src="{{ asset('js/bootstrap4-toggle.min.js') }}"></script>

<div class="col-12">
    <div class="card mb-3">
        <div class="card-header">
            @if (!empty($bulks))
                <div class="row">
                    <div class="col-6">
                        @if(Auth::user()->printer_id)
                        <a class="btn btn-danger btn-sm" href="/growthstocks/print_bulk_number/{{ $bulk_no ?? '' }}"><i
                                class="fas fa-print"></i> Print Bulk Label: {{ $bulk_no ?? '' }}</a>
                        @endif
                    </div>
                    <div class="col-6">
                        @if($plant_size)
                            <a href="#" class="float-right badge badge-info">{{ config('app.plant_sizes')[$plant_size] }}</a>
                        @endif
                        @if(Auth::user()->isDeveloper())
                            <form action="{{ route('deleteBulk', ['bulk_number' =>$bulk_no])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger btn-sm float-right" onclick="return confirm('Are you sure?')"
                                        type="submit"><i class="far fa-trash-alt"></i> Delete</button>
                            </form>
                        @endif
                    </div>
                </div>
            @else
            Search Bulk Number
            @endif
        </div>
        <div class="card-body">
            <form action="{{ route('showbulklabels', 0)}}">
                <div class="form-group">
                    <div class="input-group mt-1 mb-1">
                        <input type="number" class="form-control" id="bulk_no" name="bulk_no"
                            placeholder="Enter bulk number" aria-describedby="basic-addon2" required>
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text" id="basic-addon2">
                                <i class="fas fa-search"></i>
                                Search
                            </button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>

@if (!empty($bulks))
<div class="alert-msg"></div>
<div class="shadow-sm p-3 mb-3 bg-white rounded row">
    <div class="col-sm-3">
        <a class="btn {{ !Auth::user()->printer_id ? 'btn-outline-dark' : 'text-dark'}}" href="/users/{{ Auth::user()->id }}">
            {{ Auth::user()->printer->name ?? 'Click To Select Printer' }}
        </a>
        @if(Auth::user()->printer_id)
            <button type="button" class="btn btn-primary print-labels shadow-sm">
                <i class="fas fa-print"></i>  Print Labels <span class="badge badge-light label-count">{{ $bulks->count() }}</span>
            </button>
        @endif
    </div>
    <div class="col-sm-3">
        <input type="checkbox" data-toggle="toggle" id="checkAll" data-off="Unchecked" data-on="Checked"
               data-onstyle="success" data-offstyle="danger" checked>
    </div>
    @if(Auth::user()->isSupperAdmin())
        <div class="col-sm-5 text-right">
            <button id="set_completed" class="btn btn-info btn-grs" data-bulk-no="{{ $bulk_no }}" data-type="completed">
                Clear Completed
                <span class="badge badge-light clear-completed"></span>
            </button>

            <button id="set_dispatched" class="btn btn-dark ml-3 btn-grs" data-bulk-no="{{ $bulk_no }}" data-type="dispatched">
                Clear Dispatched
                <span class="badge badge-light clear-dispatched"></span>
            </button>
        </div>
    @endif
</div>
<div class="alert qr-alert" role="alert" style="display:none"> hello</div>
<div class="row">
@foreach ($bulks as $bulk)
@php
$rejected_bg = ($bulk->accept == 2 && $bulk->reason == null) ? 'alert-danger-contaminated': null;
$completed = ($bulk->complete == 1) ? 'Completed': null;
$dispatched = ($bulk->status == 'Dispatched') ? 'Dispatched': null;
@endphp
<div class="col-lg-2 col-md-3">
    <div class="row">
        <div class="col-12 mb-1">
            <ul class="list-group">
                <li class="list-group-item {{ $rejected_bg }} {{ $completed ? 'bg-completed': '' }}">
                    <div class="float-left">
                        <input class="form-check-input label" data-toggle="toggle" type="checkbox"
                            value="{{ $bulk->serial_no }}"
                            data-off="{{ $rejected_bg != null ? 'Contaminated' : 'Skip' }}" data-on="Print Label"
                            data-onstyle="success" data-offstyle="danger" name="print_labels[]" data-size="xs"
                            {{ ($rejected_bg != null || $completed || $dispatched) ? 'disabled' : 'checked' }}>
                        <span class="text-uppercase {{ $completed ?? $dispatched }}">{{ $completed ?? $dispatched }}</span>
                    </div>
                </li>
                <li class="list-group-item">
                    @if (file_exists(public_path().'/labels/qr/'.$bulk->img_qr))
                    <img src="{{ asset('labels/qr/'.$bulk->img_qr) }}">
                    @else
                    <button type="button" class="btn btn-outline-info generate-qrcode"
                        data-img-qr="{{ $bulk->img_qr }}" data-serial-no="{{ $bulk->serial_no }}">Generate
                        QrCode</button>
                    @endif
                </li>
                @php
                $created_date = new DateTime($bulk->date);
                $tf = explode(" ",$bulk->item_serial_no);
                @endphp
                <li class="list-group-item">
                    {{ $created_date->format("Y").'-'.$created_date->format("W") }} | Barcode: {{ $bulk->itemVariety->bar_code }}
                    <form action="{{ route('home.scanqrcode')}}" method="post">
                        @csrf
                        @method('POST')
                        <input id="s_no" name="s_no" type="hidden" value="{{$bulk->serial_no}}">
                        <button class="btn btn-sm" type="submit">Serial No: {{$bulk->serial_no}}</button>
                    </form>
                </li>
                <li class="list-group-item">
                    user code: {{ $bulk->userDetail->user_code }} | Transfers: {{ $tf[1] }}
                </li>
                <li class="list-group-item">
                    {{ $bulk->mediaVariety->name }} | {{ $bulk->containerVariety->name }}
                </li>
                <li class="list-group-item {{ $bulk->status == "Transfer Room" ? 'transfer_room' : ''}}">
                     {{ $bulk->type }} - {{ $bulk->status }}
                    <span class="badge float-right">{{ $bulk->flag != 2 ? $bulk->flag : '' }}</span>
                </li>
            </ul>
        </div>
    </div>
</div>
@endforeach
</div>
@endif


<script>
$(document).ready(function(){
    let numCompleted    = $('.Completed').length;
    let numDispatched   = $('.Dispatched').length;
    let numLabel        = $('input[name="print_labels[]"]:checked').length;
    $('.label-count').text(numLabel);

    if(numCompleted > 0 && numLabel > 0){
        $(".clear-completed").text(numCompleted);
    } else {
        $(".clear-completed").parents('button').hide();
    }
    if(numDispatched > 0){
        $(".clear-dispatched").text(numDispatched);
    } else {
        $(".clear-dispatched").parents('button').hide();
    }

    $('input[name="print_labels[]"]').change(function(e) {
        if($(this).prop("checked") == true){
            $('.label-count').text($('input[name="print_labels[]"]:checked').length);

        }
        else if($(this).prop("checked") == false){
            $('.label-count').text($('input[name="print_labels[]"]:checked').length);
        }
    });

    // uncheck all toggle button
    $("#checkAll").change(function () {
        $("input:checkbox.form-check-input").bootstrapToggle('toggle');
        $('.label-count').text($('input[name="print_labels[]"]:checked').length);
    });

    $(".generate-qrcode").click(function(e){
        e.preventDefault();
        var btnQr = $(this);
        var serial_no = $(this).data("serial-no");
        var qr_image_name  = $(this).data("img-qr");
        $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
        $.ajax({
            method: 'POST',
            url: '/growthstocks/createqrcode',
            data: {'serial_no' : serial_no, 'qr_image_name': qr_image_name },
            success: function(response){
                btnQr.hide();
                btnQr.parent().append().html(
                    '<div class="alert alert-success qr-alert" role="alert">'+response.success+'</div>'+
                    '<img src="{!! asset("'+response.img+'") !!}">'
                );
                $(".qr-alert").delay(1000).fadeOut("slow");
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    });


    $(".print-labels").click(function(e){
        e.preventDefault();
        var $this = $(this);
        var serial_nos = [];
        var loadingText = '<i class="fa fa-spinner fa-spin"></i>  Printing....';

        if ($(this).html() !== loadingText) {
            $this.data('original-text', $(this).html());
            $this.html(loadingText).prop('disabled', true);
        }

        $.each($("input[name='print_labels[]']:checked"), function(){
            serial_nos.push($(this).val());
        });

        $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
        $.ajax({
            method: 'POST',
            url: '/growthstocks/printlabels',
            data: {'serial_nos' : serial_nos},
            success: function(response){
                setTimeout(function() {
                $this.html($this.data('original-text')).prop('disabled', false);
                }, 2000);
                $( ".qr-alert" ).show();
                    if (response.error != null ){
                        $( ".qr-alert" ).addClass('alert-danger').removeClass('alert-success').text(response.error);
                    } else {
                        $( ".qr-alert" ).addClass('alert-success').removeClass('alert-danger').text(response.success);
                    }
                    $(".qr-alert" ).delay(2000).fadeOut("slow");

            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    });

    $('.btn-grs').click(function(e){
        e.preventDefault();
        var $_this = $(this);
        var bulk_number = $_this.data("bulk-no");
        var type  = $_this.data("type");
        var loadingText = '<i class="fa fa-spinner fa-spin"></i>  Updating '+type;

        if ($_this.html() !== loadingText) {
            $_this.data('original-text', $_this.html());
            $_this.html(loadingText).prop('disabled', true);
        }

        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        $.ajax({
            method: 'POST',
            url: '/backend/update_bulk_completed',
            data: {'bulk_number' : bulk_number, 'type': type,  _token    :'{{csrf_token()}}'},
            success: function(response){
                setTimeout(function() {
                    $_this.html($_this.data('original-text')).prop('disabled', false);
                }, 2000);
                $('.alert-msg').prepend(
                    '<div class="alert alert-success qr-alert" role="alert">'+response.success+'</div>'
                );
                $(".qr-alert").delay(5000).fadeOut("slow");
                // location.reload();
                setTimeout(function(){ location.href = '/growthstocks/showbulklabels'; }, 3000);
            },
            error: function(response) {
                alert('Something went wrong in this request!');// What to do if we fail
                console.log(response);
                // console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    });

});

</script>
<style>
.alert-danger-contaminated {
    color: #761b18;
    background-color: #f9d6d5;
    border-color: #f7c6c5;
}

.bg-completed {
    background-color: #d4edda;
}
.transfer_room{
    background-color: rgba(244, 208, 63, 0.61);
    /*color: white;*/
}
</style>
@endsection
