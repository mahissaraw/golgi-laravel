@extends('layouts.app')

@section('content')
<div class="col-12">
    <div class="card mb-3">
        <div class="card-header">
            Search Serial Code
        </div>
        <div class="card-body">
            <form action="{{route('home.scanqrcode')}}" method="POST" id="form_qr_scan">
                @csrf
                @method('POST')
                <div class="form-group">
                    <div class="input-group mt-1 mb-1">
                        <input type="number" class="form-control" id="s_no" name="s_no"
                               placeholder="Search Serial Code" autofocus required min="1" aria-describedby="basic-addon2" autocomplete="off">
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text" id="basic-addon2">
                                <i class="fas fa-search"></i>
                                Search
                            </button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
@endsection
