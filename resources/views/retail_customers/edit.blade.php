@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        Edit Customer
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('customers.update', $customer->id) }}">
            @csrf
            @method('PUT')
            <div class="row">
                <input type="hidden" id="type" name="type" value="{{ $customer->type }}">
                <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name" value="{{ $customer->name }}" required />
                    </div>
                    <div class="form-group">
                        <label for="name">Email:</label>
                        <input type="email" class="form-control" name="email" value="{{ $customer->email }}" required />
                    </div>

                    <div class="form-group">
                        <label for="name">Phone:</label>
                        <input type="tel" class="form-control" name="phone" value="{{ $customer->phone }}" required />
                    </div>
                    <div class="form-group">
                        <label for="nic">NIC/Passport:</label>
                        <input type="text" class="form-control" name="nic" value="{{ $customer->nic }}" />
                    </div>
                    <div class="form-group">
                        <label for="name">Status:</label>
                        <select class="form-control" id="is_active" name="is_active">
                            <option value="1" {{ $customer->is_active == 1 ? 'selected' : '' }}>Active</option>
                            <option value="0" {{ $customer->is_active == 0 ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                        <label for="name">Address:</label>
                        <input type="text" class="form-control" name="street" value="{{ $customer->street }}"
                               required maxlength="30"/>
                    </div>
                    <div class="form-group">
                        <label for="name">City:</label>
                        <input type="text" class="form-control" name="city" value="{{ $customer->city }}" required maxlength="30"/>
                    </div>
                    <div class="form-group">
                        <label for="name">State:</label>
                        <input type="text" class="form-control" name="state" value="{{ $customer->state }}" required maxlength="10"/>
                    </div>
                    <div class="form-group country_list">
                        @include('partials.country_list')
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary mt-2">Update</button>
        </form>
    </div>
</div>
@endsection
