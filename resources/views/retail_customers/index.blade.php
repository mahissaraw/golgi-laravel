@extends('layouts.app')

@section('content')

<h3>
    List of Retail Customers
    <a href="{{ route('retail_customers.create') }}" class="btn btn-secondary ml-5">Add New Retail Customer</a>
</h3>
<div class="uper">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>email</th>
                <th>Phone</th>
                <th>Street</th>
                <th>City</th>
                <th>State</th>
                <th>Country</th>
                <th>NIC</th>
                <th>Status</th>
                <th colspan="2">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($customers as $customer)
            <tr>
                <td>{{ $customer->id }}</td>
                <td>{{ $customer->name }}</td>
                <td>{{ $customer->email }}</td>
                <td>{{ $customer->phone }}</td>
                <td>{{ $customer->street ?? '' }}</td>
                <td>{{ $customer->city ?? '' }}</td>
                <td>{{ $customer->state ?? '' }}</td>
                <td>{{ $customer->country }}</td>
                <td>{{ $customer->nic ?? '' }}</td>
                <td>{{ $customer->is_active ? 'Active' : 'Inactive' }}</td>
                <td>
                    <a href="{{ route('retail_customers.show',$customer->id) }}" class="btn btn btn-info">View</a>
                </td>
                <td>
                    <a href="{{ route('retail_customers.print_address', $customer->id)}}"
                        class="btn btn-outline-dark"><i class="fas fa-print"></i> Print</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $customers->links() }}
</div>
@endsection
