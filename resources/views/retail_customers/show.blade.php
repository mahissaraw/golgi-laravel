@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <a href="{{ route('retail_customers.edit',$customer->id)}}" class="btn btn-primary">Edit</a>
            <a href="{{ route('retail_customers.print_address', $customer->id)}}"
                class="btn btn-outline-dark float-right"><i class="fas fa-print"></i> Print Address</a>
            <div class="text-center">
                <h2 class="mb-3">{{ $customer->name }}</h2>
                <p class="text-muted">
                    <i class="fas fa-map-marker-alt"></i>
                    <br />{{ $customer->street ?? '' }}
                    {{ $customer->city ?? '' }}
                    {{ $customer->state ?? '' }}
                    <br>{{ $customer->country }}
                </p>
                <hr>
            </div>
            <div class="row text-center">
                <div class="col-sm-3">
                    <p class="text-muted">
                        <i class="fas fa-envelope"></i>
                        <br />{{ $customer->email ?? '' }}
                    </p>
                </div>
                <div class="col-sm-2">
                    <p class="text-muted">
                        <i class="fas fa-phone-square"></i>
                        <br />{{ $customer->phone ?? '' }}
                    </p>
                </div>
                <div class="col-sm-3">
                    <p class="text-muted">
                        <i class="fas fa-address-card"></i>
                        <br />{{ $customer->nic ?? '' }}
                    </p>
                </div>
                <div class="col-sm-2">
                    <p class="text-muted">
                        @if ($customer->is_active)
                        <i class="fas fa-eye"></i>
                        @else
                        <i class="fas fa-eye-slash"></i>
                        @endif
                        <br />{{ $customer->is_active ? 'Active' : 'Inactive' }}
                    </p>
                </div>
                <div class="col-sm-2">
                    <p class="float-right">
                        Total Sale
                        <br />Rs: <b>{{ $sum }}</b>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@if(!$customer->sales->isEmpty())
<div class="row mt-2">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Invoice Date</th>
                <th>Invoice Number</th>
                <th>Plant</th>
                <th>Quantity</th>
                <th>Amount (LKR)</th>
                <th>Remarks</th>
                <th colspan="2">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($customer->sales as $sales_record)
            <tr>
                <td>{{ $sales_record->invoice_date }}</td>
                <td><a href="{{ route('invoiceNumber',$sales_record->invoice_number)}}">{{ $sales_record->invoice_number }}</a></td>
                <td>{{ $sales_record->item->item_code }} - {{ $sales_record->item->item_name }}</td>
                <td>{{ $sales_record->quantity  }}</td>
                <td>{{ $sales_record->amount }}</td>
                <td>{{ \Illuminate\Support\Str::limit(strip_tags($sales_record->remarks), 20, '...') }}</td>
                <td>
                    <a href="{{ route('sales_records.show',$sales_record->id)}}" class="btn btn-sm btn-success">View</a>
                    <a href="{{ route('sales_records.edit',$sales_record->id)}}" class="btn btn-sm btn-primary">Edit</a>
                </td>
                <td>
                    <form action="{{ route('sales_records.destroy', $sales_record->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')"
                            type="submit"><i class="far fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif
@endsection
