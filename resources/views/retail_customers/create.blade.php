@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        Add Retail Customer
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('customers.store') }}">
            @csrf
            @method('POST')
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <input type="hidden" id="type" name="type" value="2">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name" required />
                    </div>
                    <div class="form-group">
                        <label for="name">Email:</label>
                        <input type="email" class="form-control" name="email" required />
                    </div>
                    <div class="form-group">
                        <label for="name">Phone:</label>
                        <input type="tel" class="form-control" name="phone" required />
                    </div>
                    <div class="form-group">
                        <label for="nic">NIC/Passport:</label>
                        <input type="text" class="form-control" name="nic" />
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                        <label for="name">Address:</label>
                        <input type="text" class="form-control" name="street" required maxlength="40"/>
                    </div>
                    <div class="form-group">
                        <label for="name">City:</label>
                        <input type="text" class="form-control" name="city" required maxlength="40"/>
                    </div>
                    <div class="form-group">
                        <label for="name">State:</label>
                        <input type="text" class="form-control" name="state" required maxlength="25"/>
                    </div>
                    <div class="form-group">
                        @include('partials.country_list')
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary mt-2">Save</button>
        </form>
    </div>
</div>
<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
@endsection
