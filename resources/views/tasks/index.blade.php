@extends('layouts.app')

@section('content')

<div>
    <h2>Tasks</h2>
    <table class="table table-striped" id="mediaStocks">
        <thead>
            <tr>
                <th>Operator</th>
                <th>Description</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Frequency</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tasks as $task)
            <tr>
                <td>{{$task->user->name}}</td>
                <td>{{$task->description}}</td>
                <td>{{ date('Y-m-d', strtotime($task->date_started))}}</td>
                <td>{{date('Y-m-d', strtotime($task->date_completed))}}</td>
                <td>{{$task->frequency->frequency ?? ''}}</td>
                @php
                $start = date_create(date("Y-m-d"));
                $end = date_create(date('Y-m-d', strtotime($task->date_completed)));
                $diff=date_diff($start,$end);
                @endphp
                <td>{{($task->is_complete ==1 )? "Done":"Pending ".$diff->format("%R%a days") }}
                </td>
                <td>
                    <a href="{{ route('tasks.edit', $task->id)}}" class="btn btn-primary">
                        <i class="far fa-edit"></i> Edit
                    </a>

                    @if ($task->is_complete !== 1)
                    <a href="{{ route('tasks.mark_completed', $task->id)}}" class="btn btn-success">
                        <i class="far fa-calendar-check"></i> Mark As Complete
                    </a>
                    @endif

                    @if(Auth::user()->hasRole('Administrator'))
                    <form action="{{ route('tasks.destroy', $task->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger float-left mt-2" type="submit">
                            <i class="far fa-trash-alt"></i> Delete
                        </button>
                    </form>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{-- {{ $mediastocks->onEachSide(1)->links() }} --}}
</div>
<style>
    .dataTables_filter input[type="search"] {
        border: 1px solid #ced4da;
        border-radius: .25rem;
        padding: 2px;
    }
</style>
@endsection
