@extends('layouts.app')

@section('content')
<script type='text/javascript'>
    $(document).ready(function(){

    //     $( function() {
    //         $( " #start_date" ).datepicker( {dateFormat: 'yy-mm-dd' }, ).attr('readonly','readonly'); $( "#end_date"
    // ).datepicker( {dateFormat: 'yy-mm-dd' }, ).attr('readonly','readonly'); } ); }); 

    $('#start_date').duDatepicker({format: 'yyyy-mm-dd',theme: 'green',auto: true, rangeTo: '#end_date'});
    $('#end_date').duDatepicker({format: 'yyyy-mm-dd',theme: 'green',auto: true, rangeFrom: '#start_date'});

    });
</script>
<div class="container">
    <div class="card">
        <div class="card-header">
            Add Task
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('tasks.store') }}">
                <div class="form-group">
                    <label for="name">Operator Name:</label>
                    <select class="form-control" name="user_id" id="user_id" required>
                        <option>Select User</option>
                        @foreach ($users as $key => $value)
                        <option value="{{ $key }}">
                            {{ $value }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="price">Description :</label>
                    <textarea name="description" class="form-control" id="description"></textarea>
                </div>
                <div class="form-group">
                    <label for="name">Frequency:</label>
                    <select class="form-control" name="freq" id="freq" required>
                        <option>Select Frequency</option>
                        @foreach ($frequencies as $key => $value)
                        <option value="{{ $key }}">
                            {{ $value }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Start Date:</label>
                    <input type="text" class="form-control" name="start_date" id="start_date" required
                        data-rangeto="#end_date" />
                </div>
                <div class="form-group">
                    <label for="name">End Date:</label>
                    <input type="text" class="form-control" name="end_date" id="end_date" required
                        data-rangefrom="#start_date" />
                </div>
                <button type="submit" class="btn btn-primary">Add Task</button>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>
@endsection