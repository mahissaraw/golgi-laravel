@extends('layouts.app')

@section('content')
<script type='text/javascript'>
    $(document).ready(function(){
        $('#start_date').duDatepicker({format: 'yyyy-mm-dd',theme: 'green',auto: true, rangeTo: '#end_date'});
        $('#end_date').duDatepicker({format: 'yyyy-mm-dd',theme: 'green',auto: true, rangeFrom: '#start_date'});
    });
</script>

<div class="container">
    <div class="card">
        <div class="card-header">
            Update Task
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('tasks.update', $task->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Operator Name:</label>
                    <select class="form-control" name="user_id" id="user_id" required>
                        <option>Select User</option>
                        @foreach ($users as $key => $value)
                        <option value="{{ $key }}" {{ $key == $task->user_id ? 'selected' : '' }}>
                            {{ $value }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="price">Description :</label>
                    <textarea name="description" class="form-control"
                        id="description">{{ $task->description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="name">Frequency:</label>
                    <select class="form-control" name="frequency_id" id="frequency_id" required>
                        <option>Select Frequency</option>
                        @foreach ($frequencies as $key => $value)
                        <option value="{{ $key }}" {{ $key == $task->frequency_id ? 'selected' : '' }}>
                            {{ $value }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Start Date:</label>
                    <input type="text" class="form-control" name="start_date" id="start_date" required
                        data-rangeto="#end_date" value={{ $task->date_started }} />
                </div>
                <div class="form-group">
                    <label for="name">End Date:</label>
                    <input type="text" class="form-control" name="end_date" id="end_date" required
                        data-rangefrom="#start_date" value={{ $task->date_completed }} />
                </div>
                <div class="form-group">
                    <label for="name">Status:</label>
                    @php
                    $status = [0 => 'Pending', 1 => 'Done'];
                    @endphp
                    <select class="form-control" name="is_complete" id="is_complete">
                        @foreach ($status as $key => $value)
                        <option value="{{ $key }}" {{ $key == $task->is_complete ? 'selected' : '' }}>
                            {{ $value }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Update Task</button>
            </form>
        </div>
    </div>
</div>
@endsection