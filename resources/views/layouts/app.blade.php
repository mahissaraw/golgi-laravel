<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'GolgiTec') }} @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/duDatepicker.min.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/duDatepicker.min.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('img/favicon.png') }}">
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-dark {{ config('app.debug') ? 'bg-info' : 'bg-secondary' }} navbar-expand-md sticky-top"
            style="box-shadow: 0 1px 5px rgba(0, 0, 0, .5)">

            <!-- Authentication Links -->
            @guest
            <a href="/home">
                <img src="/img/golgi_tec_logo_menu.png" width="100" class="d-inline-block align-top">
            </a>
            @else

            <button class="navbar-toggler mr-auto" type="button" data-toggle="collapse"
                data-target="#collapsingNavbar2">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a href="/home">
                <img src="/img/golgi_tec_logo_menu.png" width="100" class="d-inline-block align-top">
            </a>
            <div class="navbar-collapse collapse justify-content-between align-items-center" id="collapsingNavbar2">
                <ul class="navbar-nav mx-auto">
                    @include('partials.menu')
                </ul>
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <i class="fas fa-user-circle"></i> {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/users/{{ Auth::user()->id }}">
                                <i class="fas fa-user-cog"></i> Profile
                            </a>
                            <a class="dropdown-item" href="#" onclick="location.reload();">
                                <i class="fas fa-sync-alt"></i> Reload Page
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fas fa-power-off"></i> {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                </ul>
            </div>
        </nav>
        <main class="container-fluid mt-2">
            @include('partials.flash-message')

            @yield('content')
        </main>
    </div>
</body>

</html>
