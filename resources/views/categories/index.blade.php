@extends('layouts.app')

@section('title', 'Category')

@section('content')
<h3>
    List of Categories
    <button type="button" class="btn btn-secondary ml-5" data-toggle="modal" data-target="#addContainerModel">
        Add New Category
    </button>
</h3>
<div class="col-xs-12 filters">
    <a class="btn btn-sm btn-outline-success" href="/categories">Reset list</a>
    <a class="btn btn-sm btn-outline-info" href="/categories?select_inactive=true">Show Inactive</a>
</div>
<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($categories as $category)
        <tr class="{{ $category->active == 0 ? 'text-danger' : '' }}">
            <td>{{ $category->id }} </td>
            <td>{{ $category->name }}</td>
            <td>
                <a href="/categories/{{ $category->id}}" class="btn btn-secondary">View</a>
                <a href="/categories/{{ $category->id}}/edit" class="btn btn-primary">Edit</a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

<div class="modal fade" id="addContainerModel" tabindex="-1" role="dialog"
     aria-labelledby="addContainerModelTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('categories.store') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="addContainerModelLongTitle">Add Main Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="form-control" name="category_name" required placeholder="Category Name" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
