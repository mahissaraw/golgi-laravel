@extends('layouts.app')

@section('content')
<div class="list-group">
    <a href="#" class="list-group-item list-group-item-dark">Main Category: {{ $category->name }}</a>
    @foreach ($category->subCategories as $subCategory)
    <a href="/subcategories/{{ $subCategory->id}}" class="list-group-item list-group-item-action">{{ $subCategory->subcat }}</a>
    @endforeach
</div>

@endsection
