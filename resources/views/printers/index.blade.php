@extends('layouts.app')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
@endif
<div class="row">
    <h2 class="col-md-5 col-sm-8">
        List of Printers
        <button type="button" class="btn btn-secondary ml-5" data-toggle="modal" data-target="#addContainerModel">
            Add New Printer
        </button>
    </h2>
</div>
<div class="uper">
    <table class="table table-striped" id="items-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Printer Name</th>
                <th>Description</th>
                <th>Users</th>
                <th colspan="2">Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($printers as $printer)
            <tr  class="{{ $printer->active == 0  ? 'alert-inactive': '' }}">
                <td>{{ $printer->id }}</td>
                <td>{{ $printer->name }}</td>
                <td>{{ $printer->description }}</td>
                <td>
                    @if($printer->users)
                        <ul>
                            @foreach($printer->users as $pu)
                                <li>{{ $pu->name }}</li>
                            @endforeach
                        </ul>
                    @endif
                </td>
                <td>{{ $printer->active == 1 ? 'Active' : 'Inactive' }}</td>
                <td><a href="{{ route('printer.edit', $printer->id)}}" class="btn btn-primary printer-edit">Edit</a></td>
{{--                <td>--}}
                    {{--<a href="#" class="btn btn-danger" onclick="--}}
                    {{--var result = confirm('Are you sure you wish to DELETE {{ $item->name }}?');--}}
                    {{--if (result){--}}
                    {{--event.preventDefault();--}}
                    {{--$('#delete-{{ $item->id }}').submit();--}}
                    {{--}">Delete</a>--}}
                    {{--<form id="delete-{{ $item->id }}" action="{{ route('items.destroy', $item->id)}}"
                    method="POST"--}}
                    {{--style="display:none">--}}
                    {{--@csrf--}}
                    {{--@method('DELETE')--}}
                    {{--<input value="delete" name="_method" type="hidden">--}}
                    {{--</form>--}}
{{--                </td>--}}
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="modal fade" id="addContainerModel" tabindex="-1" role="dialog"
     aria-labelledby="addContainerModelTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" id="printer_form" action="{{ route('printer.store') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="addContainerModelLongTitle">Add New Printer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Printer Name</label>
                        <input type="text" class="form-control" name="name" required />
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" name="description"  />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
