@extends('layouts.app')

@section('content')
<div class="card uper">
    <div class="card-header">
        Edit Printer
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <form method="post" action="{{ route('printer.update', $printer->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="name">Printer Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $printer->name }}" />
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" class="form-control" name="description" value="{{ $printer->description }}" />
            </div>
            <div class="form-group">
                <label for="active">Status</label>
                <select class="form-control" id="active" name="active">
                    <option value="1" {{ $printer->active == 1 ? 'selected' : '' }}>Active</option>
                    <option value="0" {{ $printer->active == 0 ? 'selected' : '' }}>Inactive</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
