@extends('layouts.app')

@section('content')
<script>
    $(document).ready(function() {
        $( function() {
            $('#order_date').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, dOrder: '#date_ordered'});
            $('#required_date').duDatepicker({format: 'yyyy-mm-dd',theme: 'blue',auto: true, dReq: '#date_required'});
        } );
    });
</script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-8">
            <div class="card">
                <div class="card-header">
                    Create Order
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                    <form method="post" action="{{ route('orders.store') }}">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="media_type">Customer :</label><br />
                            <select class="form-control" name="customer_id" id="customer_id" required>
                                <option value="">Select Customer</option>
                                @foreach ($customers as $key => $value)
                                <option value="{{ $value->id }}">
                                    {{ $value->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Ordered date:</label>
                            <input type="text" class="form-control" name="order_date" id="order_date" autocomplete=“off”
                                data-dOrder="#date_ordered" required />
                        </div>

                        <div class="form-group">
                            <label for="name">Required date:</label>
                            <input type="text" class="form-control" name="required_date" id="required_date"
                                autocomplete=“off” data-dReq="#date_required" required />
                        </div>
                        <button type="submit" class="btn btn-primary mt-2">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection