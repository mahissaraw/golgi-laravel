@extends('layouts.app')

@section('content')

<h3>
    List of Orders
    <a href="{{ route('orders.create') }}" class="btn btn-secondary ml-5">Add New Order</a>
</h3>
<div class="uper">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Order ID</th>
                <th>Customer Name</th>
                <th>Order Date</th>
                <th>Required Date</th>
                <th>Order Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ $order->customer->name }}</td>
                <td>{{ $order->order_date }}</td>
                <td>{{ $order->required_date }}</td>
                <td>{{ config('app.order_status')[$order->order_status] ?? '' }}</td>
                <td>
                    <a href="{{ route('orders.show',$order->id) }}" class="btn btn-primary">View</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
