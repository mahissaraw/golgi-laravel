@extends('layouts.app')

@section('content')
    <script>
        $(document).ready(function() {
            $( function() {
                $('#order_date').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, dOrder: '#date_ordered'});
                $('#required_date').duDatepicker({format: 'yyyy-mm-dd',theme: 'blue',auto: true, dReq: '#date_required'});
                $('#shipped_date').duDatepicker({format: 'yyyy-mm-dd',theme: 'red',auto: true, dShip: '#date_shipped'});
            } );
        });
    </script>
<div class="card">
    <div class="card-header">
        Edit Order: {{ $order->id }}
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('orders.update', $order->id) }}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="media_type">Customer :</label><br />
                <select class="form-control" name="customer_id" id="customer_id" required>
                    <option value="">Select Customer</option>
                    @foreach ($customers as $customer)
                        <option {{ $customer->id == $order->customer_id  ? 'selected' : ''}} value="{{ $customer->id }}">
                            {{ $customer->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Ordered date:</label>
                <input type="text" class="form-control" name="order_date" id="order_date" autocomplete=“off”
                       data-dOrder="#date_ordered" value="{{ $order->order_date }}" />
            </div>
            <div class="form-group">
                <label for="name">Required date:</label>
                <input type="text" class="form-control" name="required_date" id="required_date"
                       autocomplete=“off” data-dReq="#date_required" value="{{ $order->required_date }}" />
            </div>
            <div class="form-group">
                <label for="name">Required date:</label>
                <input type="text" class="form-control" name="shipped_date" id="shipped_date"
                       autocomplete=“off” data-dShip="#date_shipped" value="{{ $order->shipped_date }}" />
            </div>
            <div class="form-group">
                <label for="media_type">Order Status :</label><br />
                <select class="form-control" name="order_status" id="order_status">
                    <option value="">Change Status</option>
                    @foreach (config('app.order_status') as $key => $value)
                        <option {{ $key == $order->order_status  ? 'selected' : ''}} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary mt-2">Update</button>
        </form>
    </div>
</div>
@endsection
