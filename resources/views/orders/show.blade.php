@extends('layouts.app')

@section('content')
    <script src="{{ asset('js/bootstrap3-typeahead.min.js') }}"></script>
<div class="container">
    <div class="card uper">
        <div class="card-header">
            Order No: {{ $order->id }}
            <div class="btn-group float-right" role="group" aria-label="Basic example">
                <a href="{{ route('orders.edit',$order->id)}}" class="btn btn-info float-right btn-sm"><i class="far fa-edit"></i> Edit</a>
                <form action="{{ route('orders.destroy', $order->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm float-right" type="submit"><i class="far fa-trash-alt"></i></button>
                </form>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <tbody>
                    <tr>
                        <th>Customer Name</th>
                        <td>{{ $order->customer->name }}</td>
                    </tr>
                    <tr>
                        <th>Order Date</th>
                        <td>{{ $order->order_date }}</td>
                    </tr>
                    <tr>
                        <th>Required Date</th>
                        <td>{{ $order->required_date }}</td>
                    </tr>
                    <tr>
                        <th>Shipped Date</th>
                        <td>{{ $order->shipped_date ?? '' }}</td>
                    </tr>
                    <tr>
                        <th>Order Status</th>
                        <td>{{ config('app.order_status')[$order->order_status] ?? '' }}</td>
                    </tr>
                    <tr>
                        <th>Created at</th>
                        <td>{{ $order->created_at }}</td>
                    </tr>
                    <tr>
                        <th>Updated at</th>
                        <td>{{ $order->updated_at }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-header">
            Order Items
            <button type="button" class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#addItemModel">
                <i class="fas fa-plus"></i> Add Items
            </button>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Item Name</th>
                    <th>Quantity</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->order_items as $order_item)
                    <tr class="{{ $order_item->id }}">
                        <td>{{ $order_item->item->item_name }}</td>
                        <td>{{ $order_item->quantity }}</td>
                        <td>

                            <a href="#" class="btn btn-danger btn-sm" onclick="
                                var result = confirm('Are you sure you wish to DELETE {{ $order_item->item->item_name }}?');
                                if (result){
                                event.preventDefault();
                                $('#delete-item-{{ $order_item->id }}').submit();
                                }"><i class="far fa-trash-alt"></i></a>

                            <form id="delete-item-{{ $order_item->id }}"
                                  action="{{ route('orders.destroy_order_item', $order_item->id)}}" method="POST"
                                  style="display:none">
                                @csrf
                                @method('DELETE')
                                <input value="delete" name="_method" type="hidden">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="addItemModel" tabindex="-1" role="dialog" aria-labelledby="addItemModel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addItemModelTitle">Add Items to Order No: {{ $order->id }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('orders.store_order_item') }}">
                @csrf
                @method('POST')
                <div class="modal-body">
                    <input type="hidden" value="" id="selected_item_id" name="selected_item_id">
                    <input type="hidden" value="{{ $order->id }}" id="selected_order_id" name="selected_order_id">
                    <div class="form-group">
                        <label for="name">Item Name:</label>
                        <input type="text" class="form-control typeahead" name="item_name" id="item_name" required placeholder="Type Item Name" autocomplete="off"/>
                    </div>
                    <div class="form-group">
                        <label for="name">Quantity:</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" pattern="[0-9]" required autocomplete="off"/>
                    </div>
                </div>
                <div class="modal-footer">
{{--                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
    <script type="text/javascript">
        var path = "{{ route('item.autocomplete') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            },
            afterSelect: function(data){
                $('#selected_item_id').val(data.id);
            }
        });
    </script>
@endsection
