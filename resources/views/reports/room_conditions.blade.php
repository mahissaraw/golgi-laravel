@extends('layouts.app')

@section('title', 'Room Conditions')

@section('content')
<style>
    span .celsius {
        content: "\2103";
    }
</style>
<div class="row ml-1">
    @if (!empty($rndGrowthRoomContents))
        @php
            list($room_name_gr, $temperature_gr, $humidity_gr, $air_purity_gr) = explode(",", $rndGrowthRoomContents);
        @endphp
        <div class="card border-success mr-2 mb-3" style="max-width: 18rem;">
            <div class="card-header h3">{{ $room_name_gr }}</div>
            <div class="card-body text-dark">
                <h5 class="card-title h1">{{ $temperature_gr }} <span class="celsius">&#8451;</span></h5>
                <p class="card-text h4">Humidity : {{ $humidity_gr }}%</p>
                <p class="card-text h4">Air Purity : {{ $air_purity_gr }} </p>
            </div>
        </div>
    @endif
    @if (!empty($rndLabContents))
        @php
            list($room_name_rl, $temperature_rl, $humidity_rl, $air_purity_rl) = explode(",", $rndLabContents);
        @endphp
        <div class="card border-primary mr-2 mb-3" style="max-width: 18rem;">
            <div class="card-header">{{ $room_name_rl }}</div>
            <div class="card-body text-dark">
                <h5 class="card-title h1">{{ $temperature_rl }} <span class="celsius">&#8451;</span></h5>
                <p class="card-text h4">Humidity : {{ $humidity_rl }}%</p>
                <p class="card-text h4">Air Purity : {{ $air_purity_rl }} </p>
            </div>
        </div>
    @endif
</div>
@endsection
