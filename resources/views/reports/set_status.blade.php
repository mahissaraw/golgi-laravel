@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="mb-3 mt-3 text-center">Pick Status Type</h1>
    <div class="row">
        <div class="col-sm mt-1">
            <a href="/reports/bulk_status/multiply" class="btn btn-primary btn-lg btn-block">Multiply</a>
        </div>
        <div class="col-sm mt-1">
            <a href="/reports/bulk_status/rooting" class="btn btn-success btn-lg btn-block">Rooting</a>
        </div>
        <div class="col-sm mt-1">
            <a href="/reports/bulk_status/rejected_date" class="btn btn-danger btn-lg btn-block">Rejected Date</a>
        </div>
    </div>
    @if (!empty($bulk_data))
    <div class="row mt-3">
        <div class="col-sm-4 offset-sm-4 mt-1">
            <ul class="list-group text-center">
                <li class="list-group-item ">Changed
                    <label class="font-weight-bold text-uppercase">{{ $status }}</label>
                    <p>{{ count($bulk_data) }} - Bulks</p>
                </li>
                @foreach ($bulk_data as $key => $value)
                <li class="list-group-item">
                    {{ $value }}
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif

</div>

@endsection