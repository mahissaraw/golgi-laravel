@extends('layouts.app')

@section('title', 'Plant Status Sorted To Media Type')

@section('content')

    @php $colspan = 3; @endphp
    <div class="shadow-sm p-1 mb-1 bg-white rounded">
        <div class="row">
            <div class="col-8">
                <h3 class="ml-1">{{ $category ? $category->name.' - ' : '' }} Plant Status Sorted To Media Type</h3>
            </div>
            <div class="col-4">
                <form method="post" action="{{ route('plants_media_status.filter') }}" class="form-prevent-multiple-submits">
                    <select class="form-control" name="cat_id" id="cat_id">
                        <option value="null">-- Select Plant Category --</option>
                        @foreach ($categories as $key => $value)
                            <option value="{{ $value->id }}">
                                {{ $value->name }}
                            </option>
                        @endforeach
                    </select>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Plant Name</th>
                            <th>Plant Code</th>
                            <th>Plant Barcode</th>
                            <th>Media Name</th>
                            <th>Week No.</th>
                            <th>Plants</th>
                            @if(Auth::user()->hasAnyRole(['Administrator']))
                                @php $colspan = 5; @endphp
                                <th>Created</th>
                                <th>Rejected</th>
                                <th>Completed</th>
                                <th>Transferred</th>
                                <th>Infections %</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($items as $item)
                        @php
                            $total = 0;
                            $rejected = 0;
                            $completed = 0;
                            $transfer = 0;
                            $dispatched = 0;
                            $row_count = 0;
                            $item_count = 0;
                        @endphp

                        @foreach($performances as $performance)
                            @if ($performance->total_quantity > 0 && $performance->total_quantity > $performance->total_rejected_quantity )
                                <tr>
                                    @if($performance->item_id == $item->id)
                                        @php
                                            $item_count++;
                                        @endphp

                                        @if ($row_count == 0)
                                            <td>{{ $item->item_name }}</td>
                                            <td>{{ $item->item_code }}</td>
                                            <td>{{ $item->bar_code }}</td>
                                            @php
                                                $row_count++;
                                            @endphp

                                        @else
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        @endif
                                        <td>{{ $performance->media_name }}</td>
                                        <td>
                                            <span class="float-right badge badge-primary">{{ $performance->plant_size }}</span>
                                            @if(Auth::user()->hasAnyRole(['Administrator']))
                                                <a class="btn" style="color: #212529" href="{{ route('weekBulk', [$item->id, $performance->year, $performance->week, $performance->media_id, $performance->plant_size ]) }}" title="Show week bulks">
                                                    {{ $performance->year}} - {{ $performance->week }}
                                                </a>
                                            @else
                                                {{ $performance->week }}
                                            @endif
                                        </td>
                                        <td>
                                            {{ $performance->total_quantity - ($performance->total_rejected_quantity + $performance->total_completed_quantity + $performance->total_dispatched_quantity + $performance->total_transfer_quantity) }}
                                        </td>
                                        @if(Auth::user()->hasAnyRole(['Administrator']))
                                            <td>{{ $performance->total_quantity }}</td>
                                            <td>{{ $performance->total_rejected_quantity }}</td>
                                            <td>{{ $performance->total_completed_quantity + $performance->total_dispatched_quantity}}</td>
                                            <td>{{ $performance->total_transfer_quantity }}</td>
                                            <td>
                                                {{ round(($performance->total_rejected_quantity/$performance->total_quantity) *100, 2) }}%
                                            </td>
                                        @endif

                                        @php
                                            $total += $performance->total_quantity;
                                            $rejected += $performance->total_rejected_quantity;
                                            $completed += $performance->total_completed_quantity;
                                            $dispatched += $performance->total_dispatched_quantity;
                                            $transfer += $performance->total_transfer_quantity;
                                        @endphp

                                    @endif
                                </tr>
                            @endif
                        @endforeach

                        @if($item_count > 0)
                            <tr class="alert-success text-danger">
                                <td colspan="4"></td>
                                <td>Total</td>
                                <td colspan="{{ Auth::user()->hasAnyRole(['Administrator']) ? $colspan + 1 :  $colspan}}">
                                    {{ $total - ($rejected + $completed + $dispatched + $transfer) }}
                                </td>
                            </tr>
                        @endif
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(function() {
            jQuery('#cat_id').change(function() {
                this.form.submit();
            });
        });
    </script>
@endsection
