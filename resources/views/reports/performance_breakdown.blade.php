@extends('layouts.app')

@section('content')
    <script>
        $(document).ready(function () {
            $(function () {
                $('#date_from').duDatepicker({format: 'yyyy-mm-dd', theme: 'teal', auto: true, rangeTo: '#date_to'});
                $('#date_to').duDatepicker({format: 'yyyy-mm-dd', theme: 'teal', auto: true, rangeFrom: '#date_from'});
            });
        });
    </script>
    <style>
        .blink_text {

            animation: 1s blinker linear infinite;
            -webkit-animation: 1s blinker linear infinite;
            -moz-animation: 1s blinker linear infinite;
            /*background-color: darkred;*/
            color: red;
            font-weight: bold;
        }

        @-moz-keyframes blinker {
            0% {
                opacity: 1.0;
            }

            50% {
                opacity: 0.0;
            }

            100% {
                opacity: 1.0;
            }
        }

        @-webkit-keyframes blinker {
            0% {
                opacity: 1.0;
            }

            50% {
                opacity: 0.0;
            }

            100% {
                opacity: 1.0;
            }
        }

        @keyframes blinker {
            0% {
                opacity: 1.0;
            }

            50% {
                opacity: 0.0;
            }

            100% {
                opacity: 1.0;
            }
        }
    </style>
    <div class="uper">
        <div class="shadow-sm p-3 mb-2 bg-white rounded">
            <b class="h4">
                Performance Breakdown
                @if($date == 0)
                    {{ date('Y') }} - {{ date('F')}}
                @endif
                @if($date == 1)
                    {{$date_from}} to {{date('Y-m-d')}}
                @endif
                @if($date == 2)
                    {{$date_from}} to {{$to_date}}
                @endif
            </b>
            <a href="/visualization" class="btn btn-sm btn-outline-info float-right">Performance Visualization</a>
        </div>
        <div class="card mb-2 d-print-none">
            <div class="card-header">
                Filter By Date
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('performance.bd.filter') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="name">From:</label>
                                <input type="text" class="form-control" name="date_from" id="date_from" required
                                       autocomplete=“off” data-rangeto="#date_to"/>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="name">To:</label>
                                <input type="text" class="form-control" name="date_to" id="date_to" autocomplete=“off”
                                       required data-rangefrom="#date_from"/>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-filter"></i> Filter</button>
                    @if($date !== 0)
                        <a href="/reports/performance" class="btn btn-sm btn-success float-right"><i
                                class="fas fa-undo-alt"></i> Reset</a>
                    @endif
                </form>
            </div>
        </div>

        <div>
            <table class="table" id="performance">
                <thead>
                <tr>
                    <th>Employee</th>
                    <th>Plant Name</th>
                    <th>Plant BarCode</th>
                    <th>Cut</th>
                    <th>Rejected</th>
                    <th>Infections %</th>
                    <th>Production Bonus</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    @php
                        $total_cut = 0;
                        $row_count = 0;
                        $item_count = 0;
                        $total_infected =0;
                    @endphp

                    @foreach($performances as $performance)
                        <tr>
                            @if($performance->user_id == $user->id)
                                @php
                                    $item_count++;
                                @endphp

                                @if ($row_count == 0)
                                    <td>{{ $user->user_code }} - {{ $user->name }}</td>
                                    @php
                                        $row_count++;
                                    @endphp

                                @else
                                    <td></td>
                                @endif
                                <td>
                                    {{ $performance->item_name }}
                                </td>
                                <td>
                                    {{ $performance->bar_code }}
                                </td>
                                <td>
                                    {{ $performance->total_quantity }}
                                </td>
                                <td>
                                    {{ $performance->total_rejected_quantity }}
                                </td>
                                <td>
                                </td>
                                <td>
                                    {{ (($performance->total_quantity/2) - $performance->total_rejected_quantity) }}
                                </td>


                                @php
                                    $total_cut += $performance->total_quantity;
                                    $total_infected += $performance->total_rejected_quantity;
                                @endphp

                            @endif
                        </tr>
                    @endforeach

                    @if($item_count > 0)
                        <tr class="alert-success text-danger">
                            <td colspan="2"></td>
                            <td>Total</td>
                            <td>{{ $total_cut }}</td>
                            <td>{{ $total_infected }}</td>
                            <td>
                                @php
                                    if ($total_cut != 0 && $total_infected != 0) {
                                    $percentage = round(($total_infected / $total_cut) *100,2);
                                    } else {
                                    $percentage = 0;
                                    }
                                @endphp
                                {{ $percentage."%" }}
                            </td>
                            <td>{{ (($total_cut / 2) - $total_infected) }}</td>
                        </tr>
                    @endif
                @endforeach

                </tbody>
            </table>

        </div>

    </div>

@endsection
