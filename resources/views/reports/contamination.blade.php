@extends('layouts.app')

@section('content')
    {{--    https://www.daterangepicker.com/--}}
    <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ URL::asset('js/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/daterangepicker/daterangepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            let start = moment().subtract('days', 6);
            let end = moment();

            //Set the initial state of the picker label
            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            $('#reportrange').daterangepicker(
                {
                    startDate: start,
                    endDate: end,
                    minDate: '01/01/2019',
                    maxDate: moment(),
                    dateLimit: { days: 365 },
                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    opens: 'center',
                    buttonClasses: ['btn btn-default'],
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small',
                    format: 'MM/DD/YYYY',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'Submit',
                        fromLabel: 'From',
                        toLabel: 'To',
                        customRangeLabel: 'Custom Range',
                        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        firstDay: 1
                    }
                },
                function(start, end) {
                    console.log("Callback has been called!");
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    $('input[name="fromDate"]').val(start.format('YYYY-MM-DD'));
                    $('input[name="toDate"]').val(end.format('YYYY-MM-DD'));
                }
            );
            cb(start, end);
            $('input[name="fromDate"]').val(start.format('YYYY-MM-DD'));
            $('input[name="toDate"]').val(end.format('YYYY-MM-DD'));
        });
    </script>

    <div class="upper">
        <div class="shadow-sm p-3 mb-2 bg-white rounded">
            <form method="post" action="{{ route('contamination.filter') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <b class="h3">
                            Contamination Report
                        </b>
                        <h5>{{ substr($from, 0,10) }} - {{ substr($to,0,10) }}</h5>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <label for="reportrange">Select Date Range with Option</label>
                        <div class="input-group">
                            <div id="reportrange" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar icon-calendar icon-large"></i>
                                <span></span> <b class="caret"></b>
                            </div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-calendar"></i> Filter</button>
                            </div>
                            <input type="hidden" id="fromDate" name="fromDate" value="">
                            <input type="hidden" id="toDate" name="toDate" value="">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @if(!$growth_room->isEmpty())
        <div class="upper">
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>Cut Date</th>
                        <th>Rejected Date</th>
                        <th>Media Name</th>
                        <th>Plant Name</th>
                        <th>Plant BarCode</th>
                        <th class="text-center">No. of Transfers</th>
                        <th>Reasons</th>
                        <th>Operator</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($growth_room as $gr)
                        <tr>
                            <td>{{ substr($gr->date, 0,10) }}</td>
                            <td>{{ substr($gr->last_updated, 0,10) }}</td>
                            <td>{{ $gr->mediaVariety->name }}</td>
                            <td>{{ $gr->itemVariety->item_name ??  $gr->item }}</td>
                            <td>{{ $gr->itemVariety->bar_code ?? ''}}</td>
                            <td class="text-center">
                                @php
                                    list($week, $transfers, $plants) = explode(' ',$gr->item_serial_no);
                                @endphp
                                {{ $transfers }}
                            </td>
                            <td>
                                @foreach (json_decode($gr->reasons) as $key => $value)
                                    @php
                                        $reason = DB::table('reasons')->find(explode('_',$value)[1]);
                                    @endphp
                                    {{  $reason->name }}<br />
                                @endforeach
                            </td>
                            <td>{{ $gr->userDetail->user_code ?? '' }} - {{ $gr->userDetail->name ?? '' }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif

@endsection
