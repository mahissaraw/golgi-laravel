@extends('layouts.customer')

@section('content')
    <div class="uper">
            <table class="table table-striped table-bordered table-sm" id="cutters_performance">
                <thead class="text-center">
                <tr>
                    <th rowspan="2" class="align-middle">Cutter <br/> Code & Name</th>
                    <th rowspan="2" class="align-middle">Cutter <br/> Experience</th>
                    <th rowspan="2" class="align-middle">{{ date('F, Y', strtotime("$year-$month")) }} <br/> Cut Quantity</th>
                    <th rowspan="2" class="align-middle">{{ date('F, Y', strtotime("$year-$month")) }} <br/> Infected Quantity</th>
                    <th colspan="3">Infections %</th>
                    <th rowspan="2" class="align-middle">Improvement %</th>
                    <th rowspan="2" class="align-middle">Points</th>
                </tr>
                <tr>
                    <th>{{ date('F, Y', strtotime("$year-$month")) }}</th>
                    <th>Last Month</th>
                    <th>2 Months before</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $threeMonths = new \DateTime('3 month ago');
                    $today = date("Y-m-d");
                @endphp
                @foreach($resultList as $key => $value)
                <tr class="{{ strtotime($value['cutter_date']) > strtotime($threeMonths->format("Y-m-d")) ? 'bg-warning' : '' }}">
                    <td>{{ $value['code'] }} - {{ $value['name'] }}</td>
                    <td class="text-center">
                        {{ date_diff(date_create($value['cutter_date']),date_create($today))->format("%y Year %m Months") }}
                        <p class="hidden data">{{ $value['cutter_date'] }}</p>
                    </td>
                    <td class="text-center">{{ $value['cq_this_month'] ?? 0 }}</td>
                    @php
                        $nowIR = 0;
                        $oneIR = 0;
                        $twoIR = 0;

                        if ($value['iq_one_month'] != 0 && $value['cq_one_month'] != 0){
                            $oneIR = round(($value['iq_one_month'] / $value['cq_one_month']) *100, 2) ;
                        }
                        if ($value['iq_two_month'] != 0 && $value['cq_two_month'] != 0){
                            $twoIR = round(($value['iq_two_month'] / $value['cq_two_month']) *100, 2) ;
                        }
                        $ccq = $value['ccq_this_month'] ?? 0;
                        $ccq_this_month = round($ccq * (1 - ($oneIR / 100)));
                        $iq_this_month = $value['iq_this_month'] ?? 0;
                        if ($value['iq_this_month'] != 0 && $value['cq_this_month'] != 0){
                           $nowIR = round((($value['iq_this_month'] - $ccq_this_month) / $value['cq_this_month']) *100, 2) ;
                            //$nowIR = round(($value['iq_this_month'] / $value['cq_this_month']) *100, 2) ;
                        }
                        if ($nowIR <= 3){
                            $tdBg = 'text-success';
                        }elseif ($nowIR <= 10 && $nowIR > 3){
                            $tdBg = 'text-danger';
                        }elseif ($nowIR > 10){
                            $tdBg = "blink_text";
                        }
                    @endphp
                    <td class="text-center">{{ $iq_this_month - $ccq_this_month}}</td>
                    <td class="{{ $tdBg ?? '' }} text-center">
                        {{ $nowIR }} %
                        <p class="hidden data">
                            Cut: {{$value['cq_this_month']}}
                            Reject: {{$value['iq_this_month'] - $ccq_this_month}}
                        </p>
                    </td>
                    <td class="text-center">
                        {{ $oneIR }} %
                        <p class="hidden data">
                            Cut: {{$value['cq_one_month']}}
                            Reject: {{$value['iq_one_month']}}
                        </p>
                    </td>
                    <td class="text-center">
                        {{ $twoIR }} %
                        <p class="hidden data">
                            Cut: {{$value['cq_two_month']}}
                            Reject: {{$value['iq_two_month']}}
                        </p>
                    </td>
                    @php
                        $improvement = 0;
                        if($oneIR != 0 && $twoIR != 0){
                        	$improvement = round((($twoIR - $oneIR)/ $oneIR) *100, 2);
                        }
                    @endphp
                    <td class="text-center">
                        @if($improvement == 0)
                            <i class="fas fa-arrows-alt-v text-info pr-2"></i>
                        @elseif($improvement > 0)
                            <i class="fas fa-caret-up text-success fa-lg pr-2"></i>
                        @elseif($improvement < 0)
                            <i class="fas fa-caret-down text-danger fa-lg pr-2"></i>
                        @endif
                        {{ $improvement }}%
                            <p class="hidden data">
                                ({{$twoIR}}-{{$oneIR}})/{{$oneIR}}
                            </p>
                    </td>
                    @php
                        $points = 0;
                        if ($oneIR <= 3 && $improvement > 0){
                            $points = 100;
                        }
                        if ($oneIR <= 3 && $improvement < 0){
                            $points = 90;
                        }
                        if (($oneIR > 3 && $oneIR <= 4) && $improvement > 0){
                            $points = 80;
                        }
                        if (($oneIR > 3 && $oneIR <= 4) && $improvement <= 0){
                            $points = 70;
                        }
                        if (($oneIR > 4 && $oneIR <= 5) && $improvement > 0){
                            $points = 60;
                        }
                        if (($oneIR > 4 && $oneIR <= 5) && $improvement <= 0){
                            $points = 50;
                        }
                        if (($oneIR > 5 && $oneIR <= 6) && $improvement > 0){
                            $points = 40;
                        }
                        if ($oneIR > 6){
                            $points = 'Fail';
                        }
                        //if ($value['cq_one_month'] < 10000){
                        //	$points = 'Fail';
                        //}
                        @endphp
                    <td class="text-center">{{ $points }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    <div class="row">
        <div class="col-2">
            @if(Auth::user()->isSupperAdmin())
                <form action="{{ route('cutters.performance')}}" method="post">
                    <label for="year_month" class="form-label">Choose Month:</label>
                    <select name="year_month" id="year_month" class="form-control" onchange='this.form.submit()' >
                        <option value="{{ date('Y-m') }}">{{ date('F, Y') }}</option>
                        @for ($i = 1; $i < 6; $i++)
                            <option value="{{ date('Y-m', strtotime("-$i month")) }}" {{ date('Y-m', strtotime("-$i month")) == $year.'-'.$month ? 'selected' : '' }}>{{ date('F, Y', strtotime("-$i month") ) }}</option>
                        @endfor

                    </select>
                    @method('POST')
                    @csrf
                    <noscript><input type="submit" value="Submit"/></noscript>
                </form>
            @endif
            <br/>
            <a href="#" class="show btn btn-sm btn-outline-dark">Elaboration</a>
        </div>
    </div>

    <style>
        p.hidden{
            display: none;
            color: #d64161;
        }
        .blink_text {

            animation: 2s blinker linear infinite;
            -webkit-animation: 2s blinker linear infinite;
            -moz-animation: 2s blinker linear infinite;
            /*background-color: darkred;*/
            color: red;
            /*font-weight: bold;*/
        }

        @-moz-keyframes blinker {
            0% {
                opacity: 1.0;
            }

            50% {
                opacity: 0.0;
            }

            100% {
                opacity: 1.0;
            }
        }

        @-webkit-keyframes blinker {
            0% {
                opacity: 1.0;
            }

            50% {
                opacity: 0.0;
            }

            100% {
                opacity: 1.0;
            }
        }

        @keyframes blinker {
            0% {
                opacity: 1.0;
            }

            50% {
                opacity: 0.0;
            }

            100% {
                opacity: 1.0;
            }
        }
    </style>
    <script>
        $(document).ready(function(){
            $( ".show" ).click(function() {
                if($('p.data').hasClass("hidden")){
                    $('p.data').removeClass("hidden");
                    $( ".show" ).html("Hide Data");
                } else {
                    $('p.data').addClass("hidden");
                    $( ".show" ).html("Elaboration");
                }
            });
        });
    </script>

@endsection
