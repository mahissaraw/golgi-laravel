@extends('layouts.app')

@section('title', 'Plant Bulk By Week')

@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row">
        @foreach($bulks as $bulk)
        <div class="col-md-3 col-sm-6">
            <div class="card bg-white shadow-sm p-3 mb-5 rounded">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title text-center">
                                {{ $bulk->bulk_number }}
                                @if($bulk->plant_size)
                                    <span class="float-right badge badge-info">{{ config('app.plant_sizes')[$bulk->plant_size] }}</span>
                                @endif
                            </h4>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    Created: {{ $bulk->item_qty }}
                                    <span class="float-right">
                                        @if ($bulk->item_qty > $bulk->done_quantity)
                                            In GR: {{ $bulk->item_qty - $bulk->done_quantity }}
                                        @else
                                            <i class="fas fa-check-double fa-lg text-success"></i>
                                        @endif
                                    </span>
                                </li>
                                <li class="list-group-item">Plant: {{ $bulk->item_name }}</li>
                                @if(isset($bulk->name))
                                    <li class="list-group-item">Media: {{ $bulk->name ?? '' }}</li>
                                @endif
                                <li class="list-group-item">{{ $bulk->created_at }}</li>
                            </ul>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <a href="/growthstocks/showbulklabels?bulk_no={{ $bulk->bulk_number }}" class="btn btn-secondary btn-block">Show Bulk</a>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <form action="{{ route('updateBulkNumber')}}" method="post">
                                        <input type="hidden" value="{{ $bulk->bulk_number }}" name="bulk_number" id="bulk_number">
                                        <input type="hidden" value="Completed" name="type" id="type">
                                        <input type="hidden" value="1" name="pc" id="pc">
                                        <input type="hidden" value="true" name="week_bulk" id="week_bulk">
                                        <button type="submit" class="btn btn-outline-danger mt-1 btn-sm" id="basic-addon2">
                                            <i class="far fa-trash-alt"></i> Remove
                                        </button>
                                        @method('POST')
                                        @csrf
                                    </form>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <form action="{{ route('transferBulk')}}" method="post">
                                        <input type="hidden" value="{{ $bulk->bulk_number }}" name="bulk_number" id="bulk_number">
                                        <input type="hidden" value="Completed" name="type" id="type">
                                        <input type="hidden" value="1" name="pc" id="pc">
                                        <input type="hidden" value="true" name="week_bulk" id="week_bulk">
                                        <button type="submit" class="btn btn-outline-primary mt-1 btn-sm float-lg-right " id="basic-addon2">
                                            <i class="fas fa-share-square"></i> Transfer Room
                                        </button>
                                        @method('POST')
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
@endsection
