@extends('layouts.app')

@section('content')
<script>
    $(document).ready(function() {
        $( function() {
            $('#date_from').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, rangeTo: '#date_to'});
            $('#date_to').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, rangeFrom: '#date_from'});
        } );
    });
</script>

<div class="uper">
    <div class="shadow-sm p-3 mb-2 bg-white rounded">
        <b class="h4">
            Plant Stock
            @if($date == 0)
            {{ date('Y') }} - {{ date('F')}}
            @endif
            @if($date == 1)
            {{$date_from}} to {{date('Y-m-d')}}
            @endif
            @if($date == 2)
            {{$date_from}} to {{$to_date}}
            @endif
        </b>
    </div>
    <div class="card mb-2">
        <div class="card-header">
            Filter Options
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('plant_stock.filter') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label for="name">From:</label>
                            <input type="text" class="form-control" name="date_from" id="date_from" required
                                autocomplete=“off” data-rangeto="#date_to" />
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label for="name">Plant Category:</label>
                            <select class="form-control" name="cat_id" id="cat_id">
                                <option value="0">-- Select Plant Category --</option>
                                @foreach ($categories as $key => $value)
                                    <option value="{{ $value->id }}" {{ $value->id == $category ? 'selected' : '' }}>
                                        {{ $value->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label for="name">To:</label>
                            <input type="text" class="form-control" name="date_to" id="date_to" autocomplete=“off”
                                data-rangefrom="#date_from" />
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fas fa-filter"></i> Filter</button>
                @if($date !== 0)
                <a href="/reports/plant_stock" class="btn btn-sm btn-success float-right"><i
                        class="fas fa-undo-alt"></i> Reset</a>
                @endif
            </form>
        </div>
    </div>

    <div>
        <table class="table table-striped" id="performance">
            <thead>
                <tr>
                    <th>Plant Name</th>
                    <th>Plant Code</th>
                    <th class="text-right">Total Available Plants</th>
                    <th class="text-right">Total Rejected Plants</th>
                    <th class="text-right">Total Completed Plants</th>
                </tr>
            </thead>
            <tbody>
                @php
                $total = 0;
                $rejected = 0;
                @endphp
                @foreach ($plants as $plant)
                <tr>
                    <td>{{ $plant->item_name }}</td>
                    <td>{{ $plant->item_code }}</td>
                    <td class="text-right">{{ $plant->total_quantity }}</td>
                    <td class="text-right">{{$plant->total_rejected }}</td>
                    <td class="text-right">{{$plant->total_completed > 0 ? $plant->total_completed : '' }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection
