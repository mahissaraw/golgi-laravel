@extends('layouts.customer')

@section('title', 'Customer Reports')

@section('content')

<div class="card mb-1 offset-md-3 col-md-6">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <img class="img-fluid"
                @if (file_exists( public_path(). '/customer_logo/'.$customer->company->logo))
                    src="/customer_logo/{{ $customer->company->logo ?? 'nologo.png' }}">
                @else
                    src="http://124.43.9.226/customer_logo/{{ $customer->company->logo ?? 'nologo.png' }}" >
                @endif
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="d-flex justify-content-center">
                    <h1>{{ $customer->company->name ?? '' }}</h1>
                </div>
                <p class="text-center"><i class="far fa-building"></i> {{ $customer->company->description}}</p>
            </div>
        </div>
    </div>
</div>
@php $colspan = 3; @endphp
<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <th>Plant Name</th>
                    <th>Plant Code</th>
                    <th>Media Name</th>
                    <th>Week No.</th>
                    <th>Quantity</th>
                    @if(Auth::user()->hasAnyRole(['Administrator']))
                    @php $colspan = 4; @endphp
                    <th>Rejected Quantity</th>
                    <th>Infections %</th>
                    @endif
                </thead>
                <tbody>

                    @foreach($items as $item)
                    @php
                    $total = 0;
                    $rejected = 0;
                    $row_count = 0;
                    $item_count = 0;
                    @endphp

                    @foreach($performances as $performance)
                    @if ($performance->total_quantity > 0 && $performance->total_quantity > $performance->total_rejected_quantity )
                    <tr>
                        @if($performance->item_id == $item->id)
                        @php
                        $item_count++;
                        @endphp

                        @if ($row_count == 0)
                        <td><span class="d-none">{{ $item->id }}</span>{{ $item->item_name }}</td>
                        <td>{{ $item->item_code }}</td>
                        @php
                        $row_count++;
                        @endphp

                        @else
                        <td></td>
                        <td></td>
                        @endif
                        <td>{{ $performance->media_name }}</td>
                        <td>{{ $performance->week }}</td>
                        <td>
                            @if(Auth::user()->hasAnyRole(['Administrator']))
                            {{ $performance->total_quantity }}
                            @else
                            {{ $performance->total_quantity - $performance->total_rejected_quantity }}
                            @endif
                        </td>
                        @if(Auth::user()->hasAnyRole(['Administrator']))
                        <td>{{ $performance->total_rejected_quantity }}</td>
                        <td>{{ round(($performance->total_rejected_quantity/$performance->total_quantity) *100, 2) }}%
                        </td>
                        @endif

                        @php
                        $total += $performance->total_quantity;
                        $rejected += $performance->total_rejected_quantity;
                        @endphp

                        @endif
                    </tr>
                    @endif
                    @endforeach

                    @if($item_count > 0)
                    <tr class="alert-success text-danger">
                        <td colspan="{{ $colspan }}"></td>
                        <td>Total</td>
                        <td colspan="2">{{ $total - $rejected }}</td>
                    </tr>
                    @endif
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
