@extends('layouts.app')

@section('content')
<script>
    $(document).ready(function() {
        $( function() {
            $('#date_from').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, rangeTo: '#date_to'});
            $('#date_to').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, rangeFrom: '#date_from'});
        } );
    });
</script>
<style>
    .blink_text {

        animation: 1s blinker linear infinite;
        -webkit-animation: 1s blinker linear infinite;
        -moz-animation: 1s blinker linear infinite;
        /*background-color: darkred;*/
        color: red;
        font-weight: bold;
    }

    @-moz-keyframes blinker {
        0% {
            opacity: 1.0;
        }

        50% {
            opacity: 0.0;
        }

        100% {
            opacity: 1.0;
        }
    }

    @-webkit-keyframes blinker {
        0% {
            opacity: 1.0;
        }

        50% {
            opacity: 0.0;
        }

        100% {
            opacity: 1.0;
        }
    }

    @keyframes blinker {
        0% {
            opacity: 1.0;
        }

        50% {
            opacity: 0.0;
        }

        100% {
            opacity: 1.0;
        }
    }
</style>
<div class="uper">
    <div class="shadow-sm p-3 mb-2 bg-white rounded">
        <b class="h4">
            Operator Performances
            @if($date == 0)
            {{ date('Y') }} - {{ date('F')}}
            @endif
            @if($date == 1)
            {{$date_from}} to {{date('Y-m-d')}}
            @endif
            @if($date == 2)
            {{$date_from}} to {{$to_date}}
            @endif
        </b>
    </div>
    <div class="card mb-2">
        <div class="card-header">
            Filter By Date
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('reports.filter') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label for="name">From:</label>
                            <input type="text" class="form-control" name="date_from" id="date_from" required
                                autocomplete=“off” data-rangeto="#date_to" />
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label for="name">To:</label>
                            <input type="text" class="form-control" name="date_to" id="date_to" autocomplete=“off”
                                required data-rangefrom="#date_from" />
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fas fa-filter"></i> Filter</button>
                @if($date !== 0)
                <a href="/reports/performance" class="btn btn-sm btn-success float-right"><i
                        class="fas fa-undo-alt"></i> Reset</a>
                @endif
            </form>
        </div>
    </div>

    <div>
        @if(Auth::user()->hasRole('Administrator'))
        <button type="button" class="btn btn-dark btn-sm shadow-sm float-right mb-2" data-toggle="modal"
            data-target="#myModal">
            Earning Formula
        </button>
        @endif
        <table class="table table-striped" id="performance">
            <thead>
                <tr>
                    <th>Operator Code</th>
                    <th>Operator Name</th>
                    <th>Total Plants</th>
                    <th>Rejected Plants</th>
                    <th>Infections %</th>
                    @if(Auth::user()->hasRole('Administrator'))
                    <th>Earned(Rs.)</th>
                    <th>Deductions(Rs.)</th>
                    <th>Balance(Rs.)</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @php
                $total = 0;
                $rejected = 0;
                @endphp
                @foreach ($performances as $performance)

                @php
                $qty = (int)$performance->total_quantity;
                $rej_qty = (int)$performance->total_rejected_quantity;
                @endphp
                <tr {{ $performance->user_active == 0 ? 'class=bg-warning' : '' }}>
                    <td>{{ isset($performance->user_code) ? $performance->user_code : 0 }}</td>
                    <td>{{ isset($performance->user_name) ? $performance->user_name : 0 }}</td>
                    <td>{{ isset($performance->total_quantity) ? $performance->total_quantity : 0 }}</td>
                    <td>{{ isset($performance->total_rejected_quantity) ? $performance->total_rejected_quantity : 0 }}
                    </td>
                    @php
                    $deductions = 0;
                    $range = config('app.incentive_range');
                    if ($performance->total_rejected_quantity != 0 && $performance->total_quantity != 0) {
                    $percentage = round(($performance->total_rejected_quantity/$performance->total_quantity) *100, 2);
                    } else {
                    $percentage = 0;
                    }
                    if ($percentage <= 3) { $class="text-success" ; $deductions=$range['0']; } elseif ($percentage <=10
                        && $percentage> 3) {
                        $class = "text-danger";
                        $deductions = $performance->total_deductions * $range['3'];
                        } elseif ($percentage > 10) {
                        $class = "blink_text";
                        $deductions = $performance->total_deductions * $range['10'];
                        }
                        @endphp
                        <td class="{{$class}}">{{ isset($percentage) ? $percentage."%" : 0 }}</td>
                        @if(Auth::user()->hasRole('Administrator'))
                        <td>{{ isset($performance->total_earned) ? number_format($performance->total_earned, 2, '.', '') : 0 }}
                        </td>
                        <td>{{ isset($deductions) ? "-".number_format($deductions, 2, '.', '') : 0 }}</td>
                        <td>{{ isset($performance->total_earned) && isset($deductions) ? number_format($performance->total_earned - $deductions, 2, '.', '') : 0 }}
                        </td>
                        {{-- <td>
                            <form action="{{ route('operator.achievement')}}" method="post">
                        @csrf
                        @method('POST')
                        <input id="user_id" name="user_id" type="hidden" value="{{$performance->user_code}}">
                        <input id="from_date" name="from_date" type="hidden" value="{{$performance->user_code}}">
                        <input id="to_date" name="to_date" type="hidden" value="{{$performance->user_code}}">
                        <button class="btn btn-outline-info btn-sm" type="submit">View</button>
                        </form>
                        </td> --}}
                        @endif
                </tr>

                @php
                $total += (int)$qty;
                $rejected += (int)$rej_qty;
                @endphp
                @endforeach

                <tr style="border-top: 2px solid black;">
                    <td colspan="2" class=""><b>Total</b></td>
                    <td>{{$total}}</td>
                    <td>{{$rejected}}</td>
                    <td>
                        @if($total != 0)
                        {{number_format($rejected/$total*100, 2, '.', '')}}%
                        @endif
                    </td>
                    @if(Auth::user()->hasRole('Administrator'))
                    <td colspan="3"></td>
                    @endif
                </tr>

            </tbody>
        </table>

    </div>

    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Earning Calculation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body h6">
                    <table class="table thead-light">
                        <thead>
                            <tr>
                                <th>Presentage</th>
                                <th>Deductions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-success">
                                <td>0% - 3%</td>
                                <td>0</td>
                            </tr>
                            <tr class="text-danger">
                                <td>3% - 10%</td>
                                <td>2x</td>
                            </tr>
                            <tr class="blink_text">
                                <td>10% - 100%</td>
                                <td>3x</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection