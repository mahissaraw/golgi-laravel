@extends('layouts.app')

@section('title', 'Plant status')

@section('content')
    <style>
        table.floatThead-table {
            border-top: none;
            border-bottom: none;
            background-color: #fff;
        }

        th {
            position: sticky;
            top: 50px;
            background: white;
        }
        .blink-bg{
            background-color: rgba(255, 100, 120, 0.35);
        }
    </style>
    <div class="shadow-sm p-1 mb-1 bg-white rounded">
        <div class="row">
            <div class="col-8">
                <h3>{{ $category ? $category->name.' - ' : '' }}Plant Status</h3>
            </div>
            <div class="col-4">
                <form method="post" action="{{ route('plant_status.filter') }}" class="form-prevent-multiple-submits">
                    <select class="form-control" name="cat_id" id="cat_id">
                        <option value="null">-- Select Plant Category --</option>
                        @foreach ($categories as $key => $value)
                            <option value="{{ $value->id }}">
                                {{ $value->name }}
                            </option>
                        @endforeach
                    </select>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
<table class="table table-sm sticky-header">
    <thead class="thead-dark">
        <th>Plant Name</th>
        <th>Plant Code</th>
        <th>Plant Barcode</th>
        <th>Week No.</th>
        <th>Quantity</th>
    </thead>
    <tbody>

        @foreach($items as $item)
        @php
        $total = 0;
        $row_count = 0;
        $item_count = 0;
        @endphp

        @foreach($performances as $performance)
        @if ($performance->total_quantity > 0)
        <tr>
            @if($performance->item_id == $item->id)
            @php
            $item_count++;
            @endphp

            @if ($row_count == 0)
            <td>{{ $item->item_name }}</td>
            <td>{{ $item->item_code }}</td>
            <td>{{ $item->bar_code }}</td>
            @php
            $row_count++;
            @endphp

            @else
            <td></td>
            <td></td>
            <td></td>
            @endif
            @php
            $week4_ago = date("Y-m-d", strtotime("Sunday 4 week ago"));

            $date = \Carbon\Carbon::now();
            $date->setISODate($performance->year,$performance->week);

            $plant_week = date("Y-m-d", strtotime($date->endOfWeek()));
            @endphp
            <td {{ $plant_week < $week4_ago ? 'class=blink-bg' : '' }}>
                @if(Auth::user()->isSupperAdmin() || Auth::user()->hasAnyRole(['Administrator']))
                    <a class="btn" style="color: #212529" href="{{ route('weekBulk', [$item->id, $performance->year, $performance->week]) }}" title="Show week bulks">
                        {{ $performance->year}} - {{ $performance->week }}
                    </a>
                @else
                    {{ $performance->week }}
                @endif
            </td>
            <td {{ $plant_week < $week4_ago ? 'class=blink-bg' : '' }}>
                {{ $performance->total_quantity }}
            </td>

            @php
            $total += $performance->total_quantity;
            @endphp

            @endif
        </tr>
        @endif
        @endforeach

        @if($item_count > 0)
        <tr class="alert-success text-danger">
            <td colspan="3"></td>
            <td>Total</td>
            <td>{{ $total }}</td>
        </tr>
        @endif
        @endforeach

    </tbody>
</table>
<script type="text/javascript">
    jQuery(function() {
        jQuery('#cat_id').change(function() {
            this.form.submit();
        });
    });
</script>
@endsection
