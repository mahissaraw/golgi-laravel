@extends('layouts.app')
@section('content')
    @if($selected_item && $selected_customer)
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h3> Selected Customer: {{ $selected_customer->name }} </h3>
            <h4>Selected Item: {{ $selected_item->item_name }}</h4>
            <h5>Selected Item Code: {{ $selected_item->item_code }}</h5>
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            Set Cooperate Company to Plant
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('storeCustomersToPlant') }}" class="form-prevent-multiple-submits">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <label for="name">Company:</label>
                            <select class="form-control" name="customer_id" id="customer_id">
                                <option value="">-- Select Company --</option>
                                @foreach ($customers as $key => $value)
                                    <option value="{{ $value->id }}">
                                        {{ $value->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <label for="name">Plant:</label>
                            <select class="form-control" id='item_id' name='item_id' required>
                                <option value="">-- Select Plant --</option>
                                @foreach ($items as $key => $value)
                                    <option value="{{$value->id}}">
                                        ({{$value->id}}) {{ $value->item_name }} - {{ $value->item_code }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Set</button>
                            {{ csrf_field() }}
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
