@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            Find PP Bag Containers
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('findContainer') }}" class="form-prevent-multiple-submits">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select class="form-control" name="container_id" id="container_id">
                                <option value="0">-- Select PP Bag Container --</option>
                                @foreach ($containers as $key => $value)
                                    <option value="{{ $value->id }}">
                                        {{ $value->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Find</button>
                            {{ csrf_field() }}
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if(isset($selected_container))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Selected Media: {{ $selected_container->name }} </h4>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th>Plant Name</th>
                    <th>Plant Code</th>
                    <th>Plant Barcode</th>
                    <th>Week No.</th>
                    <th>Quantity</th>
                    <th>Rejected Quantity</th>
                    <th>Completed Quantity</th>
                    <th>Infections %</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($performances as $performance)
                    <tr>
                        <td>{{ $performance->item_name }}</td>
                        <td>{{ $performance->item_code }}</td>
                        <td>{{ $performance->bar_code }}</td>
                        <td>{{ $performance->year}} - {{ $performance->week }}</td>
                        <td>{{ $performance->total_quantity }}</td>
                        <td>{{ $performance->total_rejected_quantity }}</td>
                        <td>{{ $performance->total_done_quantity }}</td>
                        <td>{{ round(($performance->total_rejected_quantity/$performance->total_quantity) *100, 2) }}%</td>
                        <td>
                            @if($performance->total_rejected_quantity > 0)
                            <a class="btn btn-sm btn-outline-primary" href="{{ route('containerPlantContamination', [$performance->container_id, $performance->item_id, $performance->year, $performance->week]) }}" title="Show Contamination">
                                Contamination
                            </a>
                                @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
    @endif
@endsection
