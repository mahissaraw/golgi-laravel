@extends('layouts.app')

@section('content')

<div class="col-12">
    <div class="card mb-3">
        <div class="card-header">
            Find Bulk Number
        </div>
        <div class="card-body">
            <form action="{{ route('findBulkNumber')}}" method="post">
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="optradio" {{ $optradio ==  'multiply_rooting' ? 'checked' : ''}}  value="multiply_rooting">Multiply or Rooting
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="optradio" {{ $optradio ==  'completed' ? 'checked' : ''}} value="completed">Completed
                    </label>
                </div>
                <div class="form-check-inline disabled">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="optradio" {{ $optradio ==  'dispatched' ? 'checked' : ''}} value="dispatched">Dispatched
                    </label>
                </div>

                <div class="form-group">
                    <div class="input-group mt-1 mb-1">
                        <input type="number" class="form-control" id="bulk_no" name="bulk_no"
                            placeholder="Enter bulk number" aria-describedby="basic-addon2" autofocus required >
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text" id="basic-addon2">
                                <i class="fas fa-search"></i>
                                Search
                            </button>
                        </div>
                    </div>
                    @method('POST')
                    @csrf
            </form>
        </div>
    </div>
</div>

@if (isset($gr_stocks) || isset($performances))
    <div class="alert-msg"></div>
    <button id="set_rnd" class="btn btn-primary btn-sm ml-5 mb-2 btn-grs" data-bulk-no="{{ $bulk_no }}" data-type="R&D">
        Mark As Multyply R&D
    </button>

    <button id="set_roduction" class="btn btn-success btn-sm ml-3 mb-2 btn-grs" data-bulk-no="{{ $bulk_no }}"
            data-type="Production">
        Mark As Multyply Production
    </button>

    <button id="set_rooting" class="btn btn-danger ml-3 mb-2 btn-sm btn-grs" data-bulk-no="{{ $bulk_no }}" data-type="Rooting">Mark As
        Rooting</button>
    @if ($optradio == 'completed')
    <button id="set_completed" class="btn btn-info ml-3 mb-2 btn-sm btn-grs" data-bulk-no="{{ $bulk_no }}" data-type="Completed">Mark As
        Completed</button>
    @endif
    @if ($optradio == 'dispatched')
    <button id="set_dispatched" class="btn btn-dark ml-3 mb-2 btn-sm btn-grs" data-bulk-no="{{ $bulk_no }}" data-type="Dispatched">Mark As
        Dispatched</button>
    @endif
    <input type="hidden" value="{{ $gr_stocks->count() }}" id="gr_stocks_count" name="gr_stocks_count">
    <input type="hidden" value="{{ $performances->count() }}" id="performances_count" name="performances_count">
@endif
<div class="row">
    @if (isset($gr_stocks) && $gr_stocks->count() > 0)
    <div class="col-sm-6">
        <h3 class="bg-dark text-light">Growth Room Stock #{{ $gr_stocks->count() }}</h3>

        <table class="table">
            @foreach ($gr_stocks as $gr_stock)
                <tr>
                    <td>{{ $gr_stock->bulk_number }}</td>
                    <td>{{ $gr_stock->serial_no }}</td>
                    <td>{{ $gr_stock->type }}</td>
                    <td>{{ $gr_stock->status }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    @endif

    @if (isset($performances) && $performances->count() > 0)
    <div class="col-sm-6">
        <h3 class="bg-warning">Performances #{{ $performances->count() }}</h3>
        <table class="table">
            @foreach ($performances as $performance)
                <tr>
                    <td>{{ $performance->bulk_number }}</td>
                    <td>{{ $performance->item->item_name }}</td>
                    <td>{{ $performance->item_qty }}</td>
                    <td>{{ $performance->rejected_qty }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    @endif
</div>
<script>
    $(document).ready(function(){

        $('.btn-grs').click(function(e){
            e.preventDefault();
            var $_this = $(this);
            var bulk_number = $_this.data("bulk-no");
            var type  = $_this.data("type");
            var loadingText = '<i class="fa fa-spinner fa-spin"></i>  Updating '+type;
            var pc = $("#performances_count").val();
            var gsc = $("#gr_stocks_count").val();

            if ($_this.html() !== loadingText) {
                $_this.data('original-text', $_this.html());
                $_this.html(loadingText).prop('disabled', true);
            }

            $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
            $.ajax({
                method: 'POST',
                url: '/backend/update_bulk_number',
                data: {'bulk_number' : bulk_number, 'type': type, 'pc': pc},
                success: function(response){
                    setTimeout(function() {
                    $_this.html($_this.data('original-text')).prop('disabled', false);
                    }, 2000);
                    $('.alert-msg').prepend(
                        '<div class="alert alert-success qr-alert" role="alert">'+response.success+'</div>'
                    );
                    $(".qr-alert").delay(5000).fadeOut("slow");
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        });

    });

</script>


@endsection
