@extends('layouts.app')

@section('content')
    <div class="upper">
        <div class="shadow-sm p-3 mb-2 bg-white rounded">
            <div class="row text-center">
                <div class="col-md-4">
                    <h3>
                        Contamination History
                    </h3>
                    <h5>{{ substr($from, 0,10) }} to {{ substr($to,0,10) }}</h5>
                </div>
                <div class="col-md-4">
                    <h5>Week {{ $week }}</h5>
                </div>
                <div class="col-md-4">
                    <h3>{{ $selected_plant->item_name }}</h3>
                    <h4>{{ $selected_container->name }}</h4>
                </div>
            </div>
        </div>

            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th>Serial</th>
                    <th>Cut Date</th>
                    <th>Rejected Date</th>
                    <th>Plant Name & BarCode</th>
                    <th>Media</th>
                    <th class="text-center">No. of Plants</th>
                    <th class="text-center">No. of Transfers</th>
                    <th>Reasons</th>
                    <th>Operator</th>
                </tr>
                </thead>
                <tbody>
                @foreach($growth_room as $gr)
                    <tr>
                        <td>{{ $gr->serial_no }}</td>
                        <td>{{ substr($gr->date, 0,10) }}</td>
                        <td>{{ substr($gr->last_updated, 0,10) }}</td>
                        <td>{{ $gr->itemVariety->item_name ??  $gr->item }} - {{ $gr->itemVariety->bar_code ?? ''}}</td>
                        <td>{{ $gr->mediaVariety->name ?? ''}}</td>
                        <td class="text-center">
                            @php
                                list($week, $transfers, $plants) = explode(' ',$gr->item_serial_no);
                            @endphp
                            {{ $plants }}
                        </td>
                        <td class="text-center">{{ $transfers }}</td>
                        <td>
                            @foreach (json_decode($gr->reasons) as $key => $value)
                                @php
                                    $reason = DB::table('reasons')->find(explode('_',$value)[1]);
                                @endphp
                                {{  $reason->name }}<br />
                            @endforeach
                        </td>
                        <td>{{ $gr->userDetail->user_code ?? '' }} - {{ $gr->userDetail->name ?? '' }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
    </div>
@endsection
