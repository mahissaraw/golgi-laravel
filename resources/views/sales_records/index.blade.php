@extends('layouts.app')

@section('content')
<script src="{{ asset('js/bootstrap3-typeahead.min.js') }}"></script>
<div class="row">
    <div class="col-12"><a href="{{ route('visualization.sales') }}" class="btn btn-sm btn-outline-info mb-2 float-right">Sales Record Visualization</a></div>
</div>
<div class="row">
    <h2 class="col-md-5 col-sm-8">
        List of Sales Records
        <a href="{{ route('sales_records.create') }}" class="btn btn-sm btn-secondary ml-2">Add New Sales Record</a>
    </h2>
    <form action="#" method="get" class="col-md-3 col-sm-5 offset-md-4">
        @method('GET')
        @csrf
        <div class="input-group mb-3 float-right">
            <input type="text" name="search_invoice" id="search_invoice" class="form-control typeahead border-primary"
                placeholder="Search Invoice" aria-label="Search Item Name" aria-describedby="basic-addon2"
                autocomplete="off">
            <div class="input-group-append">
                <button class="btn btn-outline-primary" type="submit" id="search_invoice_btn"><i
                        class="fas fa-search"></i></button>
            </div>
        </div>
    </form>
</div>
<div class="uper">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Invoice Date</th>
                <th>Invoice Number</th>
                <th>Customer Name</th>
                <th>Plant</th>
                <th>Quantity</th>
                <th>Amount (LKR)</th>
                <th>Remarks</th>
                <th colspan="2">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sales_records as $sales_record)
            <tr>
                <td>{{ $sales_record->invoice_date }}</td>
                <td>
                    <a href="{{ route('invoiceNumber',$sales_record->invoice_number)}}">{{ $sales_record->invoice_number }}</a>
                </td>
                <td>
                    <a href="{{ route('retail_customers.show',$sales_record->customer->id)}}">{{ $sales_record->customer->id }}
                        - {{ $sales_record->customer->name }}</a>
                </td>
                <td>{{ $sales_record->item->SubCategory->subcat }} - {{ $sales_record->item->item_name }} ({{ $sales_record->item->bar_code }})</td>
                <td>{{ $sales_record->quantity  }}</td>
                <td>{{ $sales_record->amount }}</td>
                <td>{{ \Illuminate\Support\Str::limit(strip_tags($sales_record->remarks), 25, '...') }}</td>
                <td>
                    <a href="{{ route('sales_records.show',$sales_record->id)}}" class="btn btn-success">View</a>
                    <a href="{{ route('sales_records.edit',$sales_record->id)}}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                    <form action="{{ route('sales_records.destroy', $sales_record->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" onclick="return confirm('Are you sure?')"
                            type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $sales_records->links() }}
    <script type="text/javascript">
        var path = "{{ route('sales.autocomplete') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                console.log(data);
                    return process(data);
                });
            }
        });
    </script>
</div>
@endsection
