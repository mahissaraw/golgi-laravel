@extends('layouts.app')

@section('content')
<link href="{{ asset('css/bootstrap4-toggle.min.css') }}" rel="stylesheet">
<script src="{{ asset('js/bootstrap4-toggle.min.js') }}"></script>
<script>
    $(document).ready(function () {
            $(function () {
                $("#invoice_date").duDatepicker({format: 'yyyy-mm-dd', theme: 'teal', auto: true, cancelBtn: true});
            });
        });
</script>
<script type='text/javascript'>
    $(document).ready(function () {

            // sub categories
            $("#cat_id").change(function () {

                var id = $(this).val();

                $('#sel_sub_cat').find('option').not(':first').remove();

                $.ajax({
                    url: '/growthstocks/getsubcategories/' + id,
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        var len = 0;
                        if (response['data'] != null) {
                            len = response['data'].length;
                        }

                        if (len > 0) {
                            // Read data and create <option >
                            for (var i = 0; i < len; i++) {
                                var id = response['data'][i].id;
                                var subcat = response['data'][i].subcat;
                                var option = "<option value='" + id + "'>" + subcat + "</option>";
                                $("#sel_sub_cat").append(option);
                            }
                        }
                    }
                });
            })

            // items (plants)
            $("#sel_sub_cat").change(function () {

                var id = $(this).val();

                $('#sel_item').find('option').not(':first').remove();

                $.ajax({
                    url: '/greenhouses/plants/' + id,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var len = 0;
                        if (response['data'] != null) {
                            len = response['data'].length;
                        }

                        if (len > 0) {
                            // Read data and create <option >
                            for (var i = 0; i < len; i++) {
                                var id = response['data'][i].id;
                                var item_name = response['data'][i].item_name;
                                var bar_code = response['data'][i].bar_code;
                                //if(response['data'][i].active == 1){
                                var option = "<option value='" + id + "'>" + item_name + " ("+bar_code+")</option>";
                                $("#sel_item").append(option);
                                //}

                            }
                        }
                    }
                });
            })
        });

</script>
<div class="container">
    <div class="card">
        <div class="card-header">
            Add Sales Record
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('sales_records.store') }}" class="form-prevent-multiple-submits">
                <div class="form-group">
                    <a class="float-right" href="{{ route('retail_customers.create') }}" target="_blank">Create Customer</a>
                    <label for="customer">Customer : </label>
                    <select class="form-control" name="customer" id="customer" required>
                        <option value="">-- Select Customer --</option>
                        @foreach ($customers as $customer)
                        <option value="{{ $customer->id }}" {{ $customer->id == old('customer')  ? 'selected' : '' }}>
                            {{ $customer->id }} - {{ $customer->name }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Plant Main Category:</label>
                    <select class="form-control" name="cat_id" id="cat_id" required>
                        <option value="">-- Select Category --</option>
                        @foreach ($categories as $key => $value)
                        <option value="{{ $value->id }}">
                            {{ $value->name }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Plant Sub Category:</label>
                    <select class="form-control" id='sel_sub_cat' name='sel_sub_cat' required>
                        <option value=''>-- Select Sub Category --</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Plant:</label>
                    <select class="form-control" id='sel_item' name='sel_item' required>
                        <option value=''>-- Select Plant --</option>
                    </select>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="invoice_number">Invoice Number:</label>
                        <input type="text" class="form-control" id='invoice_number' name="invoice_number" required
                            value="{{ old('invoice_number') }}" />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="invoice_date">Invoice Date:</label>
                        <input type="text" class="form-control" name="invoice_date" id="invoice_date" required
                            placeholder="Pick a Date" autocomplete=“off” data-maxdate="today"
                            value="{{ old('invoice_date') }}" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name">Quantity:</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" required min="1"
                            value="" />
                        <div id="con_out_stock"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="amount">Amount:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">LKR</span>
                            </div>
                            <input type="number" class="form-control" name="amount" id="amount" required step="0.01"
                                value="" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="remarks">Remarks:</label>
                    <input type="text" class="form-control" id='remarks' name="remarks" value="{{ old('remarks') }}" />
                </div>
                <button type="submit" class="btn btn-primary btn-prevent-multiple-submits">Add Sales
                </button>
                <div class="form-check float-right">
                    <label for="stackedCheck1" class="form-check-label" title="Enable this to revert back with last entered values">Multiple Entries</label>
                    <input id="stackedCheck1" class="form-check-input" type="checkbox" data-toggle="toggle" data-on="Enable" data-onstyle="success" name="multiple_entries" data-size="sm">
                </div>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>
<script src="{{ asset('js/submit.js') }}"></script>
@endsection
