@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h2>Sale Record - {{ str_pad($salesRecord->id, 4, '0', STR_PAD_LEFT) }}</h2>
            <table class="table">
                <tbody>
                    <tr>
                        <th>Customer</th>
                        <td>
                            <a href="{{ route('retail_customers.show',$salesRecord->customer->id)}}">
                                {{ $salesRecord->customer->id }} - {{ $salesRecord->customer->name }}
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th>Invoice Number</th>
                        <td><a href="{{ route('invoiceNumber',$salesRecord->invoice_number)}}">{{ $salesRecord->invoice_number }}</a></td>
                    </tr>
                    <tr>
                        <th>Invoice Date</th>
                        <td>{{ $salesRecord->invoice_date }}</td>
                    </tr>
                    <tr>
                        <th>Plant Verity</th>
                        <td>{{ $salesRecord->item->item_code }} - {{ $salesRecord->item->item_name }}</td>
                    </tr>
                    <tr>
                        <th>Quantity</th>
                        <td>{{ $salesRecord->quantity }}</td>
                    </tr>
                    <tr>
                        <th>Amount</th>
                        <td>{{ $salesRecord->amount }} LKR</td>
                    </tr>
                    <tr>
                        <th>Remarks</th>
                        <td>{{ $salesRecord->remarks ?? '-' }}</td>
                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>{{ $salesRecord->created_at }}</td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{ $salesRecord->updated_at }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
