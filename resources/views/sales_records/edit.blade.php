@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="{{ URL::asset('js/charts/loader.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $("#invoice_date").duDatepicker({format: 'yyyy-mm-dd', theme: 'teal', auto: true, cancelBtn: true});
            });
        });
    </script>
    <div class="card">
        <div class="card-header">
            Edit Sales Record - {{ str_pad($salesRecord->id, 4, '0', STR_PAD_LEFT) }}
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('sales_records.update', $salesRecord->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6">
                        <h3>{{ $salesRecord->customer->id }} - {{ $salesRecord->customer->name }}</h3>
                    </div>
                    <div class="col-md-6">
                        <h3>{{ $salesRecord->item->item_code }} - {{ $salesRecord->item->item_name }}</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="invoice_number">Invoice Number:</label>
                        <input type="text" class="form-control" id='invoice_number' name="invoice_number" required
                               value="{{ $salesRecord->invoice_number }}" />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="invoice_date">Invoice Date:</label>
                        <input type="text" class="form-control" name="invoice_date" id="invoice_date" required
                               placeholder="Pick a Date" autocomplete=“off” data-maxdate="today"
                               value="{{ $salesRecord->invoice_date }}" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name">Quantity:</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" required min="1"
                               value="{{ $salesRecord->quantity }}" />
                        <div id="con_out_stock"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="amount">Amount:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">LKR</span>
                            </div>
                            <input type="number" class="form-control" name="amount" id="amount" required step="0.01"
                                   value="{{ $salesRecord->amount }}" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="remarks">Remarks:</label>
                    <input type="text" class="form-control" id='remarks' name="remarks" value="{{ $salesRecord->remarks }}" />
                </div>
                <button type="submit" class="btn btn-primary mt-2">Update</button>
            </form>
        </div>
    </div>
@endsection
