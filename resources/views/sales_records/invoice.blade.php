@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <h3>Invoice Number: {{ $invoice_number ?? '' }}</h3>
                        <h5>{{ $sales_records->sum('amount') ?? '' }} LKR</h5>
                    </div>
                    <div class="col-6 text-right">
                        <h3>Invoice Date: {{ $sales_records[0]->invoice_date }}</h3>
                        <h5><a href="{{ route('retail_customers.show',$sales_records[0]->customer->id)}}">
                                {{ $sales_records[0]->customer->id }} - {{ $sales_records[0]->customer->name }}
                            </a>
                        </h5>
                    </div>
                    <div class="col-12">
                        <table class="table">
                            <thead>
                            <th>Sale Record</th>
                            <th>Plant</th>
                            <th>Quantity</th>
                            <th class="text-right">Amount (LKR)</th>
                            <th colspan="2">Actions</th>
                            </thead>
                            <tbody>
                            @foreach($sales_records as $sales_record)
                                <tr>
                                    <td>{{ $sales_record->id }}</td>
                                    <td>{{ $sales_record->item->SubCategory->subcat }}
                                        - {{ $sales_record->item->item_name }} ({{ $sales_record->item->bar_code }})
                                    </td>
                                    <td class="text-center">{{ $sales_record->quantity  }}</td>
                                    <td class="text-right">{{ $sales_record->amount  }}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a type="button" class="btn btn-success btn-sm"
                                               href="{{ route('sales_records.show',$sales_record->id)}}">View</a>
                                            <a type="button" class="btn btn-primary btn-sm"
                                               href="{{ route('sales_records.edit',$sales_record->id)}}"><i
                                                    class="far fa-edit"></i></a>
                                            <form action="{{ route('sales_records.destroy', $sales_record->id)}}"
                                                  method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger btn-sm"
                                                        onclick="return confirm('Are you sure?')"
                                                        type="submit">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
