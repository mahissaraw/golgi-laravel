@extends('layouts.app')

@section('content')
    <h3>
        List of Companies
        <a href="{{ route('companies.create') }}" class="btn btn-outline-primary ml-5">Create Company</a>
    </h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Logo</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($companies as $company)
            <tr>
                <td>{{ $company->name }}</td>
                <td>{{ $company->description }}</td>
                <td>
                    @if (file_exists( public_path(). '/customer_logo/'.$company->logo))
                        <img src="customer_logo/{{ $company->logo ?? 'nologo.png' }}" height="50px" width="50px" />
                    @else
                        <img src="http://124.43.9.226/customer_logo/{{ $company->logo ?? 'nologo.png' }}" height="50px" width="50px" />
                    @endif
                </td>
                <td>{{ $company->active ? 'Active' : 'Inactive' }}</td>
                <td>
                    <a href="{{ route('companies.edit',$company->id)}}" class="btn btn-primary">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
