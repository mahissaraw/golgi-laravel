@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit Cooperate Company') }} <a href="{{ route('companies.index') }}" class="btn btn-sm btn-outline-primary ml-5 float-right">Companies</a></div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <form method="POST" action="{{ route('companies.update', [$company->id]) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" name="name" required value="{{ $company->name }}"/>
                            </div>
                            <div class="form-group">
                                <label for="description">Description:</label>
                                <input type="text" class="form-control" name="description" required value="{{ $company->description }}"/>
                            </div>
                            <div class="form-group">
                                <label for="active">Status:</label>
                                <select class="form-control" id="active" name="active">
                                    <option value="1" {{ $company->active == 1 ? 'selected' : '' }}>Active</option>
                                    <option value="0" {{ $company->active == 0 ? 'selected' : '' }}>Inactive</option>
                                </select>
                            </div>
                            <div class="custom-file mt-4">
                                <input type="file" class="custom-file-input" name="logo" id="logo">
                                <label class="custom-file-label" for="logo">Choose logo</label>
                            </div>
                            <div class="form-group">
                                @if (file_exists( public_path(). '/customer_logo/'.$company->logo))
                                    <img src="/customer_logo/{{ $company->logo ?? 'nologo.png' }}" width="200px" class="mt-2" />
                                @else
                                    <img src="http://124.43.9.226/customer_logo/{{ $company->logo ?? 'nologo.png' }}" width="200px" class="mt-2"/>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary mt-2 float-right">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>
@endsection
