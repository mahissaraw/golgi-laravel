@extends('layouts.app')

@section('content')
<div class="col-lg-4 col-md-4 col-sm-8 offset-sm-2 offset-md-4 col-12">
    <img class="img-fluid" src="img/golgi_tec_logo.png">
    <div class="account-wall">
        <div class="text-center">
            <h4>Log in to your Golgi<span class="text-success">tec</span> account</h4>
            <img class="profile-img" src="img/lock_icon.svg">
        </div>
        <form method="POST" action="{{ route('login') }}" class="form-signin">
            @csrf
            <div class="form-group">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                    name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">
            </div>
            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
            <div class="form-group">
                <input id="password" type="password"
                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required
                    placeholder="Password">
            </div>
            @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif

{{--            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--            <label class="form-check-label" for="remember">--}}
{{--            {{ __('Remember Me') }}--}}
{{--            </label>--}}

            <button type="submit" class="btn btn-lg btn-success btn-block shadow-sm">
                {{ __('Log in') }}
            </button>

            {{--@if (Route::has('password.request'))--}}
            {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
            {{--{{ __('Forgot Your Password?') }}--}}
            {{--</a>--}}
            {{--@endif--}}

        </form>
    </div>
</div>
@endsection

<style>
    .form-signin {
        max-width: 330px;
        padding: 15px 15px 30px 15px;
        margin: 0 auto;
    }

    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }

    .form-signin .checkbox {
        font-weight: normal;
    }

    .form-signin .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .form-signin .form-control:focus {
        z-index: 2;
    }

    .form-signin input[type="text"] {
        margin-bottom: 10px;
    }

    .form-signin input[type="password"] {
        margin-bottom: 15px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .form-signin .btn {
        /*border-radius: 2rem;*/
        padding: .5rem;
    }

    .account-wall {
        margin-top: 20px;
        padding: 20px 0px 0px 0px;
        background-color: #ffffff;
        -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        border-radius: 10px;
    }

    .profile-img {
        margin: 0 auto 10px;
        display: block;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        width: 96px;
        height: 96px;
    }
</style>
