@extends('layouts.app')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br/>
@endif
<h3>
    List of Medias
    <button type="button" class="btn btn-secondary ml-5" data-toggle="modal" data-target="#addContainerModel">
        Add New Media
    </button>
</h3>
<div class="uper">
    <table class="table table-striped">
        <thead>
        <tr>
            <td>ID</td>
            <td>Media Name</td>
            <td>Media Type</td>
            <td>State</td>
            <td>Action</td>
        </tr>
        </thead>
        <tbody>
        @foreach($medias as $media)
            <tr class="{{ $media->active == 0  ? 'alert-inactive': '' }}">
                <td>{{$media->id}}</td>
                <td>{{$media->name}}</td>
                <td>{{ config('app.media_types')[$media->type] }}</td>
                <td>{{$media->active == 1 ? 'Active' : 'Inactive'}}</td>
                <td><a href="{{ route('medias.edit',$media->id)}}" class="btn btn-primary">Edit</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
<div>
<style>
    .alert-inactive {
        color: #f21e21;
        font-weight: bold;
    }
</style>
<div class="modal fade" id="addContainerModel" tabindex="-1" role="dialog"
     aria-labelledby="addContainerModelTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('medias.store') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="addContainerModelLongTitle">Add New Media</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="media_type">Media Type :</label><br/>
                        @foreach (config('app.media_types') as $key => $value)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="media_type"
                                       value="{{ $key }}">
                                <label class="form-check-label">{{ $value }}</label>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label for="name">Media Name:</label>
                        <input type="text" class="form-control" name="name" required/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
