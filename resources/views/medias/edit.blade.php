@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Media
  </div>
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div><br />
    @endif
    <form method="post" action="{{ route('medias.update', $media->id) }}">
      @method('PATCH')
      @csrf
      <div class="form-group">
        <label for="media_type">Media Type :</label><br />
        @foreach (config('app.media_types') as $key => $value)
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="media_type" value="{{ $key }}"
            {{ $media->type == $key ? "checked" : "" }}>
          <label class="form-check-label">{{ $value }}</label>
        </div>
        @endforeach
      </div>
      <div class="form-group">
        <label for="name">Media Name:</label>
        <input type="text" class="form-control" name="name" value="{{ $media->name }}" />
      </div>
      <div class="form-group">
        <label for="active">Status</label>
        <select class="form-control" id="active" name="active">
          <option value="1" {{ $media->active == 1 ? 'selected' : '' }}>Active</option>
          <option value="0" {{ $media->active == 0 ? 'selected' : '' }}>Inactive</option>
        </select>
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </div>
</div>
@endsection