@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Share
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('medias.store') }}">
          <div class="form-group">
              {{ csrf_field() }}
              <label for="name">Media Name:</label>
              <input type="text" class="form-control" name="media_name" required/>
          </div>
          <div class="form-group">
                <label for="media_type">Media Type :</label><br/>
                @foreach (config('app.media_types') as $key => $value)
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="media_type" value="{{ $key }}">
                        <label class="form-check-label">{{ $value }}</label>
                    </div>
                @endforeach
            </div>
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@endsection
