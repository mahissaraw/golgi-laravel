@extends('layouts.app')

@section('content')

<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="card uper">
    <div class="card-header">
        Edit Plant Container
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="POST" action="{{ route('containers.update', $container->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="name">Container Name</label>
                <input type="text" class="form-control" name="name" required value="{{ $container->name }}" />
            </div>
            <div class="form-group">
                <label for="item_capacity">No of Plants</label>
                <input type="number" class="form-control" name="item_capacity" required
                    value="{{ $container->item_capacity }}" />
            </div>
            <div class="form-group">
                <label for="media_capacity">Media Capacity (ml)</label>
                <input type="number" class="form-control" name="media_capacity" required
                    value="{{ $container->media_capacity }}" />
            </div>

            <div class="form-group">
                <label for="active">Status</label>
                <select class="form-control" id="active" name="active">
                    <option value="1" {{ $container->active == 1 ? 'selected' : '' }}>Active</option>
                    <option value="0" {{ $container->active == 0 ? 'selected' : '' }}>Inactive</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>

@endsection
