@extends('layouts.app')

@section('content')

<h2 class="display-5">Main Category: {{ $category->name }}</h2>
<div class="list-group">
    <a href="#" class="list-group-item list-group-item-dark">Sub Categories</a>
    @foreach ($category->subCategories as $subCategory)
    <a href="#" class="list-group-item list-group-item-action">{{ $subCategory->subcat }}</a>
    @endforeach
</div>

@endsection