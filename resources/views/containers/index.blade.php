@extends('layouts.app')

@section('title', 'Containers')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br/>
    @endif

    <h3>
        List of Plant Containers
        @if(Auth::user()->hasAnyRole(['Administrator']))
            <button type="button" class="btn btn-secondary ml-5" data-toggle="modal" data-target="#addContainerModel">
                Add New Container
            </button>
        @endif
    </h3>
    <div class="col-xs-12 filters">
        <a class="btn btn-sm btn-outline-success" href="/containers">Reset list</a>
        <a class="btn btn-sm btn-outline-info" href="/containers?select_inactive=true">Show Inactive</a>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>No of Plants</th>
            <th>Media Capacity (ml)</th>
            <th>State</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($containers as $container)
            <tr class="{{ $container->active == 0  ? 'alert-inactive': '' }}">
                <td>{{ $container->id }} </td>
                <td>{{ $container->name }}</td>
                <td>{{ $container->item_capacity }}</td>
                <td>{{ $container->media_capacity }}</td>
                <td>{{ $container->active == 1 ? 'Active' : 'Inactive' }}</td>
                <td>
                    <a href="/containers/{{ $container->id}}/edit" class="btn btn-primary">Edit</a>
                    {{--<a href="#" class="btn btn-danger" onclick="--}}
                    {{--var result = confirm('Are you sure you wish to DELETE {{ $container->name }}?');--}}
                    {{--if (result){--}}
                    {{--event.preventDefault();--}}
                    {{--$('#delete-{{ $container->id }}').submit();--}}
                    {{--}">Delete</a>--}}
                    {{--<form id="delete-{{ $container->id }}" action="{{ route('containers.destroy', $container->id)}}"--}}
                    {{--method="POST" style="display:none">--}}
                    {{--@csrf--}}
                    {{--@method('DELETE')--}}
                    {{--<input value="delete" name="_method" type="hidden">--}}
                    {{--</form>--}}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <style>
        .alert-inactive {
            color: #f21e21;
            font-weight: bold;
        }
    </style>
    <div class="modal fade" id="addContainerModel" tabindex="-1" role="dialog"
         aria-labelledby="addContainerModelTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="POST" action="{{ route('containers.store') }}">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addContainerModelLongTitle">Add New Plant Container</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Container Name</label>
                            <input type="text" class="form-control" name="name" required/>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="item_capacity">No of Plants</label>
                            <input type="number" class="form-control" name="item_capacity" required min="1"/>
                            @if ($errors->has('item_capacity'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('item_capacity') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="media_capacity">Media Capacity (ml)</label>
                            <input type="number" class="form-control" name="media_capacity" required min="1"/>
                            @if ($errors->has('media_capacity'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('media_capacity') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
