@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Create Greenhouse Plant Stock for {{ $plant->item_name }}
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <form method="post" action="{{ route('greenhouses.store') }}">
                    @method('POST')
                    @csrf
                    <input type="hidden" id="sel_item" name="sel_item" value="{{ $plant->id }}">
                    <div class="form-group">
                        <h5>Plant Category: {{ $plant->category->name }}</h5>
                    </div>
                    <div class="form-group">
                        <h5>Plant Name: {{ $plant->item_name }}</h5>
                    </div>
                    <div class="form-group">
                        <h5>Plant Code: {{ $plant->item_code }}</h5>
                    </div>
                    <div class="form-group">
                        <label for="task">Task :</label><br/>
                        @foreach (config('app.greenhouse_tasks') as $key => $value)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="task" id="task"
                                       value="{{ $key }}"
                                       required>
                                <label class="form-check-label"><b>{{ $value }}</b></label>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label for="name">Quantity:</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" required
                               min="1" value="{{ old('quantity') }}"/>
                        <div id="con_out_stock"></div>
                    </div>
                    <div class="form-group">
                        <label for="remark">Remark:</label>
                        <input type="text" class="form-control" id='remark' name="remark"
                               value="{{ old('remark') }}"/>
                    </div>
                    <button type="submit" class="btn btn-primary btn-prevent-multiple-submits">
                        Add Stock
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
