@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Edit Greenhouse Plant Stock
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <form method="post" action="{{ route('greenhouses.update', $greenhouse->id) }}">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                        <h5>Plant Name: {{ $greenhouse->item->item_name }}</h5>
                    </div>
                    <div class="form-group">
                        <h5>Plant Code: {{ $greenhouse->item->item_code }}</h5>
                    </div>
                    <div class="form-group">
                        <label for="task">Task :</label><br/>
                        @foreach (config('app.greenhouse_tasks') as $key => $value)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="task" id="task"
                                       value="{{ $key }}"
                                       {{ ($greenhouse->task==$key)? "checked" : "" }}
                                       required>
                                <label class="form-check-label"><b>{{ $value }}</b></label>
                            </div>
                        @endforeach
                        @if($greenhouse->is_sold)
                            <div class="form-check mt-2">
                                <input type="checkbox" class="form-check-input" name="is_sold" id="is_sold" checked>
                                <label class="form-check-label" for="exampleCheck1">Stock Sold</label>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="name">Quantity:</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" required
                               min="1" value="{{ $greenhouse->quantity }}"/>
                        <div id="con_out_stock"></div>
                    </div>
                    <div class="form-group">
                        <label for="remark">Remark:</label>
                        <input type="text" class="form-control" id='remark' name="remark"
                               value="{{ $greenhouse->remark }}"/>
                    </div>
                    <button type="submit" class="btn btn-primary btn-prevent-multiple-submits">
                        Update Stock
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
