@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card uper">
            <div class="card-header">
                Reserve Greenhouse Plant Stock
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <form method="post" action="{{ route('greenhouses.reserve_stock', $plant->id) }}">
                    @method('POST')
                    @csrf
                    <div class="form-group">
                        <h5>Plant Code: {{ $plant->item_code }}</h5>
                        <input type="hidden" id="sel_item" name="sel_item" value="{{ $plant->id }}">
                    </div>
                    <div class="form-group">
                        <h5>Plant Name: {{ $plant->item_name }}</h5>
                    </div>
                    <div class="form-group">
                        <h5>Available Quantity: {{ $available_quantity }}</h5>
                    </div>
                    <div class="form-group">
                        <h5>Reserved Quantity: {{ $reserved_quantity }}</h5>
                    </div>
                    <div class="form-group">
                        <label for="name">Reserve Quantity:</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" required
                               min="1" value="{{ old('quantity') }}"/>
                        <div id="con_out_stock"></div>
                    </div>
                    <div class="form-group">
                        <label for="remark">Remark:</label>
                        <input type="text" class="form-control" id='remark' name="remark" value="{{ old('remark') }}"/>
                    </div>
                    <button type="submit" class="btn btn-primary btn-prevent-multiple-submits">
                        Reserve Stock
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
