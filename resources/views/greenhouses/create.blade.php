@extends('layouts.app')

@section('content')
    <script type='text/javascript'>
        $(document).ready(function () {

            // sub categories
            $("#cat_id").change(function () {

                var id = $(this).val();

                $('#sel_sub_cat').find('option').not(':first').remove();

                $.ajax({
                    url: '/growthstocks/getsubcategories/' + id,
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        var len = 0;
                        if (response['data'] != null) {
                            len = response['data'].length;
                        }

                        if (len > 0) {
                            // Read data and create <option >
                            for (var i = 0; i < len; i++) {
                                var id = response['data'][i].id;
                                var subcat = response['data'][i].subcat;
                                var option = "<option value='" + id + "'>" + subcat + "</option>";
                                $("#sel_sub_cat").append(option);
                            }
                        }
                    }
                });
            })

            // items (plants)
            $("#sel_sub_cat").change(function () {

                var id = $(this).val();

                $('#sel_item').find('option').not(':first').remove();

                $.ajax({
                    url: '/greenhouses/plants/' + id,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var len = 0;
                        if (response['data'] != null) {
                            len = response['data'].length;
                        }

                        if (len > 0) {
                            // Read data and create <option >
                            for (var i = 0; i < len; i++) {
                                var id = response['data'][i].id;
                                var item_name = response['data'][i].item_name;
                                var bar_code = response['data'][i].bar_code;
                                //if(response['data'][i].active == 1){
                                var option = "<option value='" + id + "'>" + item_name + " (" +bar_code+ ")</option>";
                                $("#sel_item").append(option);
                                //}

                            }
                        }
                    }
                });
            })
        });

    </script>
    <script src="{{ asset('js/submit.js') }}"></script>
    <div class="container">
        <div class="card">
            <div class="card-header">
                Add Greenhouse Plant Stock
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <form method="post" action="{{ route('greenhouses.store') }}" class="form-prevent-multiple-submits">
                        <div class="form-group">
                            <label for="task">Task :</label><br/>
                            @foreach (config('app.greenhouse_tasks') as $key => $value)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="task" id="task"
                                           value="{{ $key }}"
                                           required>
                                    <label class="form-check-label"><b>{{ $value }}</b></label>
                                </div>
                            @endforeach
                        </div>
                            <div class="form-group">
                                <label for="name">Plant Main Category:</label>
                                <select class="form-control" name="cat_id" id="cat_id" required>
                                    <option value="">-- Select Category --</option>
                                    @foreach ($categories as $key => $value)
                                        <option value="{{ $value->id }}">
                                            {{ $value->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Plant Sub Category:</label>
                                <select class="form-control" id='sel_sub_cat' name='sel_sub_cat' required>
                                    <option value=''>-- Select Sub Category --</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Plant:</label>
                                <select class="form-control" id='sel_item' name='sel_item' required>
                                    <option value=''>-- Select Item --</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Quantity:</label>
                                <input type="number" class="form-control" name="quantity" id="quantity" required
                                       min="1" value="{{ old('quantity') }}"/>
                                <div id="con_out_stock"></div>
                            </div>
                            <div class="form-group">
                                <label for="remark">Remark:</label>
                                <input type="text" class="form-control" id='remark' name="remark" value="{{ old('remark') }}"/>
                            </div>
                            <button type="submit" class="btn btn-primary btn-prevent-multiple-submits">Add Stock
                            </button>
                        {{ csrf_field() }}

                </form>
            </div>
        </div>
    </div>
@endsection
