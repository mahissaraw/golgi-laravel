@extends('layouts.app')

@section('content')
    <div class="uper">
        <div class="card">
            <div class="card-body">
                <div class="row text-center">
                    <div class="col-sm-4">
                        <p><b>{{ $plant->category->name }}</b> / {{$plant->item_code}}</p>
                        <h2>{{ $plant->item_name }}</h2>
                        <a href="/greenhouses/{{ $plant->id}}/plant_create" class="btn btn btn-success">Add Plant Stock</a>
                        <div class="form-group mt-2">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Select Task
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach (config('app.greenhouse_tasks') as $key => $value)
                                        <a class="dropdown-item" href="/greenhouses/show/{{$plant->id}}/{{$key}}">{{ $value }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <img src="/plant_images/{{ $plant->image ?? 'no_image.png' }}"
                             class="img-fluid img-thumbnail mb-1"/>
                    </div>
                    <div class="col-sm-4">
                        <p>Available Quantity</p>
                        <h1>{{ $available_quantity }}</h1>
                        <p>Reserved Quantity</p>
                        <h3>
                            {{ $reserved_quantity }}
                        </h3>
                        <a href="/greenhouses/{{ $plant->id}}/reserve"
                           class="btn-sm btn btn-danger">Reserve {{ $plant->item_name }} Plant Stock</a>
                    </div>
                </div>
                <div class="row">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Task</th>
                            <th>Quantity</th>
                            <th>Remark</th>
                            <th>Name</th>
                            <th>Created Date</th>
                            <th>Updated Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td>
                                    @if($item->is_sold)
                                        <span class="text-danger">SOLD</span>
                                    @endif
                                    @foreach (config('app.greenhouse_tasks') as $key => $value)
                                        @if($key == $item->task)
                                            {{ strtoupper(substr($value,6)) }}
                                        @endif
                                    @endforeach
                                </td>
                                <td>{{ $item->quantity }}</td>
                                <td>{{ $item->remark }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    @if($item->owenBy(Auth::user()) || Auth::user()->isSupperAdmin())
                                        <a href="/greenhouses/{{ $item->id}}/edit"
                                           class="btn btn-sm btn-primary">Edit</a>
                                        <a href="#" class="btn btn-sm btn-secondary" onclick="
                                            var result = confirm('Are you sure you wish to REMOVE reserved stock {{ $item->quantity }}?');
                                            if (result){
                                            event.preventDefault();
                                            $('#delete-{{ $item->id }}').submit();
                                            }"><i class="far fa-trash-alt"></i> Remove</a>
                                        <form id="delete-{{ $item->id }}"
                                              action="{{ route('greenhouses.destroy', $item->id)}}" method="POST"
                                              style="display:none">
                                            @csrf
                                            @method('DELETE')
                                            <input value="delete" name="_method" type="hidden">
                                        </form>
                                    @endif
                                    @if($item->task == 3)
                                        <a href="/greenhouses/{{ $item->id}}/sell" class=" btn btn-sm btn-warning"><i
                                                class="fa fa-shopping-cart"></i> SELL</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <p>Most recent {{ $items->count() }} events</p>
                </div>
            </div>
        </div>
    </div>
@endsection
