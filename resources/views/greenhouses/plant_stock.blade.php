@extends('layouts.app')

@section('content')
<script>
    $(document).ready(function() {
        $( function() {
            $('#date_from').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, rangeTo: '#date_to'});
            $('#date_to').duDatepicker({format: 'yyyy-mm-dd',theme: 'teal',auto: true, rangeFrom: '#date_from'});
        } );
    });
</script>
<div class="uper">
    <div class="shadow-sm p-3 mb-2 bg-white rounded">
        <b class="h4">
            Greenhouse Plant Stock
            @if($date == 0)
                {{ date('Y') }} - {{ date('F')}}
            @endif
            @if($date == 1)
                {{$date_from}} to {{date('Y-m-d')}}
            @endif
            @if($date == 2)
                {{$date_from}} to {{$to_date}}
            @endif
        </b>
    </div>
    <div class="card mb-2">
        <div class="card-header">
            Filter By Date
            <a href="{{ route('greenhouses.create') }}" class="btn-sm btn btn-success float-right">Create Plant Stock</a>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('greenhouses.plant_stock') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label for="name">From:</label>
                            <input type="text" class="form-control" name="date_from" id="date_from" required
                                   autocomplete=“off” data-rangeto="#date_to" />
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label for="name">To:</label>
                            <input type="text" class="form-control" name="date_to" id="date_to" autocomplete=“off”
                                   data-rangefrom="#date_from" />
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fas fa-filter"></i> Filter</button>
                @if($date !== 0)
                    <a href="{{ route('greenhouses.index') }}" class="btn btn-sm btn-secondary float-right"><i
                            class="fas fa-undo-alt"></i> Reset</a>
                @endif
            </form>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Plant Name</th>
                <th>Plant Code</th>
                <th>Quantity In</th>
                <th>Quantity Out</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($plants as $plant)
            <tr>
                <td>{{ $plant->item_name }}</td>
                <td>{{ $plant->item_code }}</td>
                <td>{{ $plant->quantity_in }}</td>
                <td>{{ $plant->quantity_out }}</td>
                <td>
                    <a href="{{ route('greenhouses.show',$plant->item_id) }}" class="btn-sm btn btn-primary">View</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
