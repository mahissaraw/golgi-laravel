@extends('layouts.app')

@section('content')
<div class="uper">
    <div class="shadow-sm p-3 mb-2 bg-white rounded">
        <b class="h4">
            Greenhouse Plant Available Stock
        </b>
        <a href="{{ route('greenhouses.create') }}" class="btn-sm btn btn-success float-right">Create Plant Stock</a>
    </div>
    <div class="card">

        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Plant Name</th>
                    <th>Plant Barcode</th>
                    <th>Available Quantity</th>
                    <th>Reserved Quantity</th>
                    <th>Draft Balance</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($plants as $plant)
                    <tr>
                        <td data-original-title="222" data-container="body" data-toggle="tooltip" data-placement="right" class="matrisHeader" title="{{ $plant->item_code }}">
                            {{$plant->sub_category }} - {{ $plant->item_name }}
                        </td>
                        <td>{{ $plant->bar_code }}</td>
                        <td>{{ $plant->available_quantity }}</td>
                        <td class="{{ $plant->available_quantity < $plant->quantity_reserved ? 'text-danger' : '' }}" >{{ $plant->quantity_reserved }}</td>
                        <td class="{{ $plant->available_quantity < $plant->quantity_reserved ? 'text-danger' : '' }}" >{{ $plant->available_quantity - $plant->quantity_reserved }}</td>
                        <td>
                            <a href="{{ route('greenhouses.show',$plant->item_id) }}" class="btn-sm btn btn-primary">View</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
    <script>
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
        });
    </script>
@endsection
