@extends('layouts.app')

@section('content')
<div class="d-none d-md-block shadow-sm p-3 mb-2 bg-white rounded">
    <b class="h4">Production Track</b>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                Serial Number <span class="h5">{{ $status_data->serial_no }}</span>
                @if(Auth::user()->printer_id)
                    <button type="button" class="btn btn-sm btn-primary print-labels shadow-sm float-right">
                        <i class="fas fa-print"></i>
                        {{ Auth::user()->printer->name }}
                    </button>
                @else
                    <a class="btn btn-sm float-right btn-outline-dark" href="/users/{{ Auth::user()->id }}">
                        Click To Select Printer
                    </a>
                @endif
                <div class="form-check float-right">
                    <input hidden class="form-check-input label" type="checkbox" value="{{ $status_data->serial_no }}"
                        name="print_labels[]" checked>
                </div>
            </div>
            <div class="card-body">

                @php
                $created_date = new \DateTime($status_data->date);
                list($week, $transfers, $plants) = explode(' ',$status_data->item_serial_no);
                @endphp

                <table class="table table-hover">
                    <tr>
                        <th scope="row">Media Name</th>
                        <td>{{ $status_data->mediaVariety->name }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Container</th>
                        <td>{{ $status_data->containerVariety->name }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Plant Name</th>
                        <td>{{ $status_data->itemVariety->item_name }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Plant BarCode</th>
                        <td>{{ $status_data->itemVariety->bar_code }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Reference Number</th>
                        <td>{{ $status_data->ref_no }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Serial Number</th>
                        <td>{{ $status_data->serial_no }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Cutter</th>
                        <td> {{ $status_data->userDetail->user_code ?? '' }} - {{ $status_data->userDetail->name ?? '' }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Bulk Number</th>
                        <td>
                            {{ $status_data->bulk_number }}
                            <a href="/growthstocks/showbulklabels/{{ $status_data->bulk_number }}"
                                class="btn btn-outline-primary btn-sm float-right">
                                Go to Bulk
                            </a>
                        </td>
                    </tr>
                    @if (App\GrowthRoomStock::getPlantSize($status_data->bulk_number))
                        <tr>
                            <th scope="row">Plant Size</th>
                            <td>
                                {{ config('app.plant_sizes')[App\GrowthRoomStock::getPlantSize($status_data->bulk_number)] }}
                            </td>
                        </tr>
                    @endif
                        <tr>
                            <th scope="row">Status</th>
                            <td>{{ $status_data->status }}</td>
                        </tr>
                    <tr>
                        <th scope="row">Section</th>
                        <td>{{ $status_data->type }}</td>
                    </tr>
                    <tr>
                        <th scope="row">No. Of Transfers</th>
                        <td>{{ $transfers }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Total No. of plants in the batch</th>
                        <td>{{ $status_data->no_plants }}</td>
                    </tr>
                    <tr>
                        <th scope="row">No. of plants per container</th>
                        <td>
                            {{ $plants }}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Total containers in the batch</th>
                        <td>
                            {{ $status_data->no_plants / $plants }}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Parent Plant Serial No.</th>
                        <td>{{ $status_data->parent_id ?? 'No Parent'  }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Created Date</th>
                        <td>{{ $created_date->format("Y-m-d") }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Created Week</th>
                        <td>{{ $created_date->format("W") }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Created by</th>
                        <td> {{ $status_data->operatorDetail->user_code ?? '' }} - {{ $status_data->operatorDetail->name ?? '' }}</td>
                    </tr>
                    <tr>
                        <th scope="row">QR code</th>
                        <td>
                            @if (file_exists(public_path().'/labels/qr/'.$status_data->img_qr))
                            <img src="{{ asset('labels/qr/'.$status_data->img_qr) }}">
                            @else
                            <button type="button" class="btn btn-sm btn-danger generate-qrcode"
                                data-img-qr="{{ $status_data->img_qr }}"
                                data-serial-no="{{ $status_data->serial_no }}">Generate QR Code</button>
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        Update Status
                    </div>
                    <div class="col-6">
                        @if(Auth::user()->isDeveloper())
                            <form action="{{ route('deleteSerialNumber', ['serial_no' =>$status_data->serial_no])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger btn-sm float-right" onclick="return confirm('Are you sure you wanted to DELETE this?')"
                                        type="submit"><i class="far fa-trash-alt"></i> Delete</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if ($status_data->accept == 2)
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Rejected !</h4>
                    <p>This Plant Container Has Been Rejected on {{ $status_data->last_updated }}</p>
                    <hr />
                    @foreach (json_decode($status_data->reasons) as $key => $value)
                    @php
                    $reason = DB::table('reasons')->find(explode('_',$value)[1]);
                    @endphp
                    {{  $reason->name }}<br />
                    @endforeach
                    {{-- @endif --}}
                </div>
                @elseif($status_data->complete == 1)
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">Completed !</h4>
                    <p>This plant container has been mark as completed</p>
                    <hr />
                    <p>Completed Date: {{ $status_data->last_updated }}</p>
                </div>
                @elseif($status_data->complete == 2)
                <div class="alert alert-dark" role="alert">
                    <h4 class="alert-heading">Dispatched !</h4>
                    <p>This plant container has been dispatched</p>
                    <hr />
                    <p>Dispatched Date: {{ $status_data->last_updated }}</p>
                </div>
                @elseif($status_data->flag == 5 || $status_data->flag == 6)
                    <div class="alert alert-warning" role="alert">
                        <h4 class="alert-heading text-uppercase">Handled !</h4>
                        <p>This plant container has been mark as handled</p>
                        <hr />
                        <p>{{ $status_data->flag == 5 ? 'Multiplication' : 'Rooting' }} Date: {{ $status_data->last_updated }}</p>
                        @if(Auth::user()->hasAnyRole(['Administrator']))
                            <a class="btn btn-sm" href="{{ route('revert', $status_data->id) }}">revert</a>
                        @endif
                    </div>
                @else
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <h3>{{ $status_data->type }} - <small>{{ $status_data->status }}</small></h3>
                    </li>
                    @if ($status_data->type === 'Production' && ($status_data->status == 'Multiply' ||
                    $status_data->status == 'Growth Room'))
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('transfertotransferroom', [$status_data->id])}}'"
                            class="btn btn-primary btn-lg btn-block">
                            <i class="fas fa-share-square"></i> Transfer Room
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('complete', $status_data->id)}}'"
                            class="btn btn-success btn-lg btn-block">
                            <i class="fas fa-check"></i> Complete
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button type="button" class="btn btn-danger btn-lg btn-block reject-button" data-toggle="modal"
                            data-target="#rejectModal" data-table-id="{{$status_data->serial_no}}">
                            <i class="far fa-times-circle"></i> Reject
                        </button>
                    </li>
                    @endif

                    @if ($status_data->type === 'Production' && $status_data->status == 'Transfer Room' )
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('createmultiplication', $status_data->id)}}'"
                            class="btn btn-secondary btn-lg btn-block">
                            <i class="fas fa-layer-group"></i> Multiply
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('transfertogrowthroom', $status_data->id)}}'"
                            class="btn btn-primary btn-lg btn-block">
                            <i class="fas fa-reply"></i> Return to Growth Room
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('createrooting',$status_data->id)}}'"
                            class="btn btn-success btn-lg btn-block">
                            <i class="fas fa-microscope"></i> Rooting
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button type="button" class="btn btn-danger btn-lg btn-block reject-button" data-toggle="modal"
                            data-target="#rejectModal" data-table-id="{{$status_data->id}}">
                            <i class="far fa-times-circle"></i> Reject
                        </button>
                    </li>
                    @endif

                    @if ($status_data->type === 'Production' && $status_data->status == 'Rooting' )
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('transfertotransferroom', [$status_data->id])}}'"
                                class="btn btn-primary btn-lg btn-block">
                            <i class="fas fa-share-square"></i> Transfer Room
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('dispatch',$status_data->id)}}'"
                            class="btn btn-dark btn-lg btn-block">
                            <i class="fas fa-file-export"></i> Dispatch
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button type="button" class="btn btn-danger btn-lg btn-block reject-button" data-toggle="modal"
                            data-target="#rejectModal" data-table-id="{{$status_data->id}}">
                            <i class="far fa-times-circle"></i> Reject
                        </button>
                    </li>
                    @endif

                    @if ($status_data->type === 'R&D' && ($status_data->status == 'Multiply' || $status_data->status =='Growth Room'))
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('transfertotransferroom',[$status_data->id])}}'"
                            class="btn btn-primary btn-lg btn-block">
                            <i class="fas fa-microscope"></i> R&D Transfer Room
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('transfertoproductionroom', [$status_data->id])}}'"
                            class="btn btn-success btn-lg btn-block">
                            <i class="fas fa-seedling"></i> Production Growth Room
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('transfertotransferroom', [$status_data->id, 2])}}'"
                            class="btn btn-info btn-lg btn-block">
                            <i class="fas fa-share-square"></i> Production Transfer Room
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button type="button" class="btn btn-danger btn-lg btn-block reject-button" data-toggle="modal"
                            data-target="#rejectModal" data-table-id="{{$status_data->id}}">
                            <i class="far fa-times-circle"></i> Reject
                        </button>
                    </li>
                    @endif

                    @if ($status_data->type === 'R&D' && $status_data->status == 'Transfer Room')
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('createmultiplication', [$status_data->id])}}'"
                            class="btn btn-secondary btn-lg btn-block">
                            <i class="fas fa-layer-group"></i> Multiply
                        </button>
                    </li>
{{--                    <li class="list-group-item">--}}
{{--                        <button--}}
{{--                            onclick="location.href='{{ route('createbulkmultiplication', [$status_data->bulk_number])}}'"--}}
{{--                            class="btn btn-secondary btn-lg btn-block">--}}
{{--                            <i class="fas fa-layer-group"></i> Bulk Multiply--}}
{{--                        </button>--}}
{{--                    </li>--}}
                    <li class="list-group-item">
                        <button onclick="location.href='{{ route('transfertogrowthroom',$status_data->id)}}'"
                            class="btn btn-primary btn-lg btn-block">
                            <i class="fas fa-undo"></i> Return to Growth Room
                        </button>
                    </li>
                    <li class="list-group-item">
                        <button type="button" class="btn btn-danger btn-lg btn-block reject-button" data-toggle="modal"
                            data-target="#rejectModal" data-table-id="{{$status_data->id}}">
                            <i class="far fa-times-circle"></i> Reject
                        </button>
                    </li>
                    @endif
                </ul>
                @endif

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".generate-qrcode").click(function(e){
            e.preventDefault();
            var btnQr = $(this);
            var serial_no = $(this).data("serial-no");
            var qr_image_name  = $(this).data("img-qr");
            $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
            $.ajax({
                method: 'POST',
                url: '/growthstocks/createqrcode',
                data: {'serial_no' : serial_no, 'qr_image_name': qr_image_name },
                success: function(response){
                    btnQr.hide();
                    btnQr.parent().append().html(
                        '<div class="alert alert-success qr-alert" role="alert">'+response.success+'</div>'+
                        '<img src="{!! asset("'+response.img+'") !!}">'
                    );
                    $(".qr-alert").delay(1000).fadeOut("slow");
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        });

        $(".print-labels").click(function(e){
            e.preventDefault();
            var $this = $(this);
            var serial_nos = [];
            var loadingText = '<i class="fas fa-circle-notch fa-spin"></i>  Printing....';

            if ($(this).html() !== loadingText) {
                $this.data('original-text', $(this).html());
                $this.html(loadingText);
            }

            $.each($("input[name='print_labels[]']:checked"), function(){
                serial_nos.push($(this).val());
            });

            $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
            $.ajax({
                method: 'POST',
                url: '/growthstocks/printlabels',
                data: {'serial_nos' : serial_nos},
                success: function(response){
                    setTimeout(function() {
                    $this.html($this.data('original-text'));
                    }, 2000);

                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        });

    });
</script>
<style>
    .btn-lg {
        height: 80px;
    }
</style>
@include('partials.reject')
@endsection
