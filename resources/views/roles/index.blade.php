@extends('layouts.app')

@section('content')
    <h3>
        List of Users
        <a href="{{ route('roles.create')}}" class="btn btn-secondary ml-5 add-reason">Create New Role</a>
    </h3>
    <div class="uper">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Name</td>
                <td colspan="2">Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($roles as $role)
                <tr>
                    <td>{{$role->name}}</td>
                    <td>
                        <a href="{{ route('roles.edit', $role->id)}}" class="btn btn-primary">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <div>
@endsection
