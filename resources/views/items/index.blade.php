@extends('layouts.app')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
@endif
<div class="row">
    <h2 class="col-md-5 col-sm-8">
        List of Plants
        <button type="button" class="btn btn-secondary ml-5" data-toggle="modal" data-target="#addContainerModel">
            Add New Plant
        </button>
    </h2>
    @include('partials.plant_search')
</div>
<div class="col-xs-12 filters">
    <a class="btn btn-sm btn-outline-success" href="/items">Reset list</a>
    <a class="btn btn-sm btn-outline-info" href="/items?select_inactive=true">Show Inactive</a>
    <a class="btn btn-sm btn-outline-dark" href="/items?select_greenhouse=true">Show Greenhouse Plants</a>
</div>
<div class="uper">
    <table class="table table-striped" id="items-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Plant Name</th>
                <th>Plant Code</th>
                <th>Plant Barcode</th>
                <th>Rate(Rs.)</th>
                <th>Greenhouse</th>
                <th>Picture</th>
                <td></td>
            </tr>
        </thead>
        <tbody>
            @foreach($items as $item)
            <tr class="{{ $item->active == 0  ? 'text-danger': '' }}">
                <td>{{$item->id}}</td>
                <td>{{$item->category->name}}</td>
                <td>{{$item->SubCategory->subcat }}</td>
                <td>{{$item->item_name }}</td>
                <td>{{$item->item_code }}</td>
                <td>{{$item->bar_code }}</td>
                <td>{{ number_format((float)$item->co_owner, 2) }}</td>
                <td>
                    @if($item->greenhouse == 1)
                        <i class="ml-4 far fa-lg fa-check-circle text-success"></i>
                    @endif
                </td>
                <td>
                    @if($item->image)
                        @if (file_exists( public_path(). '/plant_images/'.$item->image))
                            <img src="/plant_images/{{ $item->image }}" width="50px"/>
                        @else
                            <img src="http://124.43.9.226/plant_images/{{ $item->image }}" width="50px"/>
                        @endif
                    @endif
                </td>
                <td><a href="/items/{{ $item->id}}/edit" class="btn btn-primary">Edit</a></td>
{{--                <td>--}}
                    {{--<a href="#" class="btn btn-danger" onclick="--}}
                    {{--var result = confirm('Are you sure you wish to DELETE {{ $item->name }}?');--}}
                    {{--if (result){--}}
                    {{--event.preventDefault();--}}
                    {{--$('#delete-{{ $item->id }}').submit();--}}
                    {{--}">Delete</a>--}}
                    {{--<form id="delete-{{ $item->id }}" action="{{ route('items.destroy', $item->id)}}"
                    method="POST"--}}
                    {{--style="display:none">--}}
                    {{--@csrf--}}
                    {{--@method('DELETE')--}}
                    {{--<input value="delete" name="_method" type="hidden">--}}
                    {{--</form>--}}
{{--                </td>--}}
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="modal fade" id="addContainerModel" tabindex="-1" role="dialog"
     aria-labelledby="addContainerModelTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('items.store') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="addContainerModelLongTitle">Add New Plant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Select Main Category</label>
                        <select class="form-control" name="cat_id" id="cat_id" required>
                            <option value="">-- Select Category --</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Select Sub Category</label>
                        <select class="form-control" name="sub_cat_id" id="sub_cat_id" required>
                            <option value="">-- Select Sub Category --</option>
                            @foreach ($subcategories as $subcategory)
                                <option value="{{ $subcategory->id }}">{{ $subcategory->category->name }} - {{ $subcategory->subcat }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Plant Name</label>
                        <input type="text" class="form-control" name="item_name" required />
                    </div>
                    <div class="form-group">
                        <label for="name">Plant Code</label>
                        <input type="text" class="form-control" name="item_code" required />
                    </div>
                    <div class="form-group">
                        <label for="name">Plant Barcode</label>
                        <input type="text" class="form-control" name="bar_code" required />
                    </div>
                    <div class="form-group">
                        <label for="name">Rate(Rs.)</label>
                        <input type="number" min="0" step="0.01" class="form-control" name="co_owner" value="0" required />
                    </div>
                    <div class="form-group">
                        <label for="greenhouse">Greenhouse</label>
                        <select class="form-control" id="greenhouse" name="greenhouse">
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                        </select>
                    </div>
                    <div class="custom-file mb-2 mt-4">
                        <input type="file" class="custom-file-input" name="image" id="image">
                        <label class="custom-file-label" for="image">Choose Plant Image</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function(){
            $('input[name="gp[]"]').change(function () {
                if($('input[name="gp[]"]').is(":checked")){
                    a
                }
            });
        });
    </script>
@endsection
