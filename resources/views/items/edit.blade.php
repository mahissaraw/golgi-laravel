@extends('layouts.app')

@section('content')
<div class="card uper">
    <div class="card-header">
        Edit Item
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('items.update', $item->id) }}" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="name">Category Name:</label>
                <select class="form-control" name="cat_id" required>
                    <option value="">-- Select Category --</option>
                    @foreach ($categories as $category)
                    <option {{ $category->id == $item->cat_id  ? 'selected' : ''}} value="{{ $category->id }}" {{ !$category->active ? 'disabled' : ''}}>
                        {{ $category->name }}
                    </option>
                    @endforeach
                </select>

            </div>
            <div class="form-group">
                <label for="name">Sub Category Name:</label>
                <select class="form-control" name="sub_cat" required>
                    <option value="">-- Select Category --</option>
                    @foreach ($subcategories as $subcategory)
                    <option {{ $subcategory->id == $item->sub_cat ? 'selected' : ''}} value="{{ $subcategory->id }}" {{ !$subcategory->category->active || !$subcategory->active ? 'disabled' : ''}}>
                        {{ $subcategory->category->name }} - {{ $subcategory->subcat }}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Plant Name:</label>

                <input type="text" class="form-control" name="item_name" value="{{ $item->item_name }}" />
            </div>
            <div class="form-group">
                <label for="name">Plant Code:</label>

                <input type="text" class="form-control" name="item_code" value="{{ $item->item_code }}" />
            </div>
            <div class="form-group">
                <label for="active">Status</label>
                <select class="form-control" id="active" name="active">
                    <option value="1" {{ $item->active == 1 ? 'selected' : '' }}>Active</option>
                    <option value="0" {{ $item->active == 0 ? 'selected' : '' }}>Inactive</option>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Bar Code:</label>
                <input type="text" class="form-control" name="bar_code" value="{{ $item->bar_code }}" />
            </div>
            <div class="form-group">
                <label for="name">Rate(Rs.):</label>

                <input type="number" min="0" step="0.01" class="form-control" name="co_owner"
                    value="{{ $item->co_owner }}" />
            </div>
            <div class="form-group">
                <label for="greenhouse">Greenhouse</label>
                <select class="form-control" id="greenhouse" name="greenhouse">
                    <option value="1" {{ $item->greenhouse == 1 ? 'selected' : '' }}>Enabled</option>
                    <option value="0" {{ $item->greenhouse == 0 ? 'selected' : '' }}>Disabled</option>
                </select>
            </div>
            <div class="row">
                <div class="col-7">
                    <div class="custom-file mb-2 mt-4">
                        <input type="file" class="custom-file-input" name="image" id="image">
                        <label class="custom-file-label" for="image">Choose Plant Image</label>
                    </div>
                </div>
                <div class="col-3">
                    <img src="/plant_images/{{ $item->image ?? 'no_image.png' }}" width="120px" class="mt-2" />
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
@endsection
