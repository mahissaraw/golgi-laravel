<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    protected $table = 'container';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'item_capacity',
        'media_capacity'
    ];

    /**
     * @param int $id
     * @return mixed
     * @deprecated Container no longer look for item_capacity
     * ToDo: remove this function
     */
    public function getItemCapacity($id)
    {
        $container = $this->where('id', $id)->first();
        return $container->item_capacity;
    }
}
