<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';
    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'item_id',
        'quantity',
        'list_price',
        'discount'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }
}
