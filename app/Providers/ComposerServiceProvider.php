<?php

namespace App\Providers;

use App\Reason;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer('partials.reject', function ($view) {
            $view->with('reasons', Reason::all());
        });
    }
}
