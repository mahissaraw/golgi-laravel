<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'item';
    public $timestamps = false;

    protected $fillable = [
        'cat_id',
        'sub_cat',
        'item_name',
        'item_code',
        'bar_code',
        'co_owner',
        'image'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category', 'cat_id');
    }

    public function subCategory()
    {
        return $this->belongsTo('App\SubCategory', 'sub_cat');
    }

    public function getRate($id)
    {
        $item = $this->where('id', $id)->first();
        return (float) $item->co_owner;
    }

    public function order_items()
    {
        return $this->hasMany('App\OrderItem', 'item_id');
    }

    public function performance()
    {
        return $this->hasMany('App\Performance', 'item_id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeInActive($query)
    {
        return $query->where('active', 0);
    }

    public function scopeGreenhouse($query)
    {
        return $query->where('greenhouse', 1);
    }
}
