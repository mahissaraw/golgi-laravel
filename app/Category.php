<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    public $timestamps = false;

    protected $fillable = ['name'];

    public function subCategories()
    {
        return $this->hasMany('App\SubCategory', 'cat_id');
    }

    public function items()
    {
        return $this->hasMany('App\Item', 'cat_id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeInActive($query)
    {
        return $query->where('active', 0);
    }
}
