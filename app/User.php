<?php

namespace App;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_code', 'active', 'is_customer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }

    public function performance()
    {
        return $this->hasMany('App\Performance', 'user_id');
    }

    public function printer()
    {
        return $this->belongsTo('App\Printer', 'printer_id');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($role)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function getUserCode($id)
    {
        $user = $this->where('id', $id)->first();
        return $user->user_code;
    }

    public function getUserByUserCode($user_code)
    {
        return $this->where('user_code', $user_code)->first();
    }

    public function isSupperAdmin()
    {
        $users = array(
            'admin@golgitec.com',
//            'karin@golgitec.com'
        );
        if (in_array(auth()->user()->email, $users) || self::isDeveloper()){
            return true;
        } else {
            return false;
        }
    }

    public function isDeveloper(): bool
    {
        $users = array(
            'chinthaka@tailorstore.com'
        );
        if (in_array(auth()->user()->email, $users)){
            return true;
        } else {
            return false;
        }
    }

    public function isGreenhouseUser()
    {
        $users = array(
            'rohitha@golgitec.com',
        );
        if (in_array(auth()->user()->email, $users)){
            return true;
        } else {
            return false;
        }
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeInActive($query)
    {
        return $query->where('active', 0);
    }
}
