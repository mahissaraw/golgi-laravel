<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Performance extends Model
{
    protected $table = 'performance';
    public $timestamps = true;

    protected $fillable = [
        'bulk_number',
        'user_id',
        'item_id',
        'media_id',
        'container_id',
        'max_container_capacity',
        'total_containers',
        'item_qty',
        'rejected_qty',
        'add_amount',
        'rejected_amount',
        'order_id',
        'customer_id',
        'status',
        'done_status',
        'done_qty',
        'plant_size'
    ];

    /**
    * status
    * 1 - multiply
    * 2 - rooting
    * 3 - completed
    * 4 - dispatched
    */

    /**
     * this will help to identify (deduct bulk amounts) bulk
     * that are partially completed or dispatched
     * done_status
     * 1 - completed
     * 2 - dispatched
     * 3 - transferred
     */

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    public function media()
    {
        return $this->belongsTo('App\Media', 'media_id');
    }

    public function container()
    {
        return $this->belongsTo('App\Container', 'container_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'customer_id');
    }
}
