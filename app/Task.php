<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'user_id',
        'description',
        'date_started',
        'date_completed',
        'frequency_id',
        'is_complete'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function frequency()
    {
        return $this->belongsTo('App\Frequency', 'frequency_id');
    }

}