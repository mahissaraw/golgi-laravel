<?php

namespace App\Http\Controllers;

use App\Container;
use App\MediaRequest;
use App\MediaStock;
use App\Media;
use App\User;
use App\Reason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use phpDocumentor\Reflection\Types\Self_;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;

class MediaStockController extends Controller
{
    public function index(Request $request)
    {
//        $mediastocks = DB::select('SELECT
//                        transfer_room_stock.id,
//                        transfer_room_stock.ref_no AS ref,
//                        transfer_room_stock.media AS med,
//                        transfer_room_stock.container AS con,
//                        transfer_room_stock.m_type,
//                        transfer_room_stock.date,
//                        t_room_stock.ref_no,
//                        o.name as operator,
//                        o.user_code as operator_code,
//                        u.name as user_name,
//                        u.user_code as user_code,
//                        media.name AS media,
//                        container.name AS container,
//                        (SUM(t_room_stock.dr)-SUM(t_room_stock.cr)) AS qty
//                        FROM
//                        transfer_room_stock
//                        INNER JOIN t_room_stock ON transfer_room_stock.ref_no = t_room_stock.ref_no
//                        INNER JOIN media ON transfer_room_stock.media = media.id
//                        INNER JOIN container ON transfer_room_stock.container = container.id
//                        INNER JOIN `users` u ON transfer_room_stock.`user_id` = u.id
//                        INNER JOIN `users` o ON transfer_room_stock.`operator_id` = o.id
//                        WHERE accept=1
//                        GROUP BY med, con, `user`, ref
//                        HAVING (SUM(t_room_stock.dr)-SUM(t_room_stock.cr)) > 0
//                        ORDER BY `ref_no` ASC');
        $mediastocks = DB::table('transfer_room_stock')
            ->join('users as u', 'transfer_room_stock.user_id', '=', 'u.id')
            ->join('users as o', 'transfer_room_stock.operator_id', '=', 'o.id')
            ->join('container', 'transfer_room_stock.container', '=', 'container.id')
            ->join('media', 'transfer_room_stock.media', '=', 'media.id')
            ->join('t_room_stock', 'transfer_room_stock.ref_no', '=', 't_room_stock.ref_no')
            ->select('transfer_room_stock.id',
                'transfer_room_stock.ref_no AS ref',
                'transfer_room_stock.media AS med',
                'transfer_room_stock.container AS con',
                'transfer_room_stock.m_type',
                'transfer_room_stock.date',
                't_room_stock.ref_no',
                'o.name as operator',
                'o.user_code as operator_code',
                'u.name as user_name',
                'u.user_code as user_code',
                'media.name AS media',
                'container.name AS container',
                DB::raw('(SUM(t_room_stock.dr)-SUM(t_room_stock.cr)) AS qty'))
            ->where('accept','=', 1)
            ->groupBy(DB::raw('med, con, transfer_room_stock.user_id, ref'))
            ->havingRaw('SUM(t_room_stock.dr) - SUM(t_room_stock.cr) > ?', [0])
            ->orderByRaw('t_room_stock.ref_no ASC')
            ->get();

        $reasons = Reason::all();
        return view('mediastocks.index', compact('mediastocks', 'reasons'));
    }

    public function create()
    {
        $admin_user = auth()->user()->hasRole('Administrator');
        if (!$admin_user) {
            $role = 'Operator';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '=', $role);
            })->where('active', '=', '1')->pluck('name', 'id');
        } else {
            $users = User::where('active', '=', '1')->pluck('name', 'id');
        }
        return view('mediastocks.create', compact('users'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'sel_type' => 'required|integer',
            'sel_media' => 'required',
            'sel_container' => 'required',
            'container_qty' => 'required|integer'
        ]);
        $user = new User();
        $media_stock = new MediaStock([
            'media' => $request->get('sel_media'),
            'm_type' => $request->get('sel_type'),
            'container' => $request->get('sel_container'),
            'status' => 'Production Room',
            'ref_no' => self::getRefNo(),
            'user' => $user->getUserCode($request->get('user_id')),
            'operator' => $request->get('operator') ?? Auth::user()->name,
            'user_id' => $request->get('user_id'),
            'operator_id' => $request->get('operator_id') ?? Auth::user()->id,
            'date' => Carbon::now()->toDateTimeString(),
            'qr_img' => 'hey'
        ]);
        if ($media_stock->save()) {
            // update ref table with new ref id
            self::updateRefNo($media_stock->ref_no);

            // insert media stock quantity in a separate table
            DB::table('t_room_stock')->insert(
                [
                    'ref_no' => $media_stock->ref_no,
                    'dr' => $request->get('container_qty'),
                    'cr' => 0,
                    'created_at' => Carbon::now()->toDateTimeString()
                ]
            );

            if ($request->get('media_request')){
                $mr = MediaRequest::find($request->get('media_request'));
                $mr->confirmed = Carbon::now()->toDateTimeString();
                if (is_array($mr->ref_no)){
                    $mr->ref_no = array_push($mr->ref_no, $media_stock->ref_no);
                } else {
                    $mr->ref_no = [$media_stock->ref_no];
                }
                $mr->save();
                return redirect('/media_requests/'.$mr->id)->with('success', 'Media Stock has been added Successfully!');
            }
        }

        return redirect('/mediastocks')->with('success', 'Media Stock has been added');
    }

    public function edit($id)
    {
        $media = Media::find($id);

        return view('medias.edit', compact('media'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'media_name' => 'required',
            'media_type' => 'required|integer'
        ]);

        $media = Media::find($id);
        $media->name = $request->get('media_name');
        $media->type = $request->get('media_type');
        $media->save();

        return redirect('/medias')->with('success', 'Media has been updated');
    }

    public function destroy($id)
    {
        $media = Media::find($id);
        $media->delete();

        return redirect('/medias')->with('success', 'Media has been deleted Successfully');
    }

    public function reject(Request $request)
    {
        $reasons    = json_encode($request->input('reasons'));
        $ref_no     = $request->input('batch_id');
        $qty_remove = $request->input('qty_remove');

        if ($ref_no && $reasons && $qty_remove) {

            $available = DB::select('SELECT (SUM(t_room_stock.dr)-SUM(t_room_stock.cr)) AS amount FROM t_room_stock WHERE ref_no = ' . $ref_no);

            if ($available[0]->amount >= $qty_remove) {
                DB::table('t_room_stock')->insert(
                    [
                        'ref_no' => $ref_no,
                        'reasons' => $reasons,
                        'cr' => $qty_remove,
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]
                );
                return redirect('/mediastocks')->with('success', 'Reject ' . $qty_remove . ' Containers from Batch No: ' . $ref_no);
            } else {
                return back()->with('error', 'Could Not REJECT! Available container quantity is less than entered amount');
            }
        }
        return back()->with('error', 'Could not remove Containers from Batch');
    }

    /**
     * Get available container amount by reg id
     *
     * @param $ref_id
     * @return int
     */
    public function getContainerAmount($ref_id)
    {

        $results = DB::select(
            'SELECT
                    SUM(t_room_stock.dr)-SUM(t_room_stock.cr) AS qty
                    FROM t_room_stock
                    WHERE ref_no = :ref_no
                    GROUP BY ref_no
                    HAVING (SUM(t_room_stock.dr)-SUM(t_room_stock.cr)) > 0
                    ORDER BY ref_no DESC',
            ['ref_no' => $ref_id]
        );

        if (!empty($results[0])) {
            return ($results[0]->qty);
        } else {
            return 0;
        }
    }

    /**
     * get media and container list according to media type
     *
     */
    public function getMedias($type = 0)
    {

        $userData['data'] = MediaStock::getMediaByType($type);

        echo json_encode($userData);
        exit;
    }

    public function getContainers()
    {

        $userData['data'] = Container::where('active', '=', 1)->get();

        echo json_encode($userData);
        exit;
    }

    /**
     * Function for paginate DB query actions
     *
     * @param $array
     * @param $request
     * @return LengthAwarePaginator
     */
    public function arrayPaginator($array, $request)
    {
        $page = Input::get('page', 1);
        $perPage = config('app.perPage');
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
    }

    public function rejectionReport()
    {

        //transfer_room_stock.operator AS t_operator,
        $rejected_stocks = DB::table("t_room_stock")
            ->join('transfer_room_stock', 't_room_stock.ref_no', '=', 'transfer_room_stock.ref_no')
            ->join('media', 'transfer_room_stock.media', '=', 'media.id')
            ->join('users as operator', 'transfer_room_stock.operator_id', '=', 'operator.id')
            ->join('container', 'transfer_room_stock.container', '=', 'container.id')
            ->select(DB::raw("
                t_room_stock.ref_no AS trs_ref_no,
                t_room_stock.reasons AS trs_reasons,
                t_room_stock.created_at AS trs_created_at,
                t_room_stock.cr AS trs_quantity,
                operator.name AS t_operator,
                operator.user_code AS t_operator_code,
                transfer_room_stock.m_type AS t_m_type,
                transfer_room_stock.date AS t_date,
                media.name AS media,
                container.name AS container"))
            ->whereNotNull(DB::raw("t_room_stock.reasons"))
            ->orderByDesc(DB::raw("t_room_stock.ref_no"))->paginate(250);

        return view('mediastocks.rejection_report', compact('rejected_stocks'));
    }
}
