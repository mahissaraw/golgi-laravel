<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Frequency;

class FrequencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frequencies = Frequency::all();

        return view('frequencies.index', compact('frequencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //         $film=array(

        //             "user1" => array(
        //                             0 => "Pink Panther",
        //                             1 => "john English",
        //                             2 => "See no evil hear no evil"
        //                             ),

        //             "user2" => array (
        //                             0 => "Die Hard",
        //                             1 => "Expendables"
        //                             ),

        //             "user3" => array (
        //                             0 => "The Lord of the rings"
        //                             ),

        //             "user4" => array
        //                             (
        //                             0 => "Romeo and Juliet"
        //                             )

        // );
        // echo $film["comedy"][0];
        return view('frequencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'frequency' => 'required',
        ]);
        $share = new Frequency([
            'frequency' => $request->get('frequency'),
            'data' => $request->get('data')
        ]);
        $share->save();
        return redirect('/frequencies')->with('success', 'Frequency has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frequency = Frequency::find($id);

        return view('frequencies.edit', compact('frequency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'frequency' => 'required',
        ]);
        $frequency = Frequency::find($id);

        $frequency->frequency = $request->get('frequency');
        $frequency->data = $request->get('data');
        $frequency->save();

        return redirect('/frequencies')->with('success', 'Frequency has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $frequency = Frequency::find($id);
        $frequency->delete();

        return redirect('/frequencies')->with('success', 'Frequency has been deleted successfully');
    }
}
