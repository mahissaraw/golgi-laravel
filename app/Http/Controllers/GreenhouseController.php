<?php

namespace App\Http\Controllers;

use App\Category;
use App\Greenhouse;
use App\Item;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Log;

class GreenhouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        $query = DB::table("greenhouses")
            ->join('item', 'greenhouses.item_id', '=', 'item.id')
            ->join('sub_cat', 'item.sub_cat', '=', 'sub_cat.id')
            ->select(DB::raw("SUM(IF(greenhouses.task = 1, greenhouses.quantity, 0)) -
                                SUM(IF(greenhouses.task = 2, greenhouses.quantity, 0)) AS available_quantity,
                                SUM(IF(task = 3, quantity, 0)) AS quantity_reserved,
                                greenhouses.item_id,
                                sub_cat.subcat as sub_category,
                                item.item_name AS item_name,
                                item.bar_code AS bar_code,
                                item.item_code AS item_code"));

        $query->orderBy(DB::raw("sub_cat.subcat"))->groupBy(DB::raw("greenhouses.item_id"));
        $plants = $query->get();

        return view('greenhouses.index', compact('plants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('active', '=', 1)->get();
        return view('greenhouses.create', compact( 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'task' => 'required',
            'quantity' => 'required|integer',
            'sel_item' => 'required'
        ]);

        $task = $request->get('task');
        if ($task == 1){ // plant in
            $greenhouse = new Greenhouse([
                'item_id' => $request->get('sel_item'),
                'task' => $task,
                'quantity' => $request->get('quantity'),
                'user_id' => Auth::user()->id,
                'remark' => $request->get('remark'),
            ]);
            $greenhouse->save();
            return redirect('/greenhouses/'.$request->get('sel_item'))->with('success', 'Plant has been added');
        } else { // plant out
            $request_qty = $request->get('quantity');
            $available_qty = $this->availablePlantQuantity($request->get('sel_item'));
            // check do we have enough plant quantity to be deduct
            if ($available_qty >= $request_qty){
                $greenhouse = new Greenhouse([
                    'item_id' => $request->get('sel_item'),
                    'task' => $task,
                    'quantity' => $request->get('quantity'),
                    'user_id' => Auth::user()->id,
                    'remark' => $request->get('remark'),
                ]);
                $greenhouse->save();
                return redirect('/greenhouses/'.$request->get('sel_item'))->with('info', 'Plant has been deducted');
            }
            return redirect()->back()->withInput()->withErrors(['Requested quantity ('.$request_qty.') is greater than the available quantity ('.$available_qty.')']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param null $task_id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $task_id = null)
    {
        if ($task_id){
            $items = Greenhouse::where('item_id', $id)->where('task', $task_id)->orderBy('created_at', 'desc')->limit(50)->get();
        } else {
            $items = Greenhouse::where('item_id', $id)->orderBy('created_at', 'desc')->limit(50)->get();
        }

        $available_quantity = $this->availablePlantQuantity($id);
        $reserved_quantity = $this->reservedPlantQuantity($id);
        $plant = Item::find($id);
        return view('greenhouses.show', compact('plant','items','available_quantity', 'reserved_quantity' ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $greenhouse = Greenhouse::find($id);
        return view('greenhouses.edit', compact('greenhouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'task' => 'required',
            'quantity' => 'required|integer'
        ]);
        $greenhouse = Greenhouse::find($id);
        $greenhouse->task       = $request->get('task');
        $greenhouse->quantity   = $request->get('quantity');
        $greenhouse->remark     = $request->get('remark');
        $greenhouse->is_sold     = $request->get('is_sold') ?? false;

        if ($greenhouse->save()) {
            return redirect('/greenhouses/'.$greenhouse->item_id)->with('success', 'Plant has been updated');
        }

        return back()->withInput()->with('error', 'Could not update Plant Stock');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function plantCreate($id)
    {
        $plant = Item::find($id);
        return view('greenhouses.plant_create', compact('plant'));
    }

    private function availablePlantQuantity($item_id = null)
    {
        $greenhouse = DB::table('greenhouses')
            ->select(DB::raw("SUM(IF(task = 1, quantity, 0)) AS quantity_in,
                                SUM(IF(task = 2, quantity, 0)) AS quantity_out
                                "))
            ->where('item_id', '=', $item_id)
//            ->where('is_sold', '=', 0)
//            ->whereNotIn('task', [3])
            ->first();

        return $greenhouse->quantity_in - ($greenhouse->quantity_out);
    }

    private function reservedPlantQuantity($item_id = null)
    {
        $greenhouse = DB::table('greenhouses')
            ->select(DB::raw("SUM(IF(task = 3, quantity, 0)) AS quantity_reserved"))
            ->where('item_id', '=', $item_id)
            ->first();

        return $greenhouse->quantity_reserved;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function plantStock(Request  $request) {
        $date_from = '';
        $to_date = '';

        $query = DB::table("greenhouses")
            ->join('item', 'greenhouses.item_id', '=', 'item.id')
            ->select(DB::raw("SUM(IF(greenhouses.task = 1, greenhouses.quantity, 0))  AS quantity_in,
                                SUM(IF(greenhouses.task = 2, greenhouses.quantity, 0)) AS quantity_out,
                                greenhouses.item_id,
                                item.item_name AS item_name,
                                item.item_code AS item_code"));

        if ($request->get('date_from') != "" && $request->get('date_to') != "") {
            $date_from = date('Y-m-d', strtotime($request->get('date_from')));
            $date_to = date('Y-m-d', strtotime($request->get('date_to') . ' +1 day'));
            $date = 2;
            $to_date = date('Y-m-d', strtotime($request->get('date_to')));
            $query->whereBetween('greenhouses.created_at', array($date_from, $date_to));
        } elseif($request->get('date_from') != "") {
            $date = 1;
            $date_from = $request->get('date_from');
            $query->where("greenhouses.created_at", ">=", $request->get('date_from'));
        } else {
            $date = 0;
            $query->whereRaw('MONTH(greenhouses.created_at) = ?', [date('m')])
                ->whereRaw('YEAR(greenhouses.created_at) = ?', [date('Y')]);
        }

        $query->orderBy(DB::raw("item.item_name"))->groupBy(DB::raw("greenhouses.item_id"));
        $plants = $query->get();

        return view('greenhouses.plant_stock', compact('plants', 'date', 'date_from', 'to_date'));
    }

    /**
     * cancel reserve plant stock
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function sell (int $id){
        $greenhouse = Greenhouse::find($id);
        $greenhouse->task       = 2; // set it as task OUT (2)
        $greenhouse->is_sold    = true; // is_sold = true
        if ($greenhouse->save()) {
            Log::channel('daily')->info(request()->user()->name.' has SOLD '.$greenhouse->item->item_name. ' QTY:'.$greenhouse->quantity);
            return redirect('/greenhouses/'.$greenhouse->item_id)->with('success', 'Plant Stock has been Sold');
        }

        return back()->withInput()->with('error', 'Could not update Plant Stock');
    }


    public function sold (int $id){

    }

    /**
     * cancel reserve plant stock
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel (int $id){
        $greenhouse = Greenhouse::find($id);
        $greenhouse->task       = 1; // set it as task IN (1)
        if ($greenhouse->save()) {
            Log::channel('daily')->info(request()->user()->name.' has CANCELLED '.$greenhouse->item->item_name. ' QTY:'.$greenhouse->quantity);
            return redirect('/greenhouses/'.$greenhouse->item_id)->with('success', 'Plant Stock has been cancelled');
        }

        return back()->withInput()->with('error', 'Could not update Plant Stock');

    }

    /**
     * cancel reserve plant stock
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reserve (int $id){
        $plant = Item::find($id);
        $available_quantity = $this->availablePlantQuantity($id);
        $reserved_quantity = $this->reservedPlantQuantity($id);
        return view('greenhouses.reserve', compact('plant', 'available_quantity', 'reserved_quantity'));
    }

    /**
     * Store a newly created reserve stocks.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reserveStock (Request $request)
    {
        $request->validate([
            'quantity' => 'required|integer'
        ]);
        $sel_item = $request->get('sel_item');
        $quantity = $request->get('quantity');

//        if ($this->availablePlantQuantity($sel_item) > ($quantity + $this->reservedPlantQuantity($sel_item))){
            $greenhouse = new Greenhouse([
                'item_id' => $sel_item,
                'task' => 3, // reserved
                'quantity' => $quantity,
                'user_id' => Auth::user()->id,
                'remark' => $request->get('remark'),
            ]);
            if($greenhouse->save()){
                return redirect('/greenhouses/'.$sel_item)->with('success', 'Plant Stock has been reserved');
            }
//        }
        return redirect()->back()->withInput()->withErrors(['Requested quantity is greater than the available quantity']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $greenhouse = Greenhouse::find($id);
        $item_id = $greenhouse->item_id;
        $item_name = $greenhouse->item->item_name;
        $quantity = $greenhouse->quantity;
        $greenhouse->delete();

        Log::channel('daily')->info(request()->user()->name.' has REMOVED '.$item_name. ' QTY:'.$quantity);
        return redirect('/greenhouses/'.$item_id)->with('success', 'Plant Stock has been cancelled');
    }

    /**
     * Get greenhouse enabled plants
     *
     * @param int $id
     */
    public function plants(int $id)
    {
        $userData = [];
        $userData['data'] = Item::where([
            ['sub_cat', '=', $id],
            ['active', '=', 1],
            ['greenhouse', '=', 1]
        ])->orderBy('item_name')->get();

        echo json_encode($userData);
    }
}
