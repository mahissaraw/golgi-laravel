<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use DateTime;
use Illuminate\Http\Request;
use App\GrowthRoomStock;
use App\User;
use App\Performance;
use App\Item;
use DB;
use GuzzleHttp\Client;
use Log;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function performance(Request $request)
    {
        $date_from = '';
        $to_date = '';

        $performances_query = DB::table("performance")
            ->join('users', 'performance.user_id', '=', 'users.id')
            ->select(DB::raw("SUM(item_qty) AS total_quantity,
                                SUM(rejected_qty) AS total_rejected_quantity,
                                SUM(add_amount)  as total_earned,
                                SUM(rejected_amount)  as total_deductions,
                                performance.user_id,
                                users.name AS user_name,
                                users.user_code AS user_code,
                                users.active AS user_active"));

        if ($request->get('date_from') != "" && $request->get('date_to') != "") {

            $date_from = $request->get('date_from');
            $to_date = $request->get('date_to');
            $date = 2;

            $performances_query->whereBetween('performance.updated_at', array($date_from.' 00:00:00', $to_date.' 23:59:59'));

        } elseif ($request->get('date_from') != "") {

            $date_from = $request->get('date_from');
            $date = 1;

            $performances_query->where("performance.updated_at", ">=", $date_from.' 00:00:00');

        } else {
            $currentMonth = date('m');
            $currentYear = date('Y');
            $date = 0;

            $performances_query->whereRaw('MONTH(performance.updated_at) = ?', [$currentMonth])
            ->whereRaw('YEAR(performance.updated_at) = ?', [$currentYear]);

        }
        $performances = $performances_query->groupBy(DB::raw("user_id"))->get();

        Log::channel('daily')->info(request()->user()->name.' has viewed Operator Performances');
        return view('reports.performance', compact('performances', 'date', 'date_from', 'to_date'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $plant_id
     * @param int $year
     * @param int $week
     * @param null $media_id
     * @param null $plant_size
     * @return \Illuminate\Http\Response
     */
    public function weekBulk(int $plant_id, int $year, int $week, $media_id = null, $plant_size = null)
    {
        $week = $this->getStartAndEndDate($week,$year);
        $query = DB::table('performance')
            ->join('item', 'performance.item_id', '=', 'item.id')
            ->select(DB::raw("*, SUM(rejected_qty) + SUM(done_qty) as done_quantity"))
            ->whereBetween('created_at', [$week->start, $week->end])
            ->where('item_id', '=', $plant_id)
//            ->where('item_qty', '>', 0)
            ->whereNull('status');

        if ($plant_size)
        {
            $query->where('plant_size', $plant_size);
        }
        if ($media_id)
        {
            $query->join('media', 'performance.media_id', '=', 'media.id')
                ->where('media_id', '=', $media_id);
        }

        $bulks = $query->groupBy('bulk_number')->get();
        return view('reports.week_bulk', compact('bulks'));
    }

    public function room_conditions()
    {
        $client = new Client();
        $rndGrowthRoomContents = '';
        $rndLabContents = '';

        if ($client->request('GET', 'http://192.168.0.230/api')->getStatusCode() == 200) {
            $rndGrowthRoomContents = $client->request('GET', 'http://192.168.0.230/api')->getBody()->getContents();
        }

//        if ($client->request('GET', 'http://192.168.0.230/api') == 200) {
//            $rndLabContents = $client->request('GET', 'http://192.168.0.230/api')->getBody()->getContents();
//        }

        return view('reports.room_conditions', compact('rndGrowthRoomContents', 'rndLabContents'));
    }


    public function plantStatus(Request $request)
    {
        /**
         * ToDo: need to develop filter by date and customer
         */
//        ->select(DB::raw("(SUM(item_qty) - (SUM(rejected_qty) + SUM(done_qty))) AS total_quantity,
//        ->select(DB::raw("(SUM(item_qty) - SUM(rejected_qty)) AS total_quantity,
        $performances = DB::table("performance")
            //                ->join('customers', 'performance.customer_id', '=', 'customers.id')
            //->join('growth_room_stock', 'performance.bulk_number', '=', 'growth_room_stock.bulk_number')
            ->join('item', 'performance.item_id', '=', 'item.id')
            ->select(DB::raw("(SUM(item_qty) - (SUM(rejected_qty) + SUM(done_qty))) AS total_quantity,
                    item_id,
                    user_id,
                    item_name,
                    item_code,
                    WEEK(performance.created_at, 1) AS week,
                    YEAR(performance.created_at) AS year,
                    performance.bulk_number"))
            ->whereNull('status')
            ->groupBy(DB::raw("performance.item_id, year, week"))
            ->orderBy(DB::raw("item.item_name, week"))->get();

        $category = '';
        $items_query = Item::orderBy('item_name');
        if($request->get('cat_id')){
            $items_query->where('cat_id', $request->get('cat_id'));
            $category = Category::find($request->get('cat_id'));
        }
        $items = $items_query->get();
        $categories = Category::where('active', 1)->orderBy('name')->get();

        return view('reports.plant_status', compact('performances', 'items', 'categories', 'category'));
    }

    public function customerReports($id = null)
    {
        $user = User::find($id);
        $customer = Customer::where('email', $user->email)->first();
        // if ($customer != null) {
        //     $performances = DB::table("performance")
        //         //                ->join('customers', 'performance.customer_id', '=', 'customers.id')
        //         //->join('growth_room_stock', 'performance.bulk_number', '=', 'growth_room_stock.bulk_number')
        //         ->join('item', 'performance.item_id', '=', 'item.id')
        //         //->join('orders', 'performance.customer_id', '=', 'orders.id')
        //         ->select(DB::raw("(SUM(item_qty) - SUM(rejected_qty)) AS total_quantity,
        //                     item_id,
        //                     user_id,
        //                     item_name,
        //                     item_code,
        //                     WEEK(performance.created_at, 1) AS week,
        //                     performance.bulk_number,
        //                     performance.created_at"))
        //         //->where("performance.created_at", ">=", date('Y-m-d', strtotime('-7 days')))
        //         ->groupBy(DB::raw("performance.item_id, week"))
        //         ->orderBy('item.item_name')->get();
        //     //dd(DB::getQueryLog());
        //     //dd($performances);
        //     //                ->toArray();
        // }
        $performances = DB::table("performance")
            ->join('item', 'performance.item_id', '=', 'item.id')
            ->join('media', 'performance.media_id', '=', 'media.id')
            ->select(DB::raw("SUM(item_qty) AS total_quantity,
                            SUM(rejected_qty) AS total_rejected_quantity,
                            SUM(performance.done_qty) AS total_completed_quantity,
                            item_id,
                            user_id,
                            item_name,
                            item_code,
                            WEEK(performance.created_at, 1) AS week,
                            YEAR(performance.created_at) AS year,
                            performance.bulk_number,
                            media.name AS media_name,
                            performance.created_at"))

            ->where("performance.customer_id", "=", $customer->company->id)
            ->whereNull('status')
            ->where('performance.created_at', '>=', '2019-12-31 23:59:59') # greater than 2019
            ->groupBy(DB::raw("performance.item_id, performance.media_id, year, week"))
            ->orderBy(DB::raw("item.item_name, week"))->get();

        $items = Item::orderBy('item_name')->get();

        return view('reports.customer_reports', compact('performances', 'items', 'customer'));
    }

    public function setBulkStatus($status = null)
    {
        $bulk_data = '';
        $parent_bulk_numbers = [];
        $performance_bulk_numbers = [];

        if ($status && auth()->user()->hasRole('Administrator')) {
            if ($status === 'rejected_date') {
                ini_set('max_execution_time', 180);
                $added_plants = Performance::where('item_qty', '<>', 0)->where('rejected_qty', 0)->get();
                $rejecteded_plants = Performance::where('item_qty', 0)->where('rejected_qty', '<>', 0)->get();

                foreach ($added_plants as $ap) {
                    foreach ($rejecteded_plants as $rp) {
                        if ($ap->bulk_number == $rp->bulk_number) {
                            $ap_date = date('Y-m-d', strtotime($ap->created_at));
                            $rp_date = date('Y-m-d', strtotime($rp->created_at));
                            if ($ap_date != $rp_date) {
                                $performance_bulk_numbers[] = $rp->bulk_number;
                                $old_updated_at = $rp->updated_at;
                                Performance::where('id', '=', $rp->id)->update([
                                    'created_at' => $ap->created_at,
                                    'updated_at' => $old_updated_at
                                ]);
                            }
                        }
                    }
                }
                $bulk_data = array_unique($performance_bulk_numbers);
            } else {
                $parent_ids = GrowthRoomStock::select('parent_id')
                    ->where('img_qr', 'like', $status . '%')
                    ->groupBy('parent_id')
                    ->orderBy('parent_id', 'asc')
                    ->get();

                foreach ($parent_ids as $key => $value) {
                    $parent_bulk_numbers[] = GrowthRoomStock::where('serial_no', $value->parent_id)->first()->bulk_number;
                }

                foreach (array_unique($parent_bulk_numbers) as $key => $value) {
                    $per_data = Performance::where('bulk_number', $value)->whereNull('status')->get();
                    foreach ($per_data as $data) {
                        $performance_bulk_numbers[] = $data->bulk_number;
                    }
                }

                $bulk_data = array_unique($performance_bulk_numbers);

                if (!empty($bulk_data)) {
                    foreach ($bulk_data as $key => $value) {
                        DB::table('performance')->where('bulk_number', '=', $value)->update(['status' => $status == 'rooting' ? 2 : 1], ['timestamps' => false]);
                    }
                }
            }
        }

        return view('reports.set_status', compact('bulk_data', 'status'));
    }

    /**
     * current plant stock.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function PlantStock(Request $request)
    {
        $date_from = '';
        $to_date = '';

        $query = DB::table("performance")
            ->join('item', 'performance.item_id', '=', 'item.id')
            ->select(DB::raw("(SUM(item_qty) - SUM(done_qty)) AS total_quantity,
                                SUM(rejected_qty) AS total_rejected,
                                SUM(CASE
                                    WHEN done_status < 3
                                    THEN done_qty
                                    ELSE 0
                                END) AS total_completed,
                                item.cat_id AS category_id,
                                item.item_name AS item_name,
                                item.item_code AS item_code"));

        if ($request->get('date_from') != "" && $request->get('date_to') != ""){

                $query->whereBetween('performance.updated_at', array($date_from.' 00:00:00', $request->get('date_to').' 23:59:59'));

            $date = 2;
            $date_from = date('Y-m-d', strtotime($request->get('date_from')));
            $to_date = date('Y-m-d', strtotime($request->get('date_to')));

        } elseif ($request->get('date_from') != ""){

            $query->where("performance.updated_at", ">=", $request->get('date_from').' 00:00:00');

            $date = 1;
            $date_from = $request->get('date_from');

        } else {
            $date = 0;
            $query->whereRaw('MONTH(performance.updated_at) = ?', [date('m')])
                ->whereRaw('YEAR(performance.updated_at) = ?', [date('Y')]);
        }

        $category = $request->get('cat_id') ?? 0;
        if ($category){
            $query->where('item.cat_id', $request->get('cat_id'));
        }

        $query->orderBy(DB::raw("item.item_name"))
            ->groupBy(DB::raw("performance.item_id"))
            ->havingRaw('total_quantity - total_rejected > ?', [0]);
        $plants = $query->get();
        $categories = Category::where('active', 1)->orderBy('name')->get();

        return view('reports.plant_stock', compact('plants', 'date', 'date_from', 'to_date', 'categories', 'category'));
    }

    public function plantsMediaStatus(Request $request)
    {
        //SUM(performance.done_qty) AS total_done_quantity,

        $performances = DB::table("performance")
            ->join('item', 'performance.item_id', '=', 'item.id')
            ->join('media', 'performance.media_id', '=', 'media.id')
            ->select(DB::raw("SUM(performance.item_qty) AS total_quantity,
                            SUM(performance.rejected_qty) AS total_rejected_quantity,
                            SUM(CASE
                                WHEN performance.done_status = 1
                                THEN performance.done_qty
                                ELSE 0
                            END) AS total_completed_quantity,
                            SUM(CASE
                                WHEN performance.done_status = 2
                                THEN performance.done_qty
                                ELSE 0
                            END) AS total_dispatched_quantity,
                            SUM(CASE
                                WHEN performance.done_status = 3
                                THEN performance.done_qty
                                ELSE 0
                            END) AS total_transfer_quantity,
                            item_id,
                            user_id,
                            item_name,
                            item_code,
                            WEEK(performance.created_at, 1) AS week,
                            YEAR(performance.created_at) AS year,
                            performance.bulk_number,
                            performance.plant_size,
                            media.name AS media_name,
                            media.id AS media_id,
                            performance.created_at"))
            ->whereNull('status')
            ->where('performance.created_at', '>=', '2019-12-31 23:59:59') # greater than 2019
            ->havingRaw('SUM(performance.item_qty) - (SUM(performance.rejected_qty) + SUM(performance.done_qty)) > ?', [0])
            ->groupBy(DB::raw("performance.item_id, performance.media_id, year, week, performance.plant_size"))
            ->orderBy(DB::raw("item.item_name, week"))->get();

        $category = '';
        $items_query = Item::orderBy('item_name');
        if($request->get('cat_id')){
            $items_query->where('cat_id', $request->get('cat_id'));
            $category = Category::find($request->get('cat_id'));
        }
        $items = $items_query->get();
        $categories = Category::where('active', 1)->orderBy('name')->get();

        return view('reports.plants_media_status', compact('performances', 'items', 'categories', 'category'));
    }

    /**
     * @param $week
     * @param $year
     * @return object
     *
     * Get the start and end dates of the given week
     */
    public function getStartAndEndDate($week, $year) {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['start'] = $dto->format('Y-m-d').' 00:00:00';
        $dto->modify('+6 days');
        $ret['end'] = $dto->format('Y-m-d').' 23:59:59';
        return json_decode(json_encode($ret), FALSE);
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function performanceBreakdown(Request $request)
    {
        $date_from = '';
        $to_date = '';

        $performances_query = DB::table("performance")
            ->join('users', 'performance.user_id', '=', 'users.id')
            ->join('item', 'performance.item_id', '=', 'item.id')
            ->select(DB::raw("SUM(item_qty) AS total_quantity,
                                SUM(rejected_qty) AS total_rejected_quantity,
                                user_id,
                                users.name AS user_name,
                                users.user_code AS user_code,
                                item.item_name AS item_name,
                                item.bar_code AS bar_code"));

        if ($request->get('date_from') != "" && $request->get('date_to') != "") {
            $date_from = $request->get('date_from');
            $to_date = $request->get('date_to');
            $date = 2;
            $performances_query->whereBetween('performance.updated_at', array($date_from.' 00:00:00', $to_date.' 23:59:59'));

        } elseif ($request->get('date_from') != "") {
            $date_from = $request->get('date_from');
            $date = 1;
            $performances_query->where("performance.updated_at", ">=", $date_from.' 00:00:00');

        } else {
            $currentMonth = date('m');
            $currentYear = date('Y');
            $date = 0;
            $performances_query->whereRaw('MONTH(performance.updated_at) = ?', [$currentMonth])
            ->whereRaw('YEAR(performance.updated_at) = ?', [$currentYear]);
        }

        $performances = $performances_query->groupBy(DB::raw("performance.item_id, performance.user_id"))->orderBy(DB::raw("users.id"))->get();
        $users = User::orderBy('id')->get();
//        Log::channel('daily')->info(request()->user()->name.' has viewed Operator Performances');
        return view('reports.performance_breakdown', compact('performances','users', 'date', 'date_from', 'to_date'));
    }

    public function contamination(Request $request){
        $from = date('Y-m-d', strtotime('-6 days')).' 00:00:00';
//        $from = date('Y-m-d', strtotime('first day of last month')).' 00:00:00';
//        $to = date('Y-m-d', strtotime('last day of last month')).' 23:59:59';
        $to = date('Y-m-d').' 23:59:59';

        if ($request->get('fromDate') != '') {
            $from = $request->get('fromDate').' 00:00:00';
        }
        if ($request->get('toDate') != '') {
            $to = $request->get('toDate').' 23:59:59';
        }

        $growth_room = GrowthRoomStock::whereNotNull('reasons')->whereBetween('last_updated', [$from, $to])->get();

        return view('reports.contamination', compact('from', 'to', 'growth_room'));

    }

    public function cuttersPerformance(Request $request)
    {
        $month = date('m');
        $year = date('Y');

        if($request->isMethod('post'))
        {
            list($year, $month) = explode('-',$request->input('year_month'));
        }

        $cutters = User::
        whereHas(
            'roles', function($q){
            $q->where('name', 'Cutter');
        })->where([
            ['active', '=' ,  1]
        ])->get();

        $resultList = [];
        foreach ($cutters as $ct){
            $resultList [$ct->id] = [
                'name' => $ct->name,
                'code' => $ct->user_code,
                'cutter_date' => $ct->cutter_date ?? date("Y-m-d"),
                'cq_this_month' => self::getThisMonth($ct->id, $year, $month)->pluck('cq')->first(),
                'iq_this_month' => self::getThisMonth($ct->id, $year, $month)->pluck('iq')->first(),
                'cq_one_month' => self::getOneMonthAgo($ct->id, $year, $month)->pluck('cq')->first(),
                'iq_one_month' => self::getOneMonthAgo($ct->id, $year, $month)->pluck('iq')->first(),
                'cq_two_month' => self::getTwoMonthsAgo($ct->id, $year, $month)->pluck('cq')->first(),
                'iq_two_month' => self::getTwoMonthsAgo($ct->id, $year, $month)->pluck('iq')->first(),
                'ccq_this_month' => (int) self::getThisMonthContaminationCutting($ct->id, $year, $month)->pluck('plants')->first(),
            ];
        }

        /**
         * Sort multi-dimensional array by its value descending
         */
        usort($resultList, function($a, $b) {
            return $b['cq_this_month'] <=> $a['cq_this_month'];
        });

//        dd($resultList);

        return view('reports.cutters_performance', compact('resultList', 'year', 'month'));
    }

    private function getThisMonth($user_id, $year, $month)
    {
        $ps = DB::table("performance")
            ->select(DB::raw("SUM(item_qty) AS cq,
                                SUM(rejected_qty) AS iq"))
            ->whereRaw('MONTH(performance.updated_at) = ?', [$month])
            ->whereRaw('YEAR(performance.updated_at) = ?', [$year])
            ->where('user_id', '=' , $user_id)
            ->get();

        return $ps;
    }

    private function getOneMonthAgo($user_id, $year, $month )
    {
        $oneMonthAgo = new \DateTime($year.'-'.$month.'-01');
        $oneMonthAgo->modify('-1 month');

        $ps = DB::table("performance")
            ->select(DB::raw("SUM(item_qty) AS cq,
                                SUM(rejected_qty) AS iq"))
            ->whereRaw('MONTH(performance.updated_at) = ?', [$oneMonthAgo->format('m')])
            ->whereRaw('YEAR(performance.updated_at) = ?', [$oneMonthAgo->format('Y')])
            ->where('user_id', '=' , $user_id)
            ->get();

        return $ps;
    }

    private function getTwoMonthsAgo($user_id, $year, $month)
    {
        $twoMonthAgo = new \DateTime($year.'-'.$month.'-01');
        $twoMonthAgo->modify('-2 month');

        $ps = DB::table("performance")
            ->select(DB::raw("SUM(item_qty) AS cq,
                                SUM(rejected_qty) AS iq"))
            ->whereRaw('MONTH(performance.updated_at) = ?', [$twoMonthAgo->format('m')])
            ->whereRaw('YEAR(performance.updated_at) = ?', [$twoMonthAgo->format('Y')])
            ->where('user_id', '=' , $user_id)
            ->get();

        return $ps;
    }

    /**
     * @param integer $user_id
     * @param null $year
     * @param null $month
     * @return mixed
     */
    private function getThisMonthContaminationCutting($user_id, $year, $month)
    {
        $cct_query = GrowthRoomStock::
            select(DB::raw('SUM(SUBSTRING_INDEX(item_serial_no, " ", -1)) AS plants'))
            ->where('growth_room_stock.reasons', 'like', '%9%')
            ->where('growth_room_stock.user_id', '=' , $user_id);

        $cct_query->whereRaw('MONTH(growth_room_stock.last_updated) = ?', [$month])
            ->whereRaw('YEAR(growth_room_stock.last_updated) = ?', [$year]);

//        if ($date_from != "" && $date_to != "") {
//            $cct_query->whereBetween('growth_room_stock.last_updated', array($date_from.' 00:00:00', $date_to.' 23:59:59'));
//        } elseif ($date_from != "") {
//            $cct_query->where("performance.updated_at", ">=", $date_from.' 00:00:00');
//        } else {
//            $cct_query->whereRaw('MONTH(growth_room_stock.last_updated) = ?', [date('09')])
//                ->whereRaw('YEAR(growth_room_stock.last_updated) = ?', [date('Y')]);
//        }

        $cct = $cct_query->get();

        return $cct;
    }

}
