<?php

namespace App\Http\Controllers;

use Auth;
use App\Task;
use App\GrowthRoomStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\GrowthRoomStockController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->hasRole('Customer')){
            return redirect()->action(
                'ReportsController@customerReports', ['id' => auth()->user()->id]
            );
        } else {
            $tasks = Task::where('user_id', auth()->user()->id)->get();
            return view('home', compact("tasks"));
        }
    }


    public function scanQrCode(Request $request, $id = null)
    {
        $scan_status = $request->input(['label_scan']) ?? null;
        if ($id != null) {
            $serial_no = $id;
            $scan_status = session()->has('user.direct_transfer');
        } else {
            $serial_no = $request->input(['s_no']);
        }

        $status_data  = GrowthRoomStock::where('serial_no', $serial_no)->first();
        if ($status_data) {
            if(session()->has('user.direct_transfer') && $scan_status){
                $growth_controller = new GrowthRoomStockController;

                // Validation
                if($status_data->accept == 2){
                    return back()->with('error', 'Container already mark as CONTAMINATED!');
                }
                if($status_data->status == 'Transfer Room'){
                    return back()->with('error', 'Container is already in the Transfer Room');
                }
                if($status_data->complete == 1){
                    return back()->with('error', 'Container already mark as COMPLETED!');
                }
                if($status_data->status == 'Dispatched' || $status_data->complete == 2){
                    return back()->with('error', 'Container already mark as DISPATCHED!');
                }

                // Production Transfer
                if (session()->get('user.direct_transfer') == 1){
                    return $growth_controller->transferToTransferRoom($status_data->id, 2);
                }

                // Complete
                if (session()->get('user.direct_transfer') == 2){
                    if($status_data->type === 'Production' &&
                        ($status_data->status === 'Multiply' || $status_data->status === 'Growth Room')){
                        return $growth_controller->complete($status_data->id);
                    } else{
                        return back()->with('error', 'Container needs to be in Production Growth Room to mark as Completed');
                    }
                }

                // Dispatch
                if (session()->get('user.direct_transfer') == 3){
                    if($status_data->type === 'Production' && $status_data->status === 'Rooting' ){
                        return $growth_controller->dispatch($status_data->id);
                    }else {
                        return back()->with('error', 'Container needs to be in Rooting Status');
                    }

                }

            } else {
                return view("view_status", compact("status_data"));
            }
        } else {
            return back()->with('error', 'Incorrect QR Code');
        }
    }

    /**
     * @param Request $request
     * @return Object
     */
    public function directTransfer(Request $request)
    {
        $action = $request->input('action');

        if ($action > 0) {
            $request->session()->put('user.direct_transfer', $action);

            if ($action == 1){
                $task = 'Production Transfer';
            } elseif ($action == 2){
                $task = 'Complete';
            } else {
                $task = 'Dispatch';
            }
            $response = [
                'success' => 1,
                'message' => 'Container Direct '.$task.' Enabled!'
            ];
        } elseif ($action == 0) {
            $request->session()->forget('user.direct_transfer');
            $response = [
                'success' => 1,
                'message' => 'One-Click Transfer Disabled!!'
            ];
        } else {
            $response = [
                'success' => 0,
                'message' => 'something is not working as expected. please try again!!'
            ];
        }

        return response()->json($response);
    }
}
