<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use Illuminate\Support\Facades\File;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('select_inactive')) {
            $items = Item::inActive()->get()
                ->sortBy(function($query){
                    return $query->category->name;
                })->all();
        } elseif ($request->has('select_greenhouse')){
            $items = Item::greenhouse()->get()
                ->sortBy(function($query){
                    return $query->category->name;
                })->all();
        } elseif ($request->has('search_item_name')){
            $items = Item::where('bar_code', $request->input('search_item_name'))->get();
        } else {
            $items = Item::active()->get()
                ->sortBy(function($query){
                    return $query->category->name;
                })->all();
        }
        $categories = Category::active()->orderBy('name')->get();
        $subcategories = SubCategory::active()->get()->sortBy(function($query){
            return $query->category->name;
        })->all();

        return view('items.index', compact('items', 'categories', 'subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cat_id' => 'required',
            'sub_cat_id' => 'required',
            'item_name' => 'required',
            'item_code' => 'required',
            'bar_code' => 'required|unique:item,bar_code',
            'image' => 'image|nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $item = new Item();

        $item->cat_id    = $request->get('cat_id');
        $item->sub_cat   = $request->get('sub_cat_id');
        $item->item_name = $request->get('item_name');
        $item->item_code = $request->get('item_code');
        $item->bar_code  = $request->get('bar_code');
        $item->co_owner  = $request->get('co_owner');
        $item->greenhouse= $request->get('greenhouse') ?? 0;

        if ($file = $request->file('image')) {
            $destinationPath = public_path('/plant_images/'); // upload path
            File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
            $fileName = $file->getClientOriginalName();
            $plantImage = time() . '_' . $fileName;
            $file->move($destinationPath, $plantImage);
            $item->image = $plantImage;
        }

        if ($item->save()) {
            return redirect('/items')->with('success', 'Plant has been added');
        }

        return back()->withInput()->with('error', 'Could not add Sub Category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $categories = Category::get();
        $subcategories = SubCategory::get()->sortBy(function($query){
            return $query->category->name;
        })->all();

        return view('items.edit', compact('subcategories', 'categories', 'item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $request->validate([
            'cat_id' => 'required',
            'sub_cat' => 'required',
            'item_name' => 'required',
            'item_code' => 'required',
            'bar_code' => 'required|unique:item,bar_code,'.$item->id,
//            'image' => 'image|nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $item = Item::find($item->id);

        $item->cat_id    = $request->get('cat_id');
        $item->sub_cat   = $request->get('sub_cat');
        $item->item_name = $request->get('item_name');
        $item->item_code = $request->get('item_code');
        $item->active    = $request->get('active');
        $item->greenhouse= $request->get('greenhouse');
        $item->bar_code  = $request->get('bar_code');
        $item->co_owner  = $request->get('co_owner');

        if ($file = $request->file('image')) {
            $destinationPath = public_path('/plant_images/'); // upload path
            File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
            $fileName = $file->getClientOriginalName();
            $plantImage = time() . '_' . $fileName;
            $file->move($destinationPath, $plantImage);
            $item->image = $plantImage;
        }

        if ($item->save()) {
            return redirect('/items')->with('success', 'Plant has been updated');
        }

        return back()->withInput()->with('error', 'Could not update Item');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Item::find($id);
        $media->delete();

        return redirect('/items')->with('success', 'Item has been deleted Successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
//        $data = Item::select(DB::raw("concat(`item_name`, ' (' , `bar_code`, ')') as name, id"))
//            ->where("item_name", "LIKE", "{$request->input('query')}%")
//            ->get();
        $data = Item::select('bar_code as name', 'id')
            ->where("bar_code", "LIKE", "{$request->input('query')}%")
            ->get();

        return response()->json($data);
    }
}
