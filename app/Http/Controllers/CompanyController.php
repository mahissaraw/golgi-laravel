<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|unique:companies',
            'logo' => 'image|nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $company = new Company();
        $company->name = stripslashes($request->get('name'));
        $company->description = $request->get('description');

        if ($file = $request->file('logo')) {
            $destinationPath = public_path('/customer_logo/'); // upload path
            File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
            $fileName = $file->getClientOriginalName();
            $logoImage = time() . '_' . $fileName;
            $file->move($destinationPath, $logoImage);
            $company->logo = $logoImage;
        }

        $company->save();

        return redirect('/customers')->with('success', 'New Company has been added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        $company = Company::find($company->id);
        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        request()->validate([
            'name' => 'required|unique:companies,name,'.$company->id,
            'logo' => 'image|nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $company = Company::find($company->id);

        $company->name = stripslashes($request->get('name'));
        $company->description = $request->get('description');
        $company->active = $request->get('active');

        if ($file = $request->file('logo')) {
            $destinationPath = public_path('/customer_logo/'); // upload path
            File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
            $fileName = $file->getClientOriginalName();
            $logoImage = time() . '_' . $fileName;
            $file->move($destinationPath, $logoImage);
            $company->logo = $logoImage;
        }

        $company->save();

        return redirect('/companies')->with('success', 'Record has been updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
