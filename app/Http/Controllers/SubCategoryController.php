<?php

namespace App\Http\Controllers;

use App\SubCategory;
use Illuminate\Http\Request;
use App\Category;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::orderBy('name')->where('active', 1)->get();
        if ($request->has('select_inactive')) {
            $subcategories = SubCategory::inActive()->get();
        } else {
            $subcategories = SubCategory::active()->get();
        }
        return view('subcategories.index', ['subcategories' => $subcategories, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'sub_category_name'=>'required',
            'cat_id'=>'required',
        ]);
        $sub_cat = new SubCategory();

        $sub_cat->cat_id = $request->get('cat_id');
        $sub_cat->subcat = $request->get('sub_category_name');

        if ($sub_cat->save()){
            return redirect('/subcategories')->with('success', 'Sub Category has been added');
        }

        return back()->withInput()->with('error', 'Could not add Sub Category');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sub_category = SubCategory::find($id);
        return view('subcategories.show', ['sub_category' => $sub_category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subCategory = SubCategory::find($id);
        $categories = Category::all();
        //dd($subCategory);

        return view('subcategories.edit', compact('subCategory', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_name'=>'required',
            'subCategory_name'=> 'required'
        ]);

        $subCategory = SubCategory::find($id);

        $subCategory->cat_id = $request->get('category_name');
        $subCategory->subcat = $request->get('subCategory_name');
        $subCategory->active = $request->get('active');

        if ($subCategory->save()){
            return redirect('/subcategories')->with('success', 'Sub Category has been updated');
        }

        return back()->withInput()->with('error', 'Could not add Sub Category');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = SubCategory::find($id);
        $media->delete();

        return redirect('/subcategories')->with('success', 'Sub Category has been deleted Successfully');
    }
}
