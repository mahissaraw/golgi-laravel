<?php

namespace App\Http\Controllers;

use App\Company;
use App\Container;
use App\GrowthRoomStock;
use App\Item;
use App\MediaStock;
use App\Media;
use App\Category;
use App\Performance;
use App\SubCategory;
use App\User;
use App\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use DateTime;
use Log;

require(base_path('/public/fpdf/fpdf.php'));

use FPDF;


class GrowthRoomStockController extends Controller
{
    private $DEFAULT_PRINTER;

    public function __construct()
    {
        $this->DEFAULT_PRINTER = $this->getPrinter();
    }

    public function create()
    {
        $admin_user = auth()->user()->hasRole('Administrator');
        if (!$admin_user) {
            $role = 'Operator';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '=', $role);
            })->where('active', '=', '1')->pluck('name', 'id');
        } else {
            $role = 'Customer';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '!=', $role);
            })->where('active', '=', '1')->pluck('name', 'id');
        }

        $categories = Category::where('active', '=', 1)->get();
//        $customers = Customer::where('type', '=', 1)->get();
        $customers = Company::where('active', 1)->get();

        return view('growthstocks.create', compact('users', 'categories', 'customers'));
    }

    /**
     * Store growth room stock
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'section' => 'required',
            'user_id' => 'required|integer',
            'sel_type' => 'required|integer',
            'sel_media' => 'required|integer',
            'sel_container' => 'required|integer',
            'sel_item' => 'required|integer',
            'container_qty' => 'required',
            'plant_qty' => 'required',
            'total_plants' => 'required',
            'sel_container_batch' => 'required|integer',
        ]);

        $date_w = new DateTime(date("Y-m-d H:i:s"));
        $week = $date_w->format("W");

        $bulk_number = $this->getBulkNo() > $this->getMaxBulkNo() ? $this->getBulkNo() : $this->getMaxBulkNo() + 1;
        $this->updateBulkNo();

        $item_capacity = $request->get('plant_qty');

        $user = new User();
        $ref = $request->get('sel_container_batch');
        $user_code = $user->getUserCode($request->get('user_id'));

        for ($i = 0; $i < $request->get('container_qty'); $i++) {

            $dt = date("Y-m-d H:i:s");
            $s_no = $this->getSerialNo();
            $this->updateSerialNo();

            $bar_code = $this->getBarCode();
            $this->updateBarCode();

            $qr_image_name = 'growth_stock_' . md5($dt . $ref . $user_code . $s_no) . '.png';

            $this->generateQrCodeImage($s_no, $qr_image_name);

            $item_serial_no = $week . " " . '1' . " " . $item_capacity;

            $growth_stock = new GrowthRoomStock([
                'media' => $request->get('sel_media'),
                'container' => $request->get('sel_container'),
                'm_type' => $request->get('sel_type'),
                'item' => $request->get('sel_item'),
                'batch_code' => 1, // useless
                'date' => $dt,
                'ref_no' => $request->get('sel_container_batch'),
                'serial_no' => $s_no,
                'bar_code' => $bar_code,
                'flag' => NULL, // update in later stages
                's_no' => NULL, // update in later stages
                'status' => 'Growth Room',
                'accept' => 1,
                'reasons' => NULL,
                'type' => $request->get('section'),
                'operator' => Auth::user()->name, //currently logged user
                'user' => $user->getUserCode($request->get('user_id')), // selected user
                'operator_id' => Auth::user()->id, //currently logged user
                'user_id' => $request->get('user_id'),
                'transfer' => 0,
                'img_qr' => $qr_image_name,
                'parent_id' => NULL,
                'bulk_number' => $bulk_number,
                'no_plants' => $request->get('total_plants'),
                'item_serial_no' => $item_serial_no,
                'complete' => 0,
                'last_updated' => $dt,
            ]);
            $growth_stock->save();
        }
        ///////////////////////// GrowthStock /////////////////////////

        $item = new Item();
        $performance = new Performance([
            'bulk_number' => $bulk_number,
            'user_id' => $request->get('user_id'),
            'item_id' => $request->get('sel_item'),
            'media_id' => $request->get('sel_media'),
            'container_id' => $request->get('sel_container'),
            'max_container_capacity' => $request->get('plant_qty'),
            'total_containers' => $request->get('container_qty'),
            'item_qty' => $request->get('total_plants'),
            'rejected_qty' => 0,
            'add_amount' => $item->getRate($request->get('sel_item')) * (int) $request->get('total_plants'),
            'rejected_amount' => 0,
            'order_id' => 0,
            'customer_id' => $request->get('customer_id'),
            'plant_size' => $request->get('plant_size') ?? null,
        ]);
        //dd($performance);
        $performance->save();
        /////////////////////////

        // insert media stock quantity in a separate table
        DB::table('t_room_stock')->insert(
            [
                'ref_no' => $request->get('sel_container_batch'),
                'dr' => 0,
                'cr' => $request->get('container_qty'),
                'created_at' => Carbon::now()->toDateTimeString()
            ]
        );

        return redirect('/growthstocks/showbulklabels/' . $bulk_number)->with('success', 'Growth stock added to ' . $request->get('section') . ' section');
    }

    /**
     * Create multiplication stocks
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createMultiplication($id)
    {
        $growthstocks = GrowthRoomStock::find($id);
        $growthstocks['item_id'] = $growthstocks->item;
        $growthstocks['media'] = $growthstocks->mediaVariety->name;
        $growthstocks['container'] = $growthstocks->containerVariety->name;
        $growthstocks['item'] = $growthstocks->itemVariety->item_name;
        $growthstocks['item_code'] = $growthstocks->itemVariety->item_code;
        $growthstocks['bar_code'] = $growthstocks->itemVariety->bar_code;

        $admin_user = auth()->user()->hasRole('Administrator');
        if (!$admin_user) {
            $role = 'Operator';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '=', $role);
            })->where('active', '=', '1')->pluck('name', 'id');
        } else {
            $role = 'Customer';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '!=', $role);
            })->where('active', '=', '1')->pluck('name', 'id');
        }

        return view('growthstocks.multiplication', compact('users', 'growthstocks'));
    }

    /**
     * Create multiplication stocks
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createBulkMultiplication($bulk_id)
    {

        $growthstocks = GrowthRoomStock::where('bulk_number', $bulk_id)->first();
        //                ->where('type', 'R&D')
        //                ->where('transfer', 0)
        //                ->where('status', 'Transfer Room')
        //                ->where('complete', '!=', 1)
        //                ->orderBy('media', 'container', 'desc')
        //->paginate(config('app.perPage'));

        dd($growthstocks);
        $admin_user = auth()->user()->hasRole('Administrator');
        if (!$admin_user) {
            $role = 'Operator';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '=', $role);
            })->where('active', '=', '1')->pluck('name', 'id');
        } else {
            $users = User::where('active', '=', '1')->pluck('name', 'id');
        }

        return view('growthstocks.multiplication', compact('users', 'growthstocks'));
    }

    /**
     * Save multiplication stock
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeMultiplication(Request $request)
    {
        $request->validate([
            'section' => 'required',
            'user_id' => 'required|integer',
            'sel_type' => 'required|integer',
            'sel_media' => 'required|integer',
            'sel_container' => 'required|integer',
            'container_qty' => 'required',
            'plant_qty' => 'required',
            'sel_container_batch' => 'required|integer',
        ]);

        $item = new Item();
        $user = new User();

        $date_w = new DateTime(date("Y-m-d H:i:s"));
        $week = $date_w->format("W");

        if ($request->get('batch_code') === $request->get('transfers')) {
            $no_no_transfers = $request->get('batch_code') + 1;
        } else {
            $no_no_transfers = $request->get('transfers');
        }

        $bulk_number = $this->getBulkNo() > $this->getMaxBulkNo() ? $this->getBulkNo() : $this->getMaxBulkNo() + 1;
        $this->updateBulkNo();

        $item_capacity = (int) $request->get('plant_qty');
        $item_quantity = $item_capacity * (int) $request->get('container_qty');

        $ref = $request->get('sel_container_batch');
        $user_code = $user->getUserCode($request->get('user_id'));

        for ($i = 0; $i < $request->get('container_qty'); $i++) {

            $dt = date("Y-m-d H:i:s");
            $s_no = $this->getSerialNo();
            $this->updateSerialNo();

            $bar_code = $this->getBarCode();
            $this->updateBarCode();

            $qr_image_name = 'multiply_stock_' . md5($dt . $ref . $user_code . $s_no) . '.png';

            $this->generateQrCodeImage($s_no, $qr_image_name);

            $growth_stock = new GrowthRoomStock([
                'media' => $request->get('sel_media'),
                'container' => $request->get('sel_container'),
                'm_type' => $request->get('sel_type'),
                'item' => $request->get('item_id'),
                'batch_code' => $no_no_transfers,
                'date' => date("Y-m-d H:i:s"),
                'ref_no' => $ref,
                'serial_no' => $s_no,
                'bar_code' => $bar_code,
                'flag' => NULL, // update in later stages
                's_no' => NULL, // update in later stages
                'status' => 'Multiply',
                'accept' => 1,
                'reasons' => NULL,
                'type' => $request->get('section'),
                'operator' => Auth::user()->name, //currently logged user
                'user' => $user_code, // selected user
                'operator_id' => Auth::user()->id, //currently logged user
                'user_id' => $request->get('user_id'),
                'transfer' => 0,
                'img_qr' => $qr_image_name,
                'parent_id' => $request->get('serial_no'),
                'bulk_number' => $bulk_number,
                'no_plants' => $item_quantity,
                'item_serial_no' => $week . " " . $no_no_transfers . " " . $item_capacity,
                'complete' => 0,
                'last_updated' => date("Y-m-d H:i:s"),
            ]);
            $growth_stock->save();
        }

        ///////////////////////// Multiplication /////////////////////////
//        if (self::grsRecords($request->get('bulk_number'), 'create') <= 0) {
//            DB::table('performance')->where('bulk_number', '=', $request->get('bulk_number'))->update(['status' => 1], ['timestamps' => false]);
//        }
        DB::table('performance')->where('bulk_number', '=', $request->get('bulk_number'))->update(['status' => 1], ['timestamps' => false]);

        $performance = new Performance([
            'bulk_number' => $bulk_number,
            'user_id' => $request->get('user_id'),
            'item_id' => $request->get('item_id'),
            'media_id' => $request->get('sel_media'),
            'container_id' => $request->get('sel_container'),
            'max_container_capacity' => $request->get('plant_qty'),
            'total_containers' => $request->get('container_qty'),
            'item_qty' => $item_quantity,
            'rejected_qty' => 0,
            'add_amount' => $item->getRate($request->get('item_id')) * $item_quantity,
            'rejected_amount' => 0,
            'order_id' => 0,
            'customer_id' => $this->getCustomerId($request->get('bulk_number')),
            'plant_size' => $request->get('plant_size') ?? null,
        ]);
        $performance->save();
        /////////////////////////////////////////////////////////////////

        GrowthRoomStock::where([
        ['bulk_number', '=', $request->get('bulk_number')],
//        ['type', '=', $request->get('pre_type')],
        ['status', '=', $request->get('pre_status')],
        ['accept', '<>', 2],
        ['complete', '=', 0]
           ])->whereNull('flag')->update(['flag' => 5]);

        // insert media stock quantity in a separate table
        DB::table('t_room_stock')->insert(
            [
                'ref_no' => $request->get('sel_container_batch'),
                'dr' => 0,
                'cr' => $request->get('container_qty'),
                'created_at' => Carbon::now()->toDateTimeString()
            ]
        );

        Log::notice('Multiply Item_id: '.$request->get('item_id').' User: '.request()->user()->name.' bulk_number: '.$request->get('bulk_number').' customer_id: '.$this->getCustomerId($request->get('bulk_number')));
        return redirect('/growthstocks/showbulklabels/' . $bulk_number)->with('success', 'Growth stock added to ' . $request->get('section') . ' section');
    }

    /**
     * Creating rooting form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createRooting($id)
    {
        $growthstocks = GrowthRoomStock::find($id);
        $growthstocks['item_id'] = $growthstocks->item;
        $growthstocks['media'] = $growthstocks->mediaVariety->name;
        $growthstocks['container'] = $growthstocks->containerVariety->name;
        $growthstocks['item'] = $growthstocks->itemVariety->item_name;
        $growthstocks['item_code'] = $growthstocks->itemVariety->item_code;
        $growthstocks['bar_code'] = $growthstocks->itemVariety->bar_code;

        $admin_user = auth()->user()->hasRole('Administrator');
        if (!$admin_user) {
            $role = 'Operator';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '=', $role);
            })->where('active', '=', '1')->pluck('name', 'id');
        } else {
            $role = 'Customer';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '!=', $role);
            })->where('active', '=', '1')->pluck('name', 'id');
        }

        return view('growthstocks.createrooting', compact('users', 'growthstocks'));
    }

    /**
     * Save rooting stock
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeRooting(Request $request)
    {
        $request->validate([
            'section' => 'required',
            'user_id' => 'required|integer',
            'sel_media' => 'required|integer',
            'sel_container' => 'required|integer',
            'container_qty' => 'required',
            'plant_qty' => 'required',
            'sel_container_batch' => 'required|integer',
        ]);

        $item = new Item();
        $user = new User();

        $date_w = new DateTime(date("Y-m-d H:i:s"));
        $week = $date_w->format("W");

        if ($request->get('batch_code') === $request->get('transfers')) {
            $no_no_transfers = $request->get('batch_code') + 1;
        } else {
            $no_no_transfers = $request->get('transfers');
        }

        $bulk_number = $this->getBulkNo() > $this->getMaxBulkNo() ? $this->getBulkNo() : $this->getMaxBulkNo() + 1;
        $this->updateBulkNo();

        $item_capacity = (int) $request->get('plant_qty');
        $item_quantity = $item_capacity * (int) $request->get('container_qty');

        $ref = $request->get('sel_container_batch');
        $user_code = $user->getUserCode($request->get('user_id'));

        for ($i = 0; $i < $request->get('container_qty'); $i++) {

            $dt = date("Y-m-d H:i:s");

            $s_no = $this->getSerialNo();
            $this->updateSerialNo();

            $bar_code = $this->getBarCode();
            $this->updateBarCode();

            $qr_image_name = 'rooting_stock_' . md5($dt . $ref . $user_code . $s_no) . '.png';

            $this->generateQrCodeImage($s_no, $qr_image_name);

            $growth_stock = new GrowthRoomStock([
                'media' => $request->get('sel_media'),
                'container' => $request->get('sel_container'),
                'item' => $request->get('item_id'),
                'batch_code' => $no_no_transfers,
                'date' => date("Y-m-d H:i:s"),
                'ref_no' => $ref,
                'serial_no' => $s_no,
                'bar_code' => $bar_code,
                'flag' => 2, // update in later stages
                's_no' => $request->get('serial_no'), // update in later stages
                'status' => 'Rooting',
                'accept' => 3,
                'reasons' => NULL,
                'type' => $request->get('section'),
                'operator' => Auth::user()->name, //currently logged user
                'user' => $user_code, // selected user
                'operator_id' => Auth::user()->id, //currently logged user
                'user_id' => $request->get('user_id'),
                //'transfer'=> 0,
                'img_qr' => $qr_image_name,
                'parent_id' => $request->get('serial_no'),
                'bulk_number' => $bulk_number,
                'no_plants' => $item_quantity,
                'item_serial_no' => $week . " " . $no_no_transfers . " " . $item_capacity,
                'complete'=> 0,
                'last_updated' => date("Y-m-d H:i:s"),
            ]);
            $growth_stock->save();
        }
        ///////////////////////// ROOTING /////////////////////////
//        if (self::grsRecords($request->get('bulk_number'), 'create') <= 0) {
//            DB::table('performance')->where('bulk_number', '=', $request->get('bulk_number'))->update(['status' => 2], ['timestamps' => false]);
//        }
        DB::table('performance')->where('bulk_number', '=', $request->get('bulk_number'))->update(['status' => 2], ['timestamps' => false]);
        $performance = new Performance([
            'bulk_number' => $bulk_number,
            'user_id' => $request->get('user_id'),
            'item_id' => $request->get('item_id'),
            'media_id' => $request->get('sel_media'),
            'container_id' => $request->get('sel_container'),
            'max_container_capacity' => $request->get('plant_qty'),
            'total_containers' => $request->get('container_qty'),
            'item_qty' => $item_quantity,
            'rejected_qty' => 0,
            'add_amount' => $item->getRate($request->get('item_id')) * $item_quantity,
            'rejected_amount' => 0,
            'order_id' => 0,
            'customer_id' => $this->getCustomerId($request->get('bulk_number')),
            'plant_size' => $this->getPlantSize($request->get('bulk_number')),
        ]);
        $performance->save();

        /////////////////////////
        GrowthRoomStock::where([
        ['bulk_number', '=', $request->get('bulk_number')],
//        ['type', '=', $request->get('pre_type')],
        ['status', '=', $request->get('pre_status')],
        ['accept', '<>', 2],
        ['complete', '=', 0]
           ])->update(['flag' => 6]);

        // insert media stock quantity in a separate table
        DB::table('t_room_stock')->insert(
            [
                'ref_no' => $request->get('sel_container_batch'),
                'dr' => 0,
                'cr' => $request->get('container_qty'),
                'created_at' => Carbon::now()->toDateTimeString()
            ]
        );

        Log::notice('Rooting Item_id: '.$request->get('item_id').' User: '.request()->user()->name.' bulk_number: '.$request->get('bulk_number').' customer_id: '.$this->getCustomerId($request->get('bulk_number')));
        return redirect('/growthstocks/showbulklabels/' . $bulk_number)->with('success', 'Rooting stock added to ' . $request->get('section') . ' section');
    }

    public function getRndGrowthRoom(Request $request)
    {
        if ($request->has('search_item_name')) {
            $item = Item::where('bar_code', $request->input('search_item_name'))->first();
        }
        if (isset($item)) {
            $growthstocks = GrowthRoomStock::where('accept', 1)
                ->where('type', 'R&D')
                ->where('transfer', 0)
                ->where('status', '!=', 'Transfer Room')
                ->where('item', $item->id)
                ->orderBy('serial_no',  'asc')
                ->paginate(config('app.perPage'))
                ->appends(['search_item_name' => $request->input('search_item_name')]);
        } else {
            $growthstocks = GrowthRoomStock::where('accept', 1)
                ->where('type', 'R&D')
                ->where('transfer', 0)
                ->where('status', '!=', 'Transfer Room')
                ->orderBy('serial_no',  'asc')
                ->paginate(config('app.perPage'));
        }

        return view('growthstocks.rndgrowthroom', compact('growthstocks'));
    }

    public function getRndTransferRoom(Request $request)
    {
        if ($request->has('search_item_name')) {
            $item = Item::where('bar_code', $request->input('search_item_name'))->first();
        }
        if (isset($item)) {
            $growthstocks = GrowthRoomStock::where('accept', 1)
            ->where('type', 'R&D')
            ->where('transfer', 0)
            ->where('status', 'Transfer Room')
            ->where('complete', '=', 0)
            ->where('flag', '=', null)
            ->where('item', $item->id)
//            ->orderBy('media', 'container', 'serial_no', 'ref_no', 'asc')
            ->orderBy('bulk_number', 'asc')
            ->paginate(config('app.perPage'))
            ->appends(['search_item_name' => $request->input('search_item_name')]);
        } else {
        $growthstocks = GrowthRoomStock::where('accept', 1)
            ->where('type', 'R&D')
            ->where('transfer', 0)
            ->where('status', 'Transfer Room')
            ->where('complete', '=', 0)
            ->where('flag', '=', null)
            ->orderBy('media', 'container', 'serial_no', 'ref_no', 'asc')
            ->paginate(config('app.perPage'));
        }

        return view('growthstocks.rndtransferroom', compact('growthstocks'));
    }

    public function getProdGrowthRoom(Request $request)
    {
        if ($request->has('search_item_name')) {
            $item = Item::where('bar_code', $request->input('search_item_name'))->first();
        }
        if (isset($item)) {
            $growthstocks = GrowthRoomStock::where('accept', 1)
                ->where(function ($q) {
                    $q->where('type', 'Production')
                        ->orWhere('transfer', 1);
                })
                ->where('status', '!=', 'Transfer Room')
                ->where('status', '!=', 'Rooting')
                ->where('complete', '=', 0)
                ->where('item', $item->id)
                ->orderBy('bulk_number', 'asc')
                ->paginate(config('app.perPage'))
                ->appends(['search_item_name' => $request->input('search_item_name')]);
        } else {
            $growthstocks = GrowthRoomStock::where('accept', 1)
                ->where(function ($q) {
                    $q->where('type', 'Production')
                        ->orWhere('transfer', 1);
                })
                ->where('status', '!=', 'Transfer Room')
                ->where('status', '!=', 'Rooting')
                ->where('complete', '=', 0)
                ->orderBy('media', 'container', 'desc')
                ->paginate(config('app.perPage'));
        }

        return view('growthstocks.prodgrowthroom', compact('growthstocks'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProdTransferRoom(Request $request)
    {
        if ($request->has('search_item_name')) {
            $item = Item::where('bar_code', $request->input('search_item_name'))->first();
        }

        if (isset($item)) {
            $growthstocks = GrowthRoomStock::where('accept', 1)
            ->where(function ($q) {
                $q->where('type', 'Production')
                    ->orWhere('transfer', 1);
            })
            ->where('status', '=', 'Transfer Room')
            ->where('item', $item->id)
            ->where('complete', '=', 0)
            ->where('flag', null)
//            ->orderBy('media', 'container', 'desc')
            ->orderBy('bulk_number', 'asc')
            ->paginate(config('app.perPage'))
            ->appends(['search_item_name' => $request->input('search_item_name')]);
        } else {
            $growthstocks = GrowthRoomStock::where('accept', 1)
            ->where(function ($q) {
                $q->where('type', 'Production')
                    ->orWhere('transfer', 1);
            })
            ->where('status', '=', 'Transfer Room')
            ->where('complete', '=', 0)
            ->where('flag', null)
            ->orderBy('media', 'container', 'desc')
            ->paginate(config('app.perPage'));
        }

        return view('growthstocks.prodtransferroom', compact('growthstocks'));
    }

    public function getRooting(Request $request)
    {
        if ($request->has('search_item_name')) {
            $item = Item::where('bar_code', $request->input('search_item_name'))->first();
        }
        if (isset($item)) {
            $growthstocks = GrowthRoomStock::where('accept', 3)
                ->where(function ($q) {
                    $q->where('type', 'Production')
                        ->orWhere('transfer', 1);
                })
                ->where([
                    ['status', '=', 'Rooting'],
                    ['flag', '!=', 6],
                    ['item', '=', $item->id]
                ])
                ->orderBy('bulk_number', 'asc')
                ->paginate(config('app.perPage'))
                ->appends(['search_item_name' => $request->input('search_item_name')]);
        } else {
            $growthstocks = GrowthRoomStock::where('accept', 3)
                ->where(function ($q) {
                    $q->where('type', 'Production')
                        ->orWhere('transfer', 1);
                })
                ->where([
                    ['status', '=', 'Rooting'],
                    ['flag', '!=', 6],
                ])
                ->orderBy('media', 'container', 'serial_no', 'ref_no', 'desc')
                ->paginate(config('app.perPage'));
        }


//        foreach ($growthstocks as $growthstock) {
//            $media = DB::table('media')->where('id', $growthstock['media'])->first();
//            $container = DB::table('container')->where('id', $growthstock['container'])->first();
//            $item = DB::table('item')->where('id', $growthstock['item'])->first();
//
//            $growthstock['media'] = $media->name;
//            $growthstock['container'] = $container->name;
//            if (is_object($item)) {
//                $growthstock['item'] = $item->item_name;
//            } else {
//                $growthstock['item'] = "No Item";
//            }
//            //$growthstock['item'] = $item->item_name;
//        }

        return view('growthstocks.rooting', compact('growthstocks'));
    }

    /**
     * @param int $m_type
     * @param int $media
     */
    public function getContainersByMedia(int $m_type = 1, int $media = 1): \Illuminate\Http\JsonResponse
    {
        $containers = array();
//        $results = DB::select(
//            'SELECT
//                    transfer_room_stock.ref_no AS ref,
//                    transfer_room_stock.media,
//                    transfer_room_stock.m_type,
//                    t_room_stock.ref_no,
//                    transfer_room_stock.container,
//                    container.name,
//                    container.id
//                    FROM transfer_room_stock
//                    INNER JOIN t_room_stock ON transfer_room_stock.ref_no = t_room_stock.ref_no
//                    INNER JOIN container ON transfer_room_stock.container = container.id
//                    WHERE transfer_room_stock.accept =1 AND transfer_room_stock.m_type = :m_type AND transfer_room_stock.media = :media
//                    GROUP BY transfer_room_stock.container
//                    HAVING (SUM(t_room_stock.dr)-SUM(t_room_stock.cr)) > 0
//                    ORDER BY m_type DESC',
//            ['m_type' => $m_type, 'media' => $media]
//        );
//
//        foreach ($results as $key => $value) {
//            $data['name'] = $value->name;
//            $data['id'] = $value->id;
//            $containers['data'][] = $data;
//        }
//
//        echo json_encode($containers);

        $results = DB::table("transfer_room_stock")
            ->join('t_room_stock', 'transfer_room_stock.ref_no', '=', 't_room_stock.ref_no')
            ->join('container', 'transfer_room_stock.container', '=', 'container.id')
            ->select(DB::raw("
                transfer_room_stock.ref_no AS ref,
                transfer_room_stock.media,
                transfer_room_stock.m_type,
                t_room_stock.ref_no,
                transfer_room_stock.container,
                container.id,
                container.name
                "))
            ->where([
                ['transfer_room_stock.accept', '=', 1],
                ['transfer_room_stock.m_type', '=', $m_type],
                ['transfer_room_stock.media', '=', $media]
            ])
            ->groupBy(DB::raw("transfer_room_stock.container"))
            ->havingRaw('SUM(t_room_stock.dr)-SUM(t_room_stock.cr) > ?', [0])
            ->get();

        foreach ($results as $trs) {
            $data['id'] = $trs->id;
            $data['name'] = $trs->name;
            $containers['data'][] = $data;
        }
        return response()->json($containers);
    }

    /**
     * get available containers in given media stock
     *
     * @param int $m_type
     * @param int $container
     * @param int $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function getContainerBatches(int $m_type = 1, int $container = 1, int $media = 1): \Illuminate\Http\JsonResponse
    {
        $batches = array();
//        $results = DB::select(
//            'SELECT
//                    transfer_room_stock.ref_no AS ref,
//                    transfer_room_stock.media,
//                    transfer_room_stock.m_type,
//                    t_room_stock.ref_no,
//                    transfer_room_stock.container,
//                    transfer_room_stock.user,
//                    (SUM(t_room_stock.dr)-SUM(t_room_stock.cr)) AS qty
//                    FROM transfer_room_stock
//                    INNER JOIN t_room_stock ON transfer_room_stock.ref_no = t_room_stock.ref_no
//                    WHERE transfer_room_stock.accept = 1 AND transfer_room_stock.m_type = :m_type AND transfer_room_stock.media = :media AND transfer_room_stock.container = :container
//                    GROUP BY media, container, ref
//                    HAVING (SUM(t_room_stock.dr)-SUM(t_room_stock.cr)) > 0
//                    ORDER BY ref ASC',
//            ['m_type' => $m_type, 'container' => $container, 'media' => $media]
//        );
//
//        foreach ($results as $key => $value) {
//            $data['qty'] = $value->qty;
//            $data['id'] = $value->ref_no;
//            $data['operator'] = $value->user;
//            $batches['data'][] = $data;
//        }
//        echo json_encode($batches);

        $results = DB::table("transfer_room_stock")
            ->join('t_room_stock', 'transfer_room_stock.ref_no', '=', 't_room_stock.ref_no')
            ->join('users', 'transfer_room_stock.user_id', '=', 'users.id')
            ->select(DB::raw("
                transfer_room_stock.ref_no AS ref,
                (SUM(t_room_stock.dr)-SUM(t_room_stock.cr)) AS qty,
                transfer_room_stock.media,
                transfer_room_stock.m_type,
                t_room_stock.ref_no,
                transfer_room_stock.container,
                users.user_code AS user_code,
                users.name AS user_name
                "))
            ->where([
                ['transfer_room_stock.accept', '=', 1],
                ['transfer_room_stock.m_type', '=', $m_type],
                ['transfer_room_stock.media', '=', $media],
                ['transfer_room_stock.container', '=', $container]
            ])
            ->groupBy(DB::raw("media, container, ref"))
            ->havingRaw('SUM(t_room_stock.dr)-SUM(t_room_stock.cr) > ?', [0])
            ->get();

        foreach ($results as $trs) {
            $data['qty'] = $trs->qty;
            $data['id'] = $trs->ref_no;
            $data['operator'] = substr($trs->user_name, 0, 20);
            $batches['data'][] = $data;
        }

        return response()->json($batches);
    }

    /**
     * Get sub categories
     *
     * @param int $id
     */
    public function getSubCategories($id = 0)
    {
        $subCatData = array();
//        $categories = Category::find($id);
//        foreach ($categories->subCategories as $key => $value) {
//            $user = array();
//            $user['id'] = $value->id;
//            $user['cat_id'] = $value->cat_id;
//            $user['subcat'] = $value->subcat;
//            $userData['data'][] = $user;
//        }

        $subCatData['data'] = SubCategory::where([
            ['cat_id' , '=', $id],
            ['active', '=', 1]
        ])->orderBy('subcat')->get();

        return response()->json($subCatData);
    }

    public function getItems($id = 0)
    {
        $items = Item::where([
            ['sub_cat', '=', $id],
            ['active', '=', 1],
            ])
            ->orderBy('item_name')
            ->get();

        $itemData = array();
        $data = array();

        foreach ($items as $item){
            $data['id'] = $item->id;
            $data['cat_id'] = $item->cat_id;
            $data['sub_cat'] = $item->subcat;
            $data['item_name'] = $item->item_name;
            $data['item_code'] = $item->item_code;
            $data['bar_code'] = $item->bar_code;
            $itemData['data'][] = $data;
        }

        return response()->json($itemData);
    }

    public function transferToGrowthRoom($id, $type = null)
    {
        $growth = GrowthRoomStock::find($id);

        if (strtotime($growth->last_updated) > strtotime("2021-10-27 00:00:00")) {
            self::removeDoneStatus($growth, 3);
        }

        $growth->status = 'Growth Room';

        if ($growth->flag == 2){
            $growth->flag = null;
            $growth->accept = 1;
            $growth->complete = 0;
        }
        $growth->last_updated = date("Y-m-d H:i:s");
        $growth->save();

        return redirect('/')->with('success', 'Record successfully transferred to growth room');
    }

    public function transferToTransferRoom($id, $type = null)
    {

        $growth = GrowthRoomStock::find($id);
        $growth->status = 'Transfer Room';

        if ($type == 2) {
            $growth->type = 'Production';
        }

        if ($growth->flag == 2){
            $growth->flag = null;
            $growth->accept = 1;
            $growth->complete = 0;
        }

        $growth->last_updated = date("Y-m-d H:i:s");
        $growth->save();

        self::doneStatus($growth, 3);

        return redirect('/')->with('success', 'Record successfully transferred to Transfer room');
    }

    public function transferToProduction($id)
    {
        $growth = GrowthRoomStock::find($id);
        $growth->status = 'Growth Room';
        $growth->type = 'Production';
        $growth->transfer = 1;
        $growth->last_updated = date("Y-m-d H:i:s");

        if ($growth->flag == 2){
            $growth->flag = null;
            $growth->accept = 1;
            $growth->complete = 0;
        }

        $growth->save();

        return redirect('/')->with('success', 'Record successfully transferred to Production ' . $growth->status);
    }

    public function getPlants($container_id, $container_batch_id, $container_quantity)
    {
        $plantData = array();
        $plantData['data'] = DB::table('container')->where('id', $container_id)->first()->item_capacity;
        return response()->json($plantData);
    }

    public function reject(Request $request, $id)
    {

        foreach ($request->input('reasons') as $key => $value) {
            if ($value == 'reason_6'){
                return redirect('/')->with('error', 'You can not select Type Error as the rejection reason. Please try again');
            }
        }
        $reject_stock = GrowthRoomStock::find($id);
        $reject_stock->reasons = json_encode($request->input('reasons'));
        $reject_stock->accept = 2;
        $reject_stock->last_updated = date('Y-m-d H:i:s');
        if ($reject_stock->save()) {
            $item = new Item();

            $performance_item = Performance::where('bulk_number', '=', $reject_stock->bulk_number)->where('item_qty', '>', 0)->first();

            $performance = new Performance([
                'bulk_number' => $reject_stock->bulk_number,
                'user_id' => $reject_stock->userDetail->id,
                'item_id' => $reject_stock->item,
                'media_id' => $reject_stock->media,
                'container_id' => $reject_stock->container,
                'max_container_capacity' => $performance_item->max_container_capacity,
                'total_containers' => 1,
                'item_qty' => 0,
                'rejected_qty' => $performance_item->max_container_capacity,
                'status' => $performance_item->status,
                'add_amount' => 0,
                'rejected_amount' => $item->getRate($reject_stock->item) * (int) $performance_item->max_container_capacity,
                'customer_id' => $performance_item->customer_id,
                'plant_size' => $performance_item->plant_size,
            ]);
            $performance->timestamps = false;               // disable timestamps for this creation
            $performance->created_at = $reject_stock->date; // Override created_at value to get the correct week
            $performance->updated_at = date('Y-m-d H:i:s');
            $performance->save();

            /**
             * checks for how many containers are
             * left to perform before setting the performance status
             * setting completed for the time been
             * TODO: get the type completed or dispatched
             */
//            $reject_items = GrowthRoomStock::where([
//                ['bulk_number', '=', $reject_stock->bulk_number],
//                ['reasons', '=', null],
//            ])->where(function($q) {
//                    $q->where('complete', null)
//                        ->orWhere('complete', 0);
//                })->count();

            if (self::grsRecords($reject_stock->bulk_number) == 0) {
                DB::table('performance')->where('bulk_number', '=', $reject_stock->bulk_number)->update(['status' => 3], ['timestamps' => false]);
            }

            return redirect('/')->with('success', 'Record Rejected !!');
        }
        return back()->withInput();
    }

    public function complete($id)
    {
        $growth = GrowthRoomStock::find($id);
        $growth->complete = 1;
        $growth->last_updated = date('Y-m-d H:i:s');
        $growth->save();

        self::doneStatus($growth);

        /**
         * check if all Plant containers are completed
         * exclude rejected items in the bulk
         * update the performance bulk status
         */
//        $grs_items = GrowthRoomStock::where([
//            ['bulk_number', '=', $growth->bulk_number],
//            ['complete', '=', 0],
//            ['accept', '<>', 2],
//        ])->count();

        if (self::grsRecords($growth->bulk_number) == 0) {
            DB::table('performance')->where('bulk_number', '=', $growth->bulk_number)->update(['status' => 3], ['timestamps' => false]);
        }

        return redirect('/')->with('success', 'Plant container has been mark as completed successfully!!');
    }

    public function dispatch($id)
    {
        $growth = GrowthRoomStock::find($id);
        $growth->complete = 2;
        $growth->status = 'Dispatched';
        $growth->last_updated = date('Y-m-d H:i:s');
        $growth->save();

        self::doneStatus($growth, 2);

//        $grs_items = GrowthRoomStock::where([
//            ['bulk_number', '=', $growth->bulk_number],
//            ['complete', '=', null],
//            ['accept', '<>', 2],
//        ])->count();

//        Log::notice('grs_items_new : '.self::grsRecords($growth->bulk_number));

        if (self::grsRecords($growth->bulk_number) == 0) {
            DB::table('performance')->where('bulk_number', '=', $growth->bulk_number)->update(['status' => 4], ['timestamps' => false]);
        }

        return redirect('/')->with('success', 'Plant container has been mark as dispatched successfully!!');
    }

    public function showBulkLabels(Request $request, $bulk_no = null)
    {
        if ($request->input('bulk_no')) {
            $bulk_no = $request->input('bulk_no');
        }
        if ($bulk_no) {
            $bulks = GrowthRoomStock::where('bulk_number', $bulk_no)->get();
            $plant_size = GrowthRoomStock::getPlantSize($bulk_no);
            return view('growthstocks.showbulklabels', compact('bulks', 'bulk_no', 'plant_size'));
        }
        return view('growthstocks.showbulklabels');
    }

    public function createQrCode(Request $request)
    {
        $serial_no      = $request->serial_no;
        $qr_image_name  = $request->qr_image_name;

        if ($serial_no && $qr_image_name) {
            $this->generateQrCodeImage($serial_no, $qr_image_name);

            return response()->json([
                'success' => 'Qr Code Image is successfully generated',
                'img' => 'labels/qr/' . $qr_image_name
            ]);
        }
    }

    public function printLabels(Request $request)
    {
        $serial_nos   = $request->serial_nos;
        $printer_name = request()->user()->printer->name ?? $this->DEFAULT_PRINTER;
        if (empty($serial_nos)) {
            return response()->json(['error' => 'Please select label to print']);
        }

        foreach ($serial_nos as $serial_no) {
            $data = GrowthRoomStock::where('serial_no', $serial_no)->first();
            $tf = explode(" ", $data->item_serial_no);
            $created_date       = new DateTime($data->date);

            $qr_image           = $data->img_qr;
            $created_year_week  = $created_date->format("y") . ' ' . $data->item_serial_no;
            $item_code          = $data->itemVariety->bar_code ?? '';
            $serial_code        = $data->serial_no;
            $bulk_code          = '['.$data->bulk_number.']';

            $item_code = $item_code.' /'.$data->mediaVariety->name;

            $this->generateLabel($qr_image, $created_year_week, $bulk_code, $item_code, $serial_code, $printer_name);
        }
        return response()->json(['success' => 'Printed ' . count($serial_nos) . ' labels successfully !!']);
    }

    public function generateLabel($qr_image, $created_year_week, $bulk_code, $item_code, $serial_code, $printer_name)
    {
        if (!file_exists(public_path('/labels/qr/' . $qr_image))) {
            $this->generateQrCodeImage($serial_code, $qr_image);
        }
        $pdf = new FPDF('L', 'mm', array(16, 30));
        $pdf->SetMargins(1, 1);
        $pdf->image(public_path('labels/qr/' . $qr_image), 0.5, null, 14);
        $pdf->SetFont('Times', '', 6);
        $posX = 15;
        $pdf->Text($posX, 10, $item_code);
        $pdf->SetFont('Times', '', 7.8);
        $pdf->Text($posX,  4, $created_year_week);
        $pdf->Text($posX,  7, $bulk_code);
        $pdf->Text($posX, 13, $serial_code);

        $label_path = public_path('labels/' . $serial_code . '.pdf');
        $pdf->Output("F", $label_path);

        /**
         * ToDo: Uncomment below code when we have multiple label printers
         * $this->labelPrinter($label_path, $printer_name);
         */
        $printCmd = "lpr -P " . $printer_name . " -o fit-to-page ";
        $printCmd .= $label_path;
        exec($printCmd);
    }

    public function printBulkNumber($bulk_number){

        if (empty($bulk_number)) {
            return back()->with('error', 'Could not find the bulk number in the system');
        }
        $posX = 5;
        $pdf = new FPDF('L', 'mm', array(16, 30));
        $pdf->AddPage();
        $pdf->SetMargins(1, 1);
        $pdf->SetFont('Times', '',9);
        $pdf->Text($posX,  3, 'Bulk Number');
        $pdf->SetFont('Times', 'B',25);
        $pdf->Text($posX,  11, $bulk_number);

        $label_path = public_path('labels/bn_' . $bulk_number . '.pdf');
        $pdf->Output("F", $label_path);

        $this->labelPrinter($label_path, request()->user()->printer->name ?? $this->DEFAULT_PRINTER);

        return redirect()->route('showbulklabels', ['bulk_no' => $bulk_number])->with('success', 'Printed Bulk ' . $bulk_number . ' label successfully !!');
    }

    protected function labelPrinter($label_path, $printer_name){
        $printCmd = "lpr -P " . $printer_name . " -o fit-to-page ";
        $printCmd .= $label_path;

        exec($printCmd);
    }

    protected function getCustomerId($bulk_number){
        return DB::table('performance')->where('bulk_number', $bulk_number)->first()->customer_id ?? null;
    }

    protected function getPlantSize($bulk_number){
        return DB::table('performance')->where('bulk_number', $bulk_number)->first()->plant_size ?? null;
    }

    public function searchSerialCode(){
        return view('growthstocks.searchserialcode');
    }

    /**
     * this will help to identify (deduct bulk amounts) bulk
     * that are partially completed or dispatched
     */
    protected function doneStatus($growth, $status = 1){
        $performance_item = Performance::where('bulk_number', '=', $growth->bulk_number)->where('item_qty', '>', 0)->first();

        $performance = new Performance([
            'bulk_number' => $growth->bulk_number,
            'user_id' => $growth->userDetail->id,
            'item_id' => $growth->item,
            'media_id' => $growth->media,
            'container_id' => $growth->container,
            'max_container_capacity' => $performance_item->max_container_capacity,
            'total_containers' => 1,
            'item_qty' => 0,
            'rejected_qty' => 0,
            'status' => $performance_item->status,
            'done_qty' => $performance_item->max_container_capacity,
            'done_status' => $status,
            'add_amount' => 0,
            'rejected_amount' => 0,
            'customer_id' => $performance_item->customer_id,
            'plant_size' => $performance_item->plant_size,
        ]);
        $performance->timestamps = false;               // disable timestamps for this creation
        $performance->created_at = $growth->date; // Override created_at value to get the correct week
        $performance->updated_at = date('Y-m-d H:i:s');

        if ($performance->save()){
            Log::notice(' User: '.request()->user()->name.' Bulk Number: '.$growth->bulk_number.' Serial Number: '.$growth->serial_no.' done status: '. $status);
        }

    }

    /**
     * @param $growth
     * @param int $status
     */
    protected function removeDoneStatus($growth, int $status = 3){
        Performance::where([
            ['bulk_number', '=', $growth->bulk_number],
            ['done_status', '=', 3],
            ])->limit(1)->delete();
        Log::notice(' User: '.request()->user()->name.' Bulk Number: '.$growth->bulk_number.' Serial Number: '.$growth->serial_no.' done status: '. $status);
    }

    /**
     * @param $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function revert($id){
        GrowthRoomStock::where('id', '=', $id)->update(['flag' => null], ['timestamps' => false]);
        return redirect('/')->with('success', 'Record successfully revert back');
    }

    /**
     * @param $bulk_number
     * @param null $condition
     * @return mixed
     */
    protected function grsRecords($bulk_number, $condition = null)
    {
        $query = GrowthRoomStock::where('bulk_number', '=', $bulk_number);

        // Is not rejected
        $query->whereNull('reasons');

        $query->where('accept', '<>', 2);

        // multiplication or rooting create
        if ($condition == 'create') {
            $query->where('status', '!=', 'Transfer Room');
        }

        // Is not completed or dispatched
        $query->where(function($q) {
            $q->where('complete', null)
                ->orWhere('complete', 0);
        });
        $query->where(function($q) {
            $q->where('flag', null)
                ->orWhere('flag', 2);
        });

        return $query->count();
    }
}
