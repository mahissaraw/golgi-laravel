<?php

namespace App\Http\Controllers;

use App\Reason;
use Illuminate\Http\Request;

class ReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reasons = Reason::get();
        return view('reasons.index', ['reasons' => $reasons]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'reason_name'=>'required'
        ]);
        $reason = new Reason([
            'name' => $request->get('reason_name')
        ]);
        if ($reason->save()){
            return redirect('/reasons')->with('success', 'New reason has been added');
        }
        return back()->withInput()->with('error', 'Coud not add reason');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reason  $reason
     * @return \Illuminate\Http\Response
     */
    public function show(Reason $reason)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reason  $reason
     * @return \Illuminate\Http\Response
     */
    public function edit(Reason $reason)
    {
        $reason = Reason::find($reason->id);

        return view('reasons.edit', compact('reason'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reason  $reason
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $reason)
    {
        $request->validate([
            'reason_name'=>'required',

        ]);

        $reason = Reason::find($reason);
        $reason->name = $request->get('reason_name');
        $reason->save();

        return redirect('/reasons')->with('success', 'Reason has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reason  $reason
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reason $reason)
    {
        $reason = Reason::find($reason->id);
        if ($reason->delete()){
            return redirect('/reasons')->with('success', 'Reason has been deleted Successfully');
        }
        return back()->withInput()->with('error', 'Reason coud not be deleted');
    }
}
