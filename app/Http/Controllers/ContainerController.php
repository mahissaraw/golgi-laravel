<?php

namespace App\Http\Controllers;

use App\Container;
use Illuminate\Http\Request;

class ContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('select_inactive')) {
            $containers = Container::where('active', 0)->get();
        } else {
            $containers = Container::where('active', 1)->get();
        }
        return view('containers.index', ['containers' => $containers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'item_capacity' => 'required',
            'media_capacity' => 'required'
        ]);

        $container = new Container();
        $container->name = $request->input('name');
        $container->item_capacity = $request->input('item_capacity');
        $container->media_capacity = $request->input('media_capacity');

        if ($container->save()) {
            return redirect('/containers')->with('success', 'New Container has been added');
        }
        return back()->withInput()->with('error', 'Coud not add Container');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function show(Container $container)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function edit(Container $container)
    {
        $container = Container::find($container->id);
        return view('containers.edit', compact('container'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Container $container)
    {
        $request->validate([
            'name' => 'required',
            'item_capacity' => 'required',
            'media_capacity' => 'required'
        ]);

        $container = Container::find($container->id);
        $container->name = $request->input('name');
        $container->item_capacity = $request->input('item_capacity');
        $container->media_capacity = $request->input('media_capacity');
        $container->active = $request->get('active');

        if ($container->save()) {
            return redirect('/containers')->with('success', 'New Container has been updated');
        }
        return back()->withInput()->with('error', 'Coud not updated Container');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function destroy(Container $container)
    {
        $container = Container::find($container->id);
        if ($container->delete()) {
            return redirect('/containers')->with('success', 'Container has been deleted Successfully');
        }
        return back()->withInput()->with('error', 'Container coud not be deleted');
    }
}
