<?php

namespace App\Http\Controllers;

use App\Media;
use App\MediaRequest;
use App\RequestedQuantityLog;
use App\User;
use App\Container;
use Illuminate\Http\Request;

class MediaRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $media_requests = MediaRequest::orderBy('created_at', 'desc')->paginate(25);
        return view('media_requests.index', compact('media_requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('media_requests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'media_id' => 'required',
            'container_id' => 'required',
            'media_amount' => 'required',
            'requested_quantity' => 'required',
            'requested_date' => 'required'
        ]);

        $mr = new MediaRequest([
            'requested_by' => auth()->user()->id,
            'media_type' => $request->get('media_type'),
            'media_id' => $request->get('media_id'),
            'container_id' => $request->get('container_id'),
            'media_amount' => $request->get('media_amount'),
            'requested_quantity' => $request->get('requested_quantity'),
            'requested_date' => $request->get('requested_date'),
            'need_date' => $request->get('need_date'),
            'location' => $request->get('location'),
        ]);

        if ($mr->save()){
            self::setRequestedQuantityLog($mr->id, $mr->requested_quantity);
            return redirect('/media_requests')->with('success', 'New media request has been added');
        }

        return back()->withInput()->with('error', 'Could not add the request');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MediaRequest  $mediaRequest
     * @return \Illuminate\Http\Response
     */
    public function show(MediaRequest $mediaRequest)
    {
        $media_request = MediaRequest::find($mediaRequest->id);
        return view('media_requests.show', compact('media_request' ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MediaRequest  $mediaRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(MediaRequest $mediaRequest)
    {
        $readonly = 'readonly';
        $media_request = MediaRequest::find($mediaRequest->id);
        $medias = Media::where('active', 1)->get();
        $containers = Container::where('active', 1)->get();
        if (auth()->user()->id  == $media_request->requested_by ||
            auth()->user()->hasAnyRole(['Administrator'])){
            $readonly = false;
        }

        return view('media_requests.edit', compact('media_request',  'medias', 'containers', 'readonly'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MediaRequest  $mediaRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MediaRequest $mediaRequest)
    {
        $request->validate([
            'media_id' => 'required',
            'container_id' => 'required',
            'media_amount' => 'required',
            'requested_quantity' => 'required',
            'requested_date' => 'required'
        ]);

        $mr = MediaRequest::find($mediaRequest->id);
        if ($request->get('requested_quantity') != $request->get('adjustment_history')){
            self::setRequestedQuantityLog($mr->id, $request->get('requested_quantity'));
        }
        $mr->media_type = $request->get('media_type');
        $mr->media_id = $request->get('media_id');
        $mr->container_id = $request->get('container_id');
        $mr->media_amount = $request->get('media_amount');
        $mr->requested_quantity = $request->get('requested_quantity');
        $mr->requested_date = $request->get('requested_date');
        $mr->need_date = $request->get('need_date');
        $mr->location = $request->get('location');
        $mr->completed_quantity = $request->get('completed_quantity');
        $mr->incomplete_reason = $request->get('incomplete_reason');
        $mr->user_id = auth()->user()->id;
        $mr->save();
        return redirect('/media_requests/'.$mr->id)->with('success', 'media requests has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MediaRequest  $mediaRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(MediaRequest $mediaRequest)
    {
        $mr = MediaRequest::find($mediaRequest->id);
        $mr->delete();
        return redirect('/media_requests')->with('success', 'Record has been deleted Successfully!');
    }

    protected function requestedUsers() {
        $admin_user = auth()->user()->hasRole('Administrator');
        if (!$admin_user) {
            $role = 'Operator';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '=', $role);
            })->where('active', '=', '1')->select('name', 'id', 'user_code')->get();
        } else {
            $users = User::where('active', '=', '1')->select('name', 'id', 'user_code')->get();
        }

        return $users;
    }

    /**
     * Cancelled the specified resource from storage.
     * Set the current user as the cancelled person
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id = null){
        $mr = MediaRequest::find($id);
        $mr->cancelled_by = auth()->user()->id;
        $mr->save();
        return redirect('/media_requests/'.$mr->id)->with('success', 'Record has been Cancelled Successfully!');
    }

    /**
     * Revert Cancelled the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function revert($id = null){
        $mr = MediaRequest::find($id);
        $mr->cancelled_by = null;
        $mr->save();
        return redirect('/media_requests/'.$mr->id)->with('success', 'Record has been Revert Successfully!');
    }

    protected function setRequestedQuantityLog($mr_id, $quantity): void{
        $rql = new RequestedQuantityLog([
                'media_request_id' => $mr_id,
                'requested_quantity' => $quantity,
                'user_id' => auth()->user()->id,
            ]);
        $rql->save();
    }
}
