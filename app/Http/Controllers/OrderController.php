<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Customer;
use App\OrderItem;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();

        return view('orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::where('is_active', '=', 1)->get();
        return view('orders.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'customer_id' => 'required',
            'order_date' => 'required',
            'required_date' => 'required'
        ]);

        $order = new Order();
        $order->customer_id     = $request->get('customer_id');
        $order->order_date      = $request->get('order_date');
        $order->required_date   = $request->get('required_date');
        $order->user_id         = Auth::id();
        $order->order_status    = 0;

        if ($order->save()) {
            return redirect('/orders/'.$order->id)->with('success', 'new order has been created');
        }

        return back()->withInput()->with('error', 'Could not create an order');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        return view('orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $customers = Customer::where('is_active', '=', 1)->get();
        return view('orders.edit', compact('order','customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'customer_id' => 'required',
            'order_date' => 'required',
            'required_date' => 'required'
        ]);

        $order = Order::find($id);

        $order->customer_id     = $request->get('customer_id');
        $order->order_date      = $request->get('order_date');
        $order->required_date   = $request->get('required_date');
        $order->shipped_date    = $request->get('shipped_date');
        $order->order_status    = $request->get('order_status');

        if ($order->save()) {
            return redirect('/orders/'.$order->id)->with('success', 'Update order details');
        }

        return back()->withInput()->with('error', 'Could not update order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        if($order->performance->isEmpty()){
            $order->delete();
            OrderItem::where('order_id', '=', $order->id)->delete();
            return redirect('/orders')->with('success', 'Order has been removed successfully');
        }else {
            return redirect()->back()->with('error', 'This Order can\'t been deleted records exist in other tables');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyOrderItem($id)
    {
        $order_item = OrderItem::find($id);
        $order_id = $order_item->order_id;
        $order_item->delete();

        return redirect()->back()->with('success', 'Item has been removed from the order successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeOrderItems(Request $request)
    {
        $request->validate([
            'selected_order_id' => 'required',
            'selected_item_id' => 'required',
            'quantity' => 'required'
        ]);

        $order_item = new OrderItem();
        $order_item->order_id     = $request->get('selected_order_id');
        $order_item->item_id      = $request->get('selected_item_id');
        $order_item->quantity     = $request->get('quantity');

        if ($order_item->save()) {
            return redirect('/orders/'.$order_item->order_id)->with('success', 'new item has been added to the order');
        }

        return back()->withInput()->with('error', 'Could not add an item');
    }
}
