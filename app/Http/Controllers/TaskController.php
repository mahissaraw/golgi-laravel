<?php

namespace App\Http\Controllers;

use App\Frequency;
use App\User;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use phpDocumentor\Reflection\Types\Self_;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;


class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }

    public function create()
    {
        $admin_user = auth()->user()->hasRole('Administrator');
        if (!$admin_user) {
            $role = 'Operator';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '=', $role);
            })->where([['active', '=', '1'], ['user_code', 'NOT LIKE', 'CUS_%']])->pluck('name', 'id');
        } else {
            $users = User::where([['active', '=', '1'], ['user_code', 'NOT LIKE', 'CUS_%']])->pluck('name', 'id');
        }
        $frequencies = Frequency::all()->pluck('frequency', 'id');
        return view('tasks.create', compact('users', 'frequencies'));
    }

    public function store(Request $request)
    {
        //dd(date("Y-m-d H:i:s", strtotime($request->get('start_date'))));
        $request->validate([
            'user_id' => 'required',
            'description' => 'required|string',
            'freq' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        $task = new Task([
            'user_id' => $request->get('user_id'),
            'description' => $request->get('description'),
            'frequency_id' => $request->get('freq'),
            'date_started' => date("Y-m-d H:i:s", strtotime($request->get('start_date'))),
            'date_completed' => date("Y-m-d H:i:s", strtotime($request->get('end_date'))),
        ]);
        $task->save();

        return redirect('/tasks')->with('success', 'Task has been added');
    }

    public function edit($id)
    {
        $task = Task::find($id);

        $admin_user = auth()->user()->hasRole('Administrator');
        if (!$admin_user) {
            $role = 'Operator';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '=', $role);
            })->where([['active', '=', '1'], ['user_code', 'NOT LIKE', 'CUS_%']])->pluck('name', 'id');
        } else {
            $users = User::where([['active', '=', '1'], ['user_code', 'NOT LIKE', 'CUS_%']])->pluck('name', 'id');
        }

        $frequencies = Frequency::all()->pluck('frequency', 'id');

        return view('tasks.edit', compact('task', 'users', 'frequencies'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required',
            'description' => 'required|string',
            'frequency_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        $task = Task::find($id);
        $task->user_id = $request->get('user_id');
        $task->description = $request->get('description');
        $task->frequency_id = $request->get('frequency_id');
        $task->date_started = date("Y-m-d H:i:s", strtotime($request->get('start_date')));
        $task->date_completed = date("Y-m-d H:i:s", strtotime($request->get('end_date')));
        $task->is_complete = $request->get('is_complete');
        $task->save();

        return redirect('/tasks')->with('success', 'Task has been updated');
    }

    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        return redirect('/tasks')->with('success', 'Task has been deleted Successfully');
    }

    public function mark_completed($id)
    {
        $task = Task::find($id);
        $task->is_complete = 1;
        $task->save();
        return redirect('/tasks')->with('success', 'Task has been makred as Completed!!');
    }
}
