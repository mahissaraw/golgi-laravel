<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function index()
    {
        $medias = Media::all();

        return view('medias.index', compact('medias'));
    }

    public function create()
    {
        return view('medias.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:media',
            'media_type' => 'required|integer'
        ]);
        # Todo: remove this no need to double check
    //     if ($validator->fails()) {
    //         Session::flash('error', $validator->messages()->first());
    //         return redirect()->back()->withInput();
    //    }
        $share = new Media([
            'name' => $request->get('name'),
            'type' => $request->get('media_type')
        ]);
        $share->save();
        return redirect('/medias')->with('success', 'Media: '. $share->name.' has been added');
    }

    public function edit($id)
    {
        $media = Media::find($id);

        return view('medias.edit', compact('media'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'media_type' => 'required|integer'
        ]);

        $media = Media::find($id);
        $media->name = $request->get('name');
        $media->type = $request->get('media_type');
        $media->active = $request->get('active');
        $media->save();

        return redirect('/medias')->with('success', 'Media has been updated');
    }

    public function destroy($id)
    {
        $media = Media::find($id);
        $media->delete();

        return redirect('/medias')->with('success', 'Media has been deleted Successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $data = Media::select("name")
            ->where("name", "LIKE", "%{$request->input('query')}%")
            ->get();

        return response()->json($data);
    }
}