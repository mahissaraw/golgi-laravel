<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use App\SalesRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SalesRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('search_invoice')) {
            $sales_records = SalesRecord::where("invoice_number", "=", $request->input('search_invoice'))->paginate(25);
        } else {
            $sales_records = SalesRecord::orderBy('invoice_date', 'DESC')->paginate(25);
        }
        return view('sales_records.index', ['sales_records' => $sales_records]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name')->get();
        $customers = Customer::where([['type' , '=', 2], ['is_active', '=', 1]])->get();
        return view('sales_records.create', compact( 'categories', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'invoice_date' => 'required',
            'invoice_number' => 'required'
        ]);
        $sale = new SalesRecord([
            'customer_id' => $request->get('customer'),
            'item_id' => $request->get('sel_item'),
            'invoice_number' => $request->get('invoice_number'),
            'invoice_date' => $request->get('invoice_date'),
            'quantity' => $request->get('quantity'),
            'amount' => $request->get('amount'),
            'remarks' => $request->get('remarks'),
            'user_id' => Auth::user()->id,
        ]);
        $sale->save();

        // Enable this to revert back with last entered values
        if($request->input('multiple_entries') !== null){
            return redirect()->back()->withInput()->with('info', 'Sales record has been added!');
        }

        return redirect('/sales_records/')->with('success', 'New sales record has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalesRecord  $salesRecord
     * @return \Illuminate\Http\Response
     */
    public function show(SalesRecord $salesRecord)
    {
        $salesRecord = SalesRecord::find($salesRecord->id);
        return view('sales_records.show', compact('salesRecord'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesRecord  $salesRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(SalesRecord $salesRecord)
    {
        $salesRecord = SalesRecord::find($salesRecord->id);
        return view('sales_records.edit', compact('salesRecord'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesRecord  $salesRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesRecord $salesRecord)
    {
        request()->validate([
            'invoice_date' => 'required',
            'invoice_number' => 'required'
        ]);
        $salesRecord = SalesRecord::find($salesRecord->id);
        $salesRecord->invoice_number = $request->get('invoice_number');
        $salesRecord->invoice_date = $request->get('invoice_date');
        $salesRecord->quantity = $request->get('quantity');
        $salesRecord->amount = $request->get('amount');
        $salesRecord->remarks = $request->get('remarks');
        $salesRecord->save();

        Log::channel('daily')->info(Auth::user()->name.' has updated the sale record '.$salesRecord->id.' Invoice: '.$salesRecord->invoice_number);
        return redirect('/sales_records/')->with('info', 'Sales record has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesRecord  $salesRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesRecord $salesRecord)
    {
        $sr = SalesRecord::find($salesRecord->id);
        $sr->delete();
        return redirect('/sales_records')->with('success', 'Record has been deleted Successfully!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function autocomplete(Request $request)
    {
        $data = SalesRecord::select("invoice_number as name")
            ->where("invoice_number", "LIKE", "%{$request->input('query')}%")
            ->distinct()
            ->get();
        return response()->json($data);
    }

    public function invoiceNumber(string $invoice_number)
    {
        $sales_records = $this->getInvoiceSales($invoice_number);
        return view('sales_records.invoice', compact('sales_records', 'invoice_number'));
    }

    private function getInvoiceSales(string $invoice_number)
    {
        return SalesRecord::where('invoice_number', '=', $invoice_number)->get();
    }
}
