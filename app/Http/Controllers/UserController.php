<?php

namespace App\Http\Controllers;

use App\Printer;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $admin_user = auth()->user()->hasRole('Administrator');
        if ($admin_user) {
            if ($request->has('select_inactive')) {
                $query = User::inActive();
            } else {
                $query = User::active();
            }
        } else {
            $roles = ['Administrator', 'Customer'];

            $query = User::whereHas('roles', function ($q) use ($roles) {
                $q->whereNotIn('name', $roles);

            });
            if ($request->has('select_inactive')) {
                $query->inActive();
            } else {
                $query->active();
            }
        }

        $users = $query->get();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = User::find($user->id);
        $roles = Role::all();
        $printers = Printer::active()->get();
        return view('users.show', compact('user', 'roles', 'printers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = User::find($user->id);
        $roles = Role::all();
        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'user_name' => 'required',
            'email' => 'required |email|unique:users,email,'.$user->id,
            'user_code' => 'required|string|min:1|unique:users,user_code,'. $user->id,
        ]);

        $user = User::find($user->id);
        $user->name = $request->get('user_name');
        $user->email = $request->get('email');
        $user->user_code = $request->get('user_code');
        $user->active = $request->get('active');
        $user->cutter_date = $request->get('cutter_date');

        if ($user->save()) {
            $user->roles()->detach();
            foreach ($request->get('user_roles') as $key => $value) {
                $user->roles()->attach($value);
            }
            return redirect('/users')->with('success', 'User has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $users = User::find($user->id);
        if ($users->delete()) {
            return redirect('/users')->with('success', 'User has been deleted Successfully');
        }
        return back()->withInput()->with('error', 'User coud not be deleted');
    }

    public function changePassword(Request $request, User $user)
    {
        $request->validate([
            'password' => 'required|string|min:3|confirmed',
        ]);
        $passwordUpdated = User::find($request->input('id'))
            ->update(['password' => Hash::make($request->input('password'))]);

        if ($passwordUpdated) {
            return redirect()->route('users.index')->with('success', 'Password changed successfully');
        }
        return back()->withInput();
    }

    public function updatePrinter(Request $request)
    {
        $user_id = $request->input('user_id');
        $printer_id = $request->get('printer_id');

        // auth()->user()->id;
        $user = User::find($user_id);
        $user->printer_id = $printer_id != 0 ? $printer_id : '';
        $user->save();

        Log::debug(request()->user()->name.' Selected label printer '.(request()->user()->printer->name ?? $printer_id));

        if($printer_id){
            return response()->json(['success' => 'Set Printer: '.$user->printer->name]);
        } else {
            return response()->json(['success' => 'No printer selected']);
        }
    }
}
