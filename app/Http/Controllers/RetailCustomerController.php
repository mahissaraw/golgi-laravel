<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\SalesRecord;
require(base_path('/public/fpdf/fpdf.php'));

use FPDF;

class RetailCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::where('type' , '=', 2)->paginate(25);
        return view('retail_customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('retail_customers.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        $sum = SalesRecord::where('customer_id', $id)->sum('amount');
        return view('retail_customers.show', compact('customer', 'sum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('retail_customers.edit', compact('customer'));
    }

    /**
     * Print customer address.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function printAddress(int $id)
    {
        $customer = Customer::find($id);
        #todo: find paper size
        // width - 18.5 cm
        // Height - 9.5 cm
        $pdf = new FPDF('L', 'mm', array(185, 95));
        $pdf->AddPage();
        $pdf->SetMargins(1, 1);
        $pdf->SetFont('Times','',10);
        $x = 110;
        $pdf->Text($x, 14, $customer->name);
        $pdf->Text($x, 18, $customer->street);
        $pdf->Text($x, 22, $customer->city);
        $pdf->Text($x, 26, $customer->state);
        $pdf->Text($x, 30, $customer->country);
        $pdf->SetFont('Arial','',8);
        $pdf->Text($x, 35, $customer->phone);
//        $pdf->Output();
        $pdf->Output('D',$customer->name.'.pdf');
        exit;
    }
}
