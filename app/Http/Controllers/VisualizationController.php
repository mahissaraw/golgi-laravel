<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisualizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $current_date = date("Y-m-d");
        if ($request->get('datepicker') != '') {
            $current_date = $request->get('datepicker');
        }

        $performance_piechart = DB::table('performance')
            ->select(DB::raw("SUM(item_qty) AS total_quantity,
                                SUM(rejected_qty) AS total_rejected_quantity"))
            ->where('updated_at', 'like', $current_date . '%')
            ->get();

        $plant = DB::table('performance')
            ->join('item', 'performance.item_id', '=', 'item.id')
            ->select(DB::raw("
                item.item_code AS plant,
                SUM(performance.item_qty) AS cut,
                SUM(performance.rejected_qty) AS rejected
                "))
            ->where('performance.updated_at', 'like', $current_date . '%')
            ->groupBy('performance.item_id')
            ->get()
            ->toArray();

        $plant_combo = $this->convertDataToChartForm($plant);

        $employee = DB::table('performance')
            ->join('users', 'performance.user_id', '=', 'users.id')
            ->select(DB::raw("
                users.name AS name,
                SUM(performance.item_qty) AS cut,
                SUM(performance.rejected_qty) AS rejected
                "))
            ->where('performance.updated_at', 'like', $current_date . '%')
            ->groupBy('performance.user_id')
            ->get()
            ->toArray();

        $employee_combo = $this->convertDataToChartForm($employee, 'employee');

        return view('visualization.index', compact('performance_piechart', 'current_date', 'plant_combo', 'employee_combo'));
    }

    /**
     * @param $data
     * @param string $type
     * @return array
     */
    protected function convertDataToChartForm($data, $type = 'plant'): array
    {
        $newData = array();
        $chartData = array();
        $firstLine = true;


        if ($type == 'plant') {
            foreach ($data as $dataRow) {
                array_push($newData, ["Plant" => $dataRow->plant, "Cut" => (int)$dataRow->cut, "Rejected" => (int)$dataRow->rejected]);
            }
        }

        if ($type == 'employee') {
            foreach ($data as $dataRow) {
                array_push($newData, ["Name" => $dataRow->name, "Cut" => (int)$dataRow->cut, "Rejected" => (int)$dataRow->rejected]);
            }
        }


        foreach ($newData as $dataRow) {
            if ($firstLine) {
                $chartData[] = array_keys($dataRow);
                $firstLine = false;
            }

            $chartData[] = array_values($dataRow);
        }

        return $chartData;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request){

        $from = date('Y-m-d', strtotime('first day of last month')).' 00:00:00';
        $to = date('Y-m-d', strtotime('last day of last month')).' 23:59:59';

        if ($request->get('fromDate') != '') {
            $from = $request->get('fromDate').' 00:00:00';
        }
        if ($request->get('toDate') != '') {
            $to = $request->get('toDate').' 23:59:59';
        }

        $sales_pie_chart = DB::table('sales_records')
            ->select(DB::raw("SUM(sales_records.amount) AS amount_total, customers.name, customers.id"))
            ->join('customers', 'sales_records.customer_id', '=', 'customers.id')
            ->whereBetween('sales_records.updated_at', [$from, $to])
            ->groupBy(['sales_records.customer_id'])
            ->orderByDesc('amount_total')
            ->limit(10)
            ->get()->toArray();

        $sales_plant_pie_chart = DB::table('sales_records')
            ->select(DB::raw("SUM(sales_records.quantity) AS quantity_total, item.item_name, item.item_code, item.id, sub_cat.subcat"))
            ->join('item', 'sales_records.item_id', '=', 'item.id')
            ->join('sub_cat', 'item.sub_cat', '=', 'sub_cat.id')
            ->whereBetween('sales_records.updated_at', [$from, $to])
            ->groupBy(['sales_records.item_id'])
            ->orderByDesc('quantity_total')
            ->limit(10)
            ->get()->toArray();

//        dd($sales_plant_pie_chart);

        return view('visualization.sales', compact('from', 'to', 'sales_pie_chart', 'sales_plant_pie_chart'));
    }

}
