<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getRefNo()
    {
        return DB::table('ref_no')->get()->first()->ref_no;
    }

    public function updateRefNo($ref_no = 1)
    {
        DB::table('ref_no')
            ->where('id', 1)
            ->update(['ref_no' => $ref_no + 1]);
    }

    public function getBulkNo()
    {
        return DB::table('bulk_number')->get()->first()->bulk_number;
    }

    public function getMaxBulkNo()
    {
        return DB::table('performance')->max('bulk_number');
    }

    public function updateBulkNo($bulk_number = 1)
    {
        DB::table('bulk_number')->increment('bulk_number', 1, ['id' => 1]);
    }

    public function getSerialNo()
    {
        return DB::table('serial_no')->get()->first()->serial_no;
    }

    public function updateSerialNo($serial_no = 1)
    {
        DB::table('serial_no')->increment('serial_no', 1, ['id' => 1]);
//            ->where('id', 1)
//            ->update(['serial_no' => $serial_no + 1]);
    }

    // public function getSerialNo()
    // {
    //     return DB::table('growth_room_stock')->latest('serial_no')->first()->serial_no;
    // }

    public function getBarCode()
    {
        return DB::table('bar_code')->get()->first()->bcode;
    }

    public function updateBarCode($bcode = 1)
    {
        DB::table('bar_code')->increment('bcode', 1, ['id' => 1]);
//            ->where('id', 1)
//            ->update(['bcode' => $bcode + 1]);
    }

    public function generateQrCodeImage($serial_no, $qr_image_name)
    {
//        if (file_exists(public_path().'/labels/qr/'.$qr_image_name)){
//            unlink(public_path().'/labels/qr/'.$qr_image_name);
//        }
        QrCode::backgroundColor(255, 255, 0)->color(255, 0, 127)
            ->format('png')->margin(1)->size(75)
            ->generate($serial_no, public_path('labels/qr/' . $qr_image_name));
    }

    public function getPrinter(): string
    {
//         $fap = DB::table('printers')->where('active', 1)->orderBy('id')->first();
//         return $fap->name;

         return 'GT800_U';
    }
}
