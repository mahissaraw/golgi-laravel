<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use App\Item;
use App\User;
use App\GrowthRoomStock;
use Auth;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InitiationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin_user = auth()->user()->hasRole('Administrator');
        if (!$admin_user) {
            $role = 'Operator';
            $users = User::whereHas('roles', function ($q) use ($role) {
                $q->where('name', '=', $role);
            })->where('active', '=', '1')->pluck('name', 'id');
        } else {
            $users = User::where('active', '=', '1')->pluck('name', 'id');
        }

        $categories = Category::all();
        $customers = Customer::all();

        return view('initiations.create', compact('users', 'categories', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'section' => 'required',
            'user_id' => 'required|integer',
            'sel_type' => 'required|integer',
            'sel_media' => 'required|integer',
            'sel_container' => 'required|integer',
            'sel_item' => 'required|integer',
            'container_qty' => 'required',
            'no_of_plants' => 'required',
            'sel_container_batch' => 'required|integer',
        ]);


        $dt = $request->get('date').' '.date('h:i:s');
        $date_w = new \DateTime($dt);
        $week = $date_w->format("W");

        $bulk_number = $this->getBulkNo() > $this->getMaxBulkNo() ? $this->getBulkNo() : $this->getMaxBulkNo() + 1;
        $this->updateBulkNo();

        for ($i = 0; $i < $request->get('container_qty'); $i++) {

            $user = new User();

            $dt = $request->get('date').' '.date("H:i:s");
            $ref = $request->get('sel_container_batch');
            $user_code = $user->getUserCode($request->get('user_id'));
            $s_no = $this->getSerialNo();

            $qr_image_name = 'initiation_stock_' . md5($dt . $ref . $user_code . $s_no) . '.png';

            $this->generateQrCodeImage($s_no, $qr_image_name);

            $item_serial_no = $week . " " . '1' . " " . $request->get('plant_qty');

            $growth_stock = new GrowthRoomStock([
                'media' => $request->get('sel_media'),
                'container' => $request->get('sel_container'),
                'm_type' => $request->get('sel_type'),
                'item' => $request->get('sel_item'),
                'batch_code' => 1, // useless
                'date' => $dt,
                'ref_no' => $request->get('sel_container_batch'),
                'serial_no' => $this->getSerialNo(),
                'bar_code' => $this->getBarCode(),
                'flag' => NULL, // update in later stages
                's_no' => NULL, // update in later stages
                'status' => 'Initiation Growth Room',
                'accept' => 4,
                'reasons' => NULL,
                'type' => $request->get('section'),
                'operator' => Auth::user()->name, //currently logged user
                'user' => $user->getUserCode($request->get('user_id')), // selected user
                'operator_id' => Auth::user()->id, //currently logged user
                'user_id' => $request->get('user_id'),
                'transfer' => 0,
                'img_qr' => $qr_image_name,
                'parent_id' => NULL,
                'bulk_number' => $bulk_number,
                'no_plants' => $request->get('no_of_plants'),
                'item_serial_no' => $item_serial_no,
                'complete' => 0,
                'last_updated' => $dt,
            ]);
            $growth_stock->save();
            $this->updateSerialNo();
            $this->updateBarCode();
        }
        ///////////////////////// GrowthStock /////////////////////////
        //dd($request->get('customer_id'));
//        $container = new Container();
//        $item = new Item();
//        $performance = new Performance([
//            'bulk_number' => $bulk_number,
//            'user_id' => $request->get('user_id'),
//            'item_id' => $request->get('sel_item'),
//            'media_id' => $request->get('sel_media'),
//            'container_id' => $request->get('sel_container'),
//            'max_container_capacity' => $container->getItemCapacity($request->get('sel_container')),
//            'total_containers' => $request->get('container_qty'),
//            'item_qty' => $request->get('no_of_plants'),
//            'rejected_qty' => 0,
//            'add_amount' => $item->getRate($request->get('sel_item')) * (int) $request->get('no_of_plants'),
//            'rejected_amount' => 0,
//            'order_id' => 0,
//            'customer_id' => $request->get('customer_id'),
//        ]);
        //dd($performance);
//        $performance->save();
        /////////////////////////

        // insert media stock quantity in a separate table
        DB::table('t_room_stock')->insert(
            [
                'ref_no' => $request->get('sel_container_batch'),
                'dr' => 0,
                'cr' => $request->get('container_qty')
            ]
        );

        return redirect('/growthstocks/showbulklabels/' . $bulk_number)->with('success', 'Initiation stock added to ' . $request->get('section') . ' section');
    }

    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function growthRoom(Request $request)
    {
        if ($request->has('search_item_name')) {
            $item = Item::where('item_name', $request->input('search_item_name'))->first();
        }
        if (isset($item)) {
            $growthstocks = GrowthRoomStock::where([
                    ['accept', 4],
                    ['type', 'Initiation'],
                    ['status', '=', 'Initiation Growth Room'],
                    ['item', $item->id],
                    ['complete', '=', 0],
                    ['flag', null]
                ])
                ->orderBy('bulk_number', 'asc')
                ->paginate(config('app.perPage'))
                ->appends(['search_item_name' => $request->input('search_item_name')]);
        } else {
            $growthstocks = GrowthRoomStock::where([
                ['accept', 4],
                ['type', 'Initiation'],
                ['status', '=', 'Initiation Growth Room'],
                ['complete', '=', 0],
                ['flag', null]
            ])
                ->orderBy('media', 'container', 'desc')
                ->paginate(config('app.perPage'));
        }

        return view('initiations.growth_room', compact('growthstocks'));
    }

    /**
     * Show the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function transferRoom(Request $request)
    {
        if ($request->has('search_item_name')) {
            $item = Item::where('item_name', $request->input('search_item_name'))->first();
        }
        if (isset($item)) {
            $growthstocks = GrowthRoomStock::where([
                ['accept', 4],
                ['type', 'Initiation'],
                ['status', '=', 'Initiation Transfer Room'],
                ['item', $item->id],
                ['complete', '=', 0],
                ['flag', null]
            ])
                ->orderBy('bulk_number', 'asc')
                ->paginate(config('app.perPage'))
                ->appends(['search_item_name' => $request->input('search_item_name')]);
        } else {
            $growthstocks = GrowthRoomStock::where([
                ['accept', 4],
                ['type', 'Initiation'],
                ['status', '=', 'Initiation Transfer Room'],
                ['complete', '=', 0],
                ['flag', null]
            ])
                ->orderBy('media', 'container', 'desc')
                ->paginate(config('app.perPage'));
        }

        return view('initiations.transfer_room', compact('growthstocks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Move to Transfer room.
     *
     * @param int $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function toTransferRoom(int $id)
    {
        $growth = GrowthRoomStock::find($id);

        $growth->status = 'Initiation Transfer Room';
        $growth->last_updated = date("Y-m-d H:i:s");
        $growth->save();

        Log::notice(request()->user()->name.' transferred Serial Number: '.$growth->serial_no.' to Initiation Transfer Room');
        return redirect('/')->with('success', 'Record successfully transferred to Initiation Transfer Room ' . $growth->status);
    }

    /**
     * Move to Transfer room.
     *
     * @param  int  $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function toGrowthRoom(int $id)
    {
        $growth = GrowthRoomStock::find($id);

        $growth->status = 'Initiation Growth Room';
        $growth->last_updated = date("Y-m-d H:i:s");
        $growth->save();

        Log::notice(request()->user()->name.' transferred Serial Number: '.$growth->serial_no.' to Initiation Growth Room');
        return redirect('/')->with('success', 'Record successfully transferred to Initiation Growth Room ' . $growth->status);
    }

    /**
     * Move to Transfer room.
     *
     * @param  int  $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function toProdTransferRoom(int $id)
    {
        $growth = GrowthRoomStock::find($id);

        $growth->accept = 1;
        $growth->type = 'Production';
        $growth->status = 'Transfer Room';
        $growth->last_updated = date("Y-m-d H:i:s");
        $growth->save();

        Log::notice(request()->user()->name.' transferred Serial Number: '.$growth->serial_no.' to '. $growth->type.' '. $growth->status);
        return redirect('/')->with('success', 'Record successfully transferred to ' .$growth->type.' '. $growth->status);
    }

    /**
     * Move to Transfer room.
     *
     * @param  int  $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function toRndTransferRoom(int $id)
    {
        $growth = GrowthRoomStock::find($id);

        $growth->accept = 1;
        $growth->type = 'R&D';
        $growth->status = 'Transfer Room';
        $growth->last_updated = date("Y-m-d H:i:s");
        $growth->save();

        Log::notice(request()->user()->name.' transferred Serial Number: '.$growth->serial_no.' to '. $growth->type.' '. $growth->status);
        return redirect('/')->with('success', 'Record successfully transferred to ' .$growth->type.' '. $growth->status);
    }
}
