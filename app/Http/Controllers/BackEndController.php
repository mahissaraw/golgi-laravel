<?php

namespace App\Http\Controllers;

use App\Company;
use App\Container;
use App\Http\Controllers\ReportsController;
use App\Media;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use DateTime;
use App\Performance;
use App\GrowthRoomStock;
use App\Item;
use File;
use Log;


class BackEndController extends Controller
{
    /**
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
     public function findBulkNumber(Request $request)
    {
        if ($request->isMethod('post')) {

            $bulk_no = $request->input('bulk_no');
            $optradio = $request->input('optradio');
            if ($optradio == 'multiply_rooting'){
                $gr_stocks = DB::table('growth_room_stock')->where([
                    ['bulk_number', '=', $bulk_no],
                    ['flag', '=', null],
                    ['accept', '!=', 2],
                    ['complete', '=', 0],
                    ['status', '=', 'Transfer Room'],
                ])->get();
            } elseif ($optradio == 'completed'){
                $gr_stocks = DB::table('growth_room_stock')->where([
                    ['bulk_number', '=', $bulk_no],
                    ['complete', '!=', 1],
                    ['accept', '<>', 2]
                ])->get();
            } else { // dispatched
                $gr_stocks = DB::table('growth_room_stock')->where([
                    ['bulk_number', '=', $bulk_no],
                    ['accept', '<>', 2],
                    ['complete', '=', 2]
                ])->get();
            }

            $performances = Performance::where([
                ['bulk_number', '=', $bulk_no],
                ['status', '=', null]
            ])->get();

            return view('backend.find_bulk_number', compact('gr_stocks', 'performances', 'bulk_no', 'optradio'));

        } else {
            $optradio = 'multiply_rooting';
            return view('backend.find_bulk_number', compact('optradio'));
        }
    }

    public function updateBulkNumber(Request $request)
    {
        $bulk_number    = $request->bulk_number;
        $type           = $request->type;
        $performances_count = $request->pc;

        if ($type == 'Production' || $type == 'R&D'){
            GrowthRoomStock::where([
            ['bulk_number', '=', $bulk_number],
            ['type', '=', $type],
            ['status', '=', 'Transfer Room'],
            ['accept', '<>', 2],
            ['complete', '=', 0]
           ])->update(['flag' => 5]);

            if ($performances_count > 0){
                DB::table('performance')->where('bulk_number', '=', $bulk_number)->update(['status' => 1], ['timestamps' => false]);
            }
            $flag = 'Multiply';
        }
        elseif ($type == 'Completed') {
            GrowthRoomStock::where([
            ['bulk_number', '=', $bulk_number],
            ['accept', '<>', 2],
           ])->update(['complete' => 1]);

            if ($performances_count > 0){
                DB::table('performance')->where('bulk_number', '=', $bulk_number)->update(['status' => 3], ['timestamps' => false]);
            }
            $flag = 'Completed';
        }
        elseif ($type == 'Dispatched') {
            GrowthRoomStock::where([
            ['bulk_number', '=', $bulk_number],
            ['accept', '<>', 2],
           ])->update(['complete' => 2]);

            if ($performances_count > 0){
                DB::table('performance')->where('bulk_number', '=', $bulk_number)->update(['status' => 4], ['timestamps' => false]);
            }
            $flag = 'Completed';
        }
        else {
            GrowthRoomStock::where([
            ['bulk_number', '=', $bulk_number],
            ['type', '=', 'Production'],
            ['status', '=', 'Transfer Room'],
            ['accept', '<>', 2],
            ['complete', '=', 0]
           ])->update(['flag' => 6]);

            if ($performances_count > 0){
                DB::table('performance')->where('bulk_number', '=', $bulk_number)->update(['status' => 2], ['timestamps' => false]);
            }
            $flag = 'Rooting';
        }

        if ($request->week_bulk){
            return redirect()->back()->with('message', 'Bulk number:'. $bulk_number.' Mark As Removed!');
            Log::notice(' User: '.request()->user()->name.' Bulk Number: '.$bulk_number.' Mark As Removed!');
        }
        return response()->json(['success' => 'Update '.$type.' Transfer Room Bulk Number '.$bulk_number.' to '.$flag]);
    }

    public function setCustomersToPlant(Request $request){
        $items = Item::orderBy('item_name', 'asc')->get();
        $customers = Company::all();
        $selected_item = null;
        $selected_customer = null;

        if ($request->isMethod('post')) {
            $customer_id = $request->input('customer_id');
            $item_id = $request->input('item_id');

            DB::table('performance')->where('item_id', '=', $item_id)->update(['customer_id' => $customer_id ], ['timestamps' => false]);
            $selected_item = Item::find($item_id);
            $selected_customer = Company::find($customer_id);

        }
        return view('backend.set_customer_to_plant', compact( 'items', 'customers', 'selected_item', 'selected_customer'));
    }

    public function download($file_name) {
        $file_path = public_path('files/'.$file_name);
        return response()->download($file_path);
    }

    public function updateBulkCompleted(Request $request)
    {
        $bulk_number    = $request->bulk_number;
        $type           = $request->type;
        $grs_items = [];
        $status = 0;

        if($type == 'completed'){
            $grs_items = GrowthRoomStock::where([
                ['bulk_number', '=', $bulk_number],
                ['complete', '=', 1]
            ])->get();
            $status = 1;
        }
        if($type == 'dispatched'){
            $grs_items = GrowthRoomStock::where([
                ['bulk_number', '=', $bulk_number],
                ['complete', '=', 2],
                ['status', '=', 'Dispatched']
            ])->get();
            $status = 2;
        }
        if (!$grs_items->isEmpty()){
            Performance::where('bulk_number', '=', $bulk_number)->where('done_status', '=', $status)->delete();
            foreach ($grs_items as $growth){
                self::doneStatus($growth->bulk_number, $growth->last_updated, $growth->serial_no, $status);
            }
        }
        Log::notice(' User: '.request()->user()->name.' Bulk Number: '.$bulk_number.' done status: '. $type. ' ('.$status.')');
        return response()->json(['success' => 'Set '.$type.' Containers in the Bulk Number '.$bulk_number]);
    }

    protected function doneStatus($bulk_number, $last_updated, $serial_no, $status = 1){
        $performance_item = Performance::where('bulk_number', '=', $bulk_number)->where('item_qty', '>', 0)->first();

        $performance = new Performance([
            'bulk_number' => $performance_item->bulk_number,
            'user_id' => $performance_item->user_id,
            'item_id' => $performance_item->item_id,
            'media_id' => $performance_item->media_id,
            'container_id' => $performance_item->container_id,
            'max_container_capacity' => $performance_item->max_container_capacity,
            'total_containers' => 1,
            'item_qty' => 0,
            'rejected_qty' => 0,
            'status' => $performance_item->status,
            'done_qty' => $performance_item->max_container_capacity,
            'done_status' => $status,
            'add_amount' => 0,
            'rejected_amount' => 0,
            'order_id' => $performance_item->order_id,
            'customer_id' => $performance_item->customer_id,
        ]);
        $performance->timestamps = false;               // disable timestamps for this creation
        $performance->created_at = $performance_item->created_at; // Override created_at value to get the correct week
        $performance->updated_at = $last_updated; // get the completed datetime from growth stock table

        if ($performance->save()){
            Log::notice(' User: '.request()->user()->name.' Bulk Number: '.$bulk_number.' Serial Number: '.$serial_no.' done status: '. $status);
        }

    }

    public function findMedia(Request $request)
    {
        $medias = Media::select('id', 'name')->where([['name', 'like', '%_SR'], ['active', '=', 1]])->get();

       if($request->isMethod('post') && $request->input('media_id')){
           $media_id = $request->input('media_id');
           $selected_media = Media::find($media_id);
           $performances = DB::table("performance")
               ->join('item', 'performance.item_id', '=', 'item.id')
               ->select(DB::raw("SUM(performance.item_qty) AS total_quantity,
                            SUM(performance.rejected_qty) AS total_rejected_quantity,
                            SUM(performance.done_qty) AS total_done_quantity,
                            item_id,
                            item_name,
                            item_code,
                            bar_code,
                            performance.media_id,
                            WEEK(performance.created_at, 1) AS week,
                            YEAR(performance.created_at) AS year,
                            performance.created_at"))
               ->whereNull('status')
               ->where('performance.media_id', '=', $media_id)
               ->havingRaw('SUM(performance.item_qty) - (SUM(performance.rejected_qty) + SUM(performance.done_qty)) > ?', [0])
               ->groupBy(DB::raw("performance.item_id, year, week"))
               ->orderBy(DB::raw("item.item_name, year, week"))->get();
           return view('backend.find_media', compact( 'medias', 'selected_media', 'performances'));
       } else {
           return view('backend.find_media', compact( 'medias'));
       }
    }

    public function getMediaPlantContamination($media_id, $plant_id, $year, $week)
    {
        if($media_id != null && $plant_id != null){
            $controller = new ReportsController;
            $date = $controller->getStartAndEndDate($week,$year);
            $from = $date->start;
            $to = $date->end;
            $bulk_numbers = [];

            $bulk = Performance::select('bulk_number')
                ->where([
                    ['media_id', '=', $media_id],
                    ['item_id', '=', $plant_id],
                    ['rejected_qty', '>', 0]
                ])
                ->whereBetween('created_at', [$from, $to])
                ->whereNull('status')
                ->distinct()
                ->get();
            foreach ($bulk as $bn){
                array_push($bulk_numbers, $bn->bulk_number);
            }

            $growth_room = GrowthRoomStock::where([
                ['media', '=', $media_id],
                ['item', '=', $plant_id]
            ])->whereNotNull('reasons')
                ->whereIn('bulk_number', $bulk_numbers)
                ->whereBetween('date', [$from, $to])
                ->get();
//            dd($growth_room);
            $selected_media = Media::find($media_id);
            $selected_plant = Item::find($plant_id);
            return view('backend.media_contamination', compact( 'growth_room','from', 'to', 'selected_media', 'selected_plant', 'week'));
        }
    }

    public function findContainer(Request $request)
    {
        $containers = Container::select('id', 'name')->where([['name', 'like', 'PP %'], ['active', '=', 1]])->get();

        if($request->isMethod('post') && $request->input('container_id')){
            $container_id = $request->input('container_id');
            $selected_container = Container::find($container_id);
            $performances = DB::table("performance")
                ->join('item', 'performance.item_id', '=', 'item.id')
                ->select(DB::raw("SUM(performance.item_qty) AS total_quantity,
                            SUM(performance.rejected_qty) AS total_rejected_quantity,
                            SUM(performance.done_qty) AS total_done_quantity,
                            item_id,
                            item_name,
                            item_code,
                            bar_code,
                            performance.container_id,
                            WEEK(performance.created_at, 1) AS week,
                            YEAR(performance.created_at) AS year,
                            performance.created_at"))
                ->whereNull('status')
                ->where('performance.container_id', '=', $container_id)
                ->havingRaw('SUM(performance.item_qty) - (SUM(performance.rejected_qty) + SUM(performance.done_qty)) > ?', [0])
                ->groupBy(DB::raw("performance.item_id, year, week"))
                ->orderBy(DB::raw("item.item_name, year, week"))->get();
            return view('backend.find_container', compact( 'containers', 'selected_container', 'performances'));
        } else {
            return view('backend.find_container', compact( 'containers'));
        }
    }

    public function getContainerPlantContamination($container_id, $plant_id, $year, $week)
    {
        if($container_id != null && $plant_id != null) {
            $controller = new ReportsController;
            $date = $controller->getStartAndEndDate($week, $year);
            $from = $date->start;
            $to = $date->end;
            $bulk_numbers = [];

            $bulk = Performance::select('bulk_number')
                ->where([
                    ['container_id', '=', $container_id],
                    ['item_id', '=', $plant_id],
                    ['rejected_qty', '>', 0]
                ])
                ->whereBetween('created_at', [$from, $to])
                ->whereNull('status')
                ->distinct()
                ->get();
            foreach ($bulk as $bn) {
                array_push($bulk_numbers, $bn->bulk_number);
            }

            $growth_room = GrowthRoomStock::where([
                ['container', '=', $container_id],
                ['item', '=', $plant_id]
            ])->whereNotNull('reasons')
                ->whereIn('bulk_number', $bulk_numbers)
                ->whereBetween('date', [$from, $to])
                ->get();
//            dd($growth_room);
            $selected_container = Container::find($container_id);
            $selected_plant = Item::find($plant_id);
            return view('backend.container_contamination', compact('growth_room', 'from', 'to', 'selected_container', 'selected_plant', 'week'));
        }
    }

    public function deleteBulk(Request $request)
    {
        $bulk_number = $request->input('bulk_number');
        if($bulk_number){
            $pt = DB::table("performance")->where('bulk_number', '=', $bulk_number)->first();
            $gt = DB::table("growth_room_stock")->where('bulk_number', '=', $bulk_number)->first();

            $containers = $pt->total_containers;
            $date = $gt->date;
            $ref = $gt->ref_no;

//            dd($containers);
//            dd($date);
//            dd($ref);

            DB::table("t_room_stock")->where([
                ['ref_no', '=', $ref],
                ['cr', '=', $containers],
                ['created_at', 'like', substr($date,0,-2).'%']
            ])->delete();

            DB::table("performance")->where('bulk_number', '=', $bulk_number)->delete();
            DB::table("growth_room_stock")->where('bulk_number', '=', $bulk_number)->delete();
        }
        return redirect()->route('showbulklabels')->with('success', 'Bulk '.$bulk_number.' DELETED !!');
    }

    public function deleteSerialNumber(Request $request)
    {
        $serial_no = $request->input('serial_no');
        $query = DB::table("growth_room_stock")->where('serial_no', '=', $serial_no);
        $serial = $query->first();

        if ($serial)
        {
            $bulk = DB::table("performance")->where([
                ['bulk_number', '=', $serial->bulk_number],
                ['item_qty', '>', 0],
            ])->first();

            if ($bulk)
            $pf = DB::table("performance")->where('id', $bulk->id)
            ->update([
                'item_qty' => $bulk->item_qty - $bulk->max_container_capacity,
                'total_containers' => $bulk->total_containers - 1
            ]);

            $query->delete();

            return redirect()->route('home')->with('warning', 'Serial No: '.$serial_no.' DELETED !!');
        }
    }

    public function setRelationships($user_id = null)
    {
        /**
         * Todo: Set users table id instead of name and code in transfer_room_stock & growth_room_stock
         * transfer_room_stock - operator = operator_id | user = user_id
         * growth_room_stock - operator = operator_id | user = user_id
         *
         * Steps
         * 1. create migration files to add both fields
         * 2. create a model for transfer_room_stock
         * 2. Update both models with the correct relationships
         * 3. Update all the functions to insert/update operator_id & user_id
         *
         * 4. Update all the code that affected
         *      - trs Add Media Stock
         *      - trs Remove Media Stock Containers
         *
         *      - grs Add growth room stock | Add Multiplications | Add Rooting
         *
         * 5. run update query for each user
         * $users = DB::table("users")->all();
         *
         * $trs_user_row = DB::table("transfer_room_stock")->where('user', '=', $user->user_code)->update(array('user_id' => $user->id));
         * $trs_operator_row = DB::table("transfer_room_stock")->where('operator', '=', $user->name)->update(array('operator_id' => $user->id));
         *
         * $grs_user_row = DB::table("growth_room_stock")->where('user', '=', $user->user_code)->update(array('user_id' => $user->id));
         * $grs_operator_row = DB::table("growth_room_stock")->where('operator', '=', $user->name)->update(array('operator_id' => $user->id));
         *
         */

        if ($user_id != null & $user_id != 3){
            $user = DB::table("users")->find($user_id);
            $trs_user_row       = DB::table("transfer_room_stock")->where('user', '=', $user->user_code)->update(array('user_id' => $user->id));
            $trs_operator_row   = DB::table("transfer_room_stock")->where('operator', '=', $user->name)->update(array('operator_id' => $user->id));

            $grs_user_row       = DB::table("growth_room_stock")->where('user', '=', $user->user_code)->update(array('user_id' => $user->id));
            $grs_operator_row   = DB::table("growth_room_stock")->where('operator', '=', $user->name)->update(array('operator_id' => $user->id));

            $operator_row   = $trs_operator_row + $grs_operator_row;
            $user_row       = $trs_user_row + $grs_user_row;

            return redirect()->back()->with('success', 'IT WORKS! Updated User Rows: '.$user_row. ' Operator Rows: '.$operator_row);
        }

        # ToDo: Remove this after all the records are updated.
        if ($user_id == 3){
            $users = DB::table("users")->get();
            foreach ($users as $user){
                DB::table("transfer_room_stock")->where('user', '=', $user->user_code)->update(array('user_id' => $user->id));
                DB::table("transfer_room_stock")->where('operator', '=', $user->name)->update(array('operator_id' => $user->id));

                DB::table("growth_room_stock")->where('user', '=', $user->user_code)->update(array('user_id' => $user->id));
                DB::table("growth_room_stock")->where('operator', '=', $user->name)->update(array('operator_id' => $user->id));
            }

            return redirect()->back()->with('success', 'IT WORKS!');
        }

        return redirect()->back()->with('error', 'Something wrong here!!!');

    }

    public function transferBulk(Request $request)
    {
        $bulk_number    = $request->bulk_number;
        $growth_controller = new GrowthRoomStockController;
        $plants = GrowthRoomStock::where([
            ['bulk_number', '=', $bulk_number],
            ['complete', '=', 0],
            ['status', '<>', 'Transfer Room'],
        ])->whereNull('reasons')->get();

        if(!$plants->isEmpty()){
            foreach ($plants as $plant){
                $growth_controller->transferToTransferRoom($plant->id, 2);
            }
            return redirect()->back()->with('message', 'Bulk number:'. $bulk_number.' successfully transferred to Production Transfer Room!');
        }
        return redirect()->back()->with('message', 'Bulk number:'. $bulk_number.' already in Transfer Room!');
    }

}
