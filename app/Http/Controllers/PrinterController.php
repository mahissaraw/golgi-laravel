<?php

namespace App\Http\Controllers;

use App\Printer;
use Illuminate\Http\Request;

class PrinterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $printers = Printer::select('id', 'name')->where('active', 1)->get()->toArray();
            return response()->json(['data' => $printers]);
        } else {
            $printers = Printer::all();
            return view('printers.index', compact('printers'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $printer = new Printer();
        $printer->name = $request->get('name');
        $printer->description = $request->get('description');

        if ($printer->save()) {
            return redirect('/printers')->with('success', 'New printer has been added');
        }

        return back()->withInput()->with('error', 'Could not add Printer');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $printer = Printer::find($id);
        return view('printers.edit', compact('printer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $printer = Printer::find($id);
        $printer->name = $request->get('name');
        $printer->description = $request->get('description');
        $printer->active = $request->get('active');
        $printer->save();

        return redirect('/printers')->with('success', 'Printer has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
