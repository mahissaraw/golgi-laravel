<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Customer;
use App\User;
use App\Role;
use Illuminate\Support\Facades\File;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Only Cooperate customers
        $customers = Customer::where('type' , '=', 1)->get();
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::where('active', 1)->get();
        return view('customers.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:customers',
            'country' => 'required',
            'phone' => 'required',
            'logo' => 'image|nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $customer = new Customer();

        $customer->name = stripslashes($request->get('name'));
        $customer->email = $request->get('email');
        $customer->phone = $request->get('phone');
        $customer->state = $request->get('state');
        $customer->street = $request->get('street');
        $customer->city = $request->get('city');
        $customer->country = $request->get('country');
        $customer->nic = $request->get('nic') ?? null;
        $customer->type = $request->get('type') ?? 1;
        $customer->company_id = $request->get('company_id') ?? null;

        if ($file = $request->file('logo')) {
            $destinationPath = public_path('/customer_logo/'); // upload path
            File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
            $fileName = $file->getClientOriginalName();
            $logoImage = time() . '_' . $fileName;
            $file->move($destinationPath, $logoImage);
            $customer->logo = $logoImage;
        }

        $customer->save();

        if($customer->type == 1) { // Only Cooperate customers create login account
            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'user_code' => 'CUS_' . $customer->id,
                'password' => Hash::make('CUS_' . $customer->id),
            ]);

            // Assing customer role
            $user->roles()->attach(Role::where('name', 'Customer')->first());

            return redirect('/customers')->with('success', 'New Customer has been added Successfully');
        }
        return redirect('/retail_customers')->with('success', 'New Customer has been added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        return view('customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        $companies = Company::all();
        return view('customers.edit', compact('customer', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'country' => 'required',
            'phone' => 'required',
            'logo' => 'image|nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $customer = Customer::find($id);
        $customer->name = $request->get('name');
        $customer->email = $request->get('email');
        $customer->phone = $request->get('phone');
        $customer->state = $request->get('state');
        $customer->street = $request->get('street');
        $customer->city = $request->get('city');
        $customer->country = $request->get('country');
        $customer->is_active = $request->get('is_active');
        $customer->nic = $request->get('nic') ?? null;
        $customer->type = $request->get('type') ?? 1;

        if ($file = $request->file('logo')) {
            $destinationPath = public_path('/customer_logo/'); // upload path
            File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
            $fileName = $file->getClientOriginalName();
            $logoImage = time() . '_' . $fileName;
            $file->move($destinationPath, $logoImage);
            $customer->logo = $logoImage;
        }

        $customer->save();

        if ($customer->type == 1){
            return redirect('/customers')->with('success', 'Customer has been updated Successfully');
        }
        return redirect('/retail_customers')->with('success', 'Customer has been updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        if ($customer->orders->isEmpty()) {
            $user = User::where('email', $customer->email)->first();
            $user->delete();
            $customer->delete();
            return redirect('/customers')->with('success', 'Customer has been deleted Successfully');
        } else {
            return redirect('/customers')->with('error', 'This Customer can\'t been deleted records exist in other tables');
        }
    }
}
