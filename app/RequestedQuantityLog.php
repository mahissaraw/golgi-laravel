<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestedQuantityLog extends Model
{
    protected $fillable = [
        'media_request_id',
        'requested_quantity',
        'user_id'
    ];

    public function mediaRequest()
    {
        return $this->belongsTo('App\MediaRequest', 'media_request_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
