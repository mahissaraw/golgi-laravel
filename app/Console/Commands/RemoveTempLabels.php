<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;

class RemoveTempLabels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:labels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all labels generated before one year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $label_file_path = public_path().'/labels/';
        $qr_file_path = public_path().'/labels/qr/';
        $label_dir = new \DirectoryIterator($label_file_path);
        $qr_dir = new \DirectoryIterator($qr_file_path);
        $label_counter = 0;
        $qr_counter = 0;
        $expire_time = strtotime(date('Y-m-d', strtotime('13 month ago')));

        foreach ($label_dir as $file_info) {
            if (!$file_info->isDot() && $file_info->isFile()) {
                if($expire_time > $file_info->getMTime())
                {
                    unlink($label_file_path.$file_info->getFilename());
                    $label_counter++;
                }
            }
        }
        $this->info('Label Files Deleted: '.$label_counter);
        Log::notice('Label Files Deleted: '.$label_counter);

        foreach ($qr_dir as $file_info) {
            if (!$file_info->isDot() && $file_info->isFile()) {
                if($expire_time > $file_info->getMTime())
                {
                    unlink($qr_file_path.$file_info->getFilename());
                    $qr_counter++;
                }
            }
        }
        $this->info('QR Images Deleted: '.$qr_counter);
        Log::notice('QR Images Deleted: '.$qr_counter);
    }
}
