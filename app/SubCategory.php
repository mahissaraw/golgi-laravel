<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_cat';
    public $timestamps = false;

    protected $fillable = ['cat_id, subcat'];

    public function category()
    {
        return $this->belongsTo('App\Category', 'cat_id');
    }

    public function items()
    {
        return $this->hasMany('App\Item', 'sub_cat');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeInActive($query)
    {
        return $query->where('active', 0);
    }
}
