<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Greenhouse extends Model
{
    protected $fillable = [
        'item_id',
        'quantity',
        'task',
        'is_sold',
        'remark',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    public function owenBy(User $user)
    {
        return $user->id === $this->user_id;
    }
}
