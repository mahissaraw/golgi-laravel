<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesRecord extends Model
{
    protected $fillable = [
        'customer_id',
        'item_id',
        'invoice_number',
        'invoice_date',
        'quantity',
        'amount',
        'remarks',
        'user_id'
    ];
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }
}
