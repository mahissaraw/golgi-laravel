<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class MediaStock extends Model
{
    protected $table = 'transfer_room_stock';
    public $timestamps = false;

    protected $fillable = [
        'm_type',
        'media',
        'container',
        'date',
        'status',
        'operator',
        'ref_no',
        'user',
        'operator_id',
        'user_id',
        'accept',
        'qr_img'
    ];

    public function mediaVariety()
    {
        return $this->belongsTo('App\Media', 'media');
    }

    public function containerVariety()
    {
        return $this->belongsTo('App\Container', 'container');
    }

    public function userDetail()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function operatorDetail()
    {
        return $this->belongsTo('App\User', 'operator_id');
    }

    public static function getMediaStock(){
        return DB::table('transfer_room_stock')->orderBy('date', 'asc')->get();
    }

    public static function getMediaByType($type = 0){

        return DB::table('media')->where([
            ['type', '=', $type],
            ['active', '=', 1]
            ])->get();
    }


    /**
     * @param int $type
     * @return mixed
     * @deprecated Container no longer filter by the media type
     * ToDo: remove this function
     */
    public static function getContainerByType($type = 0){

        return DB::table('container')->where([
            ['type', '=', $type],
            ['active', '=', 1]
            ])->get();
    }
}
