<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
//use App\MediaStock;

class GrowthRoomStock extends Model
{
    protected $table = 'growth_room_stock';
    public $timestamps = false;

    protected $fillable = [
        'media',
        'container',
        'item',
        'batch_code',
        'date',
        'ref_no',
        'serial_no',
        'bar_code',
        'flag',
        's_no',
        'status',
        'accept',
        'reasons',
        'type',
        'operator',
        'user',
        'operator_id',
        'user_id',
        'transfer',
        'img_qr',
        'parent_id',
        'bulk_number',
        'no_plants',
        'item_serial_no',
        'complete',
        'last_updated'
    ];

    /**
    * accept
    * 1 = multiply
    * 2 = reject
    * 3 = rooting
    * 4 = initiation
    *
    * flag
    * 2 = rooting
    * 5 = bulk_multiply
    * 6 = bulk_rooting
    *
    *
    * complete
    * 0 = default
    * 1 = completed
    * 2 = dispatched
    */

    public static function getContainersByMedia($mtype = 0, $media = 0)
    {

        $value = DB::table('transfer_room_stock')
            ->select('container')
            ->where('m_type', $mtype)
            ->where('media', $media)
            ->get();

        return $value;
    }

    public function mediaVariety()
    {
        return $this->belongsTo('App\Media', 'media');
    }

    public function containerVariety()
    {
        return $this->belongsTo('App\Container', 'container');
    }

    public function itemVariety()
    {
        return $this->belongsTo('App\Item', 'item');
    }

    public function userDetail()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function operatorDetail()
    {
        return $this->belongsTo('App\User', 'operator_id', 'id');
    }

    /**
     * Get the plant week.
     *
     * @return integer
     */

    public function getWeekAttribute(): int {
        $week = explode(" ", $this->item_serial_no);
        return $week[0];
    }

    public function getReason(){
        $r1 = explode('_',$this->reasons)[1];
        $r2 = explode('"',$r1)[0];
        return DB::table('reasons')->find($r2);
    }

    protected function getPlantSize($bulk_number) : string{
        return DB::table('performance')->where('bulk_number', $bulk_number)->first()->plant_size ?? '';
    }
}
