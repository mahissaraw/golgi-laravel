<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaRequest extends Model
{
    protected $fillable = [
        'media_type',
        'media_id',
        'container_id',
        'media_amount',
        'requested_quantity',
        'requested_date',
        'requested_by',
        'need_date',
        'location',
        'completed_quantity',
        'operators',
        'incomplete_reason',
        'confirmed',
        'ref_no',
        'cancelled_by',
        'user_id'
    ];

    /**
     * Set the locations
     *
     */
    public function setLocationAttribute($value)
    {
        $this->attributes['location'] = json_encode($value);
    }

    /**
     * Get the locations
     *
     */
    public function getLocationAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the ref_no
     *
     */
    public function setOperatorsAttribute($value)
    {
        $this->attributes['operators'] = json_encode($value);
    }

    /**
     * Get the ref_no
     *
     */
    public function getOperatorsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the ref_no
     *
     */
    public function setRefNoAttribute($value)
    {
        $this->attributes['ref_no'] = json_encode($value);
    }

    /**
     * Get the ref_no
     *
     */
    public function getRefNoAttribute($value)
    {
        return json_decode($value);
    }

    public function media()
    {
        return $this->belongsTo('App\Media', 'media_id');
    }

    public function container()
    {
        return $this->belongsTo('App\Container', 'container_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function requestedBy()
    {
        return $this->belongsTo('App\User', 'requested_by');
    }

    public function cancelledBy()
    {
        return $this->belongsTo('App\User', 'cancelled_by');
    }

    public function requestedQuantityLog()
    {
        return $this->hasMany('App\RequestedQuantityLog', 'media_request_id');
    }

    /**
     * @param $needle
     * @param $locations
     * @return bool
     */
    public function hasLocation($needle, $locations): bool
    {
        foreach ($locations as $key => $value){
            if ($needle == $value){
                return true;
            }
        }
        return  false;
    }

    public function getLocationName($locations): string
    {
        $copy = $locations;
        $location = '';
        foreach ($locations as $key => $value){
            $location .= config('app.locations')[$value];
            if (next($copy)) {
                $location .= ', '; // Add comma for all elements instead of last
            }
        }
        return $location;
    }

    public function getBatchNo($batches): string
    {
        $copy = $batches;
        $batch = '';
        foreach ($batches as $key => $value){
            $batch .= $value;
            if (next($copy)) {
                $batch .= ', '; // Add comma for all elements instead of last
            }
        }
        return $batch;
    }
}
