<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'customer_id',
        'order_date',
        'required_date',
        'shipped_date',
        'order_status',
        'user_id'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function order_items()
    {
        return $this->hasMany('App\OrderItem', 'order_id');
    }

    public function performance()
    {
        return $this->hasMany('App\Performance');
    }

    public function getOrderDateAttribute($value){
        return substr($value, 0, 10);
    }

    public function getRequiredDateAttribute($value){
        return substr($value, 0, 10);
    }

    public function getShippedDateAttribute($value){
        return substr($value, 0, 10);
    }
}
