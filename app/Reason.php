<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    // protected $table = 'sub_cat';
    public $timestamps = false;

    protected $table = 'reasons';

    protected $fillable = ['name'];
}
