<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function performance()
    {
        return $this->hasMany('App\Performance');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'email', 'email');
    }

    public function sales()
    {
        return $this->hasMany('App\SalesRecord');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }
}
