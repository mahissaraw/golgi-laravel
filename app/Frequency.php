<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frequency extends Model
{
    //protected $table = 'media';
    //public $timestamps = false;

    protected $fillable = [
        'frequency',
        'data'
    ];
}
