var productionClient = {
    'showToast': function (msg) {
        if (typeof productionClientObject == 'undefined')
            alert(msg);
        else
            productionClientObject.showToast(msg);
    },
    'getQRCode': function (callback) {
        if (typeof productionClientObject == 'undefined') {
            callback(prompt('Input QR code'));
            return;
        }
        this.qrCodeCallback = callback;
        productionClientObject.getQRCode('productionClient.__qrCodeResult');
    },
    '__qrCodeResult': function (code) {
        this.qrCodeCallback(code);
    }
};

jQuery(document).ready(function ($) {

    // var isMobile = window.orientation > -1;
    // Todo: Remove below comment when the Android app is launched
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('.qr-input').hide();
    } else {
        $('.qr-scan').hide();
    }

    if ($('#scanBarcodeButton') !== 'undefined') {
        $('#scanBarcodeButton').on('click', function (event) {
            event.preventDefault();
            productionClient.getQRCode(function (code) {
                if (code !== null) {
                    location.href = 'home/scanqrcode/' + code;

                }
            });
        });
    }

    if ($("#switch_locations") !== 'undefined') {
        $("#switch_locations").on('click', function (event) {
            event.preventDefault();
            productionClientObject.loadLoginActivity();
        });
    }

});


document.addEventListener("DOMContentLoaded", function () {

    if (document.getElementById("login_form") !== null) {
        document.getElementById("login_form").onsubmit = function (e) {
            productionClientObject.showToast("Logging in");
            productionClientObject.storeUserCredentials(
                document.getElementById("email").value,
                document.getElementById("password").value
            );
            return true;
        }
    }

});
