$(document).ready(function () {
    $('.form-prevent-multiple-submits').on('submit', function () {
        $('.btn-prevent-multiple-submits').prop('disabled', true);
        $('<i class="fa fa-spinner fa-spin"></i>&nbsp;').prependTo('.btn-prevent-multiple-submits');
    })
});
